import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import typescript from '@rollup/plugin-typescript';
// import { terser } from 'rollup-plugin-terser';
import json from '@rollup/plugin-json';
import peerDepsExternal from 'rollup-plugin-peer-deps-external';
import bundleScss from 'rollup-plugin-bundle-scss';

import { replaceTscAliasPaths } from 'tsc-alias';

import rollupReadPackageJson from '@avstantso/node-js--rollup-read-package-json';

const packageJson = require('./package.json');

// A.V.Stantso:
// rewrite:  import alias from 'rollup-plugin-tsc-alias';
// https://github.com/valentiniljaz/rollup-plugin-tsc-alias/blob/main/index.js
function alias() {
  return {
    name: 'tscAlias',
    async writeBundle(options) {
      return replaceTscAliasPaths({
        ...options,
        replacers: ['./tsc-alias-replacer.js'],
      });
    },
  };
}

//'captcha-canvas',
const noExternal = ['country-flag-icons', 'gravatar'];
const external = Object.keys(packageJson.dependencies).filter(
  (name) => !noExternal.includes(name)
);

export default [
  {
    input: './src/index.ts',
    output: {
      file: packageJson.main,
      format: 'esm',
      sourcemap: true,
    },
    plugins: [
      rollupReadPackageJson({ packageJson }),
      peerDepsExternal(),
      resolve(),
      commonjs(),
      typescript({
        tsconfig: './tsconfig-build.json',
      }),
      alias(),
      json(),
      bundleScss(),
      // terser(), // hangs out
    ],
    external,
  },
];
