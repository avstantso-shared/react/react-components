import React from 'react';

import { Extend, JS } from '@avstantso/node-or-browser-js--utils';

import * as RD from '@avstantso/react--data';

import { Loading as BaseLoading } from '@components/loading';
import { ErrorBox as BaseErrorBox } from '@components/errors';

import type { AsyncAction } from './types';

/**
 * @summary Async action ref dependent components factory
 */
export function ComponentsFactory(
  ref: React.MutableRefObject<AsyncAction.Ref>
): void {
  //#region Mk
  const Mk =
    <
      TStateField extends keyof RD.AsyncAction.Readonly,
      TFCPropField extends string = never
    >(
      stateField: TStateField,
      fcPropField?: TFCPropField
    ) =>
    <
      TFC extends (...args: any) => any,
      TTemplate extends (...args: any) => any
    >(
      Template: TTemplate
    ): TFC =>
      RD.DynamicFC(Template.name, (({ children, override, ...rest }) => {
        const field =
          undefined === override
            ? JS.get.raw(ref.current.state, stateField)
            : override;

        if (!field) return <>{children}</>;

        const props = { ...rest };
        if (fcPropField) JS.set.raw(props, fcPropField, field);

        return <Template {...props} />;
      }) as TFC);

  Mk.Loading = (): AsyncAction.Components.Boundary.Loading.FC => {
    const mk = Mk('loading');

    return Extend.Arbitrary(mk(BaseLoading), {
      Text: mk(BaseLoading.Text),
      Caption: mk(BaseLoading.Caption),
    });
  };

  Mk.ErrorBox = (): AsyncAction.Components.Boundary.ErrorBox.FC => {
    const mk = Mk('lastError', 'error');

    return Extend.Arbitrary(mk(BaseErrorBox), {
      InForm: mk(BaseErrorBox.InForm),
      Simple: mk(BaseErrorBox.Simple),
    });
  };
  //#endregion

  const BoundaryIfLoading: React.FC = ({ children }) => {
    const { loading } = ref.current.state;
    return loading && <>{children}</>;
  };

  const BoundaryIfError: React.FC = ({ children }) => {
    const { lastError } = ref.current.state;
    return lastError && <>{children}</>;
  };

  //#region Boundary
  const Boundary: AsyncAction.Components.Boundary.FC = ({
    children,
    localize,
    data,
    simple,
    inForm,
    errorChildren,
    override,
    ...rest
  }) => {
    const { lastError } = ref.current.state;
    const loading =
      undefined === override ? ref.current.state.loading : override;

    return loading ? (
      <BaseLoading.Text {...rest} />
    ) : lastError ? (
      <BaseErrorBox error={lastError} {...{ localize, data, simple, inForm }}>
        {errorChildren}
      </BaseErrorBox>
    ) : (
      <>{children}</>
    );
  };

  Boundary.Loading = Mk.Loading();
  Boundary.ErrorBox = Mk.ErrorBox();
  Boundary.IfLoading = BoundaryIfLoading;
  Boundary.IfError = BoundaryIfError;
  //#endregion

  ref.current.components = { Boundary };
}
