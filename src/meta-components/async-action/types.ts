import type React from 'react';

import type * as RD from '@avstantso/react--data';

import type { Loading as BaseLoading } from '@components/loading';

import type { ErrorBox as Types } from '@components/errors/error-box/types';
import type { Toaster } from '@components/errors/error-box/toast-hook';

/**
 * @summary Async action management with `Toaster` hook
 */
export namespace AsyncAction {
  /**
   * @summary Status dependent components
   */
  export namespace Components {
    /**
     * @summary Boundary component
     */
    export namespace Boundary {
      /**
       * @summary If `loading` shows `<Loading/>`, else show `children`
       */
      export namespace Loading {
        export type PropsBase = BaseLoading.Base.Props & { override?: boolean };

        /**
         * @summary If `loading` shows `<Loading/>`, else show `children`
         */
        export type FC = BaseLoading.FC<PropsBase>;
      }

      /**
       * @summary If `lastError` shows `<ErrorBox error={lastError}/>`, else show `children`
       */
      export namespace ErrorBox {
        export type PropsBase = Omit<Types.Props.Base, 'error'> & {
          errorChildren?: React.ReactNode;
        };

        /**
         * @summary If `lastError` shows `<ErrorBox error={lastError}/>`, else show `children`
         */
        export type FC = Types.FC<PropsBase>;
      }

      export type PropsBase = BaseLoading.Text.Props<Loading.PropsBase> &
        Types.Props<ErrorBox.PropsBase>;

      /**
       * @summary If `loading` shows `<Loading/>`, elseif `lastError` shows `<ErrorBox/>`, else show `children`
       */
      export type FC = React.FC<PropsBase> & {
        /**
         * @summary If `loading` shows `<Loading/>`, else show `children`
         */
        Loading: Loading.FC;

        /**
         * @summary `Loading` boundary antipode. If `loading` shows `children`, else shows `null`
         */
        IfLoading: React.FC;

        /**
         * @summary If `lastError` shows `<ErrorBox error={lastError}/>`, else show `children`
         */
        ErrorBox: ErrorBox.FC;

        /**
         * @summary `ErrorBox` boundary antipode. If `lastError` shows `children`, else shows `null`
         */
        IfError: React.FC;
      };
    }
  }

  export type Components = {
    /**
     * @summary Boundry component
     */
    Boundary: Components.Boundary.FC;
  };

  export namespace State {
    /**
     * @summary Async action state provider: `loading` & `lastError` with `Toaster` hook
     * and state dependent components.
     */
    export type Provider = {
      /**
       * @summary Async action state: `loading` & `lastError` with `Toaster` hook
       * and state dependent components.
       */
      asyncAction: State;
    };
  }

  /**
   * @summary Async action state: `loading` & `lastError` with `Toaster` hook
   * and state dependent components.
   */
  export type State = RD.AsyncAction.State & {
    toaster: Toaster;
  } & Components;

  export namespace Readonly {
    /**
     * @summary Async action readonly part and state dependent components provider
     */
    export type Provider = {
      /**
       * @summary Async action state: `loading` & `lastError` with `Toaster` hook
       * and state dependent components.
       */
      asyncAction: Readonly;
    };
  }

  /**
   * @summary Async action readonly part and state dependent components
   */
  export type Readonly = RD.AsyncAction.Readonly & Components;

  export namespace Dispatch {
    /**
     * @summary Async action state dispatch part
     */
    export type Provider = {
      /**
       * @summary Async action state dispatch part
       */
      asyncAction: Dispatch;
    };
  }

  /**
   * @summary Async action state dispatch part
   */
  export type Dispatch = RD.AsyncAction.Dispatch;

  export namespace Ref {
    /**
     * @summary Ref components
     */
    export type Components = { components: AsyncAction.Components };
  }

  /**
   * @summary Async action state ref for additional wraps with components
   * @param TExt Extension for additional wrap
   */
  export type Ref<TExt extends Ref.Components = Ref.Components> =
    RD.AsyncAction.Ref<TExt>;
}

/**
 * @summary Async action state: `loading` & `lastError` with `Toaster` hook
 * and state dependent components.
 */
export type AsyncAction = AsyncAction.State;
