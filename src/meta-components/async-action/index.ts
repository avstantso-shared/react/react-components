import React from 'react';

import { Stub } from '@avstantso/node-or-browser-js--utils';

import * as RD from '@avstantso/react--data';

import { useToaster } from '@components/errors/error-box/toast-hook';
import { useViewport } from '@components/viewport';

import type { AsyncAction as Types } from './types';
import { ComponentsFactory } from './components-factory';

/**
 * @summary Async action management with `Toaster` hook and state dependent components provider.
 */
export namespace AsyncAction {
  /**
   * @summary Async action readonly part and state dependent components.
   */
  export type Readonly = Types.Readonly;

  export namespace Readonly {
    /**
     * @summary Async action readonly part and state dependent components provider.
     */
    export type Provider = Types.Readonly.Provider;
  }

  /**
   * @summary Async action state dispatch part
   */
  export type Dispatch = RD.AsyncAction.Dispatch;

  export namespace Dispatch {
    /**
     * @summary Async action state dispatch part
     */
    export type Provider = Types.Dispatch.Provider;
  }

  /**
   * @summary Async action state provider: `loading` & `lastError` with `Toaster` hook
   * and state dependent components.
   */
  export type Provider = Types.State.Provider;

  /**
   * @summary Type of `AsyncAction` const
   */
  export type Definition = Omit<RD.AsyncAction.Definition, 'empty' | 'use'> & {
    /**
     * @summary Async action empty structure
     */
    empty: Types;

    /**
     * @summary Async action hook
     */
    use: typeof useAsyncAction;

    /**
     * @summary Async action readonly part reconstruction for `{loading: false, lastError: null}`.
     */
    staticReadonly: RD.AsyncAction.Readonly & Types.Components;
  };
}

export type AsyncAction = Types;

/**
 * @summary Async action hook with `Toaster` hook and state dependent components.
 */
export function useAsyncAction(
  initialLoading = true,
  onSetError?: (error: Error) => unknown
): AsyncAction {
  const toaster = useToaster();
  const cursorWait = useViewport()?.cursor?.promise?.wait;

  const [ref, initial] = RD.useAsyncActionRef<Types.Ref.Components>(
    initialLoading,
    (e) => {
      onSetError && onSetError(e);
      e && toaster(e);
    }
  );

  if (initial) {
    ComponentsFactory(ref);

    if (cursorWait) {
      const { Exec: BaseExec } = ref.current.state;

      const Exec: RD.AsyncAction.HighLevelMethods.Exec.Safe = (safe) => {
        const exec = BaseExec(safe);

        return (...params: any[]) => cursorWait((exec as Function)(...params));
      };

      ref.current.state.Exec = Object.defineProperties(Exec, {
        exec: { get: () => Exec(Stub) },
      }) as RD.AsyncAction.HighLevelMethods.Exec;
    }
  }

  return { ...ref.current.state, toaster, ...ref.current.components };
}

const empty: AsyncAction = {
  ...RD.AsyncAction.empty,
  toaster: undefined,
  Boundary: undefined,
};

function StaticReadonly(): RD.AsyncAction.Readonly & Types.Components {
  const state: RD.AsyncAction.Readonly = { loading: false, lastError: null };

  const ref: any = { current: { state } };

  ComponentsFactory(ref);

  return { ...state, ...ref.current.components };
}

export const AsyncAction: AsyncAction.Definition = {
  ...RD.AsyncAction,
  empty,
  use: useAsyncAction,
  staticReadonly: StaticReadonly(),
};
