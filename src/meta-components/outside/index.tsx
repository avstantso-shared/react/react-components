import React, {
  createContext,
  useContext,
  useEffect,
  useRef,
  useState,
} from 'react';

import {
  Compare,
  Generics,
  JS,
  Perform,
} from '@avstantso/node-or-browser-js--utils';
import { InvalidArgumentError } from '@avstantso/node-or-browser-js--errors';
import { Model } from '@avstantso/node-or-browser-js--model-core';

import { StateRec } from '@avstantso/react--data';

export namespace Outside {
  export type TypeMap<
    TItemProps extends {} = {},
    TContext extends object = {}
  > = {
    ItemProps: TItemProps;
    Context?: TContext;
  };

  export namespace TypeMap {
    export type ItemPriority<
      TItemProps extends { priority?: number } = { priority?: number }
    > = TypeMap<TItemProps>;
  }

  export namespace ItemReg {
    export type Props<TTypeMap extends TypeMap = TypeMap> = {
      /**
       * @summary Key in `Outside` list
       */
      outKey: Perform<string>;

      /**
       * @summary Dependency list for update item
       */
      deps?: React.DependencyList;
    } & TTypeMap['ItemProps'];

    export type FC<TTypeMap extends TypeMap = TypeMap> = React.FC<
      Props<TTypeMap>
    >;
  }

  export namespace Context {
    export type Update<TTypeMap extends TypeMap = TypeMap> = (
      actualProps?: Perform<React.PropsWithChildren<TTypeMap['ItemProps']>>
    ) => void;

    export type Item<TTypeMap extends TypeMap = TypeMap> = {
      key: string;
      update: Update<TTypeMap>;
    } & React.PropsWithChildren<TTypeMap['ItemProps']>;

    export type List<TTypeMap extends TypeMap = TypeMap> = ReadonlyArray<
      Item<TTypeMap>
    >;

    export type Default<TTypeMap extends TypeMap = TypeMap> = StateRec<
      'list',
      Context.List<TTypeMap>
    >;
  }

  export type Context<TTypeMap extends TypeMap = TypeMap> =
    Context.Default<TTypeMap> & TTypeMap['Context'];

  export type Options<TTypeMap extends TypeMap = TypeMap> = {
    ItemView?: React.FC<Context.Item<TTypeMap>>;
    sort?: boolean;
    compare?: Compare<Context.Item<TTypeMap>>;
    customContextHook?(def?: Context.Default<TTypeMap>): TTypeMap['Context'];
    label?: string | Function | Model.Named;
  };
}

function getPriority<
  TTypeMap extends Outside.TypeMap.ItemPriority = Outside.TypeMap.ItemPriority
>({ priority }: Outside.Context.Item<TTypeMap>) {
  return JS.is.number(priority) ? priority : Number.MAX_VALUE;
}

/**
 * @summary Supports in deriveds: `priority` property (`number` type).
 *
 * Defaults compare keys.
 */
function defaultCompare<
  TTypeMap extends Outside.TypeMap.ItemPriority = Outside.TypeMap.ItemPriority
>(a: Outside.Context.Item<TTypeMap>, b: Outside.Context.Item<TTypeMap>) {
  return getPriority(a) - getPriority(b) || a.key.localeCompare(b.key);
}

export function Outside<TTypeMap extends Outside.TypeMap = Outside.TypeMap>(
  options: Outside.Options<TTypeMap> = {}
) {
  const label = JS.is.string(options?.label)
    ? options?.label
    : options?.label.name;

  type Context = Outside.Context<TTypeMap>;
  type ContextUpdate = Outside.Context.Update<TTypeMap>;
  type ContextList = Outside.Context.List<TTypeMap>;
  type ContextItem = Outside.Context.Item<TTypeMap>;

  type ItemRegFC = Outside.ItemReg.FC<TTypeMap>;

  const {
    ItemView,
    sort,
    compare = defaultCompare,
    customContextHook,
  } = options;

  const Context = createContext<Context>(undefined);

  const Provider: React.FC = (props) => {
    const [list, setList] = useState<ContextList>([]);

    const defValue: Context = {
      list,
      setList,
    };

    const value = !customContextHook
      ? defValue
      : { ...defValue, ...customContextHook(defValue) };

    return <Context.Provider {...{ value }} {...props} />;
  };

  const ItemReg: ItemRegFC = (props) => {
    const { outKey, deps, ...rest } = props;

    const key = Perform(outKey);

    if (!JS.is.string(key))
      throw new InvalidArgumentError(`Outside.ItemReg key must be a string`);

    const indexRef = useRef<number>();
    const { setList } = useContext(Context);

    const update: ContextUpdate = (actualProps = Generics.Cast.To(rest)) => {
      setList((prev) => {
        const next = [...prev];

        const item: ContextItem = { key, update, ...Perform(actualProps) };

        undefined === indexRef.current
          ? (indexRef.current = next.push(item) - 1)
          : next.splice(indexRef.current, 1, item);

        return next;
      });
    };

    useEffect(update, deps || []);

    return null;
  };

  const List: React.FC = (props) => {
    if (!ItemView)
      throw new InvalidArgumentError(
        `Outside.List requires the ItemView option`
      );

    const { children } = props;
    const context = useContext(Context);

    const list = !sort ? context.list : [...context.list].sort(compare);

    return (
      <>
        {list.map((itemProps) => (
          <ItemView {...itemProps} />
        ))}
        {children}
      </>
    );
  };

  return { Context, Provider, ItemReg, List };
}

Outside.defaultCompare = defaultCompare;
