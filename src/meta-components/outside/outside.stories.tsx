import React from 'react';
import { Meta, Story } from '@story-helpers';

import { Outside } from '.';

const { Provider, ItemReg, List } = Outside<
  Outside.TypeMap<React.HTMLProps<HTMLLIElement>>
>({
  label: 'OutsideStory',
  ItemView: ({ update, ...props }) => <li {...props} />,
});

const meta = { ...Meta(Provider, { title: 'Meta-Components/Outside' }) };

export default meta;

export const outside = Story(meta, {
  render: (args) => (
    <Provider>
      Text1
      <ul>
        <List />
      </ul>
      Text2
      <ol>
        <List />
      </ol>
      Text3
      <ItemReg outKey="a">A</ItemReg>
      <ItemReg outKey="b">B</ItemReg>
      <ItemReg outKey="c">C</ItemReg>
    </Provider>
  ),
});
