import React from 'react';

/**
 * @summary Create function component with `fragment + children` inside
 * @param fragment Addition fragment
 * @returns Function component with `fragment + children` inside
 */
export function CreateProvider(fragment: React.ReactNode): React.FC {
  return ({ children }) => (
    <>
      {fragment}
      {children}
    </>
  );
}
