import { NamesTree } from '@avstantso/node-or-browser-js--utils';

import { MakeI18nextResources } from '@avstantso/react--data';

import * as Utils from './i18n-utils';

import l_en from './locales/en.json';
import l_ru from './locales/ru.json';

//#region re-export
export {
  T,
  MakeI18nextResources,
  MakeGetLocalString,
} from '@avstantso/react--data';

export type TransSimple = Utils.TransSimple;
export type TransObj2CmpOptions = Utils.TransObj2CmpOptions;
export type Trans = Utils.Trans;

export const { MakeUseTranslation, noTags } = Utils;
//#endregion

const PACKAGE_JSON = require(`../package.json`); // See @avstantso/node-js--rollup-read-package-json

export const i18next_resources = MakeI18nextResources(
  PACKAGE_JSON.name,
  l_en,
  l_ru
);

export const [useTranslation, Trans] = MakeUseTranslation(PACKAGE_JSON.name);

export const LOCALES = NamesTree.I18ns(l_en).I18n;
