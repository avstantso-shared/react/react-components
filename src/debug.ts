import debug, { Debugger } from 'debug';

import { Extend } from '@avstantso/node-or-browser-js--utils';

type DebugEx = Debugger & {
  subNameSpace<T extends DebugEx>(subNameSpace: string): T;
};

function DebugEx<T extends DebugEx>(prefix: string = ''): T {
  return new Proxy(debug(prefix), {
    get(target, p) {
      return 'subNameSpace' === p
        ? (subNameSpace: string) => DebugEx(`${prefix}${subNameSpace}`)
        : target[p as keyof typeof target];
    },
  }) as T;
}

const dbg = DebugEx(`@avstantso/rc:`);

const grid = dbg.subNameSpace(`grid:`);
const gridRender = grid.subNameSpace(`render:`);
const gridRenderData = gridRender.subNameSpace(`data:`);

export default Extend.Arbitrary(dbg, {
  grid: Extend.Arbitrary(grid, {
    render: Extend.Arbitrary(gridRender, {
      data: gridRenderData,
    }),
  }),
});
