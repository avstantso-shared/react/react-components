import { JS } from '@avstantso/node-or-browser-js--utils';
import { HtmlEvalError } from './HtmlEvalError';

export class HtmlEvalComponentError extends HtmlEvalError {
  constructor(message: string, internal?: Error) {
    super(message, internal);

    Object.setPrototypeOf(this, HtmlEvalComponentError.prototype);
  }

  static is(error: unknown): error is HtmlEvalComponentError {
    return JS.is.error(this, error);
  }
}
