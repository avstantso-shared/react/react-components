import { JS } from '@avstantso/node-or-browser-js--utils';
import { GridError } from './GridError';

export class GridHierarchyError extends GridError {
  constructor(message: string, internal?: Error) {
    super(message, internal);

    Object.setPrototypeOf(this, GridHierarchyError.prototype);
  }

  static is(error: unknown): error is GridHierarchyError {
    return JS.is.error(this, error);
  }
}
