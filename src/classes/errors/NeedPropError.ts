import { BaseError } from '@avstantso/node-or-browser-js--errors';
import { JS } from '@avstantso/node-or-browser-js--utils';

export class NeedPropError<
  T extends string | Function = string | Function
> extends BaseError {
  constructor(
    component: T,
    prop: T extends (...args: infer P) => any ? Partial<P[0]> : object,
    propAvailableValues?: string
  ) {
    const [name, value] = Object.entries(prop)[0] || [];

    super(
      `${
        JS.is.function(component) ? component.name : component
      } component must have prop "${name}"${
        !propAvailableValues
          ? ''
          : ` (available values: ${propAvailableValues})`
      } but it's "${value}"`
    );

    Object.setPrototypeOf(this, NeedPropError.prototype);
  }

  static is(error: unknown): error is NeedPropError {
    return JS.is.error(this, error);
  }
}
