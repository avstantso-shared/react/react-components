import { JS } from '@avstantso/node-or-browser-js--utils';
import { TextDocError } from './TextDocError';

export class TextDocHierarchyError extends TextDocError {
  constructor(message: string, internal?: Error) {
    super(message, internal);

    Object.setPrototypeOf(this, TextDocHierarchyError.prototype);
  }

  static is(error: unknown): error is TextDocHierarchyError {
    return JS.is.error(this, error);
  }

  static checkContext(context: unknown, current: string, parent: string) {
    if (null === context || !JS.is.object(context))
      throw new TextDocHierarchyError(`${current} must be nested in ${parent}`);
  }
}
