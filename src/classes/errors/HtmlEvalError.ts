import { BaseError } from '@avstantso/node-or-browser-js--errors';
import { JS } from '@avstantso/node-or-browser-js--utils';

export class HtmlEvalError extends BaseError {
  constructor(message: string, internal?: Error) {
    super(message, internal);

    Object.setPrototypeOf(this, HtmlEvalError.prototype);
  }

  static is(error: unknown): error is HtmlEvalError {
    return JS.is.error(this, error);
  }
}
