import { BaseError } from '@avstantso/node-or-browser-js--errors';
import { JS } from '@avstantso/node-or-browser-js--utils';

export class GridError extends BaseError {
  constructor(message: string, internal?: Error) {
    super(message, internal);

    Object.setPrototypeOf(this, GridError.prototype);
  }

  static is(error: unknown): error is GridError {
    return JS.is.error(this, error);
  }
}
