import { BaseError } from '@avstantso/node-or-browser-js--errors';
import { JS } from '@avstantso/node-or-browser-js--utils';

export class NeedProviderError extends BaseError {
  constructor(component: string | Function, provider: string | Function) {
    super(
      `${
        JS.is.function(component) ? component.name : component
      } component must be placed into ${
        JS.is.function(provider) ? provider.name : provider
      }`
    );

    Object.setPrototypeOf(this, NeedProviderError.prototype);
  }

  static is(error: unknown): error is NeedProviderError {
    return JS.is.error(this, error);
  }
}
