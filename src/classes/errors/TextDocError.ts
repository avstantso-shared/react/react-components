import { BaseError } from '@avstantso/node-or-browser-js--errors';
import { JS } from '@avstantso/node-or-browser-js--utils';

export class TextDocError extends BaseError {
  constructor(message: string, internal?: Error) {
    super(message, internal);

    Object.setPrototypeOf(this, TextDocError.prototype);
  }

  static is(error: unknown): error is TextDocError {
    return JS.is.error(this, error);
  }
}
