import * as ReactRouter from 'react-router';

import { SiteMap } from '@avstantso/node-or-browser-js--model-core';

// A.V.Stantso:
//  extended fields for sitemap building
export namespace RouteObject {
  export namespace ToSiteMap {
    export type Options = {
      /**
       * @default {changefreq: SiteMapChangefreq.yearly, priorety: 0.8}
       */
      defaultAttrs?: SiteMap.Url.Attrs;

      /**
       * @default ['*']
       */
      excludes?: string[];
    };
  }
}

export type RouteObject = ReactRouter.RouteObject &
  SiteMap.Url.Metadata.Provider;

function isRouteObject(route: ReactRouter.RouteObject): route is RouteObject {
  return true;
}

function toSitmap(
  route: RouteObject,
  options?: RouteObject.ToSiteMap.Options
): SiteMap.Url[] {
  const defaultAttrs = {
    changefreq: SiteMap.Url.Changefreq.yearly,
    priorety: 0.8,
    ...(options?.defaultAttrs || {}),
  };
  const excludes = options?.excludes || ['*'];

  const sitemap: SiteMap.Url[] = [];

  function recursion(route: RouteObject, parentPath = ''): void {
    const { path, children, sitemapMeta = {} } = route;
    if (excludes.includes(path)) return;

    const fullPath = `${parentPath}/${path || ''}`.replace(/\/+/g, '/');

    if (!children || Object.values(sitemapMeta).length)
      sitemap.push({
        loc: `${fullPath}`.replace(/\/$/, '') || '/',
        ...defaultAttrs,
        ...sitemapMeta,
      });

    if (children)
      (children as RouteObject[]).forEach((child) => {
        const { sitemapMeta: csm } = child;
        const h = csm?.hidden || sitemapMeta.hidden;
        const p = csm?.private || sitemapMeta.private;

        recursion(
          {
            ...child,
            sitemapMeta: {
              ...(csm || {}),
              ...(undefined === h ? {} : { hidden: h }),
              ...(undefined === p ? {} : { private: p }),
            },
          },
          fullPath
        );
      });
  }

  recursion(route);

  return sitemap;
}

export const RouteObject = {
  is: isRouteObject,
  toSitmap,
};
