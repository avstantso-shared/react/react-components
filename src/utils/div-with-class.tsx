import React from 'react';

import { Generics } from '@avstantso/node-or-browser-js--utils';

import { Combiner, InnerRef } from '@avstantso/react--data';

export namespace DivWithClass {
  export type As = Extract<
    keyof JSX.IntrinsicElements,
    | 'main'
    | 'section'
    | 'aside'
    | 'nav'
    | 'article'
    | 'header'
    | 'footer'
    | 'div'
  >;

  export namespace As {
    export type Props = DivWithClass.Props & { As: DivWithClass.As };

    export type FC = React.FC<Props>;
  }

  export type Props = React.ComponentPropsWithoutRef<'div'> &
    InnerRef.Provider<HTMLDivElement>;

  export type FC = React.FC<Props>;

  export type Overload = {
    <T extends DivWithClass.FC = DivWithClass.FC>(
      staticClassName: string,
      As?: DivWithClass.As
    ): T;
    As<T extends DivWithClass.As.FC = DivWithClass.As.FC>(
      staticClassName: string
    ): T;
  };
}

export const DivWithClass: DivWithClass.Overload = <
  T extends DivWithClass.FC = DivWithClass.FC
>(
  staticClassName: string,
  As: DivWithClass.As = 'div'
): T => {
  return Generics.Cast.To((props: DivWithClass.Props) => {
    const { className, innerRef, ...rest } = props;

    return (
      <As
        {...Combiner.className(staticClassName, className)}
        {...rest}
        ref={innerRef}
      />
    );
  });
};

DivWithClass.As = <T extends DivWithClass.As.FC = DivWithClass.As.FC>(
  staticClassName: string
): T =>
  Generics.Cast.To((props: DivWithClass.As.Props) => {
    const { As, ...rest } = props;
    const Component = DivWithClass(staticClassName, As);
    return <Component {...rest} />;
  });
