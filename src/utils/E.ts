import { toast } from '@instances';

avstantso.Catch._default = (error: any) => {
  toast.error(error?.message || error);
};
