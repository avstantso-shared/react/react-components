import React from 'react';
import { Generic, JS } from '@avstantso/node-or-browser-js--utils';

export namespace DOMGet {
  export type Ref<T extends Element = Element> = React.RefObject<T> | (() => T);

  export type RefOrId<T extends Element = Element> = Ref<T> | string;
}

export const DOMGet = {
  getElement: <T extends Element = Element>(ref: DOMGet.RefOrId<T>): T =>
    JS.is.string(ref)
      ? (document.getElementById(ref) as Generic)
      : JS.is.function(ref)
      ? ref()
      : ref.current,

  getElementId: <T extends Element = Element>(
    element: T,
    idToSet: string
  ): string => {
    if (element.id) return element.id;

    if (idToSet) {
      element.id = idToSet;
      return idToSet;
    }

    console.warn('"id" attribute not setted for', element);
    return 'divWithEmptyId';
  },
};
