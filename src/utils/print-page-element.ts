import { DetectBrowser } from '@avstantso/react--data';
import { DOMGet } from './dom-get';

export namespace PrintPageElement {
  export namespace Options {
    export type FilterHead = (head: string) => string;
  }

  export type Options = {
    filterHead?: Options.FilterHead;
  };
}

const style = 'style="height: unset"';

export function printPageElement(
  elementForPrint: DOMGet.RefOrId,
  options?: PrintPageElement.Options
) {
  const { filterHead } = options || {};

  const winPrint = window.open(
    '',
    '',
    'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0'
  );

  winPrint.document.write(
    [
      `<html ${style}>`,
      `  <head>`,
      filterHead
        ? filterHead(document.head.innerHTML)
        : document.head.innerHTML,
      `  </head>`,
      `  <body ${style}>`,
      `  </body>`,
      `</html>`,
    ].join('\r\n')
  );
  winPrint.document.close();

  winPrint.document.body.appendChild(
    DOMGet.getElement(elementForPrint).cloneNode(true)
  );

  winPrint.focus();
  winPrint.print();

  DetectBrowser.isFirefox ||
  DetectBrowser.isOpera ||
  DetectBrowser.isEdgeChromium
    ? winPrint.close()
    : (winPrint.onafterprint = winPrint.close);
}
