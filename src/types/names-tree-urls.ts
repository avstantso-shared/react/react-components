import type { To } from 'react-router';

import type { TS, NamesTree } from '@avstantso/node-or-browser-js--utils';

export type PathXorNode = TS.Alt<
  [{ path: string }, { node: NamesTree.Presets.Urls.Node }]
>;

export type PathUrlXorNode = PathXorNode & { url?: To };

export const PathXorNode = {
  Path: (pathXorNode: PathXorNode) =>
    pathXorNode.path || pathXorNode.node?._path,
};
