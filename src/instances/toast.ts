import { toast } from 'react-toastify';

import { InstanceProxy } from '@avstantso/node-or-browser-js--utils';

export default InstanceProxy({ toast });
