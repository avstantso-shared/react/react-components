import { InstanceProxy } from '@avstantso/node-or-browser-js--utils';

import Toast from './toast';
import Icons from './icons';

export const { toast } = Toast;
export const { Fa, Di } = Icons;

export namespace Fa {
  export type Key = InstanceProxy.Key<typeof Fa>;
}

export namespace Di {
  export type Key = InstanceProxy.Key<typeof Di>;
}
