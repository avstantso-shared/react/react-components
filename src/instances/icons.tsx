import { IconBaseProps } from 'react-icons';

import * as Fa from 'react-icons/fa';
import * as Di from 'react-icons/di';

import { InstanceProxy } from '@avstantso/node-or-browser-js--utils';

const wrap = (getInstance: Function, p: string) => (props: IconBaseProps) => {
  const I = getInstance()[p];
  return <I {...props} />;
};

export default InstanceProxy({ Fa, Di }, wrap);
