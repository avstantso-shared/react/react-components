import React from 'react';
import { Meta, Story } from '@story-helpers';

import { Generics } from '@avstantso/node-or-browser-js--utils';

import { FamousSiteQueryKind } from './kind';
import { FamousSiteQuery } from '.';

const meta = {
  ...Meta(FamousSiteQuery, {
    argTypes: {
      kind: {
        options: FamousSiteQueryKind._keys,
        control: { type: 'radio' },
      },
      url: { control: { type: 'text' } },
    },
  }),
};

export default meta;

function fixArgs(args: FamousSiteQuery.Props): FamousSiteQuery.Props {
  const { kind, ...rest } = args;
  const r = {
    kind: FamousSiteQueryKind.fromString(Generics.Cast.To(kind)),
    ...rest,
  };
  return r;
}

export const famousSiteQuery = Story(meta, {
  args: { q: 'What is storybook' },
  render: (args) => <FamousSiteQuery {...fixArgs(args)} />,
});
