import { Enum } from '@avstantso/node-or-browser-js--utils';

import * as _ from './enum';

export namespace FamousSiteQueryKind {
  export type TypeMap = Enum.TypeMap.Make<
    _.FamousSiteQueryKind.Type,
    _.FamousSiteQueryKind.Key.List
  >;
}

export type FamousSiteQueryKind = Enum.Value<FamousSiteQueryKind.TypeMap>;

export const FamousSiteQueryKind = Enum(
  _.FamousSiteQueryKind,
  'FamousSiteQueryKind'
)<FamousSiteQueryKind.TypeMap>();
