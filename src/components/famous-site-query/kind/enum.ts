export enum FamousSiteQueryKind {
  Google = 1,
  Youtube,
  Yandex,
  Wikipedia,
}

export namespace FamousSiteQueryKind {
  export type Type = typeof FamousSiteQueryKind;

  export namespace Key {
    export type List = ['Google', 'Youtube', 'Yandex', 'Wikipedia'];
  }

  export type Key = keyof Type;
  export type Value = Type[Key];
}
