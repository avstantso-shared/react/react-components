import React from 'react';
import * as I18next from 'i18next';
import { Badge } from 'react-bootstrap';

import { JS } from '@avstantso/node-or-browser-js--utils';

import { useTranslation, Trans, T, noTags } from '@i18n';
import { Quotes } from '@components/quotes';

import { FamousSiteQueryKind } from './kind';

export namespace FamousSiteQuery {
  export namespace Base {
    export interface Props {
      q: string;
      quoted?: boolean;
      i18n?: I18next.i18n;
      t?: T;
    }

    export type FC = React.FC<Props> & JS.TypeInfo.Provider;
  }

  export namespace Raw {
    export type Url = string | ((q: string) => string);

    export interface Props extends Base.Props {
      url: Url;
    }

    export type FC = React.FC<Props> & JS.TypeInfo.Provider;
  }

  export interface Props extends Base.Props {
    kind?: FamousSiteQueryKind;
    url?: FamousSiteQuery.Raw.Url;
  }

  export interface FC extends React.FC<Props>, JS.TypeInfo.Provider {
    Google: Base.FC;
    Youtube: Base.FC;
    Yandex: Base.FC;
    Wikipedia: Base.FC;
  }
}

const typeInfo: JS.TypeInfo = { q: JS.string, quoted: JS.boolean };

const Raw: FamousSiteQuery.Raw.FC = (props) => {
  const { url, q: rawQ, quoted, i18n, t, children } = props;

  const q = !(i18n && t) ? rawQ : noTags(t(rawQ));
  const c =
    children ||
    (!(i18n && t) ? rawQ : <Trans.N {...{ i18n, t }} i18nKey={rawQ} />);

  return (
    <a
      href={JS.is.function(url) ? url(q) : `${url}${q}`}
      target="_blank"
      rel="noopener noreferrer"
    >
      {!url && (
        <Badge bg="danger">
          "url" prop is {JS.is.string ? `'${url}'` : `${url}`}
        </Badge>
      )}
      {quoted ? <Quotes>{c}</Quotes> : c}
    </a>
  );
};

Raw.typeInfo = typeInfo;

const Google: FamousSiteQuery.Base.FC = (props) => (
  <Raw url="https://www.google.com/search?q=" {...props} />
);
Google.typeInfo = typeInfo;

const Youtube: FamousSiteQuery.Base.FC = (props) => (
  <Raw url="https://www.youtube.com/results?search_query=" {...props} />
);
Youtube.typeInfo = typeInfo;

const Yandex: FamousSiteQuery.Base.FC = (props) => (
  <Raw url="https://yandex.ru/search/?text=" {...props} />
);
Yandex.typeInfo = typeInfo;

const Wikipedia: FamousSiteQuery.Base.FC = (props) => {
  const [, i18n] = useTranslation();
  return (
    <Raw
      url={`https://${i18n.language || 'en'}.wikipedia.org/w/index.php?search=`}
      {...props}
    />
  );
};
Wikipedia.typeInfo = typeInfo;

export const FamousSiteQuery: FamousSiteQuery.FC = (props) => {
  const { kind, url, ...rest } = props;

  const Component: FamousSiteQuery.Base.FC = FamousSiteQueryKind.switch(kind, {
    Google,
    Youtube,
    Yandex,
    Wikipedia,
  });

  return Component ? <Component {...rest} /> : <Raw url={url} {...rest} />;
};

FamousSiteQuery.typeInfo = typeInfo;
FamousSiteQuery.Google = Google;
FamousSiteQuery.Youtube = Youtube;
FamousSiteQuery.Yandex = Yandex;
FamousSiteQuery.Wikipedia = Wikipedia;
