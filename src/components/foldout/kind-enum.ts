export enum FoldoutKind {
  Chevron,
  Folder,
}

export namespace FoldoutKind {
  export type Type = typeof FoldoutKind;

  export namespace Key {
    export type List = ['Chevron', 'Folder'];
  }

  export type Key = keyof Type;
  export type Value = Type[Key];
}
