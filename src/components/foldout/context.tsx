import { Generics } from '@avstantso/node-or-browser-js--utils';
import { createContext, useContext, Dispatch, SetStateAction } from 'react';

export type FoldoutContextIds = {
  [id: string]: boolean;
};

export type FoldoutContextChange = (id: string, value: boolean) => void;

export type FoldoutContext = [
  FoldoutContextChange,
  FoldoutContextIds,
  Dispatch<SetStateAction<FoldoutContextIds>>
];

export const FoldoutContext = createContext<FoldoutContext>(
  Generics.Cast.To([])
);
export const useFoldoutContext = () => useContext(FoldoutContext);
