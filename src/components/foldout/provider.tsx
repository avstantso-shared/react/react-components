import React, { useCallback, useState } from 'react';
import {
  FoldoutContextIds,
  FoldoutContextChange,
  FoldoutContext,
} from './context';

const Provider: React.FC = (props) => {
  const [ids, setIds] = useState<FoldoutContextIds>({});

  const change = useCallback<FoldoutContextChange>(
    (id, value): void =>
      setIds((prev) => {
        const next = { ...prev };
        next[id] = value;
        return next;
      }),
    []
  );

  return <FoldoutContext.Provider value={[change, ids, setIds]} {...props} />;
};

export default Provider;
