import React from 'react';
import { Meta, Story } from '@story-helpers';

import { Generics } from '@avstantso/node-or-browser-js--utils';

import { FoldoutKind, Foldout } from '.';

const meta = {
  ...Meta(Foldout, {
    argTypes: {
      kind: {
        options: FoldoutKind._names,
        control: { type: 'radio' },
      },
    },
  }),
};

export default meta;

const argsBase: Foldout.Props = {
  kind: Generics.Cast.To(FoldoutKind._names[0]),
  title: 'Title',
  isFoldedOut: false,
  onChange: (newIsFoldedOut: boolean) =>
    alert(`Need change state to "${newIsFoldedOut}"`),
};

export const simple = Story(meta, {
  args: argsBase,
  render: (args) => {
    const { kind, ...rest } = args;
    const { isFoldedOut } = args;

    const element = FoldoutKind.switch(
      FoldoutKind.fromString(Generics.Cast.To(kind)),
      {
        Chevron: <Foldout.Chevron {...rest} />,
        Folder: <Foldout.Folder {...rest} />,
        default: <Foldout {...args} />,
      }
    );

    return (
      <>
        {element}
        {isFoldedOut && (
          <>
            {' '}
            Hidden <b>elements!</b>
          </>
        )}
      </>
    );
  },
});

export const withChildren = {
  ...simple,
  args: {
    ...argsBase,
    children: (
      <>
        <b>Caption</b> text
      </>
    ),
  },
};
