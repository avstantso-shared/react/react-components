export { FoldoutKind, Foldout } from './foldout';
export { useFoldoutContext } from './context';
