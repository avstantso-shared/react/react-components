import React from 'react';
import type { IconType } from 'react-icons/lib';

import { UnexpectedError } from '@avstantso/node-or-browser-js--errors';
import { Enum, JS, Perform } from '@avstantso/node-or-browser-js--utils';
import type { ChildrenProps } from '@avstantso/react--data';

import { Fa } from '@instances';

import * as _ from './kind-enum';
import { useFoldoutContext } from './context';
import ContextProvider from './provider';

const { FaChevronDown, FaChevronLeft, FaChevronRight, FaFolder, FaFolderOpen } =
  Fa;

export namespace Foldout {
  export namespace Kind {
    export type TypeMap = Enum.TypeMap.Make<
      _.FoldoutKind.Type,
      _.FoldoutKind.Key.List
    >;
  }

  export type Kind = Enum.Value<Foldout.Kind.TypeMap>;

  export type Change = (newIsFoldedOut: boolean) => void | boolean;

  export namespace Props {
    export namespace Strict {
      export interface Base
        extends Pick<React.HTMLProps<HTMLElement>, 'id' | 'title' | 'style'>,
          Pick<React.CSSProperties, 'color'>,
          ChildrenProps.Perform {
        isFoldedOut?: boolean;
        canFoldOut?: boolean;
        canFoldUp?: boolean;
        onChange?: Change;
        reversed?: boolean;
        left?: boolean;
      }
    }

    export interface Strict extends Strict.Base {
      kind?: Foldout.Kind;
    }

    export interface Base extends Strict.Base {
      [key: string]: any;
    }
  }

  export interface Props extends Props.Strict {
    [key: string]: any;
  }

  export interface FC extends React.FC<Props> {
    Kind: typeof FoldoutKind;

    Chevron: React.FC<Props.Base>;
    Folder: React.FC<Props.Base>;

    ContextProvider: React.FC;
  }
}

export const FoldoutKind = Enum(
  _.FoldoutKind,
  'Foldout.Kind'
)<Foldout.Kind.TypeMap>();

const getIcon = (
  kind: Foldout.Kind,
  isFoldedOut: boolean,
  left?: boolean
): IconType => {
  let t: number;
  switch (kind) {
    case FoldoutKind.Folder:
      t = 1;
      break;
    default:
      t = 0;
      break;
  }
  t = t * 2 + (isFoldedOut ? 1 : 0);

  switch (t) {
    case 0:
      return left ? FaChevronLeft : FaChevronRight;
    case 1:
      return FaChevronDown;
    case 2:
      return FaFolder;
    case 3:
      return FaFolderOpen;
    default:
      throw new UnexpectedError(`Unexpected Foldout type number(${t})`);
  }
};

export const Foldout: Foldout.FC = (props) => {
  const {
    isFoldedOut: propsIsFoldedOut,
    canFoldOut,
    canFoldUp,
    onChange,
    kind,
    title,
    color,
    children,
    style,
    reversed,
    left,
    ...rest
  } = props;
  const { id } = rest;

  const [change, ids] = useFoldoutContext();

  const isFromContext = id && ids;
  const isFoldedOut = JS.is.boolean(propsIsFoldedOut)
    ? propsIsFoldedOut
    : !!(isFromContext ? ids[id] : propsIsFoldedOut);

  const can = (() => {
    const r = isFoldedOut ? canFoldUp : canFoldOut;
    return r === undefined ? true : r;
  })();

  const Icon = getIcon(kind, isFoldedOut, left);
  const iconProps = { title, color };
  const componentProps = {
    style: { ...style, cursor: 'pointer', opacity: can ? 1 : 0.5 },
    onClick:
      can &&
      (() => {
        const handled = (onChange && onChange(!isFoldedOut)) || false;
        !handled && isFromContext && change(id, !isFoldedOut);
      }),
    color,
    ...rest,
  };

  if (!children) return <Icon {...iconProps} {...componentProps} />;

  return (
    <span {...componentProps}>
      {reversed ? (
        <>
          {Perform(children)}
          <Icon {...iconProps} />
        </>
      ) : (
        <>
          <Icon {...iconProps} />
          {Perform(children)}
        </>
      )}
    </span>
  );
};

Foldout.Kind = FoldoutKind;

Foldout.Chevron = (props: Foldout.Props.Base) => (
  <Foldout kind={FoldoutKind.Chevron} {...props} />
);

Foldout.Folder = (props: Foldout.Props.Base) => (
  <Foldout kind={FoldoutKind.Folder} {...props} />
);

Foldout.ContextProvider = ContextProvider;
