import React from 'react';

import { Combiner } from '@avstantso/react--data';

export namespace ButtonsGroup {
  export type Props = React.HTMLProps<HTMLDivElement>;
  export type FC = React.FC<Props>;
}

export const ButtonsGroup: ButtonsGroup.FC = (props) => {
  const { className, ...rest } = props;

  return (
    <div
      {...Combiner.className('btn-group flex-btn-group-container', className)}
      {...rest}
    />
  );
};
