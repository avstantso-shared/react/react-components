import type React from 'react';
import type { ButtonProps as RBButtonProps } from 'react-bootstrap';
import { IconBaseProps } from 'react-icons';

import type { Model } from '@avstantso/node-or-browser-js--model-core';

export namespace Button {
  export type Icon = (props: IconBaseProps) => JSX.Element;

  export namespace Url {
    export namespace Action {
      export type Props = Url.Props & { entity: Model.ID | Model.IDed };
    }

    export type Provider = { url?: string };

    export type Props = Button.Props & Provider;

    export type Get<TRouterProps extends Button.Props = Props> = (
      props: TRouterProps
    ) => string;
  }

  export namespace Base {
    export type Props<TRouterProps extends Button.Props = Url.Props> = Pick<
      RBButtonProps,
      'variant'
    > & {
      Icon: Button.Icon;
      getUrl?: Url.Get<Url.Props>;
      caption?: string;
      captionI18n?: string;
    };
  }

  export type Props = RBButtonProps & {
    htmlFor?: string;
    notResponsive?: boolean;
  };

  export namespace Submit {
    export type Props = Omit<Button.Props, 'type'>;
  }

  export type FC<TRouterProps extends Button.Props = Url.Props> =
    React.FC<Props> & {
      Router: React.FC<TRouterProps>;
      Submit: React.FC<Submit.Props>;
    };

  export namespace Custom {
    export type FC = React.FC<Props>;
  }

  export namespace Router {
    export type FC = React.FC<
      Omit<Props & Required<Url.Provider>, 'as' | 'to'>
    >;
  }

  export type Union<TRouterProps extends Button.Props = Url.Props> = {
    Small: FC<TRouterProps>;
    Large: FC<TRouterProps>;
  };
}
