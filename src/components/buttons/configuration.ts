import { UrlActions } from '@avstantso/react--data';

import { Fa } from '@instances';

import { Button } from './types';
import { SetupButtons } from './setup';

const {
  FaArrowLeft,
  FaBan,
  FaBroom,
  FaCheck,
  FaEllipsisV,
  FaEye,
  FaPencilAlt,
  FaPlusCircle,
  FaPrint,
  FaRegCopy,
  FaSave,
  FaSignInAlt,
  FaTrashAlt,
  FaTrashRestoreAlt,
  FaUserPlus,
  FaUserShield,
} = Fa;

const { Setting: B } = SetupButtons;

const ua =
  <UA extends UrlActions>(act: UA) =>
  (
    props: UA extends typeof UrlActions.New
      ? Button.Url.Props
      : Button.Url.Action.Props
  ) =>
    UrlActions.New === act
      ? UrlActions.New.toPathPostfix(props.url)
      : props.entity && act.toPathPostfix(props.url, props.entity);

export default {
  ...SetupButtons({
    primary: {
      Accept: B(FaSave),
      Back: B(FaArrowLeft),
      Copy: B(FaRegCopy),
      Create: B(FaPlusCircle, ua(UrlActions.New)),
      Edit: B(FaPencilAlt, ua(UrlActions.Edit)),
      Login: B(FaSignInAlt),
      Print: B(FaPrint),
      RegisterUp: B(FaUserPlus),
      Reset: B(FaUserShield),
      Restore: B(FaTrashRestoreAlt, ua(UrlActions.Restore)),
    },
    secondary: {
      Apply: B(FaSave),
      Cancel: B(FaBan),
      Clear: B(FaBroom),
      EllipsisV: B(FaEllipsisV),
      Ok: B(FaCheck),
      View: B(FaEye, ua(UrlActions.View)),
    },
    danger: {
      Delete: B(FaTrashAlt, ua(UrlActions.Delete)),
    },
  }),
};
