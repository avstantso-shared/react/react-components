import React from 'react';
import { useLocation, Link } from 'react-router-dom';
import * as RB from 'react-bootstrap';

import {
  Cases,
  Extend,
  Generic,
  JS,
} from '@avstantso/node-or-browser-js--utils';

import { Combiner, DynamicFC } from '@avstantso/react--data';

import { useTranslation, LOCALES } from '@i18n';

import type { Button } from './types';

export const MakeButton = <
  TName extends string,
  TRouterProps extends Button.Props = Button.Url.Props
>(
  name: TName,
  baseProps: Button.Base.Props
): Button.Union<TRouterProps> => {
  const R: Generic = {};

  const {
    Icon,
    getUrl,
    caption: outerCaption,
    captionI18n = JS.get.raw(LOCALES.buttons, Cases.camel(name)),
    ...baseRest
  } = baseProps;

  ['Small', 'Large'].map((sizeName, isLarge) => {
    if (!sizeName) return;

    function Props({
      children,
      className,
      title: outerTitle,
      notResponsive,
      ...rest
    }: Generic<Button.Props>) {
      const [t] = useTranslation();
      const caption = outerCaption || t(captionI18n);
      const title = outerTitle || (!isLarge && caption);

      return {
        ...baseRest,
        ...(isLarge ? {} : { size: 'sm' as 'sm' }),
        ...rest,
        ...(title ? { title } : {}),
        ...Combiner.className(
          'btn-avsrc',
          `btn-${Cases.kebab(name)}`,
          className
        ),
        children: (
          <>
            <Icon />
            {!!isLarge && (
              <span
                {...Combiner.className(
                  'caption',
                  !notResponsive && 'd-none d-md-inline'
                )}
              >
                {children || caption}
              </span>
            )}
          </>
        ),
      };
    }

    const r: Button.FC = Extend.Arbitrary(
      DynamicFC(`${name}${sizeName}`, (props: Button.Props) => (
        <RB.Button {...Props(props)} />
      )),
      {
        Router: (props: Button.Props) => {
          const { pathname } = useLocation();
          const propsU: Button.Url.Props = {
            ...props,
            url: (props as Button.Url.Props).url || pathname,
          };
          const url = getUrl ? getUrl(propsU) : propsU.url;

          if (!url || props.disabled)
            return (
              <RB.Button
                {...Props(props)}
                variant="secondary"
                onClick={null}
                disabled
              />
            );

          return (
            <RB.Button
              {...Props(props)}
              as={Link}
              to={url.replace(/\/+/g, '/')}
            />
          );
        },

        Submit: (props: Button.Submit.Props) => (
          <RB.Button {...Props(props)} type="submit" />
        ),
      }
    );

    JS.set.raw(R, sizeName, r);
  });

  return R;
};
