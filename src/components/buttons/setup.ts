import { Generic, JS, TS } from '@avstantso/node-or-browser-js--utils';

import type { Button } from './types';
import { MakeButton } from './make';

export namespace SetupButtons {
  export type Variant = Button.Base.Props['variant'];

  export type Setting<TRouterProps extends Button.Props = Button.Url.Props> =
    Omit<Button.Base.Props, 'variant'> & {
      RouterProps?: TRouterProps;
    };
  export type Settings = { [K in Variant]: NodeJS.Dict<SetupButtons.Setting> };

  export namespace Result {
    export type Item<
      V extends Variant,
      TSetting extends SetupButtons.Setting
    > = Button.Union<TSetting['RouterProps'] & { variant?: V }>;
  }
  export type Result<TSettings extends Settings> = TS.Flat<{
    [V in keyof TSettings]: {
      [K in keyof TSettings[V]]: Result.Item<
        Extract<V, string>,
        TSettings[V][K]
      >;
    };
  }>;

  export namespace Overload {
    export type Setting = {
      <TRouterProps extends Button.Props = Button.Url.Props>(
        Icon: Button.Icon,
        props?: Omit<Button.Base.Props, 'Icon'>,
        RouterProps?: TRouterProps
      ): SetupButtons.Setting<TRouterProps>;

      <TRouterProps extends Button.Props = Button.Url.Props>(
        Icon: Button.Icon,
        getUrl: Button.Url.Get<TRouterProps>,
        props?: Omit<Button.Base.Props, 'Icon' | 'getUrl'>,
        RouterProps?: TRouterProps
      ): SetupButtons.Setting<TRouterProps>;
    };
  }

  export type Overload = {
    <TSettings extends Settings>(settings: TSettings): Result<TSettings>;

    Setting: Overload.Setting;
  };
}

const Setting: SetupButtons.Overload.Setting = (Icon, ...params: any[]) => {
  const u = JS.is.function(params[0]) ? 1 : 0;
  const getUrl: Button.Base.Props['getUrl'] = u && params[0];
  const props: Omit<Button.Base.Props, 'Icon'> = params[0 + u];

  return { Icon, ...props, ...(getUrl ? { getUrl } : {}) };
};

export const SetupButtons: SetupButtons.Overload = (settings) => {
  const result: Generic = {};

  Object.entries(settings).forEach(([variant, variantSettings]) =>
    Object.entries(variantSettings).forEach(
      ([key, value]) =>
        (result[key] = MakeButton(key, {
          ...value,
          ...(variant ? { variant } : {}),
        }))
    )
  );

  return result;
};

SetupButtons.Setting = Setting;
