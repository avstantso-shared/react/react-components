import { JS } from '@avstantso/node-or-browser-js--utils';

import _Buttons from './configuration';
import { ButtonsGroup } from './group';

import './buttons.scss';

export type { Button } from './types';

export namespace Buttons {
  export namespace Group {
    export type Props = ButtonsGroup.Props;
    export type FC = ButtonsGroup.FC;
  }
}

type InnerButtons = typeof _Buttons;

type SizedButton<
  TSize extends string,
  K extends keyof InnerButtons
> = InnerButtons[K][Extract<TSize, keyof InnerButtons[K]>];

type SizedButtons<TSize extends string> = {
  [K in keyof InnerButtons as [SizedButton<TSize, K>] extends [never]
    ? never
    : K]: SizedButton<TSize, K>;
};

function ButtonsBySize<TSize extends string>(size: TSize): SizedButtons<TSize> {
  return Object.entries(_Buttons).reduce(
    (r, [k, v]) => JS.set.raw(r, k, JS.get.raw(v, size)),
    {} as SizedButtons<TSize>
  );
}

const Small = ButtonsBySize('Small');
const Large = ButtonsBySize('Large');

export const Buttons = {
  Group: ButtonsGroup,
  Small,
  Large,
};
