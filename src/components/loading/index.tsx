import React from 'react';
import { Spinner, SpinnerProps } from 'react-bootstrap';
import { TOptions } from 'i18next';

import { Trans, LOCALES } from '@i18n';

import './index.scss';

export namespace Loading {
  export namespace Base {
    export type Props = Pick<SpinnerProps, 'variant'> & {
      sm?: boolean;
    };
  }

  export namespace Text {
    export type Props<TBase = Base.Props> = TBase &
      // `Partial` for fix CI error: TS2741: Property 'lng' is missing in type '{}' but required in type 'Pick<TOptionsBase, "lng">'
      Partial<Pick<TOptions, 'lng'>>;

    export type FC<TBase = Base.Props> = React.FC<Props<TBase>>;
  }

  export namespace Caption {
    export type Props<TBase = Base.Props> = TBase & {
      loading?: boolean;
      before?: boolean;

      /**
       * @default true
       */
      sm?: boolean;
    };

    export type FC<TBase = Base.Props> = React.FC<Props<TBase>>;
  }

  export type Props<TBase = Base.Props> = TBase;
  export type FC<TBase = Base.Props> = React.FC<Props<TBase>> & {
    Text: Loading.Text.FC<TBase>;
    Caption: Loading.Caption.FC<TBase>;
  };
}

export const Loading: Loading.FC = (props) => {
  const { children, variant = 'primary', sm } = props;

  const spinner = (
    <Spinner
      className="loading"
      animation="border"
      {...{ variant, ...(sm ? { size: 'sm' } : {}) }}
    />
  );

  return !children ? (
    spinner
  ) : (
    <div className="loading-text">
      {spinner}
      <span>{children}</span>
    </div>
  );
};

const LoadingText: Loading.Text.FC = ({ children, lng, ...rest }) => (
  <Loading {...rest}>
    <Trans i18nKey={LOCALES.loading} tOptions={{ lng }} />
    &hellip;{children}
  </Loading>
);

const LoadingCaption: Loading.Caption.FC = ({
  children,
  loading,
  before,
  ...rest
}) => {
  if (!loading) return <>{children}</>;

  const spinner = <Loading {...{ sm: true, ...rest }} />;

  return before ? (
    <>
      {spinner}&nbsp;{children}
    </>
  ) : (
    <>
      {children}&nbsp;{spinner}
    </>
  );
};

Loading.Text = LoadingText;
Loading.Caption = LoadingCaption;
