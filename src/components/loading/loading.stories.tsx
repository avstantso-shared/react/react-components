import React from 'react';
import { Tab, Tabs } from 'react-bootstrap';

import { LocalString } from '@avstantso/node-or-browser-js--model-core';

import { Meta, Story, Bootstrap } from '@story-helpers';

import { Loading } from '.';

const meta = {
  ...Meta(Loading.Text, {
    argTypes: {
      variant: Bootstrap.Variant.argType,
      lng: {
        options: ['-detect-locale-', ...LocalString.Languages],
        control: { type: 'radio' },
      },
    },
  }),
};

export default meta;

export const loading = Story(meta, {
  args: { sm: false },
  render: (args) => <Loading {...args} />,
});

export const loadingCustom = Story(meta, {
  args: { ...loading.args, children: 'LLLLLoding!!!' },
});

export const loadingText = Story({
  args: { ...loading.args, lng: '-detect-locale-' },
  render: ({ lng: argsLng, ...args }) => {
    const lng = argsLng === '-detect-locale-' ? undefined : argsLng;

    return <Loading.Text {...args} {...{ lng }} />;
  },
});

export const loadingCaption = Story({
  args: { sm: true, loading: true, before: false, children: 'Connections' },
  render: (args) => (
    <Tabs id="loading-caption-tabs" activeKey="connections">
      <Tab eventKey="connections" title={<Loading.Caption {...args} />}></Tab>
    </Tabs>
  ),
});
