import React from 'react';

import { Extend, Generics } from '@avstantso/node-or-browser-js--utils';
import { UnexpectedError } from '@avstantso/node-or-browser-js--errors';
import { Filter } from '@avstantso/react--data';

import { FilterPopoverButton as Base } from './base';
import { FilterPopoverButtonStub as Stub } from './stub';
import { FilterPopoverButtonText as Text } from './text';
import { FilterPopoverButtonSet as Set } from './set';
import { FilterPopoverButtonCustom as Custom } from './custom';

import './filter.scss';

export namespace FilterPopoverButton {
  export type ChildrenContext = Base.ChildrenContext;

  export type Props<TFilter extends Filter> = Base.Props<TFilter>;

  export namespace Custom {
    export type Render<
      TValue = any,
      TExtender extends object = {}
    > = Base.Custom.Render<TValue, TExtender>;
  }

  export type FC = Base.FC & {
    Stub: Base.Stub.FC;
    Text: Base.Text.FC;
    Set: Base.Set.FC;
    Custom: typeof Custom;

    ByType: React.FC<Base.Props<Filter> & Base.Stub.Props>;
  };
}

export const FilterPopoverButton: FilterPopoverButton.FC = Extend(Base);

FilterPopoverButton.Text = Text;
FilterPopoverButton.Set = Set;
FilterPopoverButton.Custom = Custom;

FilterPopoverButton.ByType = (props) => {
  const { filter } = props;

  if (!filter) return Stub(props);

  const { type } = filter;

  return Filter.Type.switch<() => JSX.Element>(type, {
    text: () => Text(props as Base.Text.Props),
    set: () => Set(props as Base.Set.Props),
    bool: () => Set(Generics.Cast.To(props)),
    custom: () => Custom(props as Base.Custom.Props),
    default: () => {
      throw new UnexpectedError(`Unexpected FilterType(${type})`);
    },
  })();
};
