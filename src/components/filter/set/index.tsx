import React, { useState } from 'react';
import { Form } from 'react-bootstrap';

import { Trans, LOCALES } from '@i18n';

import { FilterPopoverButton } from '../base';

export const FilterPopoverButtonSet: FilterPopoverButton.Set.FC = (props) => {
  const { filter, ...rest } = props;
  const { id, captions, values, setValues } = filter;

  const [all, setAll] = useState(false);

  return (
    <FilterPopoverButton filter={filter} {...rest}>
      {() => {
        const allChange = (e: React.ChangeEvent<HTMLInputElement>) =>
          setAll((prev) => {
            const newSet = new Set(
              e.target.checked ? captions.map((caption, index) => index) : []
            );
            setValues(newSet);

            return !prev;
          });

        const valueChange =
          (index: number) => (e: React.ChangeEvent<HTMLInputElement>) => {
            const newSet = new Set(values);

            e.target.checked ? newSet.add(index) : newSet.delete(index);

            setValues(newSet);
          };

        return (
          <>
            <Form.Check
              id={`${id}-all`}
              type="switch"
              label={<Trans i18nKey={LOCALES.all} />}
              checked={all}
              onChange={allChange}
            />
            <hr />
            {(captions || []).map((caption, index) => (
              <Form.Check
                key={`${id}-value-${index}`}
                id={`${id}-value-${index}`}
                type="switch"
                label={caption}
                checked={values.has(index)}
                onChange={valueChange(index)}
              />
            ))}
          </>
        );
      }}
    </FilterPopoverButton>
  );
};
