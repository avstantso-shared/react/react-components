import React from 'react';

import { FilterPopoverButton } from '../base';

export function FilterPopoverButtonCustom<
  TValue = any,
  TExtender extends object = {}
>(props: FilterPopoverButton.Custom.Props<TValue, TExtender>) {
  const { render } = props.filter;

  return (
    <FilterPopoverButton {...props}>
      {(context) => render({ ...props, ...context })}
    </FilterPopoverButton>
  );
}
