import React from 'react';
import { Button, Popover, PopoverBody, OverlayTrigger } from 'react-bootstrap';

import { Filter } from '@avstantso/react--data';

import { Trans, LOCALES } from '@i18n';
import { Fa } from '@instances';

const { FaFilter } = Fa;

export namespace FilterPopoverButton {
  export interface ChildrenContext {
    toggle(): void;
  }

  export type Props<TFilter extends Filter> = {
    children?: (context: ChildrenContext) => React.ReactNode;
    filter: TFilter;
  } & Pick<React.HTMLProps<HTMLSpanElement>, 'title'>;

  export type FC = <TFilter extends Filter = Filter>(
    props: Props<TFilter>
  ) => JSX.Element;

  export namespace Stub {
    export type Props = {
      readonly canShowWarnings?: boolean;
    };
    export type FC = React.FC<Props>;
  }

  export namespace Text {
    export type Props = FilterPopoverButton.Props<Filter.Text>;
    export type FC = React.FC<Props>;
  }

  export namespace Set {
    export type Props = FilterPopoverButton.Props<Filter.Set>;
    export type FC = React.FC<Props>;
  }

  export namespace Custom {
    export type Props<
      TValue = any,
      TExtender extends object = {}
    > = FilterPopoverButton.Props<Filter.Custom<any, TValue, TExtender>>;

    export type Render<
      TValue = any,
      TExtender extends object = {}
    > = Filter.Custom.Render<
      Pick<Props<TValue, TExtender>, 'filter'> & ChildrenContext
    >;
  }
}

export const FilterPopoverButton: FilterPopoverButton.FC = (props) => {
  const { filter, title, children } = props;
  const { id, isOpen, setIsOpen, count, active } = filter;

  const toggle = () => {
    setIsOpen((prev) => !prev);
  };

  const popover = (
    <Popover id={`${id}-popover`}>
      <PopoverBody className="filter-popover">
        {children && children({ toggle })}
        <Button
          className="btn-view filter-popover-status-button"
          onClick={toggle}
        >
          &nbsp;&nbsp;
          {active ? (
            <>
              <Trans i18nKey={LOCALES.filter.nRows} />: <b>{count}</b>
            </>
          ) : (
            <Trans i18nKey={LOCALES.filter.empty} />
          )}
          &nbsp;&nbsp;
        </Button>
      </PopoverBody>
    </Popover>
  );

  return (
    <OverlayTrigger
      show={isOpen}
      onToggle={toggle}
      trigger="click"
      placement="bottom"
      rootClose
      overlay={popover}
    >
      <span
        className="filter-button"
        {...{ id, ...(title && !isOpen ? { title } : {}) }}
      >
        <FaFilter />
        {active && <sub>{count}</sub>}
      </span>
    </OverlayTrigger>
  );
};
