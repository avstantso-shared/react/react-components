import React from 'react';
import { Form, FormControl, InputGroup, FormGroup } from 'react-bootstrap';

import { Filter } from '@avstantso/react--data';

import { FamousSiteQuery as FSQ } from '@components/famous-site-query';

import { Trans, useTranslation, LOCALES } from '@i18n';
import { Fa } from '@instances';

import { FilterPopoverButton } from '../base';

const { FaBan } = Fa;

export const FilterModeCaptions = [
  <Trans i18nKey={LOCALES.filter.simple} />,
  <Trans i18nKey={LOCALES.filter.extended} />,
];

export const FilterPopoverButtonText: FilterPopoverButton.Text.FC = (props) => {
  const { filter, title, ...rest } = props;
  const { id, isOpen, value, mode, error, clear, setValue, setMode } = filter;
  const [t] = useTranslation();

  const valueChange = (e: React.ChangeEvent<HTMLInputElement>) =>
    setValue(e.target.value);

  return (
    <FilterPopoverButton
      filter={filter}
      title={undefined !== title ? title : isOpen ? '' : value}
      {...rest}
    >
      {({ toggle }) => {
        const cancel = () => {
          clear();
          toggle && toggle();
        };

        const makeModeRB = (aMode: Filter.Text.Mode) => {
          const rbId = `${id}-radio-mode-${aMode}`;

          const modeChange = (e: React.ChangeEvent) => {
            setMode(aMode);
          };

          return (
            <FormGroup>
              <Form.Check
                id={rbId}
                type="radio"
                name={rbId}
                label={
                  <>
                    {FilterModeCaptions[+aMode - 1]}&nbsp;
                    {Filter.Text.Mode.regExp === aMode && (
                      <FSQ.Google q={t(LOCALES.filter.regExGQ)}>?</FSQ.Google>
                    )}
                  </>
                }
                checked={mode === aMode}
                onChange={modeChange}
              />
            </FormGroup>
          );
        };

        return (
          <>
            <InputGroup className="mb-3">
              <FormControl
                id={`${id}-value`}
                value={value}
                onChange={valueChange}
                aria-describedby={`${id}-addon`}
                isInvalid={!!error}
              />
              <InputGroup.Text id={`${id}-addon`} onClick={cancel}>
                <FaBan />
              </InputGroup.Text>
            </InputGroup>
            <InputGroup className="mb-3">
              {makeModeRB(Filter.Text.Mode.simple)}
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              {makeModeRB(Filter.Text.Mode.regExp)}
            </InputGroup>
          </>
        );
      }}
    </FilterPopoverButton>
  );
};
