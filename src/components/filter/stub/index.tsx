import React from 'react';

import { Fa } from '@instances';

import { FilterPopoverButton } from '../base';

const { FaFilter } = Fa;

export const FilterPopoverButtonStub: FilterPopoverButton.Stub.FC = (props) => {
  const { canShowWarnings } = props;

  return (
    <span className="filter-button">
      <FaFilter
        title="Filter not found"
        color={canShowWarnings ? 'red' : 'gray'}
      />
    </span>
  );
};
