import React from 'react';
import { Meta, Story } from '@story-helpers';

import { ColorHashBadge } from '.';

const meta = {
  ...Meta(ColorHashBadge, {
    argTypes: {
      children: {
        control: 'text',
      },
    },
  }),
};

export default meta;

const args = {
  children: 'Hello world!',
};

export const simple = Story(meta, {
  args,
  render: (args) => {
    const { children = '', ...rest } = args;

    return <ColorHashBadge {...rest}>{children}</ColorHashBadge>;
  },
});

export const details = Story<typeof ColorHashBadge.Details>({
  args,
  render: (args) => {
    const { children = '', ...rest } = args;

    return (
      <ColorHashBadge.Details {...rest}>{children}</ColorHashBadge.Details>
    );
  },
});
