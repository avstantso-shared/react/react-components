import React from 'react';

import { JS, str2Color } from '@avstantso/node-or-browser-js--utils';
import { InvalidArgumentError } from '@avstantso/node-or-browser-js--errors';

import { Combiner } from '@avstantso/react--data';

import './color-hash-badge.scss';

export namespace ColorHashBadge {
  export type Props = {};

  export type FC = React.FC<Props & React.HTMLProps<HTMLDivElement>> & {
    Details: React.FC<Props & React.HTMLProps<HTMLDetailsElement>>;
  };
}

export const ColorHashBadge: ColorHashBadge.FC = (props) => {
  const { children, className, style, ...rest } = props;

  if (!JS.is.string(children))
    throw new InvalidArgumentError(
      '<ColorHashBadge> children must be a "string"'
    );

  const color = str2Color(children);

  return (
    <div
      {...Combiner.className('color-hash-badge', className)}
      style={{ ...style, backgroundColor: color }}
      {...rest}
    >
      {color}
    </div>
  );
};

ColorHashBadge.Details = (props) => {
  const { children, ...rest } = props;

  return (
    <details {...rest}>
      <summary>
        <ColorHashBadge>{children}</ColorHashBadge>
      </summary>
      {children}
    </details>
  );
};
