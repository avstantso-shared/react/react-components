import React from 'react';

import { Badge, BadgeProps } from 'react-bootstrap';

import { JS, Perform } from '@avstantso/node-or-browser-js--utils';
import { Model } from '@avstantso/node-or-browser-js--model-core';
import { Gender } from '@avstantso/react--data';

import { Loading } from '@components/loading';
import { SystemSign } from '@components/systemSign';

export namespace EntityBadge {
  export type Props<T extends object | string = object | string> = Pick<
    BadgeProps,
    'id' | 'className' | 'bg'
  > & {
    entity: T;
    name?: Perform<string | JSX.Element>;
    systemOverride?: boolean;
    systemGender?: Gender;
    children?: (
      name: string | JSX.Element,
      system: string | JSX.Element
    ) => JSX.Element;
  };
}

export function EntityBadge<T extends object | string = any>(
  props: EntityBadge.Props<T>
) {
  const {
    entity,
    bg = 'secondary',
    systemOverride,
    systemGender,
    children,
    ...rest
  } = props;

  const Loaded: React.FC = () => {
    const isString = JS.is.string(entity);

    const name = props.name
      ? Perform(props.name)
      : isString
      ? entity
      : Model.Named.Force(entity).name;

    const system =
      undefined !== systemOverride
        ? systemOverride
        : !isString && Model.Systemic.Force(entity).system;

    const sys = system && (
      <>
        &nbsp;
        <SystemSign system gender={systemGender} />
      </>
    );

    return children ? (
      children(name, sys)
    ) : (
      <>
        {name}
        {sys}
      </>
    );
  };

  return (
    <Badge {...{ bg, ...rest }}>
      {!entity ? <Loading.Text /> : <Loaded />}
    </Badge>
  );
}
