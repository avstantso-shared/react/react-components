import React from 'react';
import { Meta, Story } from '@story-helpers';

import { EntityBadge } from '.';

const Entity = { name: 'USER', href: '123' };

const meta = {
  ...Meta(EntityBadge<typeof Entity | string>),
};

export default meta;

export const fromString = Story(meta, {
  args: {
    entity: 'USER',
  },
});

export const fromEntity = Story(meta, {
  args: {
    entity: Entity,
  },
});
