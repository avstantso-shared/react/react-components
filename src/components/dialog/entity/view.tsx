import React from 'react';
import { Generics, JS } from '@avstantso/node-or-browser-js--utils';
import { Model } from '@avstantso/node-or-browser-js--model-core';

import { Button as CmpButton, Buttons } from '@components/buttons';
import { MDFDebugger } from '@components/mdfDebugger';

import type { Dialog as DialogTypes } from '../types';
import Dialog from '../component';

import type { Controller } from './types';
import { EntityDialogContext } from './context';

export namespace EntityDialog {
  export interface Button {
    button?: CmpButton.Custom.FC;
    readonly disabled?: boolean;
    props?: CmpButton.Props;
  }

  export namespace Props {
    export interface Base extends DialogTypes.Props.Base {
      canApply?: boolean;
      buttons?: {
        accept?: EntityDialog.Button;
        apply?: EntityDialog.Button;
        cancel?: EntityDialog.Button;
      };
      version?: boolean;

      /**
       * @default false
       */
      autoFocus?: boolean;
    }
  }

  export interface Props<
    TFirst extends Controller.TypeMap.First,
    TSecond extends Controller.TypeMap.Second<TFirst> = Controller.TypeMap.Second.Def<TFirst>
  > extends EntityDialog.Props.Base {
    controller: Controller<TFirst, TSecond>;
    children:
      | React.ReactNode
      | ((controller: Controller<TFirst, TSecond>) => React.ReactNode);
  }
}

export function EntityDialog<
  TFirst extends Controller.TypeMap.First,
  TSecond extends Controller.TypeMap.Second<TFirst> = Controller.TypeMap.Second.Def<TFirst>
>(props: EntityDialog.Props<TFirst, TSecond>) {
  const {
    controller,
    children,
    canApply,
    buttons: overrideButtons,
    version,
    autoFocus,
    ...rest
  } = props;
  const { entity, mdf, debug, readOnly, submit, cancel } = controller;

  const buttons = [
    {
      df: Buttons.Large.Accept,
      ov: overrideButtons?.accept,
      oc: () => submit(true),
      cn: !readOnly,
      ds: !mdf.modified,
    },
    {
      df: Buttons.Large.Apply,
      ov: overrideButtons?.apply,
      oc: () => submit(false),
      cn: !readOnly && canApply,
      ds: !mdf.modified,
    },
    {
      df: Buttons.Large.Cancel,
      ov: overrideButtons?.cancel,
      oc: cancel,
      cn: true,
    },
  ]
    .filter((item) => item.cn)
    .filter(
      ({ ov }) => !ov || ov.button || ov.props || !JS.is.undefined(ov.disabled)
    )
    .map(({ df, ov, oc, ds }) => ({
      button: ov?.button || df,
      onClick: oc,
      disabled: ds || ov?.disabled,
      props: ov?.props,
    }));

  return (
    <EntityDialogContext.Provider value={{ controller }}>
      <Dialog
        cancel={cancel}
        submit={() => submit(true)}
        buttons={buttons}
        defaultButton={-1}
        autoFocus={undefined === autoFocus ? false : autoFocus}
        {...rest}
      >
        {version && (
          <p>Version: {Model.Versioned.Is(entity) && entity.version}</p>
        )}
        {JS.is.function(children)
          ? Generics.Cast(children)(controller)
          : children}
        {debug && <MDFDebugger mdf={mdf} />}
      </Dialog>
    </EntityDialogContext.Provider>
  );
}
