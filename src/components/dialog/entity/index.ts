import { Extend, Perform } from '@avstantso/node-or-browser-js--utils';

import * as Types from './types';
import { EntityDialog as View } from './view';
import { EntityDialogSimple as ViewSimple } from './view-simple';
import { EntityDialogRedirectLink as RedirectLink } from './redirectLink';
import {
  EntityDialogContext as Context,
  useEntityDialogContext as useContext,
} from './context';
import { useEntityDialogController as useController } from './controller';

export {
  Types,
  View,
  ViewSimple,
  RedirectLink,
  Context,
  useContext,
  useController,
};

type First = Types.Controller.TypeMap.First;
type Second<
  TFirst extends First,
  T extends {} = {}
> = Types.Controller.TypeMap.Second<TFirst, T>;

export const Exports: {
  <
    TFirst extends First,
    TSecond extends Second<TFirst> = Second<TFirst, TFirst>
  >(
    props: View.Props<TFirst, TSecond>
  ): JSX.Element;
  useController: typeof useController;
  Context: typeof Context;
  useContext: typeof useContext;
  Simple: <
    TFirst extends First,
    TSecond extends Second<TFirst> = Second<TFirst, TFirst>
  >(
    props: ViewSimple.Props<TFirst, TSecond>
  ) => JSX.Element;
  RedirectLink: React.FC<RedirectLink.Props>;
} = Extend(View);
Exports.useController = useController;
Exports.Context = Context;
Exports.useContext = useContext;
Exports.Simple = ViewSimple;
Exports.RedirectLink = RedirectLink;
