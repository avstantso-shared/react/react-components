import React from 'react';
import Bluebird from 'bluebird';

import { useLocker, useModifications } from '@avstantso/react--data';

import type { Controller } from '../types';

export const useMdfLockerActions = <
  TFirst extends Controller.TypeMap.First,
  TSecond extends Controller.TypeMap.Second<TFirst> = Controller.TypeMap.Second.Def<TFirst>
>(
  props: Controller.Props<TFirst, TSecond>
): void => {
  const { current, initial, options: { debug, mdfOptions } = {} } = props;

  if (initial) {
    current.isAcceptSubmit = true;

    function doHandler<TFunc extends (...args: any) => any>(
      func: TFunc,
      ...params: Parameters<TFunc>
    ) {
      const {
        locker: { locked },
        mdf: { confirm },
      } = current;

      return !locked && Bluebird.resolve(confirm(func, ...params));
    }

    current.cancel = () => doHandler(current.handler.cancel);
    // !ref.current.locker.locked &&
    // Bluebird.resolve(
    //   ref.current.mdf.confirm(ref.current.handler.cancel)
    // );

    current.redirect = (url) => doHandler(current.handler.redirect, url);
    // !ref.current.locker.locked &&
    // Bluebird.resolve(ref.current.mdf.confirm(ref.current.handler.redirect, url));

    current.submit = (accept) => {
      const { formik } = current;

      if (formik.isSubmitting) return Bluebird.resolve();

      current.isAcceptSubmit = accept;

      return Bluebird.resolve().then(async () => {
        if (process.env.DEBUG_INFO_ENABLED) {
          const vse = await formik.validateForm();
          if (Object.values(vse).length)
            console.warn(`Validation schema errors: %O`, vse);
        }

        return formik.submitForm();
      });
    };
  }

  current.locker = useLocker();
  current.mdf = useModifications<any>(current.entity, {
    debug,
    ...(mdfOptions || {}),
  });
};
