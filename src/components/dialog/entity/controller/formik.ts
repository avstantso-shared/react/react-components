import React, { useEffect } from 'react';
import Bluebird from 'bluebird';
import * as Formik from 'formik';

import { Perform, Generics, JS } from '@avstantso/node-or-browser-js--utils';

import type { Controller } from '../types';

export const useFormik = <
  TFirst extends Controller.TypeMap.First,
  TSecond extends Controller.TypeMap.Second<TFirst> = Controller.TypeMap.Second.Def<TFirst>
>(
  props: Controller.Props<TFirst, TSecond>
): void => {
  const {
    current,
    initial,
    options: {
      canSubmit,
      doSubmit,
      catchSubmit,
      debug,
      validationSchema,
      validate,
      storage,
    } = {},
  } = props;

  if (initial) {
    current.fld = (name, options?) => {
      const { formik, options: { readOnly: readOnlyOpt } = {} } = current;
      const readOnly = !!Perform(readOnlyOpt);

      const value = JS.get.raw(formik.values, name);
      const touched = JS.get.raw(formik.touched, name);
      const errors = JS.get.raw(formik.errors, name);

      const isValid = !readOnly && touched && !errors;
      const isInvalid = !readOnly && touched && !!errors;

      const onBlur = formik.handleBlur;

      const onChange = !(options?.beforeOnChange || options?.trim)
        ? formik.handleChange
        : (e: React.ChangeEvent<any>) => {
            if (options?.trim) e.target.value = e.target.value.trim();
            options?.beforeOnChange && options.beforeOnChange(e);
            formik.handleChange(e);
          };

      return {
        name,
        value,
        onBlur,
        onChange,
        isValid,
        isInvalid,
        readOnly,
        ...(options?.checked ? { checked: !!value } : {}),
      };
    };

    current.err = (name) => JS.get.raw(current.formik.errors, name);
  }

  current.formik = Formik.useFormik<Controller.Entity<TFirst>>({
    enableReinitialize: true,
    validationSchema,
    validate,
    onSubmit: async (values, { setSubmitting }) => {
      const {
        handler,
        entity,
        setEntityRaw,
        isAcceptSubmit,
        mdf: { modified, setModified },
        options: { readOnly: readOnlyOpt, converter } = {},
      } = current;

      if (!!Perform(readOnlyOpt)) {
        console.warn(`Readonly EntityDialogController submitting attempt`);
        return;
      }

      if (!modified) return;

      const newEntity = converter.from({ ...entity, ...values });
      if (canSubmit && !(await Bluebird.resolve(canSubmit(newEntity)))) return;

      return (
        Bluebird.resolve(setSubmitting(true))
          .then(() => (doSubmit ? doSubmit(newEntity) : newEntity))
          .then((submittedEntity) => {
            return Bluebird.resolve(
              isAcceptSubmit
                ? // A.V.Stantso: after call "handler.accept" component will unmount.
                  // Call <setReactState> attempts will generate warnings
                  handler.accept(submittedEntity)
                : handler
                    .apply(submittedEntity)
                    .then(() => setEntityRaw(converter.to(submittedEntity)))
                    .then(() => setModified(false))
            );
          })
          .catch(async (e: Error) => {
            if (!catchSubmit) throw e;

            await Bluebird.resolve(catchSubmit(e, newEntity));
          })
          // A.V.Stantso: "!isAcceptSubmit", see "handler.accept" comment
          .finally(() => !isAcceptSubmit && setSubmitting(false))
      );
    },
    initialValues: Generics.Cast.To(current.entity),
  });

  useEffect(() => {
    const { mdf, formik } = current;

    mdf.update(formik.values);
  }, [current.formik.values]);
};
