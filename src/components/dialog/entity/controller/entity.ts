import React, { useState, useEffect } from 'react';

import { Perform, conveyor } from '@avstantso/node-or-browser-js--utils';

import type { Controller } from '../types';

import { checkVersionDecrease } from './check-version-decrease';

export const useEntity = <
  TFirst extends Controller.TypeMap.First,
  TSecond extends Controller.TypeMap.Second<TFirst> = Controller.TypeMap.Second.Def<TFirst>
>(
  props: Controller.Props<TFirst, TSecond>
): void => {
  const { current, initial } = props;

  const [entity, setEntity] = useState<Controller.Entity<TFirst>>();

  if (initial) {
    current.clacEntity = () => {
      const {
        originalEntity,
        options: { defaults, converter, overrides } = {},
      } = current;

      return conveyor<Controller.Entity<TFirst>>(
        { ...defaults, ...Perform(originalEntity) },
        overrides,
        converter?.to
      );
    };

    current.setEntityRaw = (param) =>
      setEntity((prev) => {
        const next = Perform(param, prev);

        checkVersionDecrease(prev, next);

        return next;
      });
  }

  current.entity = entity || current.clacEntity();

  useEffect(() => {
    const { clacEntity, setEntityRaw } = current;

    setEntityRaw(clacEntity);
  }, [current.originalEntity]);
};
