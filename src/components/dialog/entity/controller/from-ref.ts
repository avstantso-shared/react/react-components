import { Perform } from '@avstantso/node-or-browser-js--utils';

import type { Controller } from '../types';

export function fromRef<
  TFirst extends Controller.TypeMap.First,
  TSecond extends Controller.TypeMap.Second<TFirst> = Controller.TypeMap.Second.Def<TFirst>
>(current: Controller.Ref<TFirst, TSecond>): Controller<TFirst, TSecond> {
  const {
    // -local fields
    handler,
    originalEntity,
    options,
    isAcceptSubmit,

    ...rest
  } = current;

  return {
    ...rest,

    get debug() {
      return !!current.options?.debug;
    },
    get readOnly() {
      return !!Perform(current.options?.readOnly);
    },

    get entity() {
      return current.entity;
    },
    get converter() {
      return current.options?.converter;
    },
    get mdf() {
      return current.mdf;
    },
    get formik() {
      return current.formik;
    },
    get locker() {
      return current.locker;
    },
    get localData() {
      return current.localData;
    },
  };
}
