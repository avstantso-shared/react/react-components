import React, { useRef } from 'react';

import { Generics, X } from '@avstantso/node-or-browser-js--utils';
import { Dialog, EntityNulls, useInitRef } from '@avstantso/react--data';

import type { Controller } from '../types';

import { useEntity } from './entity';
import { useMdfLockerActions } from './mdf-locker-actions';
import { useFormik } from './formik';
import { useLocalData } from './local-data';
import { fromRef } from './from-ref';

export const useEntityDialogController = <
  TFirst extends Controller.TypeMap.First,
  TSecond extends Controller.TypeMap.Second<TFirst> = Controller.TypeMap.Second.Def<TFirst>
>(
  dialogProps: Dialog.Entity.Props<Controller.Entity<TFirst>>,
  options?: Controller.Options<TFirst, TSecond>
): Controller<TFirst, TSecond> => {
  const [ref, initial] = useInitRef<Controller.Ref<TFirst, TSecond>>();

  const iternalProps: Controller.Props<TFirst, TSecond> = {
    ...ref,
    initial,
    options,
  };

  ref.current.originalEntity = dialogProps.entity;
  ref.current.handler = dialogProps.handler;

  {
    const {
      defaults,
      converter = EntityNulls<Controller.Entity<TFirst>>(),
      overrides,
      readOnly,
      debug,
    } = options;
    ref.current.options = { defaults, converter, overrides, readOnly, debug };
  }

  useEntity(iternalProps);
  useMdfLockerActions(iternalProps);
  useFormik(iternalProps);
  useLocalData(iternalProps);

  return fromRef(ref.current);
};

function Empty<
  TFirst extends Controller.TypeMap.First,
  TSecond extends Controller.TypeMap.Second<TFirst> = Controller.TypeMap.Second.Def<TFirst>
>(): Controller<TFirst, TSecond> {
  return {
    entity: undefined,
    setEntityRaw: X,
    clacEntity: X,
    converter: undefined,
    mdf: undefined,
    debug: undefined,
    formik: undefined,
    fld: X,
    err: X,
    cancel: X,
    redirect: X,
    submit: X,
    locker: undefined,
    readOnly: undefined,
    localData: undefined,
  };
}

useEntityDialogController.Empty = Empty;
