import { Model } from '@avstantso/node-or-browser-js--model-core';

/**
 * @summary Check for `Model` objects to detect version decrease
 * @param prev Prev state
 * @param next Next state
 */
export function checkVersionDecrease<T extends {}>(
  prev: T,
  next: T,
  label?: string
) {
  if (
    Model.IDed.Is(prev) &&
    Model.IDed.Is(next) &&
    prev.id === next.id &&
    (Model.Versioned.Is(prev) || Model.Versioned.Is(next))
  ) {
    const pv = Model.Versioned.Force(prev).version || 0;
    const nv = Model.Versioned.Force(next).version || 0;

    if (nv < pv) {
      console.warn(
        [
          `Entity.Dialog.Controller: Version decrease detected from ${pv} to ${nv} for id = "${prev.id}" `,
          `prev: %O `,
          `next: %O`,
          ...(label ? [`label: ${label}`] : []),
        ].join('\r\n'),
        prev,
        next
      );
    }
  }
}
