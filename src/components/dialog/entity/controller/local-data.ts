import React from 'react';

import { Perform } from '@avstantso/node-or-browser-js--utils';

import { EntityLocalStorage } from '@root/components/entity-local-storage';

import type { Controller } from '../types';

export const useLocalData = <
  TFirst extends Controller.TypeMap.First,
  TSecond extends Controller.TypeMap.Second<TFirst> = Controller.TypeMap.Second.Def<TFirst>
>(
  props: Controller.Props<TFirst, TSecond>
): void => {
  type ELS = Controller.TypeMap.ToELS<TFirst, TSecond>;

  const { current, initial, options: { storage } = {} } = props;

  const { formik, clacEntity } = current;

  current.localData = EntityLocalStorage.SyncFormik.use<ELS>({
    disabled: !storage || storage.disabled,
    storageKey:
      storage?.storageKey &&
      (() => Perform(storage.storageKey, current.entity)),
    schema: storage?.schema,
    formik,
    clacEntity,
  });
};
