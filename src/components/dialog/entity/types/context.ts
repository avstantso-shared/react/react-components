import { Controller } from './controller';

export interface Context<T = any> {
  controller: Controller<T>;
}
