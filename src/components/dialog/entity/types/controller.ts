import type { Dispatch, SetStateAction } from 'react';
import type { FormikProps, FormikHandlers, FormikConfig } from 'formik';

import type { Perform } from '@avstantso/node-or-browser-js--utils';
import type {
  Dialog,
  EntityFormConverter,
  Locker,
  Modifications,
} from '@avstantso/react--data';

import type { EntityLocalStorage } from '@root/components/entity-local-storage';

export namespace Controller {
  export type TypeMap<TEntity extends {} = {}, TDefaults extends {} = {}> = {
    Entity: TEntity;
    Defaults?: TDefaults;
  };

  export namespace TypeMap {
    /**
     * @summary First generic `Controller` typemapped argument.
     *
     * `TypeMap` or `Entity` object type
     */
    export type First = TypeMap | {};

    /**
     * @summary Second generic `Controller` typemapped argument, depended from `TFirst`.
     *
     * `never`, if `TFirst` is `TypeMap`
     *
     * or `Defaults` object type, if `TFirst` is `Entity` object type
     * @param TFirst First `Controller` typemapped argument
     * @param TConstraint Optional constraint if result not `never`
     */
    export type Second<
      TFirst extends First,
      TConstraint extends {} = {}
    > = TFirst extends TypeMap ? never : TConstraint;

    export namespace Second {
      /**
       * @summary Second generic `Controller` typemapped argument, depended from `TFirst`.
       *
       * Use `TFirst` as constraint in `Second`.
       *
       * `never`, if `TFirst` is `TypeMap`
       *
       * or `Defaults` object type, if `TFirst` is `Entity` object type
       * @param TFirst First `Controller` typemapped argument
       * @see Second
       */
      export type Def<TFirst extends First> = Second<TFirst, TFirst>;
    }

    /**
     * `TypeMap` from generic `Controller` typemapped arguments
     */
    export type From<
      TFirst extends TypeMap.First,
      TSecond extends TypeMap.Second<TFirst> = TypeMap.Second.Def<TFirst>,
      R extends TFirst extends TypeMap
        ? TFirst
        : TypeMap<TFirst, TSecond> = TFirst extends TypeMap
        ? TFirst
        : TypeMap<TFirst, TSecond>
    > = R;

    /**
     * Convert to `EntityLocalStorage.SyncFormik.TypeMap`
     */
    export type ToELS<
      TFirst extends TypeMap.First,
      TSecond extends TypeMap.Second<TFirst> = TypeMap.Second.Def<TFirst>,
      TELSTypeMap extends EntityLocalStorage.SyncFormik.TypeMap<
        Entity<TFirst>,
        Defaults<TFirst, TSecond>
      > = EntityLocalStorage.SyncFormik.TypeMap<
        Entity<TFirst>,
        Defaults<TFirst, TSecond>
      >
    > = TELSTypeMap;
  }

  /**
   * @summary Extract `Entity` from first generic `Controller` typemapped argument
   * @param TFirst First `Controller` typemapped argument
   */
  export type Entity<TFirst extends TypeMap.First> = TFirst extends TypeMap
    ? TFirst['Entity']
    : TFirst;

  /**
   * @summary Extract `Defaults` from first and second generic `Controller` typemapped arguments
   * @param TFirst First `Controller` typemapped argument
   * @param TSecond Second `Controller` typemapped argument
   */
  export type Defaults<
    TFirst extends TypeMap.First,
    TSecond extends TypeMap.Second<TFirst> = TypeMap.Second.Def<TFirst>
  > = TFirst extends TypeMap ? TFirst['Defaults'] : TSecond;

  export namespace Options {
    export type RecalcStaic<
      TFirst extends TypeMap.First,
      TSecond extends TypeMap.Second<TFirst> = TypeMap.Second.Def<TFirst>
    > = {
      converter?: EntityFormConverter<Entity<TFirst>>;
      defaults?: Perform<Defaults<TFirst, TSecond>>;
      overrides?(entity: Entity<TFirst>): Entity<TFirst>;
    };

    export type Submit<TFirst extends TypeMap.First> = {
      canSubmit?(entity: Entity<TFirst>): boolean | Promise<boolean>;
      doSubmit?(
        entity: Entity<TFirst>
      ): Entity<TFirst> | Promise<Entity<TFirst>>;
      catchSubmit?(error: Error, entity: Entity<TFirst>): void | Promise<void>;
    };

    export type Storage<
      TFirst extends TypeMap.First,
      TSecond extends TypeMap.Second<TFirst> = TypeMap.Second.Def<TFirst>
    > = Pick<
      EntityLocalStorage.SyncFormik.Props<TypeMap.ToELS<TFirst, TSecond>>,
      'schema' | 'disabled'
    > & {
      /**
       * @summary Key for read/write data from storage
       */
      storageKey: Perform<EntityLocalStorage.Key, [Entity<TFirst>]>;
    };
  }

  export type Options<
    TFirst extends TypeMap.First,
    TSecond extends TypeMap.Second<TFirst> = TypeMap.Second.Def<TFirst>
  > = Options.RecalcStaic<TFirst, TSecond> &
    Options.Submit<TFirst> &
    Pick<FormikConfig<Entity<TFirst>>, 'validationSchema' | 'validate'> & {
      debug?: boolean;
      mdfOptions?: Modifications.Behavior.Options<Entity<TFirst>>;
      readOnly?: Perform<boolean>;

      /**
       * @summary Unsaved data storing options.
       * @borrows `EntityLocalStorage.Context.Provider` implementation required
       * @see `EntityLocalStorage`
       */
      storage?: Options.Storage<TFirst, TSecond>;
    };

  export namespace FormikField {
    export type Options = {
      checked?: boolean;
      trim?: boolean;
      beforeOnChange?: FormikHandlers['handleChange'];
    };

    export type Value = string | number | string[];

    export type Get = <TValue extends FormikField.Value = FormikField.Value>(
      name: string,
      options?: Options
    ) => FormikField<TValue>;
  }

  export type FormikField<
    TValue extends FormikField.Value = FormikField.Value
  > = {
    name: string;
    value: TValue;
    checked?: boolean;
    onChange: FormikHandlers['handleChange'];
    onBlur: FormikHandlers['handleBlur'];
    isValid: boolean;
    isInvalid: boolean;
    readOnly: boolean;
  };

  export type Actions = {
    cancel(): Promise<unknown>;
    redirect(url: string): Promise<unknown>;
    submit(accept: boolean): Promise<unknown>;
  };

  /**
   * @summary Raw controller for edit entity in dialog
   */
  export type Raw<
    TFirst extends TypeMap.First,
    TSecond extends TypeMap.Second<TFirst> = TypeMap.Second.Def<TFirst>
  > = Actions & {
    /**
     * @summary The state of the entity is taken from the original, and changes when saved
     */
    entity: Readonly<Entity<TFirst>>;

    /**
     * @summary Extetnal direct change state of the entity
     */
    setEntityRaw: Dispatch<SetStateAction<Entity<TFirst>>>;

    /**
     * @summary Merge defaults, original entity, overrides and use converter
     */
    clacEntity(): Entity<TFirst>;

    converter: Readonly<EntityFormConverter<Entity<TFirst>>>;
    mdf: Readonly<Modifications>;
    debug: boolean;
    formik: FormikProps<Entity<TFirst>>;
    fld: FormikField.Get;
    err: (name: string) => string;
    locker: Locker;
    readOnly: boolean;
    localData: EntityLocalStorage.SyncFormik<TypeMap.ToELS<TFirst, TSecond>>;
  };

  /**
   * @summary Entity dialog `Controller` keys
   */
  export type Key = keyof {
    [K in keyof Controller<any> as `${Extract<K, string>}`]: 0;
  };

  export namespace Key {
    /**
     * @summary Entity dialog `Controller` fields keys
     */
    export type Fields = Exclude<Key, Methods>;

    /**
     * @summary Entity dialog `Controller` methods keys
     */
    export type Methods = Extract<
      keyof Actions | 'clacEntity' | 'setEntityRaw' | 'fld' | 'err',
      Key
    >;
  }

  export type Ref<
    TFirst extends TypeMap.First,
    TSecond extends TypeMap.Second<TFirst> = TypeMap.Second.Def<TFirst>,
    TController extends Raw<TFirst, TSecond> = Raw<TFirst, TSecond>
  > = Pick<Dialog.Entity.Props<Entity<TFirst>>, 'handler'> & {
    originalEntity: Dialog.Entity.Props<Entity<TFirst>>['entity'];
    options?: Pick<
      Options<TFirst, TSecond>,
      'debug' | 'defaults' | 'overrides' | 'converter' | 'readOnly'
    >;
  } & {
    isAcceptSubmit?: boolean;
  } & Partial<Pick<TController, Key.Fields>> &
    Required<Pick<TController, Key.Methods>>;

  export type Props<
    TFirst extends TypeMap.First,
    TSecond extends TypeMap.Second<TFirst> = TypeMap.Second.Def<TFirst>
  > = {
    current: Ref<TFirst, TSecond>;
    initial: boolean;
    options?: Options<TFirst, TSecond>;
  };
}

/**
 * @summary Controller for edit entity in dialog
 */
export type Controller<
  TFirst extends Controller.TypeMap.First,
  TSecond extends Controller.TypeMap.Second<TFirst> = Controller.TypeMap.Second.Def<TFirst>
> = Readonly<Controller.Raw<TFirst, TSecond>>;
