import React, { useRef } from 'react';

import { Perform } from '@avstantso/node-or-browser-js--utils';
import type { ChildrenProps } from '@avstantso/react--data';

import { useEntityDialogContext } from './context';

const REDIRECT_IDLE = 200;

export namespace EntityDialogRedirectLink {
  export interface Props extends ChildrenProps.Perform {
    to: string;
    /**
     * @default REDIRECT_IDLE
     */
    idle?: number;
  }

  export type FC = React.FC<Props>;
}

export const EntityDialogRedirectLink: EntityDialogRedirectLink.FC = (
  props
) => {
  const { to, children } = props;
  const idle = props.idle || REDIRECT_IDLE;

  const { controller } = useEntityDialogContext();
  const { redirect } = controller;
  const timeout = useRef<NodeJS.Timeout>();

  const clickHandler =
    (isLeft: boolean) =>
    (e: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
      isLeft && e.preventDefault();

      if (timeout.current) return;

      timeout.current = setTimeout(() => {
        isLeft && redirect(to);
        timeout.current = undefined;
      }, idle);
    };

  return (
    <a
      href={to}
      onClick={clickHandler(true)}
      onContextMenu={clickHandler(false)}
    >
      {Perform(children)}
    </a>
  );
};
