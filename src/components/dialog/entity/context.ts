import React from 'react';
import type * as Types from './types';

export const EntityDialogContext = React.createContext<Types.Context>({
  controller: null,
});

export const useEntityDialogContext = <T = any>(): Types.Context<T> =>
  React.useContext(EntityDialogContext);
