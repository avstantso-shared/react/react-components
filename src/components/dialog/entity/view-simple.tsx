import React from 'react';

import type { Dialog } from '@avstantso/react--data';

import type { Controller } from './types';
import { useEntityDialogController } from './controller';
import { EntityDialog } from './view';

export namespace EntityDialogSimple {
  export interface Props<
    TFirst extends Controller.TypeMap.First,
    TSecond extends Controller.TypeMap.Second<TFirst> = Controller.TypeMap.Second.Def<TFirst>
  > extends EntityDialog.Props.Base,
      Dialog.Entity.Props<Controller.Entity<TFirst>> {
    children: (controller: Controller<TFirst, TSecond>) => React.ReactNode;
    debug?: boolean;
  }
}

export function EntityDialogSimple<
  TFirst extends Controller.TypeMap.First,
  TSecond extends Controller.TypeMap.Second<TFirst> = Controller.TypeMap.Second.Def<TFirst>
>(props: EntityDialogSimple.Props<TFirst, TSecond>) {
  const { entity, handler, ...rest } = props;

  const controller = useEntityDialogController<TFirst, TSecond>(
    { entity, handler },
    { debug: props.debug }
  );

  return <EntityDialog<TFirst, TSecond> {...{ controller }} {...rest} />;
}
