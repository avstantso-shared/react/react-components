import React, { useState, useRef, useLayoutEffect } from 'react';

import { JS } from '@avstantso/node-or-browser-js--utils';

import { Dialog } from '../types';

const DialogButtons: Dialog.Buttons.FC = (props) => {
  const { buttons, defaultButton, timerDocBodyMouseDown } = props;

  const [currentButton, setCurrentButton] = useState(
    defaultButton || buttons.length - 1
  );

  const buttonsSpanRef = useRef<HTMLSpanElement>(null);

  const buttonKeyDown =
    (index: number): React.KeyboardEventHandler<HTMLSpanElement> =>
    (e) => {
      if ('ArrowLeft' === e.code) {
        setCurrentButton(index > 0 ? index - 1 : 0);

        e.stopPropagation();
        e.nativeEvent?.stopImmediatePropagation();
        return;
      }

      if ('ArrowRight' === e.code) {
        setCurrentButton(
          index < buttons.length - 1 ? index + 1 : buttons.length - 1
        );

        e.stopPropagation();
        e.nativeEvent?.stopImmediatePropagation();
        return;
      }
    };

  const buttonClick = (onClick: (e?: any) => any) => (e: any) => {
    clearTimeout(timerDocBodyMouseDown.current);
    onClick(e);
  };

  useLayoutEffect(() => {
    if (currentButton < 0) return;

    if (currentButton >= buttonsSpanRef.current.children.length) {
      process.env.DEBUG_INFO_ENABLED &&
        console.warn(
          `Invalid currentButton index ${currentButton} in collection %O`,
          buttonsSpanRef.current.children
        );
      return;
    }

    const span = buttonsSpanRef.current.children[
      currentButton
    ] as HTMLSpanElement;

    const button = span.children[0] as HTMLButtonElement;
    button.focus();
  }, [currentButton]);

  return (
    <span ref={buttonsSpanRef}>
      {buttons.map((dlgBtn, index) => {
        const { onClick, disabled, ...btnProps } = dlgBtn.props || {};
        return (
          <span key={index} onKeyDown={buttonKeyDown(index)}>
            {dlgBtn.button({
              ...btnProps,
              disabled: !JS.is.undefined(disabled) ? disabled : dlgBtn.disabled,
              onClick: buttonClick(onClick || dlgBtn.onClick),
            })}{' '}
          </span>
        );
      })}
    </span>
  );
};

export default DialogButtons;
