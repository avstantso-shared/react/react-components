import React, { useLayoutEffect, useRef } from 'react';
import { Modal } from 'react-bootstrap';

import { Perform } from '@avstantso/node-or-browser-js--utils';

import { ErrorBox } from '@root/components/errors';
import { useDocEventsStack } from '@components/docEventsStack';
import { SubDialogs } from '@components/dialogs/sub';
import { DialogsController } from '@root/components/dialogs/controller';

import { Dialog as Types } from '../types';

import { hasParentWithClass } from './has-parent-with-class';
import DialogButtons from './buttons';

import './dialog.scss';

function removeUnnecessaryProps<T extends Types.Props>(props: T): T {
  const { entity, handler, ...rest }: any = props as T & {
    entity?: unknown;
    handler?: unknown;
  };

  return rest;
}

const Dialog: Types.FC = (props) => {
  const {
    id,
    className,
    title,
    children,
    cancel,
    submit,
    buttons,
    defaultButton,
    footerLeft,
    dialogs,
    isOpen,
    error,
    ...rest
  } = removeUnnecessaryProps(props);

  const docEventsStack = useDocEventsStack();

  const timerDocBodyMouseDown = useRef<NodeJS.Timeout>(null);

  const regRef = useRef<Types.Actions>();
  regRef.current = { cancel, submit: submit || cancel };

  const { register, unregister } = DialogsController.use();

  useLayoutEffect(() => {
    const keyDownHandlerId = docEventsStack.keyDown.add(
      (e: KeyboardEvent): boolean => {
        const handled = (action: Types.Action): boolean => {
          e.stopPropagation();
          e.stopImmediatePropagation();
          docEventsStack.keyDown.remove(keyDownHandlerId);
          action();
          return true;
        };

        if ('Enter' === e.code && e.ctrlKey) return handled(submit || cancel);

        if ('Escape' === e.code) return handled(cancel);
      }
    );

    const docBodyMouseDown = (e: any) => {
      if (
        hasParentWithClass(
          e.target,
          'modal-footer',
          'modal-body',
          'modal-header',
          'modal-open'
        )
      )
        timerDocBodyMouseDown.current = setTimeout(() => {
          e.target.click();
        }, 200);
    };

    document.body.addEventListener('mousedown', docBodyMouseDown);

    register(regRef);

    return () => {
      docEventsStack.keyDown.remove(keyDownHandlerId);
      document.body.removeEventListener('mousedown', docBodyMouseDown);

      unregister(regRef);
    };
  }, []);

  return (
    <Modal
      id={Perform(id)}
      show={undefined === isOpen ? true : isOpen}
      onHide={cancel}
      {...{ className, ...rest }}
    >
      <SubDialogs dialogs={dialogs} />
      <Modal.Header closeButton>
        <Modal.Title className="dialog-header-text">{title}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {error && (
          <>
            <ErrorBox error={error} />
            <hr />
          </>
        )}
        {children}
      </Modal.Body>
      <Modal.Footer>
        {footerLeft}
        <DialogButtons {...{ buttons, defaultButton, timerDocBodyMouseDown }} />
      </Modal.Footer>
    </Modal>
  );
};

export default Dialog;
