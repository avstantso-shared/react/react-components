export function hasParentWithClass(
  element: HTMLElement,
  aClassName: string,
  ...aNotClassNames: string[]
): boolean {
  if (!element?.className?.split) return false;

  const cna = element.className.split(' ');
  if (cna.includes(aClassName)) return true;

  for (const notCN of aNotClassNames) if (cna.includes(notCN)) return false;

  return (
    element.parentNode &&
    hasParentWithClass(element.parentElement, aClassName, ...aNotClassNames)
  );
}
