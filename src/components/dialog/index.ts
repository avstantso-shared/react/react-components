import { Extend, Perform } from '@avstantso/node-or-browser-js--utils';

import {
  Dialog as RD,
  useEntityDialog,
  useMessageDialog,
} from '@avstantso/react--data';

import * as Types from './types';
import _Dialog from './component';
import * as EntityDialog from './entity';
import { MessageDialog } from './message';

export namespace Dialog {
  export namespace Data {
    export type Base = RD.Base;

    export namespace Entity {
      export type Result<TEntity> = RD.Entity.Result<TEntity>;

      export namespace Result {
        export type Closable<TEntity> = RD.Entity.Result.Closable<TEntity>;
      }

      export type Redirect = RD.Entity.Redirect;

      export namespace Handler {
        export type Raw<TEntity> = RD.Entity.Handler.Raw<TEntity>;
      }

      export type Handler<TEntity> = RD.Entity.Handler<TEntity>;

      export type Options = RD.Entity.Options;

      export namespace Execute {
        export type Options<
          TEntity extends Perform.Able,
          TParams extends Perform.Able = never
        > = RD.Entity.Execute.Options<TEntity, TParams>;
        export type Promise<
          TEntity extends Perform.Able,
          TParams extends Perform.Able = never
        > = RD.Entity.Execute.Promise<TEntity, TParams>;
      }

      export type Execute<
        TEntity extends Perform.Able,
        TParams extends Perform.Able = never
      > = RD.Entity.Execute<TEntity, TParams>;

      export namespace ComponentBuilder {
        export type Props<
          TEntity,
          TParams = never
        > = RD.Entity.ComponentBuilder.Props<TEntity, TParams>;
      }

      export type ComponentBuilder<
        TEntity,
        TParams = never
      > = RD.Entity.ComponentBuilder<TEntity, TParams>;

      export type State<
        TEntity extends Perform.Able,
        TParams extends Perform.Able = never
      > = RD.Entity.State<TEntity, TParams>;

      export type Props<TEntity extends Perform.Able> =
        RD.Entity.Props<TEntity>;
    }

    export type Entity<
      TEntity extends Perform.Able,
      TParams extends Perform.Able = never
    > = RD.Entity<TEntity, TParams>;

    export type Message = RD.Message;
  }

  export namespace Props {
    export type Base = Types.Dialog.Props.Base;
  }

  export type Props = Types.Dialog.Props;

  export type FC = Types.Dialog.FC;

  export type Button = Types.Dialog.Button;

  export namespace Buttons {
    export type Props = Types.Dialog.Buttons.Props;
    export type FC = Types.Dialog.Buttons.FC;
  }

  export type Buttons = Types.Dialog.Buttons;

  export namespace Entity {
    export namespace Controller {
      export type TypeMap<
        TEntity extends {} = {},
        TDefaults extends {} = {}
      > = EntityDialog.Types.Controller.TypeMap<TEntity, TDefaults>;

      export namespace TypeMap {
        /**
         * @summary First generic `Controller` typemapped argument.
         *
         * `TypeMap` or `Entity` object type
         */
        export type First = EntityDialog.Types.Controller.TypeMap.First;

        /**
         * @summary Second generic `Controller` typemapped argument, depended from `TFirst`.
         *
         * `never`, if `TFirst` is `TypeMap`
         *
         * or `Defaults` object type, if `TFirst` is `Entity` object type
         * @param TFirst First `Controller` typemapped argument
         * @param TConstraint Optional constraint if result not `never`
         */
        export type Second<
          TFirst extends First,
          TConstraint extends {} = {}
        > = EntityDialog.Types.Controller.TypeMap.Second<TFirst, TConstraint>;

        export namespace Second {
          /**
           * @summary Second generic `Controller` typemapped argument, depended from `TFirst`.
           *
           * Use `TFirst` as constraint in `Second`.
           *
           * `never`, if `TFirst` is `TypeMap`
           *
           * or `Defaults` object type, if `TFirst` is `Entity` object type
           * @param TFirst First `Controller` typemapped argument
           * @see Second
           */
          export type Def<TFirst extends First> =
            EntityDialog.Types.Controller.TypeMap.Second.Def<TFirst>;
        }

        /**
         * `TypeMap` from generic `Controller` typemapped arguments
         */
        export type From<
          TFirst extends TypeMap.First,
          TSecond extends TypeMap.Second<TFirst> = TypeMap.Second.Def<TFirst>
        > = EntityDialog.Types.Controller.TypeMap.From<TFirst, TSecond>;

        /**
         * Convert to `EntityLocalStorage.SyncFormik.TypeMap`
         */
        export type ToELS<
          TFirst extends TypeMap.First,
          TSecond extends TypeMap.Second<TFirst> = TypeMap.Second.Def<TFirst>
        > = EntityDialog.Types.Controller.TypeMap.ToELS<TFirst, TSecond>;
      }

      /**
       * @summary Extract `Entity` from first generic `Controller` typemapped argument
       * @param TFirst First `Controller` typemapped argument
       */
      export type Entity<TFirst extends TypeMap.First> =
        EntityDialog.Types.Controller.Entity<TFirst>;

      /**
       * @summary Extract `Defaults` from first and second generic `Controller` typemapped arguments
       * @param TFirst First `Controller` typemapped argument
       * @param TSecond Second `Controller` typemapped argument
       */
      export type Defaults<
        TFirst extends TypeMap.First,
        TSecond extends TypeMap.Second<TFirst> = TypeMap.Second.Def<TFirst>
      > = EntityDialog.Types.Controller.Defaults<TFirst, TSecond>;

      export namespace Options {
        export type RecalcStaic<
          TFirst extends TypeMap.First,
          TSecond extends TypeMap.Second<TFirst> = TypeMap.Second.Def<TFirst>
        > = EntityDialog.Types.Controller.Options.RecalcStaic<TFirst, TSecond>;

        export type Submit<TFirst extends TypeMap.First> =
          EntityDialog.Types.Controller.Options.Submit<TFirst>;

        export type Storage<
          TFirst extends TypeMap.First,
          TSecond extends TypeMap.Second<TFirst> = TypeMap.Second.Def<TFirst>
        > = EntityDialog.Types.Controller.Options.Storage<TFirst, TSecond>;
      }

      export type Options<
        TFirst extends TypeMap.First,
        TSecond extends TypeMap.Second<TFirst> = TypeMap.Second.Def<TFirst>
      > = EntityDialog.Types.Controller.Options<TFirst, TSecond>;

      export namespace FormikField {
        export type Options = EntityDialog.Types.Controller.FormikField.Options;
        export type Get = EntityDialog.Types.Controller.FormikField.Get;
      }

      export type FormikField = EntityDialog.Types.Controller.FormikField;
      export type Actions = EntityDialog.Types.Controller.Actions;
    }

    export type Controller<
      TFirst extends Controller.TypeMap.First,
      TSecond extends Controller.TypeMap.Second<TFirst> = Controller.TypeMap.Second.Def<TFirst>
    > = EntityDialog.Types.Controller<TFirst, TSecond>;

    export type Context = EntityDialog.Types.Context;
  }
}

const Data = {
  convertHandler: RD.convertHandler,
  handleDlg: RD.handleDlg,
  useEntity: useEntityDialog,
  useMessage: useMessageDialog,
};

export const Dialog: Dialog.FC & {
  Entity: typeof EntityDialog.Exports;
  Data: typeof Data;
  Message: typeof MessageDialog;
} = Extend(_Dialog);
Dialog.Entity = EntityDialog.Exports;
Dialog.Data = Data;
Dialog.Message = MessageDialog;
