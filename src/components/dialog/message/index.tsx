import React from 'react';

import { useMessageDialog } from '@avstantso/react--data';

import { Button, Buttons } from '@components/buttons';
import { Trans, LOCALES } from '@i18n';

import Dialog from '../component';

export namespace MessageDialog {
  export interface Props {
    text: string | JSX.Element;
    title?: string | JSX.Element;
    confirm: () => void;
    cancel?: () => void;
    firstButton(props: Button.Props): JSX.Element;
  }

  export type FC = React.FC<Props>;
}

export const MessageDialog: MessageDialog.FC = (props) => {
  const buttons = [
    { button: props.firstButton, onClick: props.confirm },
    ...(props.cancel
      ? [{ button: Buttons.Large.Cancel, onClick: props.cancel }]
      : []),
  ];

  return (
    <Dialog
      title={props.title}
      cancel={props.cancel || props.confirm}
      submit={props.confirm}
      buttons={buttons}
    >
      {props.text}
      {props.children}
    </Dialog>
  );
};

export const useDeleteDialog = () =>
  useMessageDialog(
    ({ handler, entity: text, params: title }) => (
      <MessageDialog
        firstButton={Buttons.Large.Delete}
        text={text}
        title={
          title || (
            <>
              <Trans i18nKey={LOCALES.dialog.delete} />?
            </>
          )
        }
        confirm={() => handler.accept(text)}
        cancel={() => handler.cancel()}
      />
    ),
    { label: 'deleteDialog' }
  );

export const useOkCancelDialog = () =>
  useMessageDialog(
    ({ handler, entity: text, params: title }) => (
      <MessageDialog
        firstButton={Buttons.Large.Ok}
        text={text}
        title={title || <Trans i18nKey={LOCALES.dialog.confirmation} />}
        confirm={() => handler.accept(text)}
        cancel={() => handler.cancel()}
      />
    ),
    { label: 'ok-cancel-dialog' }
  );

export const useOkDialog = () =>
  useMessageDialog(
    ({ handler, entity: text, params: title }) => (
      <MessageDialog
        firstButton={Buttons.Large.Ok}
        text={text}
        title={title || <Trans i18nKey={LOCALES.dialog.notification} />}
        confirm={() => handler.accept(text)}
      />
    ),
    { label: 'ok-dialog' }
  );
