import type React from 'react';

import type { Perform } from '@avstantso/node-or-browser-js--utils';

import type { Dialog as RDDialog } from '@avstantso/react--data';

import type { Button as RCButton } from '@components/buttons/types';

export namespace Dialog {
  export namespace Props {
    export interface Base extends RDDialog.SubDialogs.Props {
      className?: string;
      title?: string | JSX.Element;
      footerLeft?: JSX.Element;
      backdrop?: boolean | 'static';
      id?: Perform<string>;
      autoFocus?: boolean;
      error?: Error;

      /**
       * @default true
       */
      isOpen?: boolean;
    }
  }

  export type Action = () => unknown;

  export type Actions = {
    cancel: Action;
    submit?: Action;
  };

  export type Props = Dialog.Props.Base & Buttons & Actions;

  export type FC = React.FC<Props>;

  export interface Button {
    button: RCButton.Custom.FC;
    onClick?: React.MouseEventHandler;
    readonly disabled?: boolean;
    props?: RCButton.Props;
  }

  export namespace Buttons {
    export interface Props extends Buttons {
      timerDocBodyMouseDown: React.MutableRefObject<NodeJS.Timeout>;
    }

    export type FC = React.FC<Props>;
  }

  export interface Buttons {
    buttons: ReadonlyArray<Button>;
    defaultButton?: number;
  }
}
