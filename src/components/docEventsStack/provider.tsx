import React from 'react';
import { DocEventsStackContext } from './context';
import useDocEventsStackController from './controller';

const DocEventsStackContextProvider: React.FC = (props) => {
  const { children } = props;

  const controller = useDocEventsStackController();

  return (
    <DocEventsStackContext.Provider value={controller}>
      {children}
    </DocEventsStackContext.Provider>
  );
};

export default DocEventsStackContextProvider;
