// export type KeyboardEventHandler = (e: KeyboardEvent) => void;

import { useEffect, useRef, useState } from 'react';
import { Generics } from '@avstantso/node-or-browser-js--utils';

import type { EventHandler, DocEventsStack } from './types';

const useDocEventStackListener = <TEvent extends Event>(
  add: (handler: EventHandler<TEvent>) => number,
  remove: (id: number) => void,
  handler: EventHandler<TEvent>
) => {
  useEffect(() => {
    const id = add(handler);
    return () => {
      remove(id);
    };
  }, []);
};

const useDocEventStackController = <TEvent extends Event>(
  eventName: string
): DocEventsStack.EventStack<TEvent> => {
  const idCounterRef = useRef(0);
  const [handlers, setHandlers] = useState<
    ReadonlyArray<DocEventsStack.EventStack.Item<TEvent>>
  >([]);

  const add = (handler: EventHandler<TEvent>): number => {
    const id = (idCounterRef.current = idCounterRef.current + 1);
    setHandlers((prev) => [...prev, { id, handler }]);
    return id;
  };

  const remove = (id: number) =>
    setHandlers((prev) => {
      const next = [...prev];

      const index = next.findIndex((item) => item.id === id);
      if (index >= 0) next.splice(index, 1);
      return next;
    });

  const use = (handler: EventHandler<TEvent>) =>
    useDocEventStackListener(add, remove, handler);

  useEffect(() => {
    const documentHandler = (e: TEvent) => {
      for (let i = handlers.length - 1; i >= 0; i--) {
        const { handler } = handlers[i];
        if (handler && handler(e)) break;
      }
    };

    document.addEventListener(Generics.Cast.To(eventName), documentHandler);

    return () => {
      document.removeEventListener(
        Generics.Cast.To(eventName),
        documentHandler
      );
    };
  }, [handlers]);

  return {
    eventName,
    add,
    remove,
    use,
  };
};

const useDocEventsStackController = (): DocEventsStack => {
  const keyDown = useDocEventStackController<KeyboardEvent>('keydown');
  const keyPress = useDocEventStackController<KeyboardEvent>('keypress');
  const keyUp = useDocEventStackController<KeyboardEvent>('keyup');

  return { keyDown, keyPress, keyUp };
};

export default useDocEventsStackController;
