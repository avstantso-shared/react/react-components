export type EventHandler<TEvent extends Event> = (e: TEvent) => boolean;

export namespace DocEventsStack {
  export namespace EventStack {
    export interface Item<TEvent extends Event> {
      readonly id: number;
      handler: EventHandler<TEvent>;
    }
  }

  export interface EventStack<TEvent extends Event> {
    readonly eventName: string;
    add(handler: EventHandler<TEvent>): number;
    remove(id: number): void;
    use(handler: EventHandler<TEvent>): void;
  }
}

export interface DocEventsStack {
  keyDown: DocEventsStack.EventStack<KeyboardEvent>;
  keyPress: DocEventsStack.EventStack<KeyboardEvent>;
  keyUp: DocEventsStack.EventStack<KeyboardEvent>;
}
