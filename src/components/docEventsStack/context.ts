import { createContext, useContext } from 'react';
import type { DocEventsStack } from './types';

export const DocEventsStackContext = createContext<DocEventsStack>(undefined);
export const useDocEventsStack = () => useContext(DocEventsStackContext);
