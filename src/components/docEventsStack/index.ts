import Provider from './provider';

export { useDocEventsStack } from './context';

export const DocEventsStack = { Context: { Provider } };
