import React from 'react';
import { Meta, Story } from '@story-helpers';

import { Nester } from '.';
import './nester.stories.scss';

type Props = React.HTMLProps<HTMLElement>;

const meta = {
  ...Meta(Nester),
  component: Nester<React.HTMLProps<HTMLElement>>,
};

export default meta;

export const nester = Story<typeof Nester<Props>>({
  args: {
    components: ['div', 'article', 'span'],
    className: 'nested',
    style: { cursor: 'cell' },
  },
  render: (args) => (
    <section className="nester-conteiner">
      <Nester<Props> {...args}>ABCD</Nester>
    </section>
  ),
});
