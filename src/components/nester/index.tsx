import React from 'react';

import { Generics, Perform } from '@avstantso/node-or-browser-js--utils';
import { ChildrenProps } from '@avstantso/react--data';

export namespace Nester {
  export type Component<TChildProps extends {} = {}> =
    | React.FC<TChildProps>
    | keyof JSX.IntrinsicElements;

  export type Components<TChildProps extends {} = {}> =
    Component<TChildProps>[];

  export type Props<TChildProps extends {} = {}> = ChildrenProps & {
    components: Components<TChildProps>;
  } & Partial<TChildProps>;

  export namespace For {
    export type Props<TChildProps extends {} = {}> = ChildrenProps &
      Partial<TChildProps>;
    export type FC<TChildProps extends {} = {}> = React.FC<Props<TChildProps>>;
  }

  /**
   * @summary Create wrap component for components chain of nestings with props.children in deep
   * @example Nester.For(A, B, C) = (props) => <Nester components={[A, B, C]} {...props}/>
   */
  export type For = {
    /**
     * @summary Create wrap component for components chain of nestings with props.children in deep
     * @example Nester.For(A, B, C) = (props) => <Nester components={[A, B, C]} {...props}/>
     */
    <TChildProps extends {} = {}>(
      ...components: Components<TChildProps>
    ): Nester.For.FC<TChildProps>;

    /**
     * @summary Create wrap component for components chain of nestings with props.children in deep
     * @example Nester.For([A, B, C]) = (props) => <Nester components={[A, B, C]} {...props}/>
     */
    <TChildProps extends {} = {}>(
      components: Components<TChildProps>
    ): Nester.For.FC<TChildProps>;
  };

  export type FC = {
    /**
     * @summary Create for props.components chain of nestings with props.children in deep
     * @example <Nester components={[A, B, C]}>D<Nester/> = <A><B><C>D</C></B></A>
     */
    <TChildProps extends {} = {}>(props: Props<TChildProps>): JSX.Element;

    /**
     * @summary Create wrap component for components chain of nestings with props.children in deep
     * @example Nester.For(A, B, C) = (props) => <Nester components={[A, B, C]} {...props}/>
     */
    For: Nester.For;
  };
}

/**
 * @summary Create for props.components chain of nestings with props.children in deep
 * @example <Nester components={[A, B, C]}>D<Nester/> = <A><B><C>D</C></B></A>
 * @example Nester.For(A, B, C) = (props) => <Nester components={[A, B, C]} {...props}/>
 */
export const Nester: Nester.FC = <TChildProps extends {} = {}>(
  props: Nester.Props<TChildProps>
): JSX.Element => {
  const { components, children, ...rest } = props;

  let r = children && <>{Perform(children)}</>;

  for (let i = components.length - 1; i >= 0; i--) {
    const C = components[i];
    r = <C {...Generics.Cast(rest)}>{r}</C>;
  }

  return r;
};

Nester.For =
  <TChildProps extends {} = {}>(
    ...components: any[]
  ): Nester.For.FC<TChildProps> =>
  (props) =>
    (
      <Nester<TChildProps>
        components={
          1 === components.length && Array.isArray(components[0])
            ? components[0]
            : components
        }
        {...props}
      />
    );
