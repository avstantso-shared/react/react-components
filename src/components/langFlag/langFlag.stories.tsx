import React from 'react';
import { Meta, Story } from '@story-helpers';

import { LocalString } from '@avstantso/node-or-browser-js--model-core';

import { useTranslation } from '@i18n';

import { LangFlag } from '.';

const meta = {
  ...Meta(LangFlag, {
    argTypes: {
      lang: {
        options: ['-detect-locale-', ...LocalString.Languages],
        control: { type: 'radio' },
      },
    },
  }),
};

export default meta;

export const langFlag = Story(meta, {
  args: {
    lang: LocalString.Languages[0],
    short: false,
  },
  render: ({ lang: argsLang, ...args }) => {
    const [, i18n] = useTranslation();

    const lang = argsLang === '-detect-locale-' ? i18n.language : argsLang;

    return <LangFlag {...args} {...{ lang }} />;
  },
});
