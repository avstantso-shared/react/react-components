import Flags from 'country-flag-icons/react/3x2';

export type FlagAndCaption = [Flags.FlagComponent, string];

export const FlagAndCaption = {
  ByLang: (lang: string): FlagAndCaption => {
    switch (lang) {
      case 'ru':
        return [Flags.RU, 'Русский'];
      default:
        return [Flags.GB, 'English'];
    }
  },
};
