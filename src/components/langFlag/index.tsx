import React from 'react';
import { Generics } from '@avstantso/node-or-browser-js--utils';
import { LocalString } from '@avstantso/node-or-browser-js--model-core';

import { FlagAndCaption } from './flag-and-caption';

import './lang-flag.scss';

export namespace LangFlag {
  export interface Props {
    lang: string;
    short?: boolean;
  }

  export type FC = React.FC<Props> & { SupportedList: JSX.Element[] };
}

export const LangFlag: LangFlag.FC = (props) => {
  const { lang, short } = props;
  const [Flag, caption] = FlagAndCaption.ByLang(lang);
  return (
    <>
      <Flag
        // fix for react-monorepo
        {...Generics.Cast.To({
          className: 'lang-flag',
          height: 16,
          title: caption,
        })}
      />
      {!short && <span className="caption">{caption}</span>}
    </>
  );
};

LangFlag.SupportedList = LocalString.Languages.map((l) => (
  <LangFlag lang={l} />
));
