import type React from 'react';
import { ReactNode } from 'react';
import type { BadgeProps } from 'react-bootstrap';

export type Data = string | number | bigint | boolean | undefined | object;

export type PrePostFix = Partial<Record<'prefix' | 'postfix', React.ReactNode>>;

export namespace Props {
  export type Base = {
    data: Data;
    noRoot?: boolean;

    /**
     * @default 20 chars
     */
    collapseLimit?: number;

    expandDepth?: number;
    diffData?: { data: Data };
  };
}

export namespace Internal {
  export namespace Level {
    export type Props = PrePostFix &
      Pick<React.ComponentProps<'div' | 'li'>, 'className'>;

    export type FC = React.FC<Props>;
  }

  export namespace Collapsable {
    export type Props = Pick<Level.Props, 'className'>;

    export type FC = React.FC<Props>;
  }

  export namespace Structure {
    export type WrapPostfix = (postfix: ReactNode) => ReactNode;

    export type Props = Pick<React.ComponentProps<'span'>, 'className'> & {
      prefixOnly?: boolean;
      postfixOnly?: boolean;
      wrapPostfix?: WrapPostfix;
    };

    export type FC = React.FC<Props>;
  }

  export type Props = Props.Base &
    PrePostFix & {
      level?: number;
      isArray?: boolean;
    };
  export type FC = React.FC<Props>;
}

export type Decorate = (data: any) => Data;

export type Props = Props.Base & {
  isJson?: boolean;
  noJsonError?: boolean;
  noCopy?: boolean;
  decorate?: Decorate;
} & Pick<React.HTMLProps<HTMLDivElement>, 'id' | 'className'>;

export type FC = React.FC<Props> & {
  Context: React.Context<Context>;
};

export namespace Colors {
  export type Item = Pick<BadgeProps, 'bg' | 'text'>;
}

export type Colors = {
  structure?: Colors.Item;
  null?: Colors.Item;
  undefined?: Colors.Item;
  function?: Colors.Item;
  symbol?: Colors.Item;
  string?: Colors.Item;
  date?: Colors.Item;
  buffer?: Colors.Item;
  valueWrap?: Colors.Item;
  recursion?: Colors.Item;
  data?: Colors.Item;
};

export type Context = {
  colors?: Colors;

  /**
   * @default = length(UUID) = 36 chars
   */
  collapseLimit?: number;
};
