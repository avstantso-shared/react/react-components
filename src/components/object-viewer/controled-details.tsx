import React, { useState, useRef, useLayoutEffect } from 'react';

export type ControledDetails = React.FC<
  Omit<React.HTMLProps<HTMLDetailsElement>, 'open' | 'ref'>
>;

// A.V.Stantso:
//   Not used now, replaced to CSS solution
export function useControledDetails(): [
  ControledDetails,
  boolean,
  React.Dispatch<React.SetStateAction<boolean>>,
  React.MutableRefObject<HTMLDetailsElement>
] {
  const [open, setOpen] = useState(false);
  const detailsRef = useRef<HTMLDetailsElement>();

  useLayoutEffect(() => {
    function toggle() {
      setOpen(detailsRef.current.open);
    }

    detailsRef.current?.addEventListener('toggle', toggle);

    return () => {
      detailsRef.current?.removeEventListener('toggle', toggle);
    };
  }, [detailsRef.current]);

  const component: ControledDetails = (props) => (
    <details {...props} open={open} ref={detailsRef} />
  );

  return [component, open, setOpen, detailsRef];
}
