import React from 'react';
import * as UUID from 'uuid';

import { Extend } from '@avstantso/node-or-browser-js--utils';

import type * as Types from './types';

export type ObjectViewerContext = React.Context<Types.Context> & {
  Default: Types.Context;
};

export const ObjectViewerContext: ObjectViewerContext = Extend(
  React.createContext<Types.Context>({})
);

ObjectViewerContext.Default = {
  colors: {
    structure: { bg: 'secondary' },
    null: { bg: 'warning', text: 'white' },
    undefined: { bg: 'warning', text: 'white' },
    function: { bg: 'dark' },
    symbol: { bg: 'info', text: 'dark' },
    string: { bg: 'info', text: 'dark' },
    date: { bg: 'success', text: 'dark' },
    buffer: { bg: 'success', text: 'dark' },
    valueWrap: { bg: 'success', text: 'dark' },
    recursion: { bg: 'warning', text: 'white' },
    data: { bg: 'success', text: 'dark' },
  },
  collapseLimit: UUID.NIL.length,
};
