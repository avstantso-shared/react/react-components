import { Buffer } from 'buffer';
import React, { useMemo, useContext } from 'react';
import { Badge } from 'react-bootstrap';

import { Generic, JS, ValueWrap } from '@avstantso/node-or-browser-js--utils';

import { Combiner } from '@avstantso/react--data';

import { Buttons } from '@components/buttons';
import { UINotifications } from '@components/ui-notifications';
import { DateText } from '@components/date-text';

import type * as Types from './types';
import { ObjectViewerContext as Context } from './context';
import './object-viewer.scss';

const FuncExt = <T extends any>(data: T) =>
  Object.entries(data).filter(([k]) => !Object.keys(FuncExt).includes(k));

class RecursionStub {
  private _data: object;

  constructor(data: object) {
    this._data = data;
    Object.setPrototypeOf(this, RecursionStub.prototype);
  }

  public static Walk(data: any, walked: object[] = []): any {
    if (JS.is.function(data)) {
      const funcExt = FuncExt(data);
      if (!funcExt.length) return data;

      function f(...params: any[]) {
        return data(...params);
      }
      funcExt.forEach(([k, v]) => JS.set.raw(f, k, this.Walk(v, walked)));

      return f;
    }

    if (
      null === data ||
      !JS.is.object(data) ||
      data instanceof Date ||
      data instanceof Buffer ||
      data instanceof ValueWrap
    )
      return data;

    if (walked.includes(data)) return new RecursionStub(data);

    return JS.clone(data, (v) => this.Walk(v, [...walked, data]));
  }
}

function someData(data1: unknown, data2: unknown) {
  if (typeof data1 !== typeof data2) return false;

  if (JS.is.object(data1)) {
    if (data1 instanceof Date && data2 instanceof Date)
      return +data1 === +data2;
  }

  return data1 === data2;
}

const InnerViewer: Types.Internal.FC = (props) => {
  const {
    data,
    level = 0,
    prefix,
    postfix,
    noRoot,
    isArray,
    expandDepth,
    diffData,
  } = props;

  const context = useContext(Context);
  const colors = { ...Context.Default.colors, ...context.colors };
  const { collapseLimit } = {
    ...Context.Default,
    ...context,
    ...props,
  };

  const Structure: Types.Internal.Structure.FC = ({
    children,
    className,
    prefixOnly,
    postfixOnly,
    wrapPostfix,
  }) => {
    const pr = !postfixOnly && prefix && `${prefix}:`;
    const ps = !prefixOnly && (wrapPostfix ? wrapPostfix(postfix) : postfix);

    return pr || ps || children ? (
      <Badge
        {...Combiner.className(
          'structure',
          !postfixOnly && 'level-prefix',
          !prefixOnly && `${children ? 'level' : 'value'}-postfix`,
          className
        )}
        {...colors.structure}
      >
        {pr}
        {pr && children && <>&nbsp;</>}
        {children}
        {ps}
      </Badge>
    ) : null;
  };

  const Level: Types.Internal.Level.FC = ({
    className,
    children,
    prefix,
    postfix,
  }) => {
    const isComplex = prefix || postfix;
    const LevelTag = level ? 'li' : noRoot && isComplex ? 'ul' : 'div';

    const hasDiff = diffData && !isComplex && !someData(diffData.data, data);

    const style: Generic<React.CSSProperties> = { '--level': level };

    return (
      <LevelTag
        {...Combiner.className(
          !level && 'root',
          'level',
          isComplex && 'complex',
          isArray && 'in-array',
          hasDiff && 'diff',
          className
        )}
        {...{ style }}
      >
        {noRoot ? (
          children
        ) : isComplex ? (
          !children ? (
            <Structure>
              {prefix}
              {postfix}
            </Structure>
          ) : (
            <>
              <details
                className="complex"
                open={JS.is.number(expandDepth) && expandDepth >= 0}
              >
                <summary>
                  <Structure
                    wrapPostfix={(outer) => (
                      <span className="closed">
                        &hellip;{postfix}
                        {outer}
                      </span>
                    )}
                  >
                    {prefix}
                  </Structure>
                </summary>
                <ul>{children}</ul>
              </details>
              <Structure className="opened" postfixOnly>
                {postfix}
              </Structure>
            </>
          )
        ) : (
          <>
            <Structure prefixOnly />
            {children}
            <Structure postfixOnly />
          </>
        )}
      </LevelTag>
    );
  };

  const Detail: React.FC<
    Pick<Types.Internal.Props, 'data' | 'prefix' | 'isArray'> & {
      length: number;
      index: number;
    }
  > = ({ data, prefix, length, index, isArray }) => (
    <InnerViewer
      {...{ data, prefix, isArray, collapseLimit }}
      key={index}
      level={level + 1}
      postfix={index < length - 1 ? ',' : ''}
      expandDepth={
        !JS.is.number(expandDepth) || Infinity === expandDepth
          ? expandDepth
          : expandDepth - 1
      }
      diffData={
        diffData && {
          data:
            diffData.data &&
            (Array.isArray(diffData.data)
              ? diffData.data[index]
              : JS.get.raw(
                  diffData.data,
                  JS.is.string(prefix) ? prefix : null
                )),
        }
      }
    />
  );

  const Collapsable: Types.Internal.Collapsable.FC = ({ className }) => {
    const body = `${data}`.substring(0, collapseLimit - 1);
    const tail = `${data}`.substring(collapseLimit - 1);

    return (
      <Level {...Combiner.className('collapsable', className)}>
        <details className="collapsable">
          <summary>
            <Badge {...colors.string} title={`${data}`}>
              {body}
              {['opened', 'closed'].map((className, closed) => (
                <span key={className} {...{ className }}>
                  {closed ? <>&hellip;</> : tail}
                </span>
              ))}
            </Badge>
          </summary>
        </details>
      </Level>
    );
  };

  if (undefined === data)
    return (
      <Level className="level-data-undefined">
        <Badge {...colors.undefined}>{JS.undefined}</Badge>
      </Level>
    );

  if (null === data)
    return (
      <Level className="level-data-null">
        <Badge {...colors.null}>null</Badge>
      </Level>
    );

  if (JS.is.function(data)) {
    const funcExt = FuncExt(data);

    const funcBadge = <Badge {...colors.function}>{JS.function}</Badge>;

    const className = 'level-data-function';

    return !funcExt.length ? (
      <Level {...{ className }}>{funcBadge}</Level>
    ) : (
      <Level
        {...{ className }}
        prefix={
          <>
            {funcBadge}&nbsp;{'{'}
          </>
        }
        postfix="}"
      >
        {funcExt.map(([key, value], index, arr) => (
          <Detail
            key={index}
            data={value}
            prefix={key}
            length={arr.length}
            index={index}
          />
        ))}
      </Level>
    );
  }

  if (JS.is.object(data)) {
    if (data instanceof Date)
      return (
        <Level className="level-data-date">
          <Badge {...colors.date}>
            <DateText date={data} format="DD.MM.yyyy HH:mm:ss.SSS" />
          </Badge>
        </Level>
      );

    if (data instanceof Buffer)
      return (
        <Level className="level-data-buffer">
          <Badge {...colors.buffer}>Buffer: {data.toString('hex')}</Badge>
        </Level>
      );

    if (data instanceof ValueWrap)
      return (
        <Level className="level-data-value-wrap">
          <Badge {...colors.valueWrap}>
            {data.debugLabel}: {`${data} (${+data})`}
          </Badge>
        </Level>
      );

    if (data instanceof RecursionStub)
      return (
        <Level className="level-data-recursion">
          <Badge {...colors.recursion}>recursion</Badge>
        </Level>
      );

    if (Array.isArray(data))
      return (
        <Level className="level-data-array" prefix="[" postfix="]">
          {data.length &&
            data.map((item, index, arr) => (
              <Detail
                key={index}
                data={item}
                length={arr.length}
                index={index}
                isArray
              />
            ))}
        </Level>
      );

    const entries = Object.entries(data);
    return (
      <Level prefix="{" postfix="}" className="level-data-structure">
        {entries.length &&
          entries.map(([key, value], index, arr) => (
            <Detail
              key={index}
              data={value}
              prefix={key}
              length={arr.length}
              index={index}
            />
          ))}
      </Level>
    );
  }

  if (JS.is.string(data) && data.length > collapseLimit)
    return <Collapsable className="level-data-string" />;

  if (JS.is.symbol(data))
    return (
      <Level className="level-data-symbol">
        <Badge {...colors.symbol} title={String(data)}>
          {String(data)}
        </Badge>
      </Level>
    );

  return (
    <Level className={`level-data-${typeof data}`}>
      <Badge
        {...(JS.is.string(data) ? colors.string : colors.data)}
        title={`${data}`}
      >{`${data}`}</Badge>
    </Level>
  );
};

export const ObjectViewer: Types.FC = (props) => {
  const {
    data: propsData,
    isJson,
    noJsonError,
    noRoot,
    noCopy,
    collapseLimit,
    decorate,
    children,
    className,
    expandDepth,
    diffData,
    ...rest
  } = props;

  const { onCopyComplete } = UINotifications.useContext();

  if (process.env.DEBUG_INFO_ENABLED && children)
    console.warn(`${ObjectViewer.name}: Children not supported`);

  return useMemo(() => {
    let data: Types.Data = props.data;
    if (isJson)
      try {
        data = JSON.parse(`${data}`);
      } catch (e) {
        if (!noJsonError) return <Badge bg="danger">Error: {e.message}</Badge>;
      }

    if (decorate)
      try {
        data = decorate(data);
      } catch (e) {
        return <Badge bg="danger">DecorateError: {e.message}</Badge>;
      }

    data = RecursionStub.Walk(data);

    return (
      <div
        {...Combiner.className(
          'object-viewer',
          !noCopy && 'can-copy',
          noRoot && 'no-root',
          isJson && 'json',
          noJsonError && 'no-json-err',
          className
        )}
        {...rest}
      >
        {!noCopy && (
          <Buttons.Small.Copy
            onClick={() =>
              navigator.clipboard
                .writeText(
                  JS.is.string(props.data)
                    ? props.data
                    : JSON.stringify(props.data, null, 2)
                )
                .then(onCopyComplete)
            }
          />
        )}
        <InnerViewer
          {...{ data, noRoot, collapseLimit, expandDepth, diffData }}
        />
      </div>
    );
  }, [
    props.data,
    isJson,
    noJsonError,
    noRoot,
    noCopy,
    collapseLimit,
    diffData?.data,
  ]);
};

ObjectViewer.Context = Context;
