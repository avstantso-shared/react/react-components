import React from 'react';
import { Meta, Story } from '@story-helpers';

import { ObjectViewer } from '.';

const meta = { ...Meta(ObjectViewer) };

export default meta;

export const objectViewer = Story(meta, {
  args: {
    data: '{"a": {"b": ["1", 2], "c": {"x":";)"}, "c1": 13}, "d": {"e": [1, 2, 3]}, "f": null, "g": "long long long long long long long long" }',
    isJson: true,
  },
});
