import * as Types from './types';
import { ObjectViewer as _ObjectViewer } from './component';

export namespace ObjectViewer {
  export type Data = Types.Data;

  export type Decorate = Types.Decorate;
  export type Props = Types.Props;

  export namespace Colors {
    export type Item = Types.Colors.Item;
  }
  export type Colors = Types.Colors;

  export type Context = Types.Context;

  export type FC = Types.FC;
}

export const ObjectViewer = _ObjectViewer;
