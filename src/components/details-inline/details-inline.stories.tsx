import React from 'react';
import { Meta, Story } from '@story-helpers';

import { DetailsInline } from '.';

const meta = { ...Meta(DetailsInline) };

export default meta;

export const detailsInline = Story(meta, {
  render: (args) => (
    <>
      This is text with{' '}
      <DetailsInline summary="DetailsInline">
        <ul>
          <li>A</li>
          <li>B</li>
          <li>C</li>
        </ul>
      </DetailsInline>{' '}
      Next text...
    </>
  ),
});
