import React from 'react';

import { Combiner } from '@avstantso/react--data';

import './details-inline.scss';

export namespace DetailsInline {
  export type Props = Omit<React.HTMLProps<HTMLDetailsElement>, 'summary'> & {
    summary?: React.ReactNode;
  };
  export type FC = React.FC<Props>;
}

export const DetailsInline: DetailsInline.FC = (props) => {
  const { id, className, children, summary, ...rest } = props;

  return (
    <>
      <details
        {...Combiner.className('details-inline', className)}
        {...(id ? { id } : {})}
        {...rest}
      >
        <summary>{summary}</summary>
      </details>
      <div
        {...Combiner.className('details-inline-content', className)}
        {...(id ? { id: `${id}-content` } : {})}
      >
        {children}
      </div>
    </>
  );
};
