import type React from 'react';
import type { StateRec } from '@avstantso/react--data';

export interface Components {
  [name: string]: React.FC | object;
}

export interface ProviderProps {
  components: Components;
}

export type NotFoundControl = StateRec<'notFound', ReadonlyArray<string>>;

export type Context = ProviderProps & NotFoundControl;
