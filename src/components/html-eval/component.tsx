import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import parse, {
  HTMLReactParserOptions,
  Element,
  domToReact,
  attributesToProps,
} from 'html-react-parser';
import reactTagNames from 'react-tag-names';

import { Generics, JS } from '@avstantso/node-or-browser-js--utils';

import { HtmlEvalComponentError } from '@classes';

import type * as Types from './types';
import { HtmlEvalContext, useContext } from './context';
import { HtmlEvalContextProvider } from './provider';

export namespace HtmlEval {
  export type Components = Types.Components;
  export type Context = Types.Context;

  export namespace Replace {
    export interface Options {
      noSelfClosingTag?: boolean;
      noTrimTagEnd?: boolean;
      noExtSymbols?: boolean;
    }
  }

  export interface Props extends Replace.Options {
    children?: string;
    components?: Components;
  }

  export interface FC {
    (props: Props): JSX.Element;
    Context: React.Context<Types.Context>;
    ContextProvider: React.FC<Types.ProviderProps>;
  }
}

function fixHtmlForParser(
  input: string,
  options: HtmlEval.Replace.Options
): string {
  const { noSelfClosingTag, noTrimTagEnd, noExtSymbols } = options;

  let r = input;

  // Erase spaces by my special symbol code "&nosp;"
  if (!noExtSymbols) r = r.replace(/\s*&nosp;\s*/gm, '');

  // Erase spaces before and after symbols: ".,!?;:…" and codes like "&trade;", "&#8482;", "&#x1F61C;" before close tag
  if (!noTrimTagEnd)
    r = r.replace(
      /\s*([.,!?;:…]|(&([a-z]*)|(#(\d*|(x[A-Fa-f\d]*)));))*\s*(?=\<\/)/gm,
      (s) => s.trim()
    );

  // <XXX /> -> <XXX></XXX>
  if (!noSelfClosingTag)
    r = r.replace(/\<[A-Z][^\/\>]*\/\>/gm, (s) => {
      const name = s.substring(1, s.length - 2).trimEnd();
      return `<${name}></${name}>`;
    });

  return r;
}

function componentByName(
  components: HtmlEval.Components,
  name: string | string[],
  index = 0
): React.FC {
  const nameParts = Array.isArray(name) ? name : name.split('.');
  const lowerKey = nameParts[index];

  try {
    const key = Object.keys(components).find(
      (k) => k.toLocaleLowerCase() === lowerKey
    );

    if (!key) return null;

    const value = components[key as keyof object];

    if (index < nameParts.length - 1)
      return componentByName(
        value as HtmlEval.Components,
        nameParts,
        index + 1
      );

    return Generics.Cast.To(value);
  } catch (e) {
    if (HtmlEvalComponentError.is(e)) throw e;

    const errPath = nameParts.splice(0, index + 1);
    throw new HtmlEvalComponentError(
      `Resolve component error for "${errPath.join('.')}"`,
      e
    );
  }
}

function fixAttribs(
  component: React.FC,
  attribs: Element['attribs']
): Element['attribs'] {
  const typeInfo = JS.tryTypeInfo(component);
  if (!typeInfo) return attribs;

  const r: any = { ...attribs };

  Object.keys(r).forEach((key) => {
    const ti = typeInfo[key as keyof JS.TypeInfo];
    if (!ti) return;

    if (JS.boolean === ti && r[key] === '') r[key] = true;
  });

  return r;
}

const NotFoundReporter: React.FC<Types.NotFoundControl> = ({
  notFound,
  setNotFound,
}) => {
  useEffect(() => {
    setNotFound((prev) => {
      const next = [...prev];

      notFound.forEach((item) => !next.includes(item) && next.push(item));

      return next;
    });
  }, [notFound.join()]);

  return null;
};

export const HtmlEval: HtmlEval.FC = (props) => {
  const { children } = props;

  const context = useContext();

  if (!children) return null;

  const components = {
    ...(context?.components || {}),
    ...(props.components || {}),
  };

  const notFound: string[] = [];

  const options: HTMLReactParserOptions = {
    replace: (domNode) => {
      if (!(domNode instanceof Element)) return;

      const { name } = domNode;

      // A.V.Stantso: "Link" children is empty. parser bug? Use "a" with hashed href
      if ('a' === name && domNode.attribs['href'].startsWith('#'))
        return (
          <Link to={domNode.attribs['href']}>
            {domToReact(domNode.children, options)}
          </Link>
        );

      const Component = componentByName(components, name);
      if (Component) {
        const attribs = fixAttribs(Component, domNode.attribs);

        //#region for debug
        // if ('XXX'.toLocaleLowerCase() === name) {
        //   console.log(
        //     `HtmlEval.Component "${name}". domNode: %O Component: %O attribs: %O`,
        //     domNode,
        //     Component,
        //     attribs
        //   );
        // }
        //#endregion

        const p = attributesToProps(attribs);
        return (
          <Component {...p}>
            {domNode.children.length
              ? domToReact(domNode.children, options)
              : null}
          </Component>
        );
      }

      if (context && !reactTagNames.includes(name) && !notFound.includes(name))
        notFound.push(name);
    },
  };

  return (
    <>
      {parse(fixHtmlForParser(children, props), options)}
      {!!context && !!notFound.length && (
        <NotFoundReporter {...{ notFound, setNotFound: context.setNotFound }} />
      )}
    </>
  );
};

HtmlEval.Context = HtmlEvalContext;
HtmlEval.ContextProvider = HtmlEvalContextProvider;
