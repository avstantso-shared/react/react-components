import React from 'react';
import { Link } from 'react-router-dom';

import { X } from '@avstantso/node-or-browser-js--utils';

import { Nowrap } from '@components/nowrap';
import { Decorations } from '@components/decorations';
import { Quotes } from '@components/quotes';
import { BrN } from '@components/br-if-narrow';

import type * as Types from './types';

const components = {
  f: React.Fragment,
  n: Nowrap,
  q: Quotes,
  d: Decorations,
  BrN,
  Link, // A.V.Stantso: children is empty. parser bug?
};

export const HtmlEvalContext = React.createContext<Types.Context>({
  notFound: [],
  setNotFound: X,
  components,
});
export const useContext = () => React.useContext(HtmlEvalContext);
