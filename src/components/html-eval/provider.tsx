import React, { useState } from 'react';
import { Alert } from 'react-bootstrap';
import _ from 'lodash';

import { ObjectViewer } from '@components/object-viewer';

import type { ProviderProps } from './types';
import { HtmlEvalContext, useContext } from './context';

export const HtmlEvalContextProvider: React.FC<ProviderProps> = (props) => {
  const { children, ...rest } = props;
  const parent = useContext();

  const [notFound, setNotFound] = useState<ReadonlyArray<string>>([]);

  const components = parent
    ? _.merge({}, parent.components, props.components)
    : props.components;

  return (
    <HtmlEvalContext.Provider
      {...rest}
      value={{ components, notFound, setNotFound }}
    >
      {!!notFound.length && (
        <Alert variant="warning">
          <h6>&lt;HtmlEval&gt; not&nbsp; found components:</h6>
          <ul>
            {notFound.map((name) => (
              <li key={name}>{name}</li>
            ))}
          </ul>
          {process.env.DEBUG_INFO_ENABLED && (
            <ObjectViewer data={{ components }} noRoot />
          )}
        </Alert>
      )}
      {children}
    </HtmlEvalContext.Provider>
  );
};
