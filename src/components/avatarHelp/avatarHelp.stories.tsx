import React from 'react';
import { Meta, Story } from '@story-helpers';

import { AvatarHelp } from '.';

const meta = { ...Meta(AvatarHelp) };

export default meta;

export const avatarHelp = Story(meta, {
  args: { email: 'example@example.com', responsive: false },
});
