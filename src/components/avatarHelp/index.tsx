import React, { useMemo } from 'react';
import { Button, Alert } from 'react-bootstrap';

import { JS } from '@avstantso/node-or-browser-js--utils';

import { Combiner, InnerRef } from '@avstantso/react--data';

import { Trans, LOCALES } from '@i18n';
import { Fa } from '@instances';
import { GravatarImg } from '@components/gravatarImg';

import './index.scss';

const { FaChevronRight, FaChevronDown } = Fa;

export namespace AvatarHelp {
  export type Props = React.ComponentPropsWithoutRef<'details'> &
    InnerRef.Provider<HTMLDetailsElement> & {
      readonly email: string;
      readonly responsive?: boolean;
    };

  export type FC = React.FC<Props>;
}

function MkTrn(email: string) {
  const A: React.FC<{ href: string; isEmail?: boolean }> = ({
    href,
    isEmail,
  }) => (
    <a
      href={isEmail ? `mailto:${href}` : href}
      {...(isEmail ? {} : { target: '_blank', rel: 'noopener noreferrer' })}
    >
      {isEmail ? href : href.replace(/(^http(s)?:\/\/(www\.)?)|(\/$)/g, '')}
    </a>
  );

  const C = {
    components: {
      W: <A href="https://wordpress.com/" />,
      G: <A href="https://gravatar.com" />,
      E: <A href={email} isEmail />,
    },
  };

  function trn(index: number) {
    return (
      <Trans.N
        key={index}
        i18nKey={JS.get.raw(LOCALES.avatarHelper, `_${index}`)}
        {...C}
      />
    );
  }
  trn.li = (index: number) => <li key={index}>{trn(index)}</li>;

  return trn;
}

export const AvatarHelp: AvatarHelp.FC = (props) => {
  const { email, responsive, innerRef, children, className, ...rest } = props;

  const trn = useMemo(() => MkTrn(email), [email]);

  return (
    <details
      {...rest}
      ref={innerRef}
      {...Combiner.className(
        'avatar-help',
        responsive && 'responsive',
        className
      )}
    >
      <summary>
        <Button variant="secondary" as="span">
          <GravatarImg {...{ email }} size={24} />{' '}
          <Trans i18nKey={LOCALES.avatarHelper.caption} />
          <FaChevronRight className="closed" />
          <FaChevronDown className="opened" />
        </Button>
      </summary>
      <div className="avatar-help-body">
        <GravatarImg email={email} size={100} />
        <Alert variant="success">
          {trn(1)}
          <ol>{[2, 3, 4].map(trn.li)}</ol>
        </Alert>
        {children}
      </div>
    </details>
  );
};
