import React from 'react';
import { Meta, Story } from '@story-helpers';

import { Asterisk } from '.';

const meta = { ...Meta(Asterisk) };

export default meta;

export const asterisk = Story(meta, {
  args: { title: 'This is text comment!', color: 'red' },
  render: (args) => <Asterisk {...args}>Any text</Asterisk>,
});
