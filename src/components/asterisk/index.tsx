import React from 'react';

export namespace Asterisk {
  export interface Props extends React.HTMLProps<HTMLSpanElement> {
    /**
     * @default red
     */
    color?: string;
  }

  export interface FC extends React.FC<Props> {}
}

export const Asterisk: Asterisk.FC = (props) => {
  const { children, title, color, style, ...rest } = props;
  return (
    <>
      {children}
      <sup
        title={title}
        style={{ color: color || 'red', cursor: 'help', ...(style || {}) }}
        {...rest}
      >
        *
      </sup>
    </>
  );
};
