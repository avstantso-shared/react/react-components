import React from 'react';

import { Decorations } from '@components/decorations';

import type { Quotes } from './types';
import { quotesMeta } from './meta';

const RawQuotes: Quotes.Raw.FC = ({ children, i18n, ...rest }) => {
  const [left, right] = quotesMeta[
    i18n?.language as keyof typeof quotesMeta
  ] || ['"', '"'];

  return (
    <Decorations {...rest}>
      {left}
      {children}
      {right}
    </Decorations>
  );
};

export default RawQuotes;
