import React from 'react';
import { Meta, Story } from '@story-helpers';

import { LocalString } from '@avstantso/node-or-browser-js--model-core';

import { useTranslation } from '@i18n';

import Quotes from './raw';

const meta = {
  ...Meta(Quotes as any, {
    argTypes: {
      lang: {
        options: ['-detect-locale-', ...LocalString.Languages],
        control: { type: 'radio' },
      },
    },
  }),
};
export default meta;

const LongText = () => (
  <>
    Long long long long long long long long long long long long long long long
    long long long long long long long long long long long long long long long
    long long long long long long long long long long long long long long long
    long long long long long long long long long long long long long long long
    long long long long long long long long long long long long long long long
    long long long long text
  </>
);

export const quotes = Story(meta, {
  args: {
    lang: LocalString.Languages[0],
    bold: false,
    b: false,
    italic: false,
    i: false,
    underline: false,
    u: false,
    strikeout: false,
    s: false,
    nowrap: false,
    n: false,
  },
  render: ({ lang, ...rest }) => {
    const [, i18n] = useTranslation();

    return (
      <Quotes
        i18n={{ language: lang === '-detect-locale-' ? i18n.language : lang }}
        {...rest}
      >
        <LongText />
      </Quotes>
    );
  },
});
