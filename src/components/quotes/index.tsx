import React from 'react';

import { JS } from '@avstantso/node-or-browser-js--utils';

import { useTranslation } from '@i18n';
import { Decorations } from '@components/decorations';

import type { Quotes as Types } from './types';
import Raw from './raw';

export namespace Quotes {
  export type FC = Types.FC & JS.TypeInfo.Provider;
}

export const Quotes: Quotes.FC = (props) => {
  const [, i18n] = useTranslation();
  return <Raw {...{ i18n, ...props }} />;
};

Quotes.typeInfo = Decorations.typeInfo;
