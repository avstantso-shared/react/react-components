import type * as I18next from 'i18next';

import type { Decorations } from '@components/decorations';

export namespace Quotes {
  export type Props = Decorations.Props;

  export namespace Raw {
    export type Props = { i18n: Pick<I18next.i18n, 'language'> } & Quotes.Props;

    export type FC = React.FC<Props>;
  }

  export type FC = React.FC<Props>;
}
