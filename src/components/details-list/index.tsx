import React from 'react';
import { Badge, BadgeProps } from 'react-bootstrap';

import { Combiner } from '@avstantso/react--data';

import './details-list.scss';

export namespace DetailsList {
  export namespace Ellipsis {
    export type Props = Pick<BadgeProps, 'id' | 'className' | 'bg'>;
  }

  export type Props<T> = Pick<
    React.HTMLProps<HTMLDivElement>,
    'id' | 'className'
  > & {
    /**
     * @summary List for show
     */
    list: ReadonlyArray<T>;

    /**
     * @summary Number of always visible items
     * @default 5
     */
    limit?: number;

    /**
     * @summary Get react keys rule
     * @default `${item}-${index}`
     */
    getKey?(item: T, index: number): string;

    /**
     * @summary Render item rule
     */
    renderItem(item: T, index: number): React.ReactNode;

    /**
     * @summary Render ellipsis rule
     * @default DetailsList.Ellipsis
     */
    renderEllipsis?(props?: {}): React.ReactNode;

    /**
     * @summary Items separater
     * @default ' '
     */
    separator?: React.ReactNode;

    /**
     * @summary `summary` children
     */
    summary?: React.ReactNode;
  };
}

export function DetailsList<T>(props: DetailsList.Props<T>) {
  const {
    className,
    list,
    limit = 5,
    getKey,
    renderItem,
    renderEllipsis = DetailsList.Ellipsis,
    separator = ' ',
    summary,
    ...rest
  } = props;

  const ellipsisClick: React.MouseEventHandler = (e) => {
    e.preventDefault();

    let element = e.currentTarget;
    while (!element.classList.contains('details-list'))
      element = element.parentElement;

    const details = element.firstElementChild as HTMLDetailsElement;

    if ('DETAILS' !== details?.tagName)
      throw Error(`Unexpected tag "${details?.tagName}"`);

    details.open = true;
  };

  function mapper(item: T, index: number) {
    return (
      <React.Fragment key={getKey ? getKey(item, index) : `${item}-${index}`}>
        {limit === index && (
          <span className="ellipsis">
            {index > 0 && separator}
            <a onClick={ellipsisClick} href="#open">
              {renderEllipsis({})}
            </a>
          </span>
        )}
        <span {...(index < limit ? {} : { className: 'allow-hide' })}>
          {index > 0 && separator}
          {renderItem(item, index)}
        </span>
      </React.Fragment>
    );
  }

  return (
    <div {...Combiner.className('details-list', className)} {...rest}>
      {list.length > limit && (
        <details>
          <summary>{summary}</summary>
        </details>
      )}
      {list.map(mapper)}
    </div>
  );
}

DetailsList.Ellipsis = (props: DetailsList.Ellipsis.Props) => {
  const { bg = 'secondary', ...rest } = props;
  return <Badge {...{ ...rest, bg }}>&hellip;</Badge>;
};
