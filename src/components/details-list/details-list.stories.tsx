import React from 'react';
import { Badge } from 'react-bootstrap';
import { Meta, Story } from '@story-helpers';

import { DetailsList } from '.';

const meta = { ...Meta(DetailsList<string>) };

export default meta;

export const detailsList = Story(meta, {
  args: {
    limit: 5,
    separator: ' ',
    summary: '',
    list: ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'],
    renderItem: (item) => <Badge bg="secondary">{item}</Badge>,
  },
});
