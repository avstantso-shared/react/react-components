import React from 'react';
import { Meta, Story } from '@story-helpers';

import { Nowrap } from '.';

const meta = { ...Meta(Nowrap) };

export default meta;

const LongText = () => (
  <>
    Long long long long long long long long long long long long long long long
    long long long long long long long long long long long long long long long
    long long long long long long long long long long long long long long long
    long long long long long long long long long long long long long long long
    long long long long long long long long long long long long long long long
    long long long long text
  </>
);

export const nowrap = Story(meta, {
  render: (args) => (
    <>
      Without Nowrap:
      <p>
        <LongText />
      </p>
      <br />
      With Nowrap:
      <p>
        <Nowrap {...args}>
          <LongText />
        </Nowrap>
      </p>
    </>
  ),
});
