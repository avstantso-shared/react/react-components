import React from 'react';

import { Extend, Generic, JS } from '@avstantso/node-or-browser-js--utils';
import { Combiner, DynamicFC, GFC } from '@avstantso/react--data';

import './nowrap.scss';

// Todo: use GFC.AsElement
export namespace Nowrap {
  export type As = Extract<
    keyof JSX.IntrinsicElements,
    'span' | 'b' | 'i' | 'u' | 's'
  >;

  export namespace Props {
    export type Base<As extends Nowrap.As = 'span'> =
      React.ComponentPropsWithoutRef<As>;
  }

  export type Props<As extends Nowrap.As = 'span'> = {
    as?: As;
  } & Props.Base<As>;

  export namespace FC {
    export type Main = <As extends Nowrap.As = 'span'>(
      props: React.PropsWithChildren<Props<As>>,
      context?: any
    ) => JSX.Element;

    export type Sub<As extends Nowrap.As = 'span'> = React.FC<Props.Base<As>>;

    export type Subs = { [K in Exclude<As, 'span'> as Capitalize<K>]: Sub<K> };
  }

  export type FC = FC.Main & { select(as: As): FC.Sub } & FC.Subs;
}

export const Nowrap: Nowrap.FC = Extend.ByRecord<keyof Nowrap.FC.Subs>([
  'B',
  'I',
  'U',
  'S',
] as Generic)(
  Extend.Arbitrary(
    <As extends Nowrap.As = 'span'>({
      as: As = 'span',
      className,
      ...rest
    }: Generic<React.PropsWithChildren<Nowrap.Props<As>>>) => (
      <As {...Combiner.className('white-space-nowrap', className)} {...rest} />
    ),
    {
      select: (asValue: Nowrap.As) =>
        'span' === asValue
          ? Nowrap
          : JS.get.raw(Nowrap, asValue.toCapitalized()),
    }
  ),
  (asValue, O): Nowrap.FC.Sub<any> =>
    DynamicFC(asValue, (props) => (
      <O<any> as={asValue.toLowerCase()} {...props} />
    ))
);
