import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Meta, Story } from '@story-helpers';

import { BackIconLink } from '.';

const meta = { ...Meta(BackIconLink) };

export default meta;

export const backIconLink = Story(meta, {
  args: {
    url: '?path=/story',
  },
  render: (args) => (
    <BrowserRouter>
      <BackIconLink {...args} />
    </BrowserRouter>
  ),
});
