import React from 'react';
import { Link } from 'react-router-dom';

import { useTranslation, LOCALES } from '@i18n';
import { Fa } from '@instances';

export const { FaArrowLeft } = Fa;

export namespace BackIconLink {
  export interface Props {
    readonly url: string;
  }

  export type FC = React.FC<Props>;
}

export const BackIconLink: BackIconLink.FC = ({ url }) => {
  const [t] = useTranslation();
  return (
    <>
      <Link className="back-icon-link" to={url}>
        <FaArrowLeft title={t(LOCALES.buttons.back)} />
      </Link>{' '}
    </>
  );
};
