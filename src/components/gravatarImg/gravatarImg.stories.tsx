import React from 'react';
import { Meta, Story } from '@story-helpers';

import { GravatarImg } from '.';

const meta = { ...Meta(GravatarImg) };

export default meta;

export const gravatarImg = Story(meta, {
  args: {
    email: 'example@example.com',
    size: 32,
  },
});
