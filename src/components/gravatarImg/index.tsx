import React from 'react';
import gravatar from 'gravatar';

import { Combiner, InnerRef } from '@avstantso/react--data';

export namespace GravatarImg {
  export type DefaultImageSet =
    | 404
    | 'mp'
    | 'identicon'
    | 'monsterid'
    | 'wavatar';

  export type Props = Omit<
    React.ComponentPropsWithoutRef<'img'>,
    'src' | 'width' | 'height'
  > &
    InnerRef.Provider<HTMLImageElement> & {
      readonly email: string;
      readonly size: number;

      /**
       * @default 'identicon'
       */
      readonly default?: DefaultImageSet;
    };

  export type FC = React.FC<Props>;
}

export const GravatarImg: GravatarImg.FC = (props) => {
  const {
    email,
    size,
    default: defaultImageSet = 'identicon',
    className,
    innerRef,
    alt = 'O_O',
    onLoad: outerOnLoad,
    ...rest
  } = props;

  let loading = true;
  const onLoad: React.ReactEventHandler<HTMLImageElement> = (e) => {
    loading = false;

    e.currentTarget.classList.remove('loading');

    outerOnLoad && outerOnLoad(e);
  };

  return (
    <img
      {...rest}
      {...Combiner.className('gravatar-img', loading && 'loading', className)}
      {...{ alt, onLoad }}
      ref={innerRef}
      width={size}
      height={size}
      src={gravatar.url(email, {
        size: `${size}`,
        default: `${defaultImageSet}`,
      })}
    />
  );
};
