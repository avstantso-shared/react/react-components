import { useContext, useMemo, useRef, useState } from 'react';

import { DialogsControllerContext, emptyAllShowed } from './context';

import type { Dialogs } from '../types';

import { UrlSearchActions } from './url-search-actions';

export const DialogsController: Dialogs.Controller.FC = (props) => {
  const { children } = props;

  const [allShowed, setAllShowed] = useState(emptyAllShowed);

  const ref = useRef<Dialogs.Controller>();
  if (ref.current) ref.current.allShowed = allShowed;
  else {
    const register: Dialogs.Controller['register'] = (reg) =>
      setAllShowed((prev) => {
        const all = [...prev.all, reg];
        return { all, current: all.peek() };
      });

    const unregister: Dialogs.Controller['unregister'] = (reg) =>
      setAllShowed((prev) => {
        const all = [...prev.all];
        const index = all.indexOf(reg);

        all.splice(index, 1);

        return { all, current: all.peek() };
      });

    ref.current = {
      allShowed,
      setAllShowed,
      register,
      unregister,
    };
  }

  const value = useMemo(
    () => ({
      ...ref.current,
      get allShowed() {
        return ref.current.allShowed;
      },
    }),
    [allShowed]
  );

  return <DialogsControllerContext.Provider {...{ value, children }} />;
};

DialogsController.Context = DialogsControllerContext;
DialogsController.use = () => useContext(DialogsControllerContext);
DialogsController.UrlSearchActions = UrlSearchActions;
