import { createContext } from 'react';

import { X } from '@avstantso/node-or-browser-js--utils';

import type { Dialogs } from '../types';

export const emptyAllShowed: Dialogs.Controller.AllShowed.State = {
  all: [],
  current: undefined,
};

const emptyDialogsController: Dialogs.Controller = {
  allShowed: emptyAllShowed,
  setAllShowed: X,
  register: X,
  unregister: X,
};

export const DialogsControllerContext = createContext<Dialogs.Controller>(
  emptyDialogsController
);
