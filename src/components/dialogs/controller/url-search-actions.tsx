import { useContext, useEffect } from 'react';
import { useLocation } from 'react-router';

import { JS } from '@avstantso/node-or-browser-js--utils';

import type { Dialog } from '@components/dialog/types';

import { DialogsControllerContext } from './context';
import { Dialogs } from '../types';

const sDialogAction = 'dialog-action';

function useUrlSearchActions() {
  const {
    allShowed: { current },
  } = useContext(DialogsControllerContext);

  const location = useLocation();
  const searchParams = new URLSearchParams(location.search);

  const actionName = searchParams.get(sDialogAction);

  useEffect(() => {
    if (!actionName || !current) return;

    const { current: actions } = current;

    const action: Dialog.Action = JS.get.raw(actions, actionName);
    if (action) {
      action();
      return;
    }

    console.warn(
      `${useUrlSearchActions.name}: unsupported "${sDialogAction}" search param value "${actionName}". Available values %O.`,
      Object.keys(actions)
    );
  }, [current, actionName]);
}

export const UrlSearchActions: Dialogs.Controller.UrlSearchActions.FC = () => {
  useUrlSearchActions();
  return null;
};

UrlSearchActions.use = useUrlSearchActions;
