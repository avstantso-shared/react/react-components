import { Extend } from '@avstantso/node-or-browser-js--utils';

import {
  Dialog as RD,
  useEntityDialog,
  useMessageDialog,
} from '@avstantso/react--data';

import * as Types from './types';
import { CommonDialogs as Common } from './common';
import { DialogsController as Controller } from './controller';
import { SubDialogs as Sub } from './sub';

export namespace Dialogs {
  export type Common = Types.Dialogs.Common;

  export namespace Sub {
    export type Props = Types.Dialogs.Sub.Props;
    export type FC = Types.Dialogs.Sub.FC;
  }

  export namespace Controller {
    export type FC = Types.Dialogs.Controller.FC;
  }

  export type Controller = Types.Dialogs.Controller;
}

export const Dialogs = {
  Common,
  Controller,
  Sub,
};
