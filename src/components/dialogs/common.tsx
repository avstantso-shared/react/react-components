import React, { createContext, useContext } from 'react';

import {
  useOkDialog,
  useOkCancelDialog,
  useDeleteDialog,
} from '@components/dialog/message';

import type { Dialogs } from './types';
import { SubDialogs } from './sub';

const emptyCommonDialogs: Dialogs.Common = {
  okDialog: null,
  okCancelDialog: null,
  deleteDialog: null,
};

const CommonDialogsContext = createContext<Dialogs.Common>(emptyCommonDialogs);

export const CommonDialogs: Dialogs.Common.FC = (props) => {
  const { children } = props;

  const okDialog = useOkDialog();
  const okCancelDialog = useOkCancelDialog();
  const deleteDialog = useDeleteDialog();

  return (
    <CommonDialogsContext.Provider
      value={{ okDialog, okCancelDialog, deleteDialog }}
    >
      <SubDialogs dialogs={[okDialog, okCancelDialog, deleteDialog]} />
      {children}
    </CommonDialogsContext.Provider>
  );
};

CommonDialogs.Context = CommonDialogsContext;
CommonDialogs.use = () => useContext(CommonDialogsContext);
