import type React from 'react';

import type { Dialog as RDDialog, StateRec } from '@avstantso/react--data';

import type { Dialog } from '@components/dialog/types';

export namespace Dialogs {
  export namespace Common {
    export type FC = React.FC & {
      Context: React.Context<Common>;
      use(): Common;
    };
  }

  export interface Common {
    okDialog: RDDialog.Message;
    okCancelDialog: RDDialog.Message;
    deleteDialog: RDDialog.Message;
  }

  export namespace Sub {
    export type Props = RDDialog.SubDialogs.Props;
    export type FC = React.FC<Props>;
  }

  export namespace Controller {
    export type RegProps = React.RefObject<Dialog.Actions>;

    export namespace AllShowed {
      export type State = {
        readonly all: ReadonlyArray<RegProps>;
        readonly current: RegProps;
      };
    }

    export type AllShowed = StateRec<'allShowed', AllShowed.State>;

    export type Methods = {
      register(props: RegProps): void;
      unregister(props: RegProps): void;
    };

    /**
     * @summary Ability to control dialog via the url search parameter `dialog-action`.
     *
     * Needed for improve developing
     */
    export namespace UrlSearchActions {
      export type FC = React.FC & {
        /**
         * @summary Ability to control dialog via the url search parameter `dialog-action`.
         *
         * Needed for improve developing
         */
        use(): void;
      };
    }

    export type FC = React.FC & {
      Context: React.Context<Controller>;
      use(): Controller;
      UrlSearchActions: UrlSearchActions.FC;
    };
  }

  export type Controller = Controller.AllShowed & Controller.Methods;
}
