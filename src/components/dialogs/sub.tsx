import React, { useEffect } from 'react';

import { Componented } from '@avstantso/react--data';

import type { Dialogs } from './types';

export const SubDialogs: Dialogs.Sub.FC = (props) => {
  const { dialogs } = props;

  const d: ReadonlyArray<Componented> = !dialogs
    ? []
    : Array.isArray(dialogs)
    ? dialogs
    : Componented.is(dialogs)
    ? [dialogs]
    : Object.values(dialogs).filter(Componented.is);

  // A.V.Stantso:
  // TODO: May be needed later
  // useEffect(() => {
  //   return () => {
  //     if (isEmpty(dialogs)) return;

  //     (Array.isArray(dialogs) ? dialogs : [dialogs]).forEach(
  //       ({ close, label }) => {
  //         close();
  //         console.log(`Sub dialog "${label}" closed!`);
  //       }
  //     );
  //   };
  // }, []);

  return !d.length ? null : (
    <>
      {d.map(({ Component }, index) => (
        <Component key={index} />
      ))}
    </>
  );
};
