import React from 'react';

import { Generic, JS, TS } from '@avstantso/node-or-browser-js--utils';

import { Nowrap } from '@components/nowrap';

export namespace Decorations {
  export namespace Props {
    export type Bold = [{ b?: boolean }, { bold?: boolean }];
    export type Italic = [{ i?: boolean }, { italic?: boolean }];
    export type Underline = [{ u?: boolean }, { underline?: boolean }];
    export type Strikeout = [{ s?: boolean }, { strikeout?: boolean }];
    export type Nowrap = [{ n?: boolean }, { nowrap?: boolean }];

    export type Alts = TS.Alt<Bold> &
      TS.Alt<Italic> &
      TS.Alt<Underline> &
      TS.Alt<Strikeout> &
      TS.Alt<Nowrap>;

    export type Union = TS.Array.ObjectCombine<
      [...Bold, ...Italic, ...Underline, ...Strikeout, ...Nowrap]
    >;

    export namespace Union {
      export type Short = TS.Array.ObjectCombine<
        [Bold[0], Italic[0], Underline[0], Strikeout[0], Nowrap[0]]
      >;

      export type Long = TS.Array.ObjectCombine<
        [Bold[1], Italic[1], Underline[1], Strikeout[1], Nowrap[1]]
      >;
    }
  }

  export type Props = Props.Alts;

  export type Extract = <TProps extends Props, TStrict extends boolean>(
    props: TProps,
    strict?: TStrict
  ) => TStrict extends true
    ? Pick<TProps, keyof Props.Union.Short>
    : Omit<TProps, keyof Props.Union.Long>;

  export type FC = React.FC<Props> & {
    extract: Extract;
  } & JS.TypeInfo.Provider;
}

const typeInfo: JS.TypeInfo = {
  b: JS.boolean,
  bold: JS.boolean,
  i: JS.boolean,
  italic: JS.boolean,
  u: JS.boolean,
  underline: JS.boolean,
  s: JS.boolean,
  strikeout: JS.boolean,
  n: JS.boolean,
  nowrap: JS.boolean,
};

const extractDecorations: Decorations.Extract = (
  { bold, b, italic, i, underline, u, strikeout, s, nowrap, n, ...rest },
  strict?
) => {
  const r: Generic = strict ? {} : { ...rest };

  Object.entries({
    b: b || bold,
    i: i || italic,
    u: u || underline,
    s: s || strikeout,
    n: n || nowrap,
  }).forEach(([k, v]) => {
    if (undefined !== v) r[k] = v;
  });

  return r;
};

export const Decorations: Decorations.FC = ({ children, ...rest }) => {
  const dp = extractDecorations(rest, true);
  const wraps = Object.entries(dp).reduce((r, [k, v]) => {
    if ('n' !== k && v) r.push(k);
    return r;
  }, [] as string[]);

  if (!dp.n && !wraps.length) return <>{children}</>;

  let jsx: JSX.Element = null;
  if (dp.n) {
    const w = wraps.shift();
    const W = !w ? Nowrap : Nowrap.select(w as Generic);

    jsx = <W>{children}</W>;
  }

  for (let j = 0; j < wraps.length; j++) {
    const C = wraps[j] as keyof JSX.IntrinsicElements;
    jsx = <C>{jsx || children}</C>;
  }

  return jsx;
};

Decorations.extract = extractDecorations;

Decorations.typeInfo = typeInfo;
