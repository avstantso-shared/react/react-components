import React from 'react';

import { JS, Rus } from '@avstantso/node-or-browser-js--utils';

import { BindFC, DynamicFC } from '@avstantso/react--data';

import { Lang } from '@components/lang';
import { Decorations } from '@components/decorations';

export namespace AgeText {
  export namespace Props {
    export type Base = { short?: boolean } & Decorations.Props;
  }

  export type Props = {
    age: number;
  } & Props.Base;

  export type Bind = (name: string, defProps: Props) => FC.Base;

  export namespace FC {
    export type Base<TProps extends Props.Base = Props.Base> =
      React.FC<TProps> & JS.TypeInfo.Provider;
  }

  export type FC = FC.Base<Props> & { Bind: AgeText.Bind };
}

const typeInfo: JS.TypeInfo = {
  age: JS.number,
  short: JS.boolean,
  ...Decorations.typeInfo,
};

// @ts-ignore fix CI TS2589: Type instantiation is excessively deep and possibly infinite
const ruYears = Rus.Number.withSubstantive('год', 'года', 'лет');

export const AgeText: AgeText.FC = ({ age, short, children, ...rest }) => {
  return (
    <Decorations {...rest}>
      {age}
      {!short && (
        <>
          {' '}
          <Lang en={`year${1 === age ? '' : 's'}`} ru={ruYears(age)} />
        </>
      )}
      {children}
    </Decorations>
  );
};

AgeText.typeInfo = typeInfo;

AgeText.Bind = (name, defProps) => BindFC(name, AgeText, defProps);
