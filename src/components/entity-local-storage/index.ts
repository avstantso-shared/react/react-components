import { EntityLocalStorage as Types } from './types';
import * as _ from './internals';

export namespace EntityLocalStorage {
  export type Key = Types.Key;

  export type Read = Types.Read;
  export type AddOrSet = Types.AddOrSet;
  export type Delete = Types.Delete;

  export namespace SyncFormik {
    export type TypeMap<
      TFormik extends {} = {},
      TStored extends {} = {}
    > = _.SyncFormik.TypeMap<TFormik, TStored>;

    export type Props<TTypeMap extends TypeMap> = _.SyncFormik.Props<TTypeMap>;
  }
  export type SyncFormik<TTypeMap extends SyncFormik.TypeMap> =
    _.SyncFormik<TTypeMap>;

  export namespace StoredDataAlert {
    export type Props = _.StoredDataAlert.Props;
    export type FC = _.StoredDataAlert.FC;
  }
}

export type EntityLocalStorage = Types;

export const EntityLocalStorage = { ..._ };
