export namespace EntityLocalStorage {
  export type Key = string;

  export type Read = <T = unknown>(key: Key) => T;
  export type AddOrSet = <T = unknown>(key: Key, entity: T) => void;
  export type Delete = (key: Key) => void;
}

export type EntityLocalStorage = Readonly<{
  isReady: boolean;
  read: EntityLocalStorage.Read;
  addOrSet: EntityLocalStorage.AddOrSet;
  delete: EntityLocalStorage.Delete;
}>;
