import { createContext } from 'react';

import type { EntityLocalStorage } from './types';

export const EntityLocalStorageContext =
  createContext<EntityLocalStorage>(undefined);
