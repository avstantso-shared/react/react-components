import React from 'react';
import { Alert, CloseButton } from 'react-bootstrap';

import { Buttons } from '@components/buttons';

import { Trans, LOCALES } from '@i18n';

import type { SyncFormik } from '../sync-formik';

import './stored-data-alert.scss';

export namespace StoredDataAlert {
  export type Props = SyncFormik & {
    visible?: boolean;
  };

  export type FC = React.FC<Props> & { HR: React.FC<Props> };
}

export const StoredDataAlert: StoredDataAlert.FC = (props) => {
  const { children, data, clear, visible = !!data } = props;

  const close: React.MouseEventHandler = (e) =>
    (e.currentTarget.parentElement.parentElement.style.display = 'none');

  return !visible ? null : (
    <div className="recovered-unsaved-data">
      <Alert variant="warning">
        <Trans
          i18nKey={LOCALES.entityLocalStorage.recoveredUnsavedData.alert}
        />
        &emsp;
        <Buttons.Large.Clear onClick={clear} />
        <CloseButton onClick={close} />
      </Alert>
      {children}
    </div>
  );
};

const StoredDataAlertHR: StoredDataAlert.FC['HR'] = (props) => {
  const { children, ...rest } = props;

  return (
    <StoredDataAlert {...rest}>
      {children}
      <hr />
    </StoredDataAlert>
  );
};

StoredDataAlert.HR = StoredDataAlertHR;
