import { useContext, useEffect, useRef } from 'react';
import type { JSONSchema7 } from 'json-schema';
import type { FormikProps, FormikTouched } from 'formik';
import _ from 'lodash';

import { Generics, JS, Perform } from '@avstantso/node-or-browser-js--utils';
import { cutBySchema } from '@avstantso/node-or-browser-js--model-core';

import { useLateEffect } from '@avstantso/react--data';

import type { EntityLocalStorage } from '../types';
import { EntityLocalStorageContext } from '../context';

const defaultTimeout = 500;

export namespace SyncFormik {
  export type TypeMap<TFormik extends {} = {}, TStored extends {} = {}> = {
    Formik: TFormik;
    Stored: TStored;
  };

  export type Props<TTypeMap extends TypeMap = TypeMap> = {
    /**
     * @summary Is hook disabled
     */
    disabled?: boolean;

    /**
     * @summary Key for read/write data from storage
     */
    storageKey: Perform<EntityLocalStorage.Key>;

    /**
     * @summary Method for calculate original entity
     */
    clacEntity(): TTypeMap['Formik'];

    /**
     * @summary Controlled formik
     */
    formik: FormikProps<TTypeMap['Formik']>;

    /**
     * @summary Schema for transform (cut) data to stored data
     */
    schema?: JSONSchema7;

    /**
     * @summary Timeout for late save data
     * @default 500
     */
    timeout?: number;
  };

  export type Clear = {
    (): void;

    /**
     * @summary Clear without change `formik.values`
     */
    submitted(): void;
  };
}

export type SyncFormik<
  TTypeMap extends SyncFormik.TypeMap = SyncFormik.TypeMap
> = {
  /**
   * @summary Data from storage, if this data exists
   */
  data: TTypeMap['Stored'];

  /**
   * @summary Clear with change `formik.values`
   */
  clear: SyncFormik.Clear;
};

export function useSyncFormik<TTypeMap extends SyncFormik.TypeMap>(
  props: SyncFormik.Props<TTypeMap>
): SyncFormik<TTypeMap> {
  const {
    storageKey,
    formik,
    clacEntity,
    schema,
    timeout = defaultTimeout,
  } = props;

  const storage = useContext(EntityLocalStorageContext);

  const disabled = !storage?.isReady || !storageKey || props.disabled;

  const key = !disabled && Perform(storageKey);
  const ref = useRef<TTypeMap['Stored']>();

  function isDiffFromFormik(
    entity: TTypeMap['Formik'] | TTypeMap['Stored'],
    forAllFields?: boolean
  ) {
    return (
      entity &&
      Object.keys(forAllFields ? formik.values : entity).some(
        (key) =>
          !_.isEqual(JS.get.raw(entity, key), JS.get.raw(formik.values, key))
      )
    );
  }

  // read data
  useEffect(() => {
    if (disabled) return;

    const readed = storage.read(key);

    if (!isDiffFromFormik(readed)) return;

    ref.current = readed;

    const touched: FormikTouched<TTypeMap['Formik']> = {};
    Object.entries(ref.current).forEach(([key, value]) => {
      if (_.isEqual(JS.get.raw(formik.values, key), value)) return;

      JS.set.raw(touched, key, true);
    });

    formik.setTouched(touched, false);

    formik.setValues((prev) => JS.patch({ ...prev }, ref.current), true);
  }, [key]);

  // write data
  const { cancel } = useLateEffect(
    timeout,
    () => {
      if (disabled || (ref.current && !isDiffFromFormik(ref.current, true)))
        return;

      const originalEntity = clacEntity();
      if (!isDiffFromFormik(originalEntity, true)) return;

      const cutted: TTypeMap['Stored'] = schema
        ? cutBySchema(formik.values, schema, {
            ignoreError: ({ keyword }) =>
              ['minLength', 'pattern'].includes(keyword),
          })
        : Generics.Cast.To(formik.values);

      storage.addOrSet(key, cutted);
    },
    [key, formik.values]
  );

  function doClear(doNotChangeFormik?: boolean) {
    if (disabled) return;

    cancel();

    storage.delete(key);

    ref.current = undefined;

    if (!doNotChangeFormik) {
      formik.setTouched({}, false);
      formik.setValues(clacEntity(), true);
    }
  }

  const clear = () => doClear();
  clear.submitted = () => doClear(true);

  return {
    get data() {
      return !disabled ? ref.current : undefined;
    },
    clear,
  };
}

export const SyncFormik = {
  use: useSyncFormik,
};
