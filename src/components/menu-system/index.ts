import type { MenuSystem as Types } from './types';
import { Context, Provider } from './outside';
import { MenuSystemReg as Reg } from './reg';
import { MenuSystemItems as Items } from './items';

export namespace MenuSystem {
  export type Props = Types.Props;

  export namespace Context {
    export type Item = Types.Context.Item;
    export type List = Types.Context.List;
  }

  export namespace Reg {
    export type Props = Types.Reg.Props;
    export type FC = Types.Reg.FC;
  }

  export namespace Items {
    export type Props = Types.Items.Props;
    export type FC = Types.Items.FC;
  }

  export type Node = Types.Node;

  export namespace Stored {
    export type Item = Types.Stored.Item;
  }

  export type Stored = Types.Stored;
}

export type MenuSystem = Types;

export const MenuSystem: MenuSystem = {
  Context,
  Provider,
  Reg,
  Items,
};
