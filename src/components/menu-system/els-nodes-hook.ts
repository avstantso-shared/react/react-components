import { useContext, useEffect } from 'react';

import { EntityLocalStorage } from '@components/entity-local-storage';

import type { MenuSystem } from './types';
import { Context } from './outside';

export function useELSNodes(
  treeRef: MenuSystem.TreeRef,
  path: string
): MenuSystem.Node.Update {
  const { setList } = useContext(Context);

  const elsKey = `menus${path || ''}`;
  const els = useContext(EntityLocalStorage.Context);

  function updateNode(node: MenuSystem.Node) {
    node.current.update();

    if (!els?.isReady) return;

    const stored: string[] = els.read(elsKey) || [];

    if (node.state.open) {
      stored.push(node.current.key);
      stored.sort((a, b) => a.localeCompare(b));
    } else stored.splice(stored.indexOf(node.current.key), 1);

    stored.length ? els.addOrSet(elsKey, stored) : els.delete(elsKey);
  }

  useEffect(() => {
    if (!els?.isReady) return;

    const stored: string[] = els.read(elsKey) || [];
    if (!stored.length) return;

    let changed = false;

    function walk(node: MenuSystem.Node) {
      if (stored.includes(node.current.key)) {
        if (!node.state.open) {
          node.state.open = true;
          changed = true;
        }
      } else if (node.state.open) {
        delete node.state.open;
        changed = true;
      }

      node.items && node.items.forEach(walk);
    }

    treeRef.current.forEach(walk);

    if (changed) setList((prev) => [...prev]);
  }, [els?.isReady]);

  return updateNode;
}
