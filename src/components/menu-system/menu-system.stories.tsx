import React from 'react';
import { useState } from '@storybook/preview-api';
import { createBrowserHistory } from 'history';
import { Router } from 'react-router';
import { Menu, Sidebar } from 'react-pro-sidebar';

import { Meta, Story, Data } from '@story-helpers';

import { Notifications } from '../notifications';
import { MenuSystem } from '.';

const FV = Data.FruitsVegetables;

const meta = { ...Meta(MenuSystem.Provider, { title: 'MenuSystem' }) };

export default meta;

const history = createBrowserHistory();

export const menuSystem = Story(meta, {
  render: (args) => {
    const [showVegetables, setShowVegetables] = useState<boolean>(true);
    const [showNotifications, setShowNotifications] = useState<boolean>(true);

    return (
      <MenuSystem.Provider>
        <Notifications.Provider>
          <div>
            <div>
              <input
                type="checkbox"
                checked={showVegetables}
                onChange={(e) => setShowVegetables(e.currentTarget.checked)}
              />{' '}
              ShowVegetables
            </div>
            <div>
              <input
                type="checkbox"
                checked={showNotifications}
                onChange={(e) => setShowNotifications(e.currentTarget.checked)}
              />{' '}
              ShowNotifications
            </div>
          </div>

          <hr />

          <Notifications.Reg
            node={FV.fruits.citrus.orange}
            color="warning"
            disabled={!showNotifications}
          >
            This orange is very sweet
          </Notifications.Reg>
          <Notifications.Reg
            node={FV.fruits.citrus.lemon}
            color="danger"
            disabled={!showNotifications}
          >
            This lemon is very sour
          </Notifications.Reg>
          <Notifications.Reg
            node={FV.vegetables}
            color="info"
            disabled={!showNotifications}
          >
            This vegetables is so so
          </Notifications.Reg>

          <Router location={window.location} navigator={history}>
            <MenuSystem.Reg
              path={`${FV.Path.fruits}`}
              caption={`${FV.Name.fruits}`}
            />
            <MenuSystem.Reg
              node={FV.fruits.citrus}
              caption={`${FV.Name.fruits.citrus}`}
            />
            <MenuSystem.Reg
              node={FV.fruits.citrus.orange}
              caption={`${FV.Name.fruits.citrus.orange}`}
              priority={1}
            />
            <MenuSystem.Reg
              node={FV.fruits.citrus.lemon}
              caption={`${FV.Name.fruits.citrus.lemon}`}
            />
            <MenuSystem.Reg
              node={FV.vegetables}
              caption={`${FV.Name.vegetables}`}
              visible={showVegetables}
            />
            <Sidebar className="sidebar" breakPoint="md">
              <Menu>
                <MenuSystem.Items />
              </Menu>
            </Sidebar>
          </Router>
        </Notifications.Provider>
      </MenuSystem.Provider>
    );
  },
});
