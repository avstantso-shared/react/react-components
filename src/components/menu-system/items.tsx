import React, { useContext, useRef } from 'react';

import { PathXorNode } from '@types';
import { NeedProviderError } from '@classes';

import { Menu } from '@components/menu';

import type { MenuSystem } from './types';
import { Context } from './outside';
import { useELSNodes } from './els-nodes-hook';
import { listToTree, patchTree } from './utils';
import { MenuSystemNode } from './node';

export const MenuSystemItems: MenuSystem.Items.FC = (props) => {
  const {
    children,
    node,
    path = node?._path,
    hideNotification,
    ...fragment
  } = props;

  const { list } = useContext(Context) || {};
  if (!list)
    throw new NeedProviderError('MenuSystem.Items', 'MenuSystem.Provider');

  const treeRef = useRef<MenuSystem.Node[]>([]);

  const filtered = (
    !path
      ? [...list]
      : list.filter((props) => PathXorNode.Path(props).startsWith(path))
  ).sort((a, b) => PathXorNode.Path(a).localeCompare(PathXorNode.Path(b)));

  const tree = listToTree(filtered);
  if (hideNotification)
    tree.forEach(({ current }) => (current.hideNotification = true));

  patchTree(treeRef.current, tree);

  const updateNode = useELSNodes(treeRef, path);

  return (
    <Menu.Provider {...fragment}>
      {treeRef.current.map((node) => (
        <MenuSystemNode key={node.current.key} {...{ node, updateNode }} />
      ))}
      {children}
    </Menu.Provider>
  );
};
