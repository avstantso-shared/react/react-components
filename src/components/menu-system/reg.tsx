import React from 'react';
import { useTranslation } from 'react-i18next';

import { JS } from '@avstantso/node-or-browser-js--utils';

import { NeedPropError } from '@classes';

import type { MenuSystem } from './types';
import { ItemReg } from './outside';

export const MenuSystemReg: MenuSystem.Reg.FC = (props) => {
  const {
    node,
    path = node?._path,
    visible,
    url = node?._url,
    caption,
    captI18nKey,
    disabled,
  } = props;

  if (!path) {
    const e = new NeedPropError(MenuSystemReg, { path });

    JS.patch.raw(e, { context: props });

    throw e;
  }

  const [, i18n] = useTranslation();

  return (
    <ItemReg
      outKey={path}
      {...props}
      deps={[
        i18n.language,
        node,
        path,
        visible,
        url,
        caption,
        captI18nKey,
        disabled,
      ]}
    />
  );
};
