import { Outside } from '@root/meta-components';
import type { MenuSystem } from './types';

export const { Context, Provider, ItemReg } = Outside<MenuSystem.TypeMap>({
  label: 'MenuSystem',
});
