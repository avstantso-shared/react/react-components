import _ from 'lodash';

import { Outside } from '@meta-components/outside';

import { PathXorNode } from '@types';

import type { MenuSystem } from './types';

/**
 * @summary Quick visibility check
 *
 * ⚠ Not checks `visible` function here! For its check require `useContext(MenuFragmentContext)`
 */
export function isVisible(props: MenuSystem.Context.Item): boolean {
  return !('visible' in props) || !!props.visible;
}

export function listToTree(list: MenuSystem.Context.List): MenuSystem.Node[] {
  const r: MenuSystem.Node[] = [];

  let i = 0;
  while (i < list.length) {
    const visible = isVisible(list[i]);

    const add = (items?: MenuSystem.Node['items']) =>
      visible &&
      r.push({
        current: { ...list[i] },
        ...(items ? { items } : {}),
        state: {},
      });

    if (
      i < list.length - 1 &&
      PathXorNode.Path(list[i + 1]).startsWith(PathXorNode.Path(list[i]))
    ) {
      let j = i + 1;
      let sub: Outside.Context.Item<MenuSystem.TypeMap>[] = [];
      while (
        j < list.length &&
        PathXorNode.Path(list[j]).startsWith(PathXorNode.Path(list[i]))
      ) {
        visible && sub.push(list[j]);
        j++;
      }

      add(listToTree(sub));

      i = j;
    } else {
      add();
      i++;
    }
  }

  return r;
}

export function patchTree(
  nodes: MenuSystem.Node[],
  patchNodes: MenuSystem.Node[],
  level = 0
): boolean {
  let r = false;

  patchNodes.sort(({ current: a }, { current: b }) =>
    Outside.defaultCompare(a, b)
  );

  for (let l = Math.max(nodes.length, patchNodes.length), i = 0; i < l; i++) {
    const p = patchNodes[i];
    const n = nodes[i];

    const c = !n ? 1 : !p ? -1 : Outside.defaultCompare(n.current, p.current);

    if (0 === c) {
      if (
        patchTree(n.items || [], p.items || [], level + 1) ||
        !_.isEqual(n.current, p.current)
      ) {
        p.state = n.state;

        nodes[i] = p;

        r = true;
      }
    } else if (c > 0) {
      nodes.splice(i, 0, p);
    } else {
      nodes.splice(i, 1);
      r = true;
    }
  }

  if (nodes.length > patchNodes.length) {
    nodes.length = patchNodes.length;
    r = true;
  }

  return r;
}
