import type React from 'react';

import type { PathXorNode, PathUrlXorNode } from '@types';
import type { Outside } from '@meta-components/outside';
import type { Menu } from '@components/menu';

export namespace MenuSystem {
  export type Props = PathUrlXorNode &
    Partial<Menu.Item.Fixed.Props & Menu.Fragment.Fixed.Props> & {
      priority?: number;
    };

  export type TypeMap = Outside.TypeMap<Props>;

  export namespace Context {
    export type Item = Outside.Context.Item<TypeMap>;
    export type List = Outside.Context.List<TypeMap>;
  }

  export namespace Reg {
    export type Props = MenuSystem.Props;
    export type FC = React.FC<Props>;
  }

  export namespace Items {
    export type Props = Partial<PathXorNode> &
      Menu.Fragment &
      Pick<Menu.Fragment.Props, 'hideNotification'>;
    export type FC = React.FC<Props>;
  }

  export type Node = {
    current: Context.Item;
    items?: Node[];
    state?: { open?: boolean };
  };

  export namespace Node {
    export type Update = (node: Node) => void;

    export type FC = React.FC<{
      node: Node;
      updateNode: Update;
    }>;
  }

  export namespace Stored {
    export type Item = { open?: boolean };
  }

  export type Stored = NodeJS.Dict<Stored.Item>;

  export type TreeRef = React.MutableRefObject<MenuSystem.Node[]>;
}

export type MenuSystem = Omit<
  ReturnType<typeof Outside<MenuSystem.TypeMap>>,
  'ItemReg' | 'List'
> & {
  Reg: MenuSystem.Reg.FC;
  Items: MenuSystem.Items.FC;
};
