import React from 'react';
import { useLocation } from 'react-router';

import { Menu } from '@components/menu';

import type { MenuSystem } from './types';

export const MenuSystemNode: MenuSystem.Node.FC = (props) => {
  const {
    node: {
      current: { update, ...rest },
      items,
      state,
    },
    updateNode,
  } = props;

  const { pathname } = useLocation();
  const active = pathname.startsWith(`/${rest.key}`);

  if (!items?.length) {
    return <Menu.Item {...rest} {...{ active }} />;
  } else {
    const onOpenChange: Menu.Fragment.Props['onOpenChange'] = (open) => {
      state.open = open;
      updateNode(props.node);
    };

    return (
      <Menu.Fragment {...rest} open={state.open} {...{ active, onOpenChange }}>
        {items.map((node) => (
          <MenuSystemNode key={node.current.key} {...{ node, updateNode }} />
        ))}
      </Menu.Fragment>
    );
  }
};
