import React, { useState } from 'react';

import { Combiner } from '@avstantso/react--data';

import { genHtml } from './gen-html';
import './reconstructor.scss';

export namespace Reconstructor {
  export type Props = {
    onReconstruction?(html: string): void;
    preview?: boolean;
    html?: boolean;
  };

  export type FC = React.FC<Props>;
}

export const Reconstructor: Reconstructor.FC = (props) => {
  const { children, onReconstruction, preview, html } = props;

  const [code, setCode] = useState('');

  const reconstruction: React.RefCallback<HTMLDivElement> = (container) => {
    const element = container?.firstElementChild;
    if (!element) return;

    const next = genHtml(element);

    onReconstruction && onReconstruction(next);

    setCode(next);
  };

  return (
    <div className="reconstructor">
      <div
        {...Combiner.className('reconstructor-container', !preview && 'hidden')}
        ref={reconstruction}
      >
        {children}
      </div>

      {!html ? null : <pre>{code}</pre>}
    </div>
  );
};
