import React from 'react';
import { Meta, Story } from '@story-helpers';

import { Reconstructor } from '.';

import { Loading } from '@components/loading';

const meta = { ...Meta(Reconstructor) };

export default meta;

export const reconstructor = Story(meta, {
  args: {
    preview: true,
    html: true,
    children: <Loading.Text />,
  },
});
