import { exportStyles } from '@avstantso/react--data';

export function genHtml(element: Element, level = 0): string {
  const e = element as HTMLElement;

  const indent = '  '.repeat(level);

  let r = `${indent}<${e.tagName.toLowerCase()}`;

  for (let l = e.attributes.length, i = 0; i < l; i++) {
    const a = e.attributes.item(i);

    r += ` ${a.name}="${a.textContent}"`;
  }

  r += ` style="${Object.entries(exportStyles(e))
    .map(([k, v]) => `${k}: ${v}`)
    .join('; ')}">\r\n`;

  {
    let t = '';
    for (let l = e.childNodes.length, i = 0; i < l; i++) {
      const n = e.childNodes.item(i);

      if (n.nodeType === Node.ELEMENT_NODE)
        r += genHtml(n as Element, level + 1);
      else if (n.nodeType === Node.TEXT_NODE) t += n.textContent;
    }
    t = t.trimEnd();

    if (t) r += `${indent}  ${t}\r\n`;
  }

  r += `${indent}</${e.tagName.toLowerCase()}>\r\n`;

  return r;
}
