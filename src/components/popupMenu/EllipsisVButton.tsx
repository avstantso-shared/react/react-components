import React, { useState } from 'react';

import { Buttons } from '@components/buttons';

import { PopupMenu } from './popupMenu';

export namespace PopupMenuEllipsisVButton {
  export type Props = PopupMenu.Common.Props &
    Pick<React.HTMLProps<HTMLElement>, 'id'>;
  export type FC = React.FC<Props>;
}

export const PopupMenuEllipsisVButton: PopupMenuEllipsisVButton.FC = (
  props
) => {
  const { id, placement, items, width, ...rest } = props;

  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen((prev) => !prev);

  return (
    <>
      <PopupMenu
        placement={placement}
        items={items}
        isOpen={isOpen}
        toggle={toggle}
        width={width}
      >
        <Buttons.Small.EllipsisV id={id} onClick={toggle} {...rest} />
      </PopupMenu>
    </>
  );
};
