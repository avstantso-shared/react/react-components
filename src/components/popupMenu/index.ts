import React from 'react';

import { Extend } from '@avstantso/node-or-browser-js--utils';

import { PopupMenu as Base } from './popupMenu';
import { PopupMenuEllipsisVButton as EVB } from './EllipsisVButton';

export namespace PopupMenu {
  export namespace EllipsisVButton {
    export type Props = EVB.Props;

    export type FC = EVB.FC;
  }

  export type Item = Base.Item;

  export namespace Common {
    export type Props = Base.Common.Props;
  }

  export type Props = Base.Props;

  export type FC = Base.FC & { EllipsisVButton: EVB.FC };
}

export const PopupMenu: PopupMenu.FC = Extend(Base);
PopupMenu.EllipsisVButton = EVB;
