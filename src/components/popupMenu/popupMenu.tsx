import React from 'react';
import { Button, OverlayTrigger, Popover, PopoverBody } from 'react-bootstrap';

import { Generics, JS } from '@avstantso/node-or-browser-js--utils';
import { useDocBodyClickTimer } from '@avstantso/react--data';

export namespace PopupMenu {
  export interface Item {
    key?: string;
    caption: JSX.Element | string;
    disabled?: boolean;
    onClick?: React.MouseEventHandler;
  }

  export namespace Common {
    export interface Props {
      items: Item[];

      /**
       * @default 'left'
       */
      placement?: string;

      /**
       * @default '100%'
       */
      width?: string | number;
    }
  }

  export interface Props extends Common.Props {
    isOpen: boolean;
    toggle(): void;
  }

  export type FC = React.FC<Props>;
}

export const PopupMenu: PopupMenu.FC = (props) => {
  const { children, isOpen, placement, items, width } = props;

  // AVStantso:
  // Any click can potentially hide the Popover via a timer.
  // If the click was on the Popover itself or if bodyClick.clear() was called, the timer hide is cancelled
  const bodyClick = useDocBodyClickTimer(isOpen, props.toggle);

  // TODO: old code, need debug
  const toggle =
    props.toggle &&
    (() => {
      bodyClick.clear();
      props.toggle();
    });

  const buttonStyle = {
    display: 'block',
    width: width ? (JS.is.string(width) ? width : `${width}px`) : '100%',
  };

  const buttons = items.map((item, index) => {
    const { key, caption, ...rest } = item;

    return '-' === caption ? (
      <hr key={key || index} style={buttonStyle} />
    ) : (
      <Button
        key={key || index}
        className="text-left"
        style={buttonStyle}
        {...rest}
      >
        {caption}
      </Button>
    );
  });

  const popover = (
    <Popover>
      <PopoverBody>{buttons}</PopoverBody>
    </Popover>
  );

  return (
    <OverlayTrigger
      trigger="click"
      placement={Generics.Cast.To(placement || 'left')}
      overlay={popover}
      {...{ children }}
    >
      <>{children}</>
    </OverlayTrigger>
  );
};
