import React from 'react';
import type * as Types from './types';

export const UINotificationsContext = React.createContext<Types.Context>({
  onCopyComplete: () =>
    console.log(
      'Copied to clipboard!\r\n>> Use UINotificationsContext.Context.Provider for suppress this console message'
    ),
});
export const useContext = () => React.useContext(UINotificationsContext);
