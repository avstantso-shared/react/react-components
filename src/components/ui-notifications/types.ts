export namespace OnCopyComplete {
  export type Handler = () => void;
}

export type Context = {
  onCopyComplete?: OnCopyComplete.Handler;
};
