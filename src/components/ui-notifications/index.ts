import * as Types from './types';
import { UINotificationsContext, useContext } from './context';

export namespace UINotifications {
  export namespace OnCopyComplete {
    export type Handler = Types.OnCopyComplete.Handler;
  }

  export type Context = Types.Context;
}

export const UINotifications = { Context: UINotificationsContext, useContext };
