import React, { useCallback, useEffect, useRef, useState } from 'react';

import { Generics, JS, Perform } from '@avstantso/node-or-browser-js--utils';
import { InvalidArgumentError } from '@avstantso/node-or-browser-js--errors';
import type { Model } from '@avstantso/node-or-browser-js--model-core';
import {
  findIndexRef,
  useIndexedChild,
  StateRecs,
  Reducer,
  useIncrement,
  useInitRef,
} from '@avstantso/react--data';

import { DataContainer } from '@components/dataContainer';

import dbg from '@debug';

import type {
  Grid,
  Column as GridColumn,
  Columns as GridColumns,
  Level as GridLevel,
} from '../types';
import { GridRootLevel as sGridRootLevel } from '../const';

import { renderColumn, renderLevel } from './render';

type GridStateRecs<TDataUnion extends Model.IDed> = StateRecs<{
  columns: GridColumn<TDataUnion>[];
  levels: GridLevel<TDataUnion, TDataUnion>[];
  levelsDefaults: GridLevel.Root<TDataUnion>;
  headerVersion: number;
  dataVersion: number;
}>;

type GridState<TDataUnion extends Model.IDed> = Pick<
  GridStateRecs<TDataUnion>,
  'columns' | 'levels' | 'levelsDefaults' | 'headerVersion' | 'dataVersion'
>;

type GridParentProps<TDataUnion extends Model.IDed> = Required<
  Pick<Grid.Props<TDataUnion>, 'sorts' | 'filters'> &
    Omit<
      GridStateRecs<TDataUnion>,
      Exclude<keyof GridState<TDataUnion>, 'columns'>
    >
>;

const { useRefReducer, meta: ReducerMeta } = Reducer<GridState<any>>({
  columns: [],
  levels: [],
  levelsDefaults: undefined,
  headerVersion: 0,
  dataVersion: 0,
}).as((f) => ({
  SET_COLUMNS: f('columns'),
  SET_LEVELS: f('levels'),
  SET_LEVELS_DEF: f('levelsDefaults'),
  SET_HEADER_VER: f('headerVersion'),
  SET_DATA_VER: f('dataVersion'),
}));

type GridRef<TDataUnion extends Model.IDed> = {
  state: GridState<TDataUnion>;
  dispatch: typeof ReducerMeta.dispatch;

  parentProps: GridParentProps<TDataUnion>;
} & Pick<
  Grid<TDataUnion>,
  | 'setColumnsOrder'
  | 'useColumn'
  | 'useLevel'
  | 'useColumnsOrder'
  | 'renderHeader'
  | 'renderData'
>;

function gridColumnCaptionByIdx(index: number) {
  return `column${index}`;
}

function gridLevelCaptionByIdx(index: number) {
  return `level${index}`;
}

function calcNotNullIndex<TListItem, TField>(
  list: ReadonlyArray<TListItem>,
  limit: number,
  getField: (item: TListItem) => TField
): number {
  let r = 0;
  for (let i = 0; i < limit; i++) if (null !== getField(list[i])) r++;
  return r;
}

function useChildColumn<TDataUnion extends Model.IDed>(
  parentProps: GridParentProps<TDataUnion>,
  props: GridColumn.Props<TDataUnion>
): GridColumn<TDataUnion> {
  const {
    sorts,
    filters,
    columns,
    setColumns,
    setHeaderVersion,
    setDataVersion,
  } = parentProps;

  // for sort & filter
  let columnsOrderedByIndex: GridColumn<TDataUnion>[];
  const getColumnsOrderedByIndex = () =>
    columnsOrderedByIndex ||
    (columnsOrderedByIndex = [...columns].sort((a, b) => a.index - b.index));

  const {
    noFilter,
    filter: propsFilter,
    filterIndex,
    noSort,
    sortIndex,

    caption,
    stretch,

    isHidden,
    getProps,
    getContent,

    headerCell,
    dataCell,

    ...mainCellProps
  } = props;

  const {
    caption: hCaption,
    getProps: hGetProps,
    getContent: hGetContent,
    render: hRender,
    ...headerCellProps
  } = headerCell || {};
  const {
    getProps: dGetProps,
    getContent: dGetContent,
    render: dRender,
    ...dataCellProps
  } = dataCell || {};

  const indexRef = useRef(-1);

  // null - sort is not used
  const innerSortIndex: number =
    noSort || !sorts
      ? undefined
      : JS.is.number(sortIndex)
      ? sortIndex
      : calcNotNullIndex(
          getColumnsOrderedByIndex(),
          indexRef.current,
          ({ sort }) => sort
        );
  const sort = noSort || !sorts ? null : sorts.byColIndex(innerSortIndex);

  // null - filter is not used
  const filter =
    noFilter || !filters
      ? null
      : propsFilter ||
        filters.filters[
          JS.is.number(filterIndex)
            ? filterIndex
            : calcNotNullIndex(
                getColumnsOrderedByIndex(),
                indexRef.current,
                ({ filter }) => filter
              )
        ];

  // header render changed
  useEffect(() => {
    setHeaderVersion((prev) => prev + 1);
  }, [
    caption,
    stretch,
    sort,
    filter,
    isHidden,
    getProps,
    getContent,
    ...Object.values(headerCell || {}),
    ...Object.values(mainCellProps),
  ]);

  // data render changed
  useEffect(() => {
    setDataVersion((prev) => prev + 1);
  }, [
    isHidden,
    getProps,
    getContent,
    ...Object.values(dataCell || {}),
    ...Object.values(mainCellProps),
  ]);

  return useIndexedChild<GridColumn<TDataUnion>>(
    `columns (${caption})`,
    setColumns,
    indexRef,
    () => ({
      index: indexRef.current,
      caption: caption || gridColumnCaptionByIdx(indexRef.current),
      stretch,
      sortIndex: innerSortIndex,
      sort,
      filter,

      isHidden,
      getProps,
      getContent,

      cellProps: mainCellProps,

      headerCell: {
        caption: hCaption,
        getProps: hGetProps,
        getContent: hGetContent,
        render: hRender,
        cellProps: headerCellProps,
      },
      dataCell: {
        getProps: dGetProps,
        getContent: dGetContent,
        render: dRender,
        cellProps: dataCellProps,
      },
    }),
    [
      caption,
      stretch,
      sort,
      filter,
      isHidden,
      getProps,
      getContent,
      ...Object.values(headerCell || {}),
      ...Object.values(dataCell || {}),
      ...Object.values(mainCellProps),
    ]
  );
}

function parseRootLevelProps<TDataUnion extends Model.IDed>(
  props: GridLevel.Root.Props<TDataUnion>
): [
  GridLevel.Root<TDataUnion>,
  React.DependencyList,
  React.DependencyList,
  React.DependencyList
] {
  const { getProps, headerRow, dataRow, ...mainRowProps } = props;

  const {
    isHidden: hIsHidden,
    getProps: hGetProps,
    render: hRender,
    ...headerRowProps
  } = headerRow || {};
  const {
    isHidden: dIsHidden,
    getProps: dGetProps,
    render: dRender,
    ...dataRowProps
  } = dataRow || {};

  const level: GridLevel.Root<TDataUnion> = {
    getProps,

    rowProps: mainRowProps,

    headerRow: {
      isHidden: hIsHidden,
      getProps: hGetProps,
      render: hRender,
      rowProps: headerRowProps,
    },
    dataRow: {
      isHidden: dIsHidden,
      getProps: dGetProps,
      render: dRender,
      rowProps: dataRowProps,
    },
  };

  return [
    level,
    [getProps, ...Object.values(mainRowProps)],
    [...Object.values(headerRow || {})],
    [getProps, ...Object.values(dataRow || {})],
  ];
}

function useChildLevel<
  TDataUnion extends Model.IDed,
  TData extends Model.IDed = TDataUnion,
  TParentData extends Model.IDed = TDataUnion
>(
  parentProps: GridParentProps<TDataUnion>,
  props: GridLevel.Props<TData, TParentData>
): GridLevel<TData, TParentData> {
  const { setLevels, setHeaderVersion, setDataVersion } = parentProps;

  const { caption, getRows, ...rest } = props;
  const [rootData, commonDeps, headerDeps, dataDeps] =
    parseRootLevelProps(rest);

  const indexRef = useRef(-1);

  // header render changed
  useEffect(() => {
    setHeaderVersion((prev) => prev + 1);
  }, [...commonDeps, ...headerDeps]);

  // data render changed
  useEffect(() => {
    setDataVersion((prev) => prev + 1);
  }, [getRows, ...commonDeps, ...dataDeps]);

  return useIndexedChild<GridLevel<TData, TParentData>>(
    `levels (${caption})`,
    // GridLevel<TDataUnion, TDataUnion> / IGridLevel<TData, TParentData> mismatch, used Cast
    Generics.Cast.To(setLevels),
    indexRef,
    () => ({
      index: indexRef.current,
      caption: caption || gridLevelCaptionByIdx(indexRef.current),

      getRows,
      ...rootData,

      get is0() {
        return 0 === indexRef.current;
      },
      get is1() {
        return 1 === indexRef.current;
      },
      get is2() {
        return 2 === indexRef.current;
      },
      get is3() {
        return 3 === indexRef.current;
      },
      get is4() {
        return 4 === indexRef.current;
      },
      get is5() {
        return 5 === indexRef.current;
      },
      is: (...N: number[]) => N.some((n) => n === indexRef.current),
    }),
    [caption, getRows, ...commonDeps, ...headerDeps, ...dataDeps]
  );
}

function useLevelDefaults<TDataUnion extends Model.IDed>(
  parentProps: GridParentProps<TDataUnion>,
  props: GridLevel.Root.Props<TDataUnion>
): void {
  const { setLevelsDefaults, setHeaderVersion, setDataVersion } = parentProps;

  const [rootData, commonDeps, headerDeps, dataDeps] =
    parseRootLevelProps(props);

  // header render changed
  useEffect(() => {
    setHeaderVersion((prev) => prev + 1);
  }, [...commonDeps, ...headerDeps]);

  // data render changed
  useEffect(() => {
    setDataVersion((prev) => prev + 1);
  }, [...commonDeps, ...dataDeps]);

  useEffect(() => {
    setLevelsDefaults(rootData);
  }, [...commonDeps, ...headerDeps, ...dataDeps]);
}

function useLevelRoot<TDataUnion extends Model.IDed>(
  parentProps: GridParentProps<TDataUnion>,
  props: GridLevel.Root.Props<TDataUnion>
): void {
  const { setLevels, setHeaderVersion, setDataVersion } = parentProps;

  const [rootData, commonDeps, headerDeps, dataDeps] =
    parseRootLevelProps(props);

  // header render changed
  useEffect(() => {
    setHeaderVersion((prev) => prev + 1);
  }, [...commonDeps, ...headerDeps]);

  // data render changed
  useEffect(() => {
    setDataVersion((prev) => prev + 1);
  }, [...commonDeps, ...dataDeps]);

  useEffect(() => {
    setLevels((prev) => {
      const next = [...prev];
      const index = next.findIndex(({ caption }) => sGridRootLevel === caption);

      const rootLevel = {
        ...next[index],
        ...rootData,
      };

      next.splice(index, 1, rootLevel);

      return next;
    });
  }, [...commonDeps, ...headerDeps, ...dataDeps]);
}

function useColumnsInternalOrder<TDataUnion extends Model.IDed>(
  setter: GridColumns.Order.Dispatch<TDataUnion>,
  ...params: any[]
) {
  useEffect(
    () => {
      setter(...params);
    },
    1 !== params.length || JS.is.function(params[0]) ? params : params[0]
  );
}

const useCanShowWarnings = !process.env.DEBUG_INFO_ENABLED
  ? () => false
  : (): boolean => {
      const [canShowWarnings, setCanShowWarnings] = useState(false);

      useEffect(() => {
        const timeout = setTimeout(() => setCanShowWarnings(true), 3000);
        return () => {
          clearTimeout(timeout);
        };
      }, []);

      return canShowWarnings;
    };

function fromRef<TDataUnion extends Model.IDed>(
  current: GridRef<TDataUnion>
): Grid<TDataUnion> {
  const {
    // -local fields
    state,
    dispatch,
    parentProps,

    ...rest
  } = current;

  return {
    ...rest,
    get columns() {
      return current.state.columns;
    },
    get levels() {
      return current.state.levels;
    },
  };
}

export function useGrid<TDataUnion extends Model.IDed = any>(
  props: Grid.Props<TDataUnion> = {}
): Grid<TDataUnion> {
  const { sorts, filters } = props;

  const [ref, initial] = useInitRef<GridRef<TDataUnion>>();

  const [, dispatch] = useRefReducer.Raw(ref, initial, useIncrement());

  if (initial) {
    type G = Grid<TDataUnion>;

    const setColumnsOrder: G['setColumnsOrder'] = (...params) =>
      dispatch.SET_COLUMNS((prev) => {
        const order: GridColumns.Order.Item<TDataUnion>[] =
          1 !== params.length
            ? params
            : Perform<any, [GridColumn<TDataUnion>[]]>(params[0], prev);

        const next = [...prev];

        // Empty array => restore normal order
        if (!order?.length) return next.sort((a, b) => a.index - b.index);

        if (order.length > next.length)
          throw new InvalidArgumentError(
            `setColumnsOrder length ${order.length} exceed columns.length ${next.length}`
          );

        order.forEach((item, index) => {
          const colIndex = JS.is.number(item)
            ? prev.findIndex(({ index }) => index === item)
            : JS.is.string(item)
            ? prev.findIndex(({ caption }) => caption === item)
            : prev.findIndex(findIndexRef(item));

          if (colIndex >= 0) next.splice(index, 0, next.splice(colIndex, 1)[0]);
          else if (process.env.DEBUG_INFO_ENABLED)
            console.warn('setColumnsOrder index not found for %O', item);
        });

        return next;
      });

    const useColumn: G['useColumn'] = (...params: any[]) => {
      const [props, caption] = params.reverse();

      return useChildColumn(ref.current.parentProps, {
        ...props,
        caption: props.caption || caption,
      });
    };

    const useLevel: G['useLevel'] = (...params: any[]) => {
      const [props, caption] = params.reverse();

      return useChildLevel(ref.current.parentProps, {
        ...props,
        caption: props.caption || caption,
      });
    };

    useLevel.Defaults = (props) =>
      useLevelDefaults(ref.current.parentProps, props);

    useLevel.Root = (props) => useLevelRoot(ref.current.parentProps, props);

    const useColumnsOrder: G['useColumnsOrder'] = (...params) =>
      useColumnsInternalOrder(setColumnsOrder, ...params);

    JS.patch(ref.current, {
      setColumnsOrder,
      useColumn,
      useLevel,
      useColumnsOrder,
    });
  }

  const canShowWarnings = useCanShowWarnings();

  ref.current.parentProps = {
    sorts,
    filters,
    columns: ref.current.state.columns,
    setColumns: dispatch.SET_COLUMNS,
    setLevels: dispatch.SET_LEVELS,
    setLevelsDefaults: dispatch.SET_LEVELS_DEF,
    setHeaderVersion: dispatch.SET_HEADER_VER,
    setDataVersion: dispatch.SET_DATA_VER,
  };

  // Add level0 (root level)
  useChildLevel<TDataUnion>(ref.current.parentProps, {
    caption: sGridRootLevel,
    getRows: undefined,
  });

  ref.current.renderHeader = useCallback(
    (options = {}) => {
      const { columns, levels, levelsDefaults } = ref.current.state;

      if (!(levels.length && columns.length)) return null;

      return levels
        .filter(({ headerRow }) => !Perform(headerRow?.isHidden))
        .map((level) => {
          const cells = columns.map((column) => (
            <React.Fragment key={column.index}>
              {renderColumn({
                level,
                column,
                sorting: sorts,
                canShowWarnings,
              })}
            </React.Fragment>
          ));

          return (
            <React.Fragment key={level.index}>
              {renderLevel(cells, level, levelsDefaults)}
            </React.Fragment>
          );
        });
    },
    [ref.current.state.headerVersion, canShowWarnings]
  );

  ref.current.renderData = useCallback(
    (rootDataList, options = {}) => {
      const { columns, levels, levelsDefaults } = ref.current.state;

      if (!(levels.length && columns.length)) return null;

      const { noDataContainer, loading, customLoading, customAlert } = options;

      function processLevelData(
        level: GridLevel<TDataUnion>,
        dataList: ReadonlyArray<TDataUnion>
      ): React.ReactNode[] {
        if (!dataList.length) return null;

        dbg.grid.render.data(`dataList=%O`, dataList);

        return dataList.map((data) => {
          const isVisible = !Perform(level.dataRow?.isHidden, {
            data,
          });

          const cells =
            isVisible &&
            columns
              .filter(({ isHidden }) => !Perform(isHidden))
              .map((column) => (
                <React.Fragment key={column.index}>
                  {renderColumn({
                    level,
                    column,
                    sorting: sorts,
                    data,
                  })}
                </React.Fragment>
              ));

          const row = isVisible && (
            <React.Fragment key={`level-${level.index}--data-${data.id}`}>
              {renderLevel(cells, level, levelsDefaults, data)}
            </React.Fragment>
          );

          const nextLevel = levels[level.index + 1];
          if (!nextLevel) return row;

          const nextRows = nextLevel.getRows(data);
          if (!(Array.isArray(nextRows) && nextRows.length)) return row;

          return (
            <React.Fragment key={`${data.id}`}>
              {row}
              {processLevelData(nextLevel, nextRows)}
            </React.Fragment>
          );
        });
      }

      const Root: React.FC = () => (
        <>{processLevelData(levels[0], rootDataList)}</>
      );

      return noDataContainer ? (
        <Root />
      ) : (
        <DataContainer
          data={rootDataList}
          inTableCols={columns.length}
          {...{ loading, customLoading, customAlert }}
        >
          <Root />
        </DataContainer>
      );
    },
    [ref.current.state.dataVersion]
  );

  return fromRef(ref.current);
}
