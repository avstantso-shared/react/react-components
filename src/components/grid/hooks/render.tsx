import { Generics } from '@avstantso/node-or-browser-js--utils';
import type { Model } from '@avstantso/node-or-browser-js--model-core';

import { Sorts, Sort } from '@avstantso/react--data';

import { FilterPopoverButton } from '@components/filter';
import { SortButton } from '@components/sortButton';

import type {
  GridCellProps,
  GridRenderUnion,
  GridRowProps,
  Column as GridColumn,
  Level as GridLevel,
} from '../types';
import './render.scss';

export function mergeProps<TProps extends GridCellProps | GridRowProps>(
  ...props: TProps[]
): TProps {
  let styles = {};
  const classNames: string[] = [];
  let result = {} as TProps;

  props
    .filter((p) => p && Object.keys(p).length)
    .forEach(({ style, className, ...p }) => {
      if (style) styles = { ...styles, ...style };

      if (className) classNames.push(className);

      result = { ...result, ...p };
    });

  result = { ...result, style: styles, className: classNames.join(' ') };
  return result;
}

export function getLevelRowProps<TDataUnion extends Model.IDed>(
  level: GridLevel<TDataUnion>,
  data?: TDataUnion
): React.HTMLAttributes<HTMLTableRowElement> {
  if (!level) return {};

  const { getProps, headerRow, dataRow, rowProps: mainRowProps } = level;

  const custom = data ? dataRow : headerRow;
  const { getProps: customGetProps, rowProps: customRowProps } = custom || {};

  return mergeProps(
    mainRowProps,
    getProps ? getProps({ data }) : {},
    customRowProps,
    customGetProps ? customGetProps({ data }) : {}
  );
}

export function renderLevel<TDataUnion extends Model.IDed>(
  cells: React.ReactNode | React.ReactNode[],
  level: GridLevel<TDataUnion>,
  defaults: GridLevel.Root<TDataUnion>,
  data?: TDataUnion
): React.ReactNode {
  const props = {
    ...getLevelRowProps(Generics.Cast(defaults), data),
    ...getLevelRowProps(level, data),
  };

  const render = data
    ? level.dataRow.render || defaults?.dataRow?.render
    : level.headerRow.render || defaults?.headerRow?.render;

  if (render) return render({ cells, props, data });

  return <tr {...props}>{cells}</tr>;
}

export function getColumnCellProps<TDataUnion extends Model.IDed>(
  level: GridLevel<TDataUnion>,
  column: GridColumn<TDataUnion>,
  data?: TDataUnion
): React.HTMLAttributes<HTMLTableCellElement> {
  const {
    stretch,
    getProps,

    headerCell,
    dataCell,

    cellProps: mainCellProps,
  } = column;

  const custom = data ? dataCell : headerCell;
  const { getProps: customGetProps, cellProps: customRowProps } = custom || {};

  return mergeProps(
    data
      ? null
      : {
          className: 'grid-column-header',
          style: { whiteSpace: 'nowrap', ...(stretch ? { width: '99%' } : {}) },
        },
    mainCellProps,
    getProps ? getProps({ level, data }) : {},
    customRowProps,
    customGetProps ? customGetProps({ level, data }) : {}
  );
}

export type RenderColumnProps<TDataUnion extends Model.IDed> = {
  level: GridLevel<TDataUnion>;
  column: GridColumn<TDataUnion>;
  sorting: Sorts<TDataUnion>;
  data?: TDataUnion;
  canShowWarnings?: boolean;
};

export function renderColumn<TDataUnion extends Model.IDed>(
  renderProps: RenderColumnProps<TDataUnion>
): React.ReactNode {
  const isHeader = !renderProps.hasOwnProperty('data');
  const { level, column, sorting, data, canShowWarnings } = renderProps;
  const props = getColumnCellProps(level, column, data);

  const {
    caption,
    sortIndex,
    sort: sortInfo,
    filter: filterInfo,

    getContent,

    headerCell,
    dataCell,
  } = column;

  const custom = isHeader ? headerCell : dataCell;
  const { getContent: customGetContent, render } = custom;

  let content: GridRenderUnion =
    customGetContent && customGetContent({ level, data });
  if (undefined === content)
    content = getContent && getContent({ level, data });
  if (undefined === content && isHeader)
    content = undefined !== headerCell.caption ? headerCell.caption : caption;

  const sort = isHeader && null !== sortInfo && (
    <SortButton
      sortKind={sortInfo?.sortKind || Sort.Kind.none}
      sortIndex={
        sortInfo && sorting.columns.length > 1
          ? sorting.columns.indexOf(sortInfo) + 1
          : null
      }
      onClick={sorting.toggleClick(sortIndex)}
    />
  );

  const filter = isHeader && null !== filterInfo && (
    <FilterPopoverButton.ByType
      filter={filterInfo}
      canShowWarnings={canShowWarnings}
    />
  );

  if (render)
    return render({ level, column, props, data, content, sort, filter });

  return isHeader ? (
    <th {...props}>
      <div className="th-inner">
        {content}
        {sort && (
          <>
            &nbsp;&nbsp;
            {sort}
          </>
        )}
        {filter && (
          <>
            &nbsp;&nbsp;
            {filter}
          </>
        )}
      </div>
    </th>
  ) : (
    <td {...props}>{content}</td>
  );
}
