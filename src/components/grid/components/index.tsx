import React, { useMemo, useRef } from 'react';
import { Table } from 'react-bootstrap';

import { Generics } from '@avstantso/node-or-browser-js--utils';
import type { Model } from '@avstantso/node-or-browser-js--model-core';

import { Combiner } from '@avstantso/react--data';

import { useScrollHolder } from '@root/components/scrollHolder';

import { Hierarchy as GridHierarchy, Grid } from '../types';
import { useGrid } from '../hooks';

import { GridContext } from './contexts';
import { gridLevels, gridLevel } from './levels';
import { gridColumns, gridColumn } from './columns';
import Sentry from './sentry';
import './grid.scss';

const grid = <TDataUnion extends Model.IDed>(): Grid.FC<TDataUnion> => {
  const GridHeader: React.FC<Grid.Provider<TDataUnion>> = ({ grid }) =>
    useMemo(() => {
      // console.log('GridHeader rerender');
      return <thead>{grid.renderHeader()}</thead>;
    }, [grid.renderHeader]);

  const GridData: React.FC<
    Grid.FC.DataProps<TDataUnion> &
      Grid.Provider<TDataUnion> &
      Grid.Methods.RenderData.Options
  > = ({ grid, data, loading, customLoading, customAlert }) =>
    useMemo(() => {
      // console.log('GridData rerender');
      return (
        <tbody>
          {grid.renderData(data, { loading, customLoading, customAlert })}
        </tbody>
      );
    }, [grid.renderData, data, loading, customLoading, customAlert]);

  const GridInternal: React.FC<Grid.FC.Internal.Props<TDataUnion>> = (
    props
  ) => {
    Sentry.useValidateNestedGrid();

    const {
      children,
      data,
      grid,
      noDataContainer,
      loading,
      customLoading,
      customAlert,
      floatHeader,
      ...tableProps
    } = props;
    const { id } = tableProps;

    const refTable = useRef<HTMLTableElement>();

    useScrollHolder({
      ref: () => refTable.current?.parentElement as HTMLDivElement,
      id: `${id}-parent-div`,
      loading,
      label: `grid`,
    });

    return (
      <>
        {children && (
          <GridContext.Provider value={{ grid, hierarchy: GridHierarchy.Grid }}>
            {children}
          </GridContext.Provider>
        )}
        <Table
          {...Combiner.className('grid', floatHeader && 'float-header')}
          ref={refTable}
          {...tableProps}
        >
          <GridHeader {...{ grid }} />
          <GridData
            {...{
              grid,
              data,
              noDataContainer,
              loading,
              customLoading,
              customAlert,
            }}
          />
        </Table>
      </>
    );
  };

  const GridDefiner: React.FC<Grid.FC.Definer.Props<TDataUnion>> = (props) => {
    const { sorts, filters, ...rest } = props;
    const grid = useGrid({ sorts, filters });
    return <GridInternal {...rest} grid={grid} />;
  };

  const Grid: Grid.FC<TDataUnion> = (props) =>
    Generics.Cast(props).grid ? (
      <GridInternal {...Generics.Cast(props)} />
    ) : (
      <GridDefiner {...Generics.Cast(props)} />
    );

  Grid.Levels = gridLevels<TDataUnion>();
  Grid.Level = gridLevel<TDataUnion>();
  Grid.Columns = gridColumns<TDataUnion>();
  Grid.Column = gridColumn<TDataUnion>();

  return Grid;
};

export default grid;
