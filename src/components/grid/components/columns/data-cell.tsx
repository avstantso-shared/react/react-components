import React from 'react';
import type { Model } from '@avstantso/node-or-browser-js--model-core';
import type { Column as GridColumn } from '../../types';
import { useGridColumnContext } from '../contexts';
import Sentry from '../sentry';

const gridColumnDataCell =
  <TDataUnion extends Model.IDed>(): GridColumn.DataCell.FC<TDataUnion> =>
  (props) => {
    Sentry.useValidateCell();

    const columnProps = useGridColumnContext<TDataUnion>();
    columnProps.dataCell = {
      ...(columnProps.dataCell ? columnProps.dataCell : {}),
      ...props,
    };
    return null;
  };

export default gridColumnDataCell;
