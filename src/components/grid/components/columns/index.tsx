import React from 'react';
import type { Model } from '@avstantso/node-or-browser-js--model-core';
import { Hierarchy as GridHierarchy } from '../../types';
import Sentry from '../sentry';

export { default as gridColumn } from './column';

export const gridColumns =
  <TDataUnion extends Model.IDed>(): React.FC =>
  (props) => {
    Sentry.useValidateColumns();

    const { children } = props;
    return (
      <>
        <Sentry.Change hierarchy={GridHierarchy.Columns} />
        {children}
        <Sentry.Change hierarchy={GridHierarchy.Grid} />
      </>
    );
  };
