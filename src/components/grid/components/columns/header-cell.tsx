import React from 'react';
import type { Model } from '@avstantso/node-or-browser-js--model-core';
import type { Column as GridColumn } from '../../types';
import { useGridColumnContext } from '../contexts';
import Sentry from '../sentry';

const gridColumnHeaderCell =
  <TDataUnion extends Model.IDed>(): GridColumn.HeaderCell.FC<TDataUnion> =>
  (props) => {
    Sentry.useValidateCell();

    const columnProps = useGridColumnContext<TDataUnion>();
    columnProps.headerCell = {
      ...(columnProps.headerCell ? columnProps.headerCell : {}),
      ...props,
    };
    return null;
  };

export default gridColumnHeaderCell;
