import React from 'react';
import type { Model } from '@avstantso/node-or-browser-js--model-core';
import { Hierarchy as GridHierarchy, Column as GridColumn } from '../../types';
import {
  GridColumnContext,
  useGridColumnContext,
  useGridContext,
} from '../contexts';
import gridColumnHeaderCell from './header-cell';
import gridColumnDataCell from './data-cell';
import Sentry from '../sentry';

const Apply: React.FC = () => {
  const { grid } = useGridContext();
  const columnProps = useGridColumnContext();
  grid.useColumn(columnProps);
  return null;
};

const gridColumn = <
  TDataUnion extends Model.IDed
>(): GridColumn.FC<TDataUnion> => {
  const GridColumn: GridColumn.FC<TDataUnion> = (props) => {
    Sentry.useValidateColumn();

    const { children, ...rest } = props;
    return (
      <GridColumnContext.Provider value={{ ...rest }}>
        <Sentry.Change hierarchy={GridHierarchy.Column} />
        {children}
        <Apply />
        <Sentry.Change hierarchy={GridHierarchy.Columns} />
      </GridColumnContext.Provider>
    );
  };

  GridColumn.HeaderCell = gridColumnHeaderCell<TDataUnion>();
  GridColumn.DataCell = gridColumnDataCell<TDataUnion>();

  return GridColumn;
};

export default gridColumn;
