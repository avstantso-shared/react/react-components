import React from 'react';
import type { Model } from '@avstantso/node-or-browser-js--model-core';
import type { Level as GridLevel } from '../../types';
import { useGridContext, useGridLevelContext } from '../contexts';
import gridLevel from './abstract-level';
import gridLevelDefaults from './default-level';
import gridLevelRoot from './root-level';

const Apply: React.FC = () => {
  const { grid } = useGridContext();
  const levelProps = useGridLevelContext();
  grid.useLevel(levelProps);
  return null;
};

export default <TDataUnion extends Model.IDed>(): GridLevel.FC<TDataUnion> => {
  const details = gridLevel<TDataUnion>(Apply);

  const Level = <
    TData extends Model.IDed = TDataUnion,
    TParentData extends Model.IDed = TDataUnion
  >(
    props: GridLevel.Props<TData, TParentData>
  ) => gridLevel<TData, TParentData>(Apply)(props);

  Level.HeaderRow = details.HeaderRow;
  Level.DataRow = details.DataRow;
  Level.Defaults = gridLevelDefaults<TDataUnion>();
  Level.Root = gridLevelRoot<TDataUnion>();

  return Level;
};
