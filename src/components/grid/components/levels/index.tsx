import React from 'react';
import type { Model } from '@avstantso/node-or-browser-js--model-core';
import { Hierarchy as GridHierarchy } from '../../types';
import Sentry from '../sentry';

export { default as gridLevel } from './level';

export const gridLevels =
  <TDataUnion extends Model.IDed>(): React.FC =>
  (props) => {
    Sentry.useValidateLevels();

    const { children } = props;
    return (
      <>
        <Sentry.Change hierarchy={GridHierarchy.Levels} />
        {children}
        <Sentry.Change hierarchy={GridHierarchy.Grid} />
      </>
    );
  };
