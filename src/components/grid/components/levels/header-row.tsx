import React from 'react';
import type { Model } from '@avstantso/node-or-browser-js--model-core';
import type { Level as GridLevel } from '../../types';
import { useGridLevelContext } from '../contexts';
import Sentry from '../sentry';

const GridLevelHeaderRow: GridLevel.HeaderRow.FC = (props) => {
  Sentry.useValidateRow();

  const levelProps = useGridLevelContext();
  levelProps.headerRow = {
    ...(levelProps.headerRow ? levelProps.headerRow : {}),
    ...props,
  };
  return null;
};

export default GridLevelHeaderRow;
