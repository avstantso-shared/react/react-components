import React from 'react';
import type { Model } from '@avstantso/node-or-browser-js--model-core';
import type { Level as GridLevel } from '../../types';
import { useGridLevelContext } from '../contexts';
import Sentry from '../sentry';

const gridLevelDataRow =
  <TDataUnion extends Model.IDed>(): GridLevel.DataRow.FC<TDataUnion> =>
  (props) => {
    Sentry.useValidateRow();

    const levelProps = useGridLevelContext<TDataUnion>();
    levelProps.dataRow = {
      ...(levelProps.dataRow ? levelProps.dataRow : {}),
      ...props,
    };
    return null;
  };

export default gridLevelDataRow;
