import React from 'react';
import type { Model } from '@avstantso/node-or-browser-js--model-core';
import type { Level as GridLevel } from '../../types';
import { useGridContext, useGridLevelContext } from '../contexts';
import gridLevel from './abstract-level';

const Apply: React.FC = () => {
  const { grid } = useGridContext();
  const { caption, getRows, ...levelProps } = useGridLevelContext();
  grid.useLevel.Root(levelProps);
  return null;
};

export default <
  TDataUnion extends Model.IDed
>(): GridLevel.Root.FC<TDataUnion> => gridLevel<TDataUnion>(Apply);
