import React from 'react';
import type { Model } from '@avstantso/node-or-browser-js--model-core';
import { Hierarchy as GridHierarchy, Level as GridLevel } from '../../types';
import { GridLevelContext } from '../contexts';
import GridLevelHeaderRow from './header-row';
import gridLevelDataRow from './data-row';
import Sentry from '../sentry';

const gridLevel = <
  TData extends Model.IDed,
  TParentData extends Model.IDed = never
>(
  Apply: React.FC
) => {
  const GridLevel = (
    props: GridLevel.Props<TData, TParentData> | GridLevel.Root.Props<TData>
  ): JSX.Element => {
    Sentry.useValidateLevel();

    const { children, ...rest } = props;
    return (
      <GridLevelContext.Provider
        value={rest as GridLevel.Props<TData, TParentData>}
      >
        <Sentry.Change hierarchy={GridHierarchy.Level} />
        {children}
        <Apply />
        <Sentry.Change hierarchy={GridHierarchy.Levels} />
      </GridLevelContext.Provider>
    );
  };

  GridLevel.HeaderRow = GridLevelHeaderRow;
  GridLevel.DataRow = gridLevelDataRow<TData>();

  return GridLevel;
};

export default gridLevel;
