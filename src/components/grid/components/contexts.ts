import { createContext, useContext } from 'react';
import type { Model } from '@avstantso/node-or-browser-js--model-core';
import type { Column as GridColumn, Level as GridLevel, Grid } from '../types';

export const GridContext = createContext<Grid.FC.Context<any>>(undefined);
export const useGridContext = <TDataUnion extends Model.IDed = any>() =>
  useContext(GridContext) as Grid.FC.Context<TDataUnion>;

export const GridColumnContext =
  createContext<GridColumn.Props<any>>(undefined);
export const useGridColumnContext = <TDataUnion extends Model.IDed>() =>
  useContext(GridColumnContext) as GridColumn.Props<TDataUnion>;

export const GridLevelContext = createContext<GridLevel.Props<any>>(undefined);
export const useGridLevelContext = <
  TData extends Model.IDed,
  TParentData extends Model.IDed = never
>() => useContext(GridLevelContext) as GridLevel.Props<TData, TParentData>;
