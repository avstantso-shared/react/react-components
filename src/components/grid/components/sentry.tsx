import React from 'react';
import { X } from '@avstantso/node-or-browser-js--utils';
import { GridHierarchyError } from '@root/classes';
import { Hierarchy as GridHierarchy, Grid } from '../types';
import {
  useGridContext,
  useGridLevelContext,
  useGridColumnContext,
} from './contexts';

const GridHierarchySentryProd = (): GridHierarchy.Sentry => ({
  Change: X,
  useValidateNestedGrid: X,
  useValidateLevels: X,
  useValidateLevel: X,
  useValidateColumns: X,
  useValidateColumn: X,
  useValidateRow: X,
  useValidateCell: X,
});

const GridHierarchySentryChangeDev: React.FC<
  GridHierarchy.Sentry.Change.Props
> = ({ hierarchy }) => {
  const ctx = useGridContext();
  ctx.hierarchy = hierarchy;
  return null;
};

const componentsMustBePlacedInside = (
  g: Grid.FC.Context<any>,
  children: string | string[],
  parent: string | string[]
) => {
  const arrOrStrToStr = (arrOrStr: string | string[]) =>
    (Array.isArray(arrOrStr) ? arrOrStr : [arrOrStr])
      .map((s) => `<${s}>`)
      .join('/');

  return `Component${Array.isArray(children) ? 's' : ''} ${arrOrStrToStr(
    children
  )} must be placed inside ${arrOrStrToStr(
    parent
  )}, but hierarchy is "${GridHierarchy.toName(g?.hierarchy)}"`;
};

const GridHierarchySentryDev = (): GridHierarchy.Sentry => {
  const useValidateNestedGrid = () => {
    const g = useGridContext();
    if (g)
      throw new GridHierarchyError(
        '<Grid> nesting attempt in another grid <Grid>'
      );

    const l = useGridLevelContext();
    if (l)
      throw new GridHierarchyError(
        '<Grid> nesting attempt in another grid <Grid.Level>'
      );

    const c = useGridColumnContext();
    if (c)
      throw new GridHierarchyError(
        '<Grid> nesting attempt in another grid <Grid.Column>'
      );
  };

  const useValidateLevels = () => {
    const l = useGridLevelContext();
    if (l)
      throw new GridHierarchyError(
        '<Grid.Levels> nesting attempt in <Grid.Level>'
      );

    const c = useGridColumnContext();
    if (c)
      throw new GridHierarchyError(
        '<Grid.Levels> nesting attempt in <Grid.Column>'
      );

    const g = useGridContext();
    if (!g || GridHierarchy.Grid !== g.hierarchy)
      throw new GridHierarchyError(
        componentsMustBePlacedInside(g, 'Grid.Levels', 'Grid')
      );
  };

  const useValidateLevel = () => {
    const l = useGridLevelContext();
    if (l)
      throw new GridHierarchyError(
        '<Grid.Level> nesting attempt in another <Grid.Level>'
      );

    const c = useGridColumnContext();
    if (c)
      throw new GridHierarchyError(
        '<Grid.Level> nesting attempt in <Grid.Column>'
      );

    const g = useGridContext();
    if (!(g && GridHierarchy.Levels === g.hierarchy))
      throw new GridHierarchyError(
        componentsMustBePlacedInside(
          g,
          ['Grid.Level', 'Grid.Level.Defaults', 'Grid.Level.Root'],
          'Grid.Levels'
        )
      );
  };

  const useValidateColumns = () => {
    const l = useGridLevelContext();
    if (l)
      throw new GridHierarchyError(
        '<Grid.Columns> nesting attempt in <Grid.Level>'
      );

    const c = useGridColumnContext();
    if (c)
      throw new GridHierarchyError(
        '<Grid.Columns> nesting attempt in <Grid.Column>'
      );

    const g = useGridContext();
    if (!g || GridHierarchy.Grid !== g.hierarchy)
      throw new GridHierarchyError(
        componentsMustBePlacedInside(g, 'Grid.Columns', 'Grid')
      );
  };

  const useValidateColumn = () => {
    const l = useGridLevelContext();
    if (l)
      throw new GridHierarchyError(
        '<Grid.Column> nesting attempt in <Grid.Level>'
      );

    const c = useGridColumnContext();
    if (c)
      throw new GridHierarchyError(
        '<Grid.Column> nesting attempt in another <Grid.Column>'
      );

    const g = useGridContext();
    if (!(g && GridHierarchy.Columns === g.hierarchy))
      throw new GridHierarchyError(
        componentsMustBePlacedInside(g, 'Grid.Column', 'Grid.Columns')
      );
  };

  const useValidateRow = () => {
    const c = useGridColumnContext();
    if (c)
      throw new GridHierarchyError(
        '<Grid.Level.HederRow>/<Grid.Level.DataRow> nesting attempt in <Grid.Column>'
      );

    const g = useGridContext();
    const l = useGridLevelContext();
    if (!(g && l && GridHierarchy.Level === g.hierarchy))
      throw new GridHierarchyError(
        componentsMustBePlacedInside(
          g,
          ['Grid.Level.HederRow', 'Grid.Level.DataRow'],
          ['Grid.Level', 'Grid.Level.Defaults', 'Grid.Level.Root']
        )
      );
  };

  const useValidateCell = () => {
    const l = useGridLevelContext();
    if (l)
      throw new GridHierarchyError(
        '<Grid.Column.HeaderCell>/<Grid.Column.DataCell> nesting attempt in <Grid.Level>'
      );

    const g = useGridContext();
    const c = useGridColumnContext();
    if (!(g && c && GridHierarchy.Column === g.hierarchy))
      throw new GridHierarchyError(
        componentsMustBePlacedInside(
          g,
          ['Grid.Column.HeaderCell', 'Grid.Column.DataCell'],
          'Grid.Column'
        )
      );
  };

  return {
    Change: GridHierarchySentryChangeDev,
    useValidateNestedGrid,
    useValidateLevels,
    useValidateLevel,
    useValidateColumns,
    useValidateColumn,
    useValidateRow,
    useValidateCell,
  };
};

export default process.env.DEBUG_INFO_ENABLED
  ? GridHierarchySentryDev()
  : GridHierarchySentryProd();
