import type React from 'react';
import type * as ReactBootstrap from 'react-bootstrap';

import type { Model } from '@avstantso/node-or-browser-js--model-core';

import type { ChildrenProps, Filters, Sorts } from '@avstantso/react--data';
import type { DataContainer } from '@components/dataContainer';

import type { Level } from './level';
import type { Column } from './column';
import type { Columns } from './columns-order';
import { Hierarchy } from './hierarchy';

export namespace Grid {
  export namespace Fields {
    export interface Props<TDataUnion extends Model.IDed> {
      sorts?: Sorts<TDataUnion>;
      filters?: Filters<TDataUnion>;
    }
  }

  export namespace Methods {
    export namespace RenderHeader {
      export type Options = {};
    }

    export type RenderHeader = (
      options?: RenderHeader.Options
    ) => React.ReactNode | React.ReactNode[];

    export namespace RenderData {
      export interface Options extends DataContainer.Options {
        readonly noDataContainer?: boolean;
      }
    }

    export type RenderData<TDataUnion extends Model.IDed> = (
      data: ReadonlyArray<TDataUnion>,
      options?: RenderData.Options
    ) => React.ReactNode;
  }

  export interface Provider<TDataUnion extends Model.IDed> {
    grid: Grid<TDataUnion>;
  }

  export type Props<TDataUnion extends Model.IDed> = Fields.Props<TDataUnion>;

  export namespace FC {
    export interface TableProps
      extends ReactBootstrap.TableProps,
        React.RefAttributes<HTMLTableElement> {
      floatHeader?: boolean;
    }

    export interface DataProps<TDataUnion extends Model.IDed> {
      data: ReadonlyArray<TDataUnion>;
    }

    export namespace Internal {
      export type Props<TDataUnion extends Model.IDed> = DataProps<TDataUnion> &
        TableProps &
        ChildrenProps &
        Methods.RenderData.Options &
        Grid.Provider<TDataUnion>;
    }

    export namespace Definer {
      export type Props<TDataUnion extends Model.IDed> = DataProps<TDataUnion> &
        TableProps &
        ChildrenProps &
        Methods.RenderData.Options &
        Grid.Props<TDataUnion>;
    }

    export type Props<TDataUnion extends Model.IDed> =
      | Internal.Props<TDataUnion>
      | Definer.Props<TDataUnion>;

    export interface Typed {
      Typed: <TDataUnion extends Model.IDed>() => FC<TDataUnion>;
    }

    export interface Context<TDataUnion extends Model.IDed> {
      readonly grid: Grid<TDataUnion>;
      hierarchy: Hierarchy;
    }
  }

  export interface FC<TDataUnion extends Model.IDed>
    extends React.FC<FC.Props<TDataUnion>> {
    Levels: React.FC;
    Level: Level.FC<TDataUnion>;
    Columns: React.FC;
    Column: Column.FC<TDataUnion>;
  }
}

export interface Grid<TDataUnion extends Model.IDed> {
  columns: ReadonlyArray<Column<TDataUnion>>;
  levels: ReadonlyArray<Level<TDataUnion, TDataUnion>>;

  setColumnsOrder: Columns.Order.Dispatch<TDataUnion>;

  useColumn: Column.Use<TDataUnion>;

  useLevel: Level.Use<TDataUnion>;
  useColumnsOrder: Columns.Order.Dispatch<TDataUnion>;

  renderHeader: Grid.Methods.RenderHeader;
  renderData: Grid.Methods.RenderData<TDataUnion>;
}
