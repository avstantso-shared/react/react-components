import type React from 'react';
import type { Model } from '@avstantso/node-or-browser-js--model-core';
import type { GridRowProps, Providers, IsHidden } from './misc';

export namespace Abstract {
  export type RowGetMethods<TGetProps> = Providers.GetProps<TGetProps>;

  export type RowMethods<TGetProps, TRender> = RowGetMethods<TGetProps> &
    Providers.Render<TRender>;

  export interface RowsProvider<THeaderRow, TDataRow> {
    headerRow?: THeaderRow;
    dataRow?: TDataRow;
  }
}

export namespace Level {
  export namespace HeaderRow {
    export namespace Fields {
      export type Props = GridRowProps;
    }

    export type Fields = GridRowProps.Provider;

    export namespace Methods {
      export type GetProps = () => GridRowProps;
      export type Render = (
        props: Providers.Cells & Providers.TagProps<GridRowProps>
      ) => React.ReactNode;
    }

    export interface Methods
      extends Abstract.RowMethods<Methods.GetProps, Methods.Render> {
      isHidden?: IsHidden;
    }

    export type Props = Fields.Props & Methods;

    export type FC = React.FC<Props>;
  }

  export type HeaderRow = HeaderRow.Fields & HeaderRow.Methods;

  export namespace DataRow {
    export namespace Fields {
      export type Props = GridRowProps;
    }

    export type Fields = GridRowProps.Provider;

    export namespace Methods {
      export type IsHidden<TData extends Model.IDed> = (
        props: Providers.Data<TData>
      ) => boolean;
      export type GetProps<TData extends Model.IDed> = (
        props: Providers.Data<TData>
      ) => GridRowProps;
      export type Render<TData extends Model.IDed> = (
        props: Providers.Cells &
          Providers.TagProps<GridRowProps> &
          Providers.Data<TData>
      ) => React.ReactNode;
    }

    export interface Methods<TData extends Model.IDed>
      extends Abstract.RowMethods<
        Methods.GetProps<TData>,
        Methods.Render<TData>
      > {
      isHidden?: IsHidden<Methods.IsHidden<TData>>;
    }

    export type Props<TData extends Model.IDed> = Fields.Props & Methods<TData>;

    export type FC<TDataUnion extends Model.IDed> = React.FC<Props<TDataUnion>>;
  }

  export type DataRow<TData extends Model.IDed> = DataRow.Fields &
    DataRow.Methods<TData>;

  export namespace Rows {
    export interface FC<TDataUnion extends Model.IDed> {
      HeaderRow: HeaderRow.FC;
      DataRow: DataRow.FC<TDataUnion>;
    }
  }

  export namespace Root {
    export namespace Fields {
      export type Props = GridRowProps &
        Abstract.RowsProvider<HeaderRow.Fields.Props, DataRow.Fields.Props>;
    }

    export type Fields = GridRowProps.Provider &
      Abstract.RowsProvider<HeaderRow.Fields, DataRow.Fields>;

    export type Methods<TData extends Model.IDed> = Abstract.RowGetMethods<
      DataRow.Methods.GetProps<TData>
    > &
      Abstract.RowsProvider<HeaderRow.Methods, DataRow.Methods<TData>>;

    export type Props<TDataUnion extends Model.IDed> = Fields.Props &
      Methods<TDataUnion> &
      Abstract.RowsProvider<HeaderRow.Props, DataRow.Props<TDataUnion>>;

    export type FC<TDataUnion extends Model.IDed> = React.FC<
      Props<TDataUnion>
    > &
      Rows.FC<TDataUnion>;
  }

  export type Root<TDataUnion extends Model.IDed> = Root.Fields &
    Root.Methods<TDataUnion> &
    Abstract.RowsProvider<HeaderRow, DataRow<TDataUnion>>;

  export namespace Fields {
    export interface Base {
      readonly caption?: string;
    }

    export type Props = Root.Fields.Props & Base;

    export interface Provider {
      level: Fields;
    }
  }

  export type Fields = Root.Fields &
    Fields.Base &
    Readonly<{
      index: number;

      is0: boolean;
      is1: boolean;
      is2: boolean;
      is3: boolean;
      is4: boolean;
      is5: boolean;
      is(...N: number[]): boolean;
    }>;

  export namespace Methods {
    export type GetRows<
      TData extends Model.IDed,
      TParentData extends Model.IDed = never
    > = (parentData: TParentData) => ReadonlyArray<TData>;

    export interface Base<
      TData extends Model.IDed,
      TParentData extends Model.IDed = never
    > extends Root.Methods<TData> {
      getRows: GetRows<TData, TParentData>;
    }

    export type Props<
      TData extends Model.IDed,
      TParentData extends Model.IDed = never
    > = Base<TData, TParentData>;
  }

  export type Methods<
    TData extends Model.IDed,
    TParentData extends Model.IDed = never
  > = Methods.Base<TData, TParentData>;

  export type Base<
    TData extends Model.IDed,
    TParentData extends Model.IDed = never
  > = Fields.Base & Methods.Base<TData, TParentData>;

  export type Props<
    TData extends Model.IDed,
    TParentData extends Model.IDed = never
  > = Fields.Props &
    Methods.Props<TData, TParentData> &
    Base<TData, TParentData> &
    Root.Props<TData> &
    Abstract.RowsProvider<HeaderRow.Props, DataRow.Props<TData>>;

  export interface Use<TDataUnion extends Model.IDed> {
    <
      TData extends Model.IDed = TDataUnion,
      TParentData extends Model.IDed = TDataUnion
    >(
      caption: string,
      props: Props<TData, TParentData>
    ): Level<TData>;
    <
      TData extends Model.IDed = TDataUnion,
      TParentData extends Model.IDed = TDataUnion
    >(
      props: Props<TData, TParentData>
    ): Level<TData>;

    Defaults(props: Root.Props<TDataUnion>): void;
    Root(props: Root.Props<TDataUnion>): void;
  }

  export interface FC<TDataUnion extends Model.IDed>
    extends Rows.FC<TDataUnion> {
    <
      TData extends Model.IDed = TDataUnion,
      TParentData extends Model.IDed = TDataUnion
    >(
      props: Props<TData, TParentData>
    ): JSX.Element;
    Defaults: Root.FC<TDataUnion>;
    Root: Root.FC<TDataUnion>;
  }
}

export type Level<
  TData extends Model.IDed,
  TParentData extends Model.IDed = never
> = Level.Fields &
  Level.Methods<TData, TParentData> &
  Level.Base<TData, TParentData> &
  Level.Root<TData> &
  Abstract.RowsProvider<Level.HeaderRow, Level.DataRow<TData>>;
