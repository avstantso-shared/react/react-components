import type React from 'react';

import type { Model } from '@avstantso/node-or-browser-js--model-core';

import type { Column } from './column';

export namespace Columns {
  export namespace Order {
    export type Item<TDataUnion extends Model.IDed> =
      | Column<TDataUnion>
      | number
      | string;

    export interface Dispatch<TDataUnion extends Model.IDed> {
      (order: Item<TDataUnion>[]): void;
      (...order: Item<TDataUnion>[]): void;
      (order: (prev: Item<TDataUnion>[]) => Item<TDataUnion>[]): void;
    }
  }
}
