import { Enum } from '@avstantso/node-or-browser-js--utils';

import * as _ from './enum';

export namespace Hierarchy {
  export type TypeMap = Enum.TypeMap.Make<
    _.GridHierarchy.Type,
    _.GridHierarchy.Key.List
  >;

  export namespace Sentry {
    export namespace Change {
      export interface Props {
        hierarchy: Hierarchy;
      }

      export type FC = React.FC<Props>;
    }
  }

  export interface Sentry {
    Change: Sentry.Change.FC;

    useValidateNestedGrid(): void;
    useValidateLevels(): void;
    useValidateLevel(): void;
    useValidateColumns(): void;
    useValidateColumn(): void;
    useValidateRow(): void;
    useValidateCell(): void;
  }
}

export type Hierarchy = Enum.Value<Hierarchy.TypeMap>;
export const Hierarchy = Enum(
  _.GridHierarchy,
  'Grid.Hierarchy'
)<Hierarchy.TypeMap>();
