export enum GridHierarchy {
  Grid,
  Levels,
  Level,
  Columns,
  Column,
}

export namespace GridHierarchy {
  export type Type = typeof GridHierarchy;

  export namespace Key {
    export type List = ['Grid', 'Levels', 'Level', 'Columns', 'Column'];
  }

  export type Key = keyof Type;
  export type Value = Type[Key];
}
