import React from 'react';

import { Model } from '@avstantso/node-or-browser-js--model-core';

export type GridRenderUnion = React.ReactNode | React.ReactNode[] | string;

export type GridCellProps = React.HTMLProps<HTMLTableCellElement>;
export namespace GridCellProps {
  export interface Provider {
    cellProps?: GridCellProps;
  }
}

export type GridRowProps = React.HTMLProps<HTMLTableRowElement>;
export namespace GridRowProps {
  export interface Provider {
    rowProps?: GridRowProps;
  }
}

export namespace Providers {
  export interface Data<TData extends Model.IDed> {
    data: TData;
  }

  export interface TagProps<TProps> {
    props: TProps;
  }

  export interface Cells {
    cells: React.ReactNode | React.ReactNode[];
  }

  export interface Content {
    content: GridRenderUnion;
  }

  export interface Sort {
    sort: React.ReactNode;
  }

  export interface Filter {
    filter: React.ReactNode;
  }

  export interface GetProps<TGetProps> {
    getProps?: TGetProps;
  }

  export interface Render<TRender> {
    render?: TRender;
  }
}

export type IsHidden<T extends Function = () => boolean> = boolean | T;
