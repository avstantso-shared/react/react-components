import type React from 'react';

import type { Model } from '@avstantso/node-or-browser-js--model-core';
import type { Filter, Sort } from '@avstantso/react--data';

import type {
  GridRenderUnion,
  GridCellProps,
  Providers,
  IsHidden,
} from './misc';
import type { Level } from './level';

export namespace Abstract {
  export interface CellGetMethods<TGetProps, TGetContent>
    extends Providers.GetProps<TGetProps> {
    getContent?: TGetContent;
  }

  export type CellMethods<TGetProps, TGetContent, TCellRender> = CellGetMethods<
    TGetProps,
    TGetContent
  > &
    Providers.Render<TCellRender>;

  export interface CellsProvider<THeaderCell, TDataCell> {
    headerCell?: THeaderCell;
    dataCell?: TDataCell;
  }
}

export namespace Column {
  export namespace HeaderCell {
    export namespace Fields {
      export interface Base {
        readonly caption?: string | JSX.Element;
      }

      export type Props = Base & GridCellProps;
    }

    export type Fields = Fields.Base & GridCellProps.Provider;

    export namespace Methods {
      export type GetProps = (props: Level.Fields.Provider) => GridCellProps;
      export type GetContent = (
        props: Level.Fields.Provider
      ) => GridRenderUnion;
      export type CellRender<TDataUnion extends Model.IDed> = (
        props: Level.Fields.Provider &
          Column.Fields.Provider<TDataUnion> &
          Providers.TagProps<GridCellProps> &
          Providers.Content &
          Providers.Sort &
          Providers.Filter
      ) => React.ReactNode;
    }

    export type Methods<TDataUnion extends Model.IDed> = Abstract.CellMethods<
      Methods.GetProps,
      Methods.GetContent,
      Methods.CellRender<TDataUnion>
    >;

    export type Props<TDataUnion extends Model.IDed> = Fields.Props &
      Methods<TDataUnion>;

    export type FC<TDataUnion extends Model.IDed> = React.FC<Props<TDataUnion>>;
  }

  export type HeaderCell<TDataUnion extends Model.IDed> = HeaderCell.Fields &
    HeaderCell.Methods<TDataUnion>;

  export namespace DataCell {
    export namespace Fields {
      export type Props = GridCellProps;
    }

    export type Fields = GridCellProps.Provider;

    export namespace Methods {
      export type GetProps<TDataUnion extends Model.IDed> = (
        props: Level.Fields.Provider & Providers.Data<TDataUnion>
      ) => GridCellProps;
      export type GetContent<TDataUnion extends Model.IDed> = (
        props: Level.Fields.Provider & Providers.Data<TDataUnion>
      ) => GridRenderUnion;
      export type CellRender<TDataUnion extends Model.IDed> = (
        props: Level.Fields.Provider &
          Column.Fields.Provider<TDataUnion> &
          Providers.Data<TDataUnion> &
          Providers.TagProps<GridCellProps> &
          Providers.Content
      ) => React.ReactNode;
    }

    export type Methods<TDataUnion extends Model.IDed> = Abstract.CellMethods<
      Methods.GetProps<TDataUnion>,
      Methods.GetContent<TDataUnion>,
      Methods.CellRender<TDataUnion>
    >;

    export type Props<TDataUnion extends Model.IDed> = Fields.Props &
      Methods<TDataUnion>;

    export type FC<TDataUnion extends Model.IDed> = React.FC<Props<TDataUnion>>;
  }

  export type DataCell<TDataUnion extends Model.IDed> = DataCell.Fields &
    DataCell.Methods<TDataUnion>;

  export namespace Fields {
    export type Base<TDataUnion extends Model.IDed> = Readonly<{
      caption?: string;
      stretch?: boolean;
      sortIndex?: number;
      filter?: Readonly<Filter<TDataUnion>>;
    }>;

    export type Props<TDataUnion extends Model.IDed> = GridCellProps &
      Base<TDataUnion> &
      Abstract.CellsProvider<HeaderCell.Fields.Props, DataCell.Fields.Props> &
      Readonly<{
        noFilter?: boolean;
        filterIndex?: number;
        noSort?: boolean;
      }>;

    export interface Provider<TDataUnion extends Model.IDed> {
      column: Fields<TDataUnion>;
    }
  }

  export type Fields<TDataUnion extends Model.IDed> = GridCellProps.Provider &
    Fields.Base<TDataUnion> &
    Abstract.CellsProvider<HeaderCell.Fields, DataCell.Fields> &
    Readonly<{
      index: number;
      sort?: Sort.Column;
    }>;

  export namespace Methods {
    export interface Base<TDataUnion extends Model.IDed>
      extends Abstract.CellGetMethods<
          DataCell.Methods.GetProps<TDataUnion>,
          DataCell.Methods.GetContent<TDataUnion>
        >,
        Abstract.CellsProvider<
          HeaderCell.Methods<TDataUnion>,
          DataCell.Methods<TDataUnion>
        > {
      isHidden?: IsHidden;
    }

    export type Props<TDataUnion extends Model.IDed> = Base<TDataUnion>;
  }

  export type Methods<TDataUnion extends Model.IDed> = Methods.Base<TDataUnion>;

  export type Base<TDataUnion extends Model.IDed> = Fields.Base<TDataUnion> &
    Methods.Base<TDataUnion>;

  export type Props<TDataUnion extends Model.IDed> = Fields.Props<TDataUnion> &
    Methods.Props<TDataUnion> &
    Base<TDataUnion> &
    Abstract.CellsProvider<
      HeaderCell.Props<TDataUnion>,
      DataCell.Props<TDataUnion>
    >;

  export interface Use<TDataUnion extends Model.IDed> {
    (caption: string, props?: Props<TDataUnion>): Column<TDataUnion>;
    (props?: Props<TDataUnion>): Column<TDataUnion>;
  }

  export interface FC<TDataUnion extends Model.IDed>
    extends React.FC<Props<TDataUnion>> {
    HeaderCell: HeaderCell.FC<TDataUnion>;
    DataCell: DataCell.FC<TDataUnion>;
  }
}

export type Column<TDataUnion extends Model.IDed> = Column.Fields<TDataUnion> &
  Column.Methods<TDataUnion> &
  Column.Base<TDataUnion> &
  Abstract.CellsProvider<
    Column.HeaderCell<TDataUnion>,
    Column.DataCell<TDataUnion>
  >;
