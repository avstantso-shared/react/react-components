import React from 'react';

import type { Model } from '@avstantso/node-or-browser-js--model-core';

import type * as Types from './types';
import { GridRootLevel } from './const';
import grid from './components';

export const Grid = grid<any>() as Types.Grid.FC<any> &
  Types.Grid.FC.Typed & { RootLevel: string };
Grid.Typed = grid;
Grid.RootLevel = GridRootLevel;

export namespace Grid {
  export type RenderUnion = Types.GridRenderUnion;

  export namespace Cell {
    export namespace Props {
      export type Provider = Types.GridCellProps.Provider;
    }

    export type Props = Types.GridCellProps;
  }

  export namespace Row {
    export namespace Props {
      export type Provider = Types.GridRowProps.Provider;
    }

    export type Props = Types.GridRowProps;
  }

  export namespace Column {
    export namespace HeaderCell {
      export namespace Fields {
        export type Props = Types.Column.HeaderCell.Fields.Props;
      }
      export type Fields = Types.Column.HeaderCell.Fields;

      export namespace Methods {
        export type GetProps = Types.Column.HeaderCell.Methods.GetProps;
        export type GetContent = Types.Column.HeaderCell.Methods.GetContent;
        export type CellRender<TDataUnion extends Model.IDed> =
          Types.Column.HeaderCell.Methods.CellRender<TDataUnion>;
      }
      export type Methods<TDataUnion extends Model.IDed> =
        Types.Column.HeaderCell.Methods<TDataUnion>;

      export type Props<TDataUnion extends Model.IDed> =
        Types.Column.HeaderCell.Props<TDataUnion>;

      export type FC<TDataUnion extends Model.IDed> =
        Types.Column.HeaderCell.FC<TDataUnion>;
    }
    export type HeaderCell<TDataUnion extends Model.IDed> =
      Types.Column.HeaderCell<TDataUnion>;

    export namespace DataCell {
      export namespace Fields {
        export type Props = Types.Column.DataCell.Fields.Props;
      }
      export type Fields = Types.Column.DataCell.Fields;

      export namespace Methods {
        export type GetProps<TDataUnion extends Model.IDed> =
          Types.Column.DataCell.Methods.GetProps<TDataUnion>;
        export type GetContent<TDataUnion extends Model.IDed> =
          Types.Column.DataCell.Methods.GetContent<TDataUnion>;
        export type CellRender<TDataUnion extends Model.IDed> =
          Types.Column.DataCell.Methods.CellRender<TDataUnion>;
      }
      export type Methods<TDataUnion extends Model.IDed> =
        Types.Column.DataCell.Methods<TDataUnion>;

      export type Props<TDataUnion extends Model.IDed> =
        Types.Column.DataCell.Props<TDataUnion>;

      export type FC<TDataUnion extends Model.IDed> =
        Types.Column.DataCell.FC<TDataUnion>;
    }
    export type DataCell<TDataUnion extends Model.IDed> =
      Types.Column.DataCell<TDataUnion>;

    export namespace Fields {
      export type Props<TDataUnion extends Model.IDed> =
        Types.Column.Fields.Props<TDataUnion>;
      export type Provider<TDataUnion extends Model.IDed> =
        Types.Column.Fields.Provider<TDataUnion>;
    }
    export type Fields<TDataUnion extends Model.IDed> =
      Types.Column.Fields<TDataUnion>;

    export namespace Methods {
      export type Props<TDataUnion extends Model.IDed> =
        Types.Column.Methods.Props<TDataUnion>;
    }
    export type Methods<TDataUnion extends Model.IDed> =
      Types.Column.Methods<TDataUnion>;

    export type Props<TDataUnion extends Model.IDed> =
      Types.Column.Props<TDataUnion>;

    export type Use<TDataUnion extends Model.IDed> =
      Types.Column.Use<TDataUnion>;

    export type FC<TDataUnion extends Model.IDed> = Types.Column.FC<TDataUnion>;
  }
  export type Column<TDataUnion extends Model.IDed> = Types.Column<TDataUnion>;

  export namespace Columns {
    export namespace Order {
      export type Item<TDataUnion extends Model.IDed> =
        Types.Columns.Order.Item<TDataUnion>;

      export type Dispatch<TDataUnion extends Model.IDed> =
        Types.Columns.Order.Dispatch<TDataUnion>;
    }
  }

  export namespace Level {
    export namespace HeaderRow {
      export namespace Fields {
        export type Props = Types.Level.HeaderRow.Fields.Props;
      }
      export type Fields = Types.Level.HeaderRow.Fields;

      export namespace Methods {
        export type GetProps = Types.Level.HeaderRow.Methods.GetProps;
        export type Render = Types.Level.HeaderRow.Methods.Render;
      }

      export type Methods = Types.Level.HeaderRow.Methods;

      export type Props = Types.Level.HeaderRow.Props;

      export type FC = Types.Level.HeaderRow.FC;
    }

    export type HeaderRow = Types.Level.HeaderRow;

    export namespace DataRow {
      export namespace Fields {
        export type Props = Types.Level.DataRow.Fields.Props;
      }
      export type Fields = Types.Level.DataRow.Fields;

      export namespace Methods {
        export type IsHidden<TData extends Model.IDed> =
          Types.Level.DataRow.Methods.IsHidden<TData>;
        export type GetProps<TData extends Model.IDed> =
          Types.Level.DataRow.Methods.GetProps<TData>;
        export type Render<TData extends Model.IDed> =
          Types.Level.DataRow.Methods.Render<TData>;
      }
      export type Methods<TData extends Model.IDed> =
        Types.Level.DataRow.Methods<TData>;

      export type Props<TData extends Model.IDed> =
        Types.Level.DataRow.Props<TData>;

      export type FC<TDataUnion extends Model.IDed> =
        Types.Level.DataRow<TDataUnion>;
    }

    export type DataRow<TData extends Model.IDed> = Types.Level.DataRow<TData>;

    export namespace Rows {
      export type FC<TDataUnion extends Model.IDed> =
        Types.Level.Rows.FC<TDataUnion>;
    }

    export namespace Root {
      export namespace Fields {
        export type Props = Types.Level.Root.Fields.Props;
      }
      export type Fields = Types.Level.Root.Fields;

      export type Methods<TData extends Model.IDed> =
        Types.Level.Root.Methods<TData>;

      export type Props<TDataUnion extends Model.IDed> =
        Types.Level.Root.Props<TDataUnion>;

      export type FC<TDataUnion extends Model.IDed> =
        Types.Level.Root.FC<TDataUnion>;
    }
    export type Root<TDataUnion extends Model.IDed> =
      Types.Level.Root<TDataUnion>;

    export namespace Fields {
      export type Props = Types.Level.Fields.Props;
      export type Provider = Types.Level.Fields.Provider;
    }
    export type Fields = Types.Level.Fields;

    export namespace Methods {
      export type GetRows<
        TData extends Model.IDed,
        TParentData extends Model.IDed = never
      > = Types.Level.Methods.GetRows<TData, TParentData>;

      export type Props<
        TData extends Model.IDed,
        TParentData extends Model.IDed = never
      > = Types.Level.Methods.Props<TData, TParentData>;
    }
    export type Methods<
      TData extends Model.IDed,
      TParentData extends Model.IDed = never
    > = Types.Level.Methods<TData, TParentData>;

    export type Props<
      TData extends Model.IDed,
      TParentData extends Model.IDed = never
    > = Types.Level.Props<TData, TParentData>;

    export type Use<TDataUnion extends Model.IDed> =
      Types.Level.Use<TDataUnion>;

    export type FC<TDataUnion extends Model.IDed> = Types.Level.FC<TDataUnion>;
  }

  export type Level<
    TData extends Model.IDed,
    TParentData extends Model.IDed = never
  > = Types.Level<TData, TParentData>;

  export namespace Fields {
    export type Props<TDataUnion extends Model.IDed> =
      Types.Grid.Fields.Props<TDataUnion>;
  }

  export namespace Methods {
    export namespace RenderHeader {
      export type Options = Types.Grid.Methods.RenderHeader.Options;
    }
    export type RenderHeader = Types.Grid.Methods.RenderHeader;

    export namespace RenderData {
      export type Options = Types.Grid.Methods.RenderData.Options;
    }

    export type RenderData<TDataUnion extends Model.IDed> =
      Types.Grid.Methods.RenderData<TDataUnion>;
  }

  export type Provider<TDataUnion extends Model.IDed> =
    Types.Grid.Provider<TDataUnion>;

  export type Props<TDataUnion extends Model.IDed> =
    Types.Grid.Props<TDataUnion>;

  export namespace FC {
    export type TableProps = Types.Grid.FC.TableProps;

    export type DataProps<TDataUnion extends Model.IDed> =
      Types.Grid.FC.DataProps<TDataUnion>;

    export namespace Internal {
      export type Props<TDataUnion extends Model.IDed> =
        Types.Grid.FC.Internal.Props<TDataUnion>;
    }

    export namespace Definer {
      export type Props<TDataUnion extends Model.IDed> =
        Types.Grid.FC.Definer.Props<TDataUnion>;
    }

    export type Props<TDataUnion extends Model.IDed> =
      Types.Grid.FC.Props<TDataUnion>;

    export type Context<TDataUnion extends Model.IDed> =
      Types.Grid.FC.Context<TDataUnion>;
  }

  export type FC<TDataUnion extends Model.IDed> = Types.Grid.FC<TDataUnion>;
}

export type Grid<TDataUnion extends Model.IDed> = Types.Grid<TDataUnion>;

export { useGrid } from './hooks';
