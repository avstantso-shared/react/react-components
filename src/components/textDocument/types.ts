export interface TextPart {
  header?: JSX.Element | string;
  paragraphs?: (JSX.Element | string)[];
}

export interface TextSection {
  header: JSX.Element | string;
  parts?: TextPart[];
}
