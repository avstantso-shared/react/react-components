import React from 'react';

import type * as Types from './types';
import { TextParagraph } from './paragraph';

export type TextPart = Types.TextPart;

export namespace TextPart {
  export interface Props {
    part: TextPart;
    sectionIndex: number;
    index: number;
    totalNum: number;
  }

  export type FC = React.FC<Props>;
}

export const TextPart: TextPart.FC = (props) => {
  const { part, sectionIndex, index, totalNum } = props;

  const id = `part_${sectionIndex + 1}_${index + 1}`;

  return (
    <>
      {!!part.header && (
        <h4 className="text-document-part-header" id={id}>
          {part.header}
        </h4>
      )}
      {!!part.paragraphs?.length && (
        <ol className="text-document-part-ol">
          {part.paragraphs.map((paragraph, paragraphIndex) => (
            <TextParagraph
              key={`${sectionIndex + 1}_${index + 1}_${paragraphIndex + 1}`}
              paragraph={paragraph}
              sectionIndex={sectionIndex}
              index={paragraphIndex}
              totalNum={totalNum}
            />
          ))}
        </ol>
      )}
    </>
  );
};
