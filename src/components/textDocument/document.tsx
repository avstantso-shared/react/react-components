import React from 'react';

import { TextSection } from './section';
import './document.scss';

export namespace TextDocument {
  export interface Props {
    sections?: TextSection | TextSection[];
  }

  export type FC = React.FC<Props>;
}

export const TextDocument: TextDocument.FC = (props) => {
  const { sections } = props;

  return (
    <section className="text-document">
      {(!sections ? [] : Array.isArray(sections) ? sections : [sections]).map(
        (section, sectionIndex) => {
          let totalNum = 0;

          return (
            <TextSection
              key={sectionIndex + 1}
              section={section}
              index={sectionIndex}
              totalNum={totalNum}
              incTotalNum={(value) => (totalNum += value)}
            />
          );
        }
      )}
    </section>
  );
};
