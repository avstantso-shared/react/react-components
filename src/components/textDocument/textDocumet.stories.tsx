import React from 'react';
import { Meta, Story } from '@story-helpers';

import { TextSection } from './types';
import { TextDocument } from './document';

const meta = { ...Meta(TextDocument) };

export default meta;

const SECTIONS: TextSection[] = [
  {
    header: <>DEFINITION OF CONCEPTS</>,
    parts: [
      {
        paragraphs: [
          <>
            "<b>Law</b>" means the Federal Law of the Russian Federation "On
            personal data" with all changes and additions, as well as other
            legislative acts of the Russian Federation.
          </>,
          <>
            "<b>Controller</b>" means the person who is responsible for
            processing and protection of personal data of Users located on
            territory of the EU within the meaning of the General Regulation for
            the Protection of Personal data (General Data Protection Regulation)
            dated April 27, 2016 (hereinafter "<b>GDRP</b>").
          </>,
        ],
      },
    ],
  },
  {
    header: <>RELATIONSHIPS TO WHICH THE POLICY APPLYS</>,
    parts: [
      {
        header: <>General provisions</>,
        paragraphs: [
          <>
            This Policy is used and applicable solely to Personal data received
            from the User in connection with their use of the Site. The
            provisions of this Policy are aimed at: ...
          </>,
        ],
      },
    ],
  },
];

export const textDocumet = Story(meta, {
  args: {
    sections: SECTIONS,
  },
});
