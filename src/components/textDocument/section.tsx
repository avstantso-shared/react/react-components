import React from 'react';

import type * as Types from './types';
import { TextPart } from './part';

export type TextSection = Types.TextSection;

export namespace TextSection {
  export interface Props {
    section: Types.TextSection;
    index: number;
    totalNum: number;
    incTotalNum(value: number): number;
  }

  export type FC = React.FC<Props>;
}

export const TextSection: TextSection.FC = (props) => {
  const { section, index, incTotalNum } = props;

  const id = `section_${index + 1}`;

  let totalNum = props.totalNum;

  return (
    <section className="text-document-section">
      <h3 className="section-header" id={id}>
        {index + 1}. {section.header}
      </h3>
      {(section.parts || []).map((part, partIndex) => {
        const partContent = (
          <TextPart
            key={`${index + 1}_${partIndex + 1}`}
            part={part}
            sectionIndex={index}
            index={partIndex}
            totalNum={totalNum}
          />
        );

        totalNum = incTotalNum((part.paragraphs || []).length);

        return partContent;
      })}
    </section>
  );
};
