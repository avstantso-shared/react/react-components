import React from 'react';

export namespace TextParagraph {
  export interface Props {
    paragraph: JSX.Element | string;
    sectionIndex: number;
    index: number;
    totalNum: number;
  }

  export type FC = React.FC<Props>;
}

export const TextParagraph: TextParagraph.FC = (props) => {
  const { paragraph, sectionIndex, index, totalNum } = props;

  const id = `paragraph_${sectionIndex + 1}_${totalNum + index + 1}_${
    index + 1
  }`;

  return (
    <li
      className="text-document-paragraph"
      id={id}
      value={sectionIndex * 1000 + totalNum + index + 1}
    >
      {paragraph}
    </li>
  );
};
