import type React from 'react';

import { GFC } from '@avstantso/react--data';

export namespace DateText {
  export type Value = string | Date;
  export type Format = {
    /**
     * @default `L`
     */
    format?: string;
  };

  export namespace Props {
    export type Base = { date?: Value } & Format;
  }

  export type Props<T extends React.ElementType = typeof React.Fragment> =
    GFC.AsElement.Fragment<T> & Props.Base;
  export type FC = GFC.AsElement.Fragment.FC<Props.Base>;

  export namespace Range {
    export namespace Props {
      export type Base = {
        from?: Value;
        to?: Value;
      } & Format;
    }

    export type Props<T extends React.ElementType = typeof React.Fragment> =
      GFC.AsElement.Fragment<T> & Props.Base;
    export type FC = GFC.AsElement.Fragment.FC<Props.Base>;
  }
}
