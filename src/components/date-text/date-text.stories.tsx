import React from 'react';
import { Meta, Story } from '@story-helpers';

import { DateText } from './index';

const meta = {
  ...Meta(DateText),
};

export default meta;

export const datesRangeText = Story(meta, {
  render: (args) => {
    return (
      <ul>
        <li>
          <DateText />
        </li>
        <li>
          <DateText<'span'> as="span" date="10.10.2000" title="YYYYY">
            XXXXX
          </DateText>
        </li>
        <li>
          <DateText format="LLLL" date={new Date()} />
        </li>
        <hr />
        <li>
          <DateText.Range from="10.10.2000" />
        </li>
        <li>
          <DateText.Range to="11.11.2011" />
        </li>
        <li>
          <DateText.Range<'div'> as="div" from="10.10.2000" to="11.11.2011">
            , CEO
          </DateText.Range>
        </li>
      </ul>
    );
  },
});
