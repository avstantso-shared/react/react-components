import React from 'react';

import { GFC } from '@avstantso/react--data';

import { DateText as Types } from './types';
import { DateText } from './text';

const DateTextRangeAs = GFC.AsElement.Fragment('date-text-range');

export const DateTextRange: Types.Range.FC = (props) => {
  const { children, from, to, format, ...rest } = props;

  return (
    <DateTextRangeAs {...rest}>
      <DateText {...{ format }} date={from} />
      {' — '}
      <DateText {...{ format }} date={to} />
      {children}
    </DateTextRangeAs>
  );
};
