import React from 'react';
import moment from 'moment';

import { GFC } from '@avstantso/react--data';

import { LOCALES, useTranslation } from '@i18n';

import { DateText as Types } from './types';

const DateTextAs = GFC.AsElement.Fragment('date-text');

export const DateText: Types.FC = (props) => {
  const { children, date, format = 'L', ...rest } = props;

  const [t, { language }] = useTranslation();

  const value = !date
    ? t(LOCALES.now)
    : moment(date).locale(language).format(format);

  return (
    <DateTextAs {...rest}>
      {value}
      {children}
    </DateTextAs>
  );
};
