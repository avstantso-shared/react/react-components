import React from 'react';

import { Extend } from '@avstantso/node-or-browser-js--utils';

import { DateText as Types } from './types';
import { DateText as _DateText } from './text';
import { DateTextRange as Range } from './range';
import './date-text.scss';

/**
 * @summary Show date or date string in locale format.
 */
export namespace DateText {
  export type Value = Types.Value;
  export type Format = Types.Format;

  export type Props<T extends React.ElementType = typeof React.Fragment> =
    Types.Props<T>;
  export type FC = Types.FC;

  export namespace Range {
    export type Props<T extends React.ElementType = typeof React.Fragment> =
      Types.Range.Props<T>;

    export type FC = Types.Range.FC;
  }
}

export const DateText = Extend.Arbitrary(_DateText, {
  Range,
});
