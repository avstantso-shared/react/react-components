import React, { useEffect, useRef, useState } from 'react';

import { Perform, Canceler, Stub } from '@avstantso/node-or-browser-js--utils';

import { useViewport } from '@root/components/viewport';

export namespace LateLoader {
  export type ComponentInEffect<TData, TProps extends {}> = React.FC<
    { data: TData } & TProps
  >;

  export type Props<TData extends Perform.Able, TProps extends {} = {}> = {
    setLoading: React.Dispatch<React.SetStateAction<boolean>>;
    prepareData: Perform.Promise<TData, [Canceler.Context]>;
    ComponentInEffect: ComponentInEffect<TData, TProps>;
    Placeholder?: React.FC;
    props?: TProps;
    deps?: React.DependencyList;
  };
}

export function LateLoader<TData extends Perform.Able, TProps extends {} = {}>({
  setLoading,
  prepareData,
  ComponentInEffect,
  Placeholder,
  props,
  deps = [],
}: LateLoader.Props<TData, TProps>) {
  const [data, setData] = useState<TData>();
  const contentRef = useRef<JSX.Element>();

  const { cursor } = useViewport();
  const [release, wait] = Stub(cursor?.release, cursor?.acquire?.wait);

  useEffect(() => {
    if (data) return;

    const { cancel, context } = Canceler();

    let id: number;

    async function prepare() {
      id = wait();
      try {
        const d = await Perform.Promise(prepareData, context);

        context.safe(setData)(d);
      } finally {
        release(id);
      }
    }

    prepare();

    return () => {
      cancel();
      release(id);
    };
  }, []);

  useEffect(() => {
    const id = wait();
    try {
      contentRef.current = data && (
        <ComponentInEffect {...{ data, ...props }} />
      );

      setLoading(!contentRef.current);
    } finally {
      release(id);
    }
  }, [data, ...deps]);

  return contentRef.current || (Placeholder && <Placeholder />) || null;
}
