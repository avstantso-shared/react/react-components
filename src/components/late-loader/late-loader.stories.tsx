import React from 'react';
import { Meta, Story } from '@story-helpers';
import { useState } from '@storybook/preview-api';
import { Placeholder as RBPlaceholder, Spinner } from 'react-bootstrap';

import { LateLoader } from '.';

const meta = { ...Meta(LateLoader) };

export default meta;

const ComponentInEffect: React.FC<{ data: number[] } & { factor: number }> = ({
  data,
  factor = 1,
}) => {
  return (
    <ul>
      {data.map((item, key) => (
        <li {...{ key }}>{item * factor}</li>
      ))}
    </ul>
  );
};

const Placeholder: React.FC = () => (
  <RBPlaceholder as="ul" animation="glow">
    {Array.from({ length: 3 }).map((v, key) => (
      <li key={key}>
        <RBPlaceholder xs="2" />
      </li>
    ))}
  </RBPlaceholder>
);

const timeout = 3000;

const prepareData = new Promise<number[]>((resolve) =>
  setTimeout(() => resolve([1, 3, 5, 7, 9]), timeout)
);

export const lateLoader = Story(meta, {
  render: () => {
    const [loading, setLoading] = useState(true);

    const [factor, setFactor] = useState(1);

    return (
      <>
        {loading ? (
          <>
            <Spinner variant="primary" size="sm" />
            {' Loading'}&hellip;{` (${timeout} msec)`}
          </>
        ) : (
          <>
            Current factor is <b>{factor}.</b>
          </>
        )}
        <LateLoader
          {...{
            setLoading,
            prepareData,
            ComponentInEffect,
            Placeholder,
            props: { factor },
            deps: [factor],
          }}
        />
        {!loading && (
          <button onClick={() => setFactor(factor + 1)}>Inc factor</button>
        )}
      </>
    );
  },
});
