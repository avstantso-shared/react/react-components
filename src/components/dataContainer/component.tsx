import React from 'react';
import { Alert } from 'react-bootstrap';

import { Trans, LOCALES } from '@i18n';
import { Loading } from '@components/loading';

export namespace DataContainer {
  export type Options = {
    readonly loading?: boolean;
    readonly customLoading?: React.ReactNode;
    readonly customAlert?: React.ReactNode;
  };

  export type Props<T = any> = Options & {
    readonly data: Readonly<T>;
    readonly inTableCols?: number;
  };

  export type FC = <T = any>(
    props: React.PropsWithChildren<Props<T>>
  ) => JSX.Element;
}

export const DataContainer: DataContainer.FC = (props) => {
  const { children, data, inTableCols, loading, customLoading, customAlert } =
    props;

  if (!data || (Array.isArray(data) && !data.length)) {
    const content = loading ? (
      undefined !== customLoading ? (
        customLoading
      ) : (
        <Loading.Text />
      )
    ) : undefined !== customAlert ? (
      customAlert
    ) : (
      <Alert variant="warning">
        <Trans i18nKey={LOCALES.noData} />
        &hellip;
      </Alert>
    );

    return !content ? null : inTableCols ? (
      <tr>
        <td colSpan={inTableCols}>{content}</td>
      </tr>
    ) : (
      <>{content}</>
    );
  }

  return <>{children}</>;
};
