import React from 'react';
import { Meta, Story } from '@story-helpers';
import { Table } from 'react-bootstrap';

import { DataContainer } from '.';

const meta = { ...Meta(DataContainer) };

export default meta;

export const notInTable = Story(meta, {
  args: {
    data: '123',
  },
  render: (args) => (
    <DataContainer {...args}>{() => <>Data:{args.data}</>}</DataContainer>
  ),
});

export const inTable = Story(meta, {
  args: {
    data: '123',
    inTableCols: 1,
  },
  render: (args) => (
    <Table responsive>
      <thead>
        <tr>
          <th>col 1</th>
          <th>col 2</th>
          <th>col 3</th>
        </tr>
      </thead>
      <tbody>
        <DataContainer {...args}>
          {() => (
            <tr>
              <td colSpan={args.inTableCols}>Data:{args.data}</td>
            </tr>
          )}
        </DataContainer>
      </tbody>
    </Table>
  ),
});
