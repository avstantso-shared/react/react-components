import React from 'react';

import { useTranslation } from '@i18n';

import * as Types from './types';
import { MakeLingual } from './lingual';
import { useSelect } from './select-hook';

export namespace Lang {
  export type Content<T> = Types.Content<T>;

  export namespace UseSelect {
    export type Options = Types.UseSelect.Options;
  }

  export type UseSelect = Types.UseSelect;

  export type Props = Types.Props;
  export type FC = Types.FC;
}

export const Lang: Lang.FC = ({ children, en, ru }) => {
  const [, i18n] = useTranslation();

  const content = useSelect(en, ru);

  return (
    <>
      {content}
      {children && children(i18n.language)}
    </>
  );
};

Lang.En = MakeLingual('en');
Lang.Ru = MakeLingual('ru');

Lang.useSelect = useSelect;
