import React from 'react';

import type { LocalString } from '@avstantso/node-or-browser-js--model-core';

import { DynamicFC } from '@avstantso/react--data';

import { useTranslation } from '@i18n';

export function MakeLingual(language: LocalString.Language): React.FC {
  return DynamicFC(language.toCapitalized(), ({ children }) => {
    const [, i18n] = useTranslation();
    return i18n.language === language ? <>{children}</> : null;
  });
}
