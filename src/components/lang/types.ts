import type React from 'react';

import type { TS } from '@avstantso/node-or-browser-js--utils';
import type { LocalString } from '@avstantso/node-or-browser-js--model-core';

export namespace Content {
  export type Arr<T> = TS.Array.Create<
    TS.Array.Length<LocalString.Languages>,
    T
  >;
}

export type Content<T> = Record<Capitalize<LocalString.Language>, T>;

export namespace UseSelect {
  export type Options = {
    lang?: string;
  };
}

export type UseSelect = {
  <T extends string>(content: Content<T> | LocalString | T): T;
  <T>(...params: [...Content.Arr<T>, UseSelect.Options?]): T;
  <T>(
    content: Content<T> | (T extends string ? LocalString : never),
    options?: UseSelect.Options
  ): T;
};

export type Props = {
  [K in LocalString.Language]?: React.ReactNode | React.ReactNode[];
} & {
  children?: (lang: string) => React.ReactNode | React.ReactNode[];
};

export type FC = React.FC<Props> &
  Content<React.FC> & {
    useSelect: UseSelect;
  };
