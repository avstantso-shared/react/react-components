import { JS } from '@avstantso/node-or-browser-js--utils';
import { InvalidArgumentError } from '@avstantso/node-or-browser-js--errors';
import { LocalString } from '@avstantso/node-or-browser-js--model-core';

import { useTranslation } from '@i18n';

import { Content, UseSelect } from './types';

export const useSelect: UseSelect = <T>(...params: any[]): T => {
  const [, i18n] = useTranslation();

  if (params.length < 1)
    throw new InvalidArgumentError(
      `Lang.${useSelect.name} must have 1 or greater than params`
    );

  const isContentObj = (() => {
    if (!params[0] || !JS.is.object<any>(params[0])) return false;

    if (LocalString.is(params[0])) return true;

    for (let i = 0; i < LocalString.Languages.length; i++)
      if (LocalString.Languages[i].toCapitalized() in params[0]) return true;

    return false;
  })();

  // unlocalized case
  if (!isContentObj && 1 === params.length) return params[0];

  const content: Content<T> = (() => {
    if (isContentObj) return params[0];

    const r: any = {};
    for (let i = 0; i < LocalString.Languages.length; i++)
      r[LocalString.Languages[i].toCapitalized()] = params[i];

    return r;
  })();

  const options: UseSelect.Options =
    (isContentObj ? params[1] : params[params.length - 1]) || {};

  const lang = options.lang || i18n.language;

  let langContent = JS.get.raw(content, lang.toCapitalized());
  if (undefined === langContent) langContent = JS.get.raw(content, lang);

  return langContent;
};
