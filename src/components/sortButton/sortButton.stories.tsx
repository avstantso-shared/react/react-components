import React from 'react';
import { Meta, Story } from '@story-helpers';

import { Generics, JS } from '@avstantso/node-or-browser-js--utils';
import { Sort } from '@avstantso/react--data';

import { SortButton } from '.';

const meta = {
  ...Meta(SortButton, {
    argTypes: {
      sortKind: {
        options: Sort.Kind._names,
        control: { type: 'radio' },
      },
    },
  }),
};

export default meta;

export const single = Story(meta, {
  args: {
    sortKind: Generics.Cast.To(Sort.Kind.asc.name),
    onClick: () => alert('Click!'),
  },
  render: (args) => {
    const { sortKind, ...rest } = args;
    if (!JS.is.string(sortKind)) return <SortButton {...args} />;

    return (
      <SortButton
        sortKind={Sort.Kind.fromString(sortKind as string)}
        {...rest}
      />
    );
  },
});

export const multiple = {
  ...single,
  args: {
    sortKind: Generics.Cast.To(Sort.Kind.asc.name),
    sortIndex: 0,
    onClick: () => alert('Click!'),
  },
};
