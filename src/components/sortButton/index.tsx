import React from 'react';

import { JS } from '@avstantso/node-or-browser-js--utils';

import { Sort } from '@avstantso/react--data';

import { Fa } from '@instances';

import './index.scss';

const { FaSort, FaSortDown, FaSortUp } = Fa;

export namespace SortButton {
  export interface Props {
    sortKind: Sort.Kind;
    sortIndex?: number;
    onClick?: React.MouseEventHandler;
  }

  export type FC = React.FC<Props>;
}

export const SortButton: SortButton.FC = (props) => {
  const { sortKind, sortIndex, onClick } = props;

  const Icon: React.FC = () =>
    Sort.Kind.switch(sortKind, {
      asc: <FaSortDown />,
      desc: <FaSortUp />,
      default: <FaSort />,
    });

  return (
    <span className="sort-button" onClick={onClick}>
      <Icon />
      {JS.is.number(sortIndex) && <sub>{sortIndex}</sub>}
    </span>
  );
};
