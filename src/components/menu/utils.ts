import { useContext } from 'react';
import { JS } from '@avstantso/node-or-browser-js--utils';

import type { Common } from './types';
import { MenuFragmentContext } from './context';

export function useMergeProps<TProps extends Common.Props>(
  props: TProps
): TProps {
  const context = useContext(MenuFragmentContext) || {};

  return { ...context, ...props } as TProps;
}

export function isVisible<TProps extends Common.Props>(props: TProps): boolean {
  return (
    !('visible' in props) ||
    (JS.is.function(props.visible) ? props.visible(props) : props.visible)
  );
}

export function clacCaption<TProps extends Common.Props>(
  props: TProps
): React.ReactNode {
  const { caption, captI18nKey, t } = props;

  if (JS.is.function(caption)) return caption(props);

  if (caption && !JS.is.function(caption)) return caption;

  return caption || (t && captI18nKey && t(captI18nKey));
}
