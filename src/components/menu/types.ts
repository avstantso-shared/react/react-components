import type React from 'react';
import type { NavDropdownProps } from 'react-bootstrap';
import type { SubMenuProps, MenuItemProps } from 'react-pro-sidebar';

import type { PathXorNode, PathUrlXorNode } from '@types';
import type { T } from '@i18n';

export type VisibleCallBack = (props: Common.Props) => boolean;
export type CaptionCallBack = (props: Common.Props) => React.ReactNode;
export type RenderCallBack = (
  props: Common.Props & { Default: React.FC }
) => JSX.Element;

export namespace Common {
  export type Props = Fragment & {
    id?: string;
    icon?: JSX.Element;
    caption?: React.ReactNode | CaptionCallBack;
    captI18nKey?: string;
    visible?: boolean | VisibleCallBack;
    render?: RenderCallBack;
    hideNotification?: boolean;
  };
}

export namespace Fragment {
  export type Outer = {
    isNav?: boolean;
    isDropdown?: boolean;
  };

  export namespace Fixed {
    export type Props = Common.Props &
      Omit<NavDropdownProps, 'title'> &
      SubMenuProps;
  }

  export type Props = Fixed.Props & Partial<PathXorNode>;

  export type FC = React.FC<Props>;
}

export type Fragment = Fragment.Outer & {
  t?: T;
  disabled?: boolean;
};

export namespace Item {
  export namespace Fixed {
    export type Props = Common.Props &
      MenuItemProps & {
        onClick?: React.MouseEventHandler;
        children?: React.ReactNode;
      };
  }

  export type Props = Fixed.Props & Partial<PathUrlXorNode>;
}
