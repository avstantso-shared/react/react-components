import React from 'react';
import { Notifications } from '@components/notifications';

import type { Common } from './types';

export namespace NtfSign {
  export type Props = Pick<Notifications.Sign.Props, 'path'> &
    Pick<Common.Props, 'hideNotification'>;

  export type FC = React.FC<Props>;
}

export const NtfSign: NtfSign.FC = ({ hideNotification, path }) =>
  !hideNotification && path ? (
    <Notifications.Sign {...{ path }} withPrefix size={-2} />
  ) : null;
