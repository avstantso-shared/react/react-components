import React from 'react';
import { NavDropdown } from 'react-bootstrap';
import { SubMenu } from 'react-pro-sidebar';

import type { Fragment } from './types';
import { MenuFragmentContext } from './context';
import { clacCaption, isVisible, useMergeProps } from './utils';
import { NtfSign } from './ntf-sign';

export const MenuFragment: Fragment.FC = (props) => {
  const mergedProps = useMergeProps(props);

  if (!isVisible(mergedProps)) return null;

  const {
    isNav,
    isDropdown,
    t,
    icon,
    children,
    node,
    path = node?._path,
    caption,
    render,
    hideNotification,
    active,
    open,
    onOpenChange,
    disabled,
    ...rest
  } = mergedProps;

  const MenuFragmentDefault: React.FC = ({
    children: defChildren = children,
  }) => {
    const ntfSign = <NtfSign {...{ hideNotification, path }} />;

    return (
      <MenuFragmentContext.Provider value={{ isNav, isDropdown, t }}>
        {!isNav ? (
          <SubMenu
            label={clacCaption(mergedProps)}
            suffix={ntfSign}
            {...{ icon, active, open, onOpenChange, disabled }}
          >
            {defChildren}
          </SubMenu>
        ) : isDropdown ? (
          <NavDropdown
            title={
              <>
                {clacCaption(mergedProps)}
                {ntfSign}
              </>
            }
            {...rest}
            {...{ disabled }}
          >
            {defChildren}
          </NavDropdown>
        ) : (
          <>
            {defChildren}
            {ntfSign}
          </>
        )}
      </MenuFragmentContext.Provider>
    );
  };

  return !render ? (
    <MenuFragmentDefault />
  ) : (
    render({ ...mergedProps, Default: MenuFragmentDefault })
  );
};
