import type * as Types from './types';
import { MenuFragmentContext as Context } from './context';
import { MenuFragmentProvider as Provider } from './provider';
import { MenuFragment as Fragment } from './fragment';
import { MenuItem as Item } from './item';
import './menu.scss';

export namespace Menu {
  export type VisibleCallBack = Types.VisibleCallBack;
  export type CaptionCallBack = Types.CaptionCallBack;
  export type RenderCallBack = Types.RenderCallBack;

  export namespace Fragment {
    export type Outer = Types.Fragment.Outer;

    export namespace Fixed {
      export type Props = Types.Fragment.Fixed.Props;
    }

    export type Props = Types.Fragment.Props;
    export type FC = Types.Fragment.FC;
  }

  export type Fragment = Types.Fragment;

  export namespace Item {
    export namespace Fixed {
      export type Props = Types.Item.Fixed.Props;
    }
    export type Props = Types.Item.Props;
  }
}

export const Menu = { Context, Provider, Fragment, Item };
