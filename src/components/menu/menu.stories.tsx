import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Nav, Navbar } from 'react-bootstrap';
import { Menu as ProMenu, Sidebar } from 'react-pro-sidebar';
import { Meta, Story } from '@story-helpers';

import { JS } from '@avstantso/node-or-browser-js--utils';

import { Fa } from '@instances';

import { Menu } from '.';

const meta = {
  ...Meta(Menu.Item, {
    title: 'MenuItem',
    argTypes: {
      icon: {
        control: 'select',
        options: Object.keys(Fa),
      },
    },
  }),
};

export default meta;

export const menuItem = Story(meta, {
  args: {
    caption: 'MenuItem',
    url: 'gotourl',
    isNav: true,
    isDropdown: true,
    icon: 'FaCat' as any,
  },
  render: (args) => {
    const { isNav } = args;

    const Icon: React.FC = JS.get.raw(Fa, `${args.icon}`);
    JS.patch.raw(args, {
      icon: Icon && <Icon />,
    });

    return (
      <BrowserRouter>
        {isNav ? (
          <Navbar expand="lg">
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="me-auto">
                <Menu.Item {...args} />
                <Menu.Item {...args} />
              </Nav>
            </Navbar.Collapse>
          </Navbar>
        ) : (
          <Sidebar className="sidebar" breakPoint="md">
            <ProMenu>
              <Menu.Item {...args} />
              <Menu.Item {...args} />
            </ProMenu>
          </Sidebar>
        )}
      </BrowserRouter>
    );
  },
});
