import React from 'react';

import type { Fragment } from './types';
import { MenuFragmentContext } from './context';

export const MenuFragmentProvider: React.FC<Fragment> = ({
  children,
  ...rest
}) => <MenuFragmentContext.Provider value={rest} {...{ children }} />;
