import { createContext } from 'react';
import type { Fragment } from './types';

export const MenuFragmentContext = createContext<Fragment>({});
