import React from 'react';
import { Link } from 'react-router-dom';
import { Nav, NavDropdown } from 'react-bootstrap';
import * as ProSidebar from 'react-pro-sidebar';

import { HTMLUtils } from '@avstantso/node-or-browser-js--utils';

import type * as Menu from './types';
import { clacCaption, isVisible, useMergeProps } from './utils';
import { NtfSign } from './ntf-sign';

export const MenuItem: React.FC<Menu.Item.Props> = (props) => {
  const mergedProps = useMergeProps(props);

  if (!isVisible(mergedProps)) return null;

  const {
    isNav,
    isDropdown,
    node,
    path = node?._path,
    url = node?._url,
    id = path && HTMLUtils.toValidId(path),
    onClick,
    icon,
    children,
    render,
    hideNotification,
    target = '_blank',
    rel = 'noopener noreferrer',
    active,
    disabled,
  } = mergedProps;

  const text = clacCaption(mergedProps);

  const MenuItemDefault: React.FC = ({ children: defChildren = children }) => {
    const ntfSign = <NtfSign {...{ hideNotification, path }} />;

    if (!isNav) {
      const sUrl = `${url || ''}`;

      return (
        <ProSidebar.MenuItem
          {...{
            id,
            icon,
            onClick,
            active,
            disabled,
            ...(url
              ? sUrl.startsWith('/')
                ? { component: <Link to={url} /> }
                : {
                    href: sUrl,
                    target,
                    rel,
                  }
              : {}),
          }}
          suffix={ntfSign}
        >
          {text}
          {defChildren}
        </ProSidebar.MenuItem>
      );
    }
    const Cmp = isDropdown ? NavDropdown.Item : Nav.Link;

    return (
      <Cmp
        as={Link}
        to={url}
        {...{ id, onClick, disabled }}
        className="menu-item"
      >
        {icon}
        <span className="menu-text">
          {text}
          {children}
          {ntfSign}
        </span>
      </Cmp>
    );
  };

  return !render ? (
    <MenuItemDefault />
  ) : (
    render({ ...mergedProps, Default: MenuItemDefault })
  );
};
