import React from 'react';

import { Combiner } from '@avstantso/react--data';

import type { Schema } from './types';
import { SchemaLevel } from './level';

export const SchemaBlock: Schema.Block.FC = ({
  className,
  children,
  icon,
  caption,
  ...rest
}) => (
  <SchemaLevel {...Combiner.className('schema-block', className)} {...rest}>
    {(icon || caption) && (
      <div className="schema-block-caption">
        {icon && <div className="schema-block-icon">{icon}</div>}
        {caption && <div className="schema-block-caption-text">{caption}</div>}
      </div>
    )}
    {children && <ul>{children}</ul>}
  </SchemaLevel>
);
