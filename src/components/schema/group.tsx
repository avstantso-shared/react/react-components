import React from 'react';

import { Combiner } from '@avstantso/react--data';

import type { Schema } from './types';
import { SchemaLevel } from './level';

export const SchemaGroup: Schema.Group.FC = ({
  className,
  children,
  icon,
  header: outerHeader,
  ...rest
}) => {
  const header = (icon || outerHeader) && (
    <header className="schema-group-header">
      {icon}
      {icon && outerHeader && <>&nbsp;</>}
      {outerHeader}
    </header>
  );

  return (
    <SchemaLevel
      {...Combiner.className('schema-group', className)}
      {...rest}
      {...{ header }}
    >
      {children && <ul>{children}</ul>}
    </SchemaLevel>
  );
};
