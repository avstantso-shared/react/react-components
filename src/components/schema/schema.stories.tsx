import React from 'react';

import { Meta, Story } from '@story-helpers';

import { JS } from '@avstantso/node-or-browser-js--utils';

import { Fa } from '@instances';

import { Schema } from '.';

const { FaFrog, FaHorse, FaHome, FaCat, FaDog } = Fa;

const meta = {
  ...Meta(Schema),
};

export default meta;

export const schema = Story(meta, {
  args: {},
  render: (args) => {
    return (
      <Schema>
        <Schema.Group icon={<FaFrog />} header="Animals">
          <Schema.Group icon={<FaHorse />} header="Mammals">
            <Schema.Group icon={<FaHome />} header="Pets">
              <Schema.Block icon={<FaCat />} caption="Cat">
                There is a cat
              </Schema.Block>

              <Schema.Block icon={<FaDog />} caption="Dog">
                There is a dog
              </Schema.Block>
            </Schema.Group>
          </Schema.Group>
        </Schema.Group>
      </Schema>
    );
  },
});
