import { Extend } from '@avstantso/node-or-browser-js--utils';

import type { Schema as Types } from './types';

import { Schema as _Schema } from './schema';
import { SchemaContext as Context } from './context';
import { SchemaGroup as Group } from './group';
import { SchemaBlock as Block } from './block';

import './schema.scss';

export namespace Schema {
  export type Context = Types.Context;

  export namespace Group {
    export type Props = Types.Group.Props;

    export type FC = Types.Group.FC;
  }

  export namespace Block {
    export type Props = Types.Block.Props;

    export type FC = Types.Block.FC;
  }

  export type Props = Types.Props;

  export type FC = Types.FC & {
    Group: Group.FC;
    Block: Block.FC;
    Context: typeof Context;
  };
}

export const Schema: Schema.FC = Extend.Arbitrary(_Schema, {
  Context,
  Group,
  Block,
});
