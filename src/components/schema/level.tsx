import React, { useContext } from 'react';

import { Combiner } from '@avstantso/react--data';

import type { Schema } from './types';
import { SchemaContext } from './context';

export const SchemaLevel: Schema.Level.FC = ({
  className,
  children,
  header = null,
  style,
  ...props
}) => {
  const { level = 0, ...ctx } = useContext(SchemaContext) || {};

  return (
    <SchemaContext.Provider value={{ level: level + 1, ...ctx }}>
      <li
        {...Combiner.className(
          'schema-level',
          `schema-level-${level}`,
          1 === level % 2 && 'schema-level-odd',
          className
        )}
        {...Combiner.style(
          { '--schema-level': level } as React.CSSProperties,
          style
        )}
        {...props}
      >
        {header}
        {children && <div className="schema-level-body"> {children}</div>}
      </li>
    </SchemaContext.Provider>
  );
};
