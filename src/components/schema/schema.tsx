import React from 'react';

import { Combiner } from '@avstantso/react--data';

import type { Schema as Types } from './types';
import { SchemaContext } from './context';

export const Schema: Types.FC = ({ className, children, ...rest }) => (
  <SchemaContext.Provider value={{ level: 0 }}>
    <section {...Combiner.className('schema', className)} {...rest}>
      {children && <ul>{children}</ul>}
    </section>
  </SchemaContext.Provider>
);
