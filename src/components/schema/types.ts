import React from 'react';

export namespace Schema {
  export type Context = { level: number };

  export namespace Level {
    export type Props = React.HTMLProps<HTMLLIElement>;

    export type FC = React.FC<Props & { header?: React.ReactNode }>;
  }

  export namespace Group {
    export type Props = Level.Props & {
      icon?: React.ReactNode;
      header: React.ReactNode;
    };

    export type FC = React.FC<Props>;
  }

  export namespace Block {
    export type Props = Level.Props & {
      icon?: React.ReactNode;
      caption: React.ReactNode;
    };

    export type FC = React.FC<Props>;
  }

  export type Props = React.HTMLProps<HTMLDivElement>;

  export type FC = React.FC<Props>;
}
