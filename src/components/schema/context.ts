import { createContext } from 'react';
import type { Schema } from './types';

export const SchemaContext = createContext<Schema.Context>(undefined);
