import React from 'react';

import { Trans, LOCALES } from '@i18n';

import type { Captcha as Types } from './types';

const Captcha: Types.FC = (props) => {
  const { captcha } = props;
  return (
    <div className="img-c-container">
      {captcha.isLoading ? (
        <Trans i18nKey={LOCALES.loading} />
      ) : (
        <img className="img-c" src={captcha.src} />
      )}
    </div>
  );
};

export default Captcha;
