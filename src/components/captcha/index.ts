import { Captcha as Types } from './types';
import View from './view';
import './captcha.scss';

export * from './controller';

export namespace Captcha {
  export type Props = Types.Props;
  export type FC = Types.FC;
}

export type Captcha = Types;

export const Captcha = View;
