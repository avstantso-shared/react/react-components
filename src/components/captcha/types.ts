import type React from 'react';
import type * as CC from 'captcha-canvas';

export namespace Captcha {
  export type State = {
    captcha: CC.Captcha;
    isLoading: boolean;
    data: Buffer;
  };

  export type Raw = Pick<State, 'isLoading' | 'data'> & {
    text: string;
    src: string;
    reload(): void;
  };

  export type Ref = { state: State; dispatch: any } & Pick<
    Raw,
    'src' | 'reload'
  >;

  export type Props = {
    readonly captcha: Captcha;
  };

  export type FC = React.FC<Props>;
}

export type Captcha = Readonly<Captcha.Raw>;
