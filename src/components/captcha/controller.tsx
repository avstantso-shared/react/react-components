import { Buffer } from 'buffer';
import { useEffect } from 'react';
import * as CC from 'captcha-canvas';

import { Reducer, useIncrement, useInitRef } from '@avstantso/react--data';

import { Captcha } from './types';

export const CAPTCHA_SIZE = {
  width: 300,
  height: 100,
};

function makeCaptcha() {
  const c = new CC.Captcha(CAPTCHA_SIZE.width, CAPTCHA_SIZE.height);
  c.async = true;
  c.addDecoy();
  c.drawTrace();
  c.drawCaptcha();
  return c;
}

const { useRefReducer } = Reducer<Captcha.State>(() => ({
  captcha: makeCaptcha(),
  isLoading: true,
  data: undefined as Buffer,
})).as((f) => ({
  SET_CAPTCHA: f('captcha'),
  SET_IS_LOADING: f('isLoading'),
  SET_DATA: f('data'),
}));

export function useCaptcha(): Captcha {
  const [ref, initial] = useInitRef<Captcha.Ref>();

  const [state, dispatch] = useRefReducer.Raw(ref, initial, useIncrement());

  if (initial) ref.current.reload = () => dispatch.SET_CAPTCHA(makeCaptcha);

  useEffect(() => {
    Promise.resolve(dispatch.SET_IS_LOADING(true))
      .then(() => {
        // AVStantso: hack, captcha.png get error
        const { _canvas }: any = state.captcha;
        return _canvas.png;
      })
      .then(dispatch.SET_DATA)
      .finally(() => dispatch.SET_IS_LOADING(false));
  }, [state.captcha]);

  return {
    get isLoading() {
      return ref.current.state.isLoading;
    },
    get text() {
      return ref.current.state.captcha.text;
    },
    get data() {
      return ref.current.state.data;
    },
    get src() {
      return ref.current.state.isLoading
        ? undefined
        : `data:image/png;base64,${Buffer.from(ref.current.state.data).toString(
            'base64'
          )}`;
    },
    reload: ref.current.reload,
  };
}
