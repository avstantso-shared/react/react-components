import Bluebird from 'bluebird';

import {
  Extend,
  Perform,
  Transition,
} from '@avstantso/node-or-browser-js--utils';

import { Viewport } from './types';
import CursorsValuesRaw from './cursors.json';

/**
 * @summary Copy from VSCode tooltip
 */
export const CursorValues: Viewport.Cursor[] = CursorsValuesRaw.split(' | ');

const cursorExtend = Extend.ByRecord(CursorValues);

// acquire: { (cursor: Cursor): ID } & Record<Cursor, () => ID>;
// release(id: ID): Cursor;
// current(): Cursor;

export function makeViewportCursor(
  getViewport: () => HTMLElement,
  defaultCursor: Viewport.Cursor = 'auto'
): () => Viewport.Cursor.Stack {
  let counter = 0;
  const stack: Viewport.Cursor.Stack.Item[] = [];

  return (): Viewport.Cursor.Stack => {
    function current(): Viewport.Cursor {
      const l = stack.length;
      return l ? stack[l - 1].cursor : defaultCursor;
    }

    function release(id: Viewport.Cursor.ID): Viewport.Cursor {
      const vp = getViewport();

      const l = stack.length;
      for (let i = l - 1; i >= 0; i--)
        if (stack[i].id === id) {
          stack.splice(i, 1);
          break;
        }

      if (l <= 1) vp.classList.remove('force-cursor');

      const cursor = current();
      vp.style.cursor = cursor;

      return cursor;
    }

    function acquire(cursor: Viewport.Cursor): Viewport.Cursor.ID {
      const vp = getViewport();

      const l = stack.length;
      if (l < 1) vp.classList.add('force-cursor');

      const id = ++counter;

      stack.push({ id, cursor });
      vp.style.cursor = cursor;

      return id;
    }

    function promise<T extends Perform.Able = Perform.Able>(
      cursor: Viewport.Cursor,
      action: Perform.Promise.Sync<T>
    ): Promise<T> {
      return Bluebird.resolve(acquire(cursor)).then((id) =>
        Perform.Promise(action).finally(() => release(id))
      );
    }

    function transition<
      TDefaultReturn extends Transition.Able = Transition.Able
    >(cursor: Viewport.Cursor): Transition<TDefaultReturn> {
      return (action) => promise(cursor, action);
    }

    return {
      current,
      release,
      acquire: cursorExtend(acquire, (c, f) => () => f(c)),
      promise: cursorExtend(
        promise,
        (c, f) =>
          <T extends Perform.Able = Perform.Able>(
            action: Perform.Promise.Sync<T>
          ) =>
            f(c, action)
      ),
      transition: cursorExtend(transition, (c, f) => () => f(c)),
    };
  };
}
