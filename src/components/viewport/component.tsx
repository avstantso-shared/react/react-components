import React, {
  createContext,
  useContext,
  useLayoutEffect,
  useMemo,
  useRef,
} from 'react';

import { Combiner } from '@avstantso/react--data';

import { Viewport as Types } from './types';

import './viewport.scss';
import { CursorValues, makeViewportCursor } from './cursor';

const ViewportContext = createContext<Types>({
  cursor: undefined,
  classList: document.body.classList,
});

export const useViewport = () => useContext(ViewportContext);

export const Viewport: Types.FC = (props) => {
  const { children, containerRef, defaultCursor = 'auto' } = props;

  const getViewport = () => containerRef?.current || document.body;

  useLayoutEffect(() => {
    const vp = getViewport();

    // if not predefined
    vp.classList.add('viewport');

    // if predefined
    vp.classList.remove('force-cursor');
    vp.style.cursor = defaultCursor;
  }, []);

  const cursor = useMemo<Types.Cursor.Stack>(
    makeViewportCursor(getViewport, defaultCursor),
    []
  );

  return (
    <ViewportContext.Provider
      {...{ value: { cursor, classList: getViewport().classList }, children }}
    />
  );
};

Viewport.Cursor = { Values: CursorValues };
Viewport.Context = ViewportContext;
Viewport.use = useViewport;

Viewport.Div = ({ children, defaultCursor, ...rest }) => {
  const containerRef = useRef<HTMLDivElement>();

  return (
    <div {...Combiner(rest, { className: 'viewport' })} ref={containerRef}>
      <Viewport {...{ containerRef, defaultCursor, children }} />
    </div>
  );
};
