import type React from 'react';

import type { Perform, Transition } from '@avstantso/node-or-browser-js--utils';

export namespace Viewport {
  export type Cursor = React.CSSProperties['cursor'];

  export namespace Cursor {
    export type ID = number;

    export namespace Stack {
      export type Item = { cursor: Cursor; id: ID };
    }

    export type Stack = {
      acquire: { (cursor: Cursor): ID } & Record<Cursor, () => ID>;
      release(id: ID): Cursor;
      current(): Cursor;

      /**
       * @summary Push cursor, await action and pop cursor after await
       */
      promise: {
        <T extends Perform.Able = Perform.Able>(
          cursor: Cursor,
          action: Perform.Promise.Sync<T>
        ): Promise<T>;
      } & Record<
        Cursor,
        <T extends Perform.Able = Perform.Able>(
          action: Perform.Promise.Sync<T>
        ) => Promise<T>
      >;

      /**
       * @summary Create transition with cursor for TransitStack
       */
      transition: { (cursor: Cursor): Transition } & Record<
        Cursor,
        () => Transition
      >;
    };
  }

  export namespace Props {
    export type Base = {
      /**
       * @default "auto"
       */
      defaultCursor?: Cursor;
    };
  }

  export type Props = Props.Base & {
    /**
     * @default document.body
     */
    containerRef?: React.MutableRefObject<HTMLElement>;
  };

  export type FC = React.FC<Props> & {
    Cursor: { Values: Cursor[] };
    Context: React.Context<Viewport>;
    use: () => Viewport;
    Div: React.FC<Props.Base & Omit<React.HTMLProps<HTMLDivElement>, 'ref'>>;
  };
}

export type Viewport = {
  cursor: Viewport.Cursor.Stack;
  classList: DOMTokenList;
};
