import React from 'react';
import { Meta, Story } from '@story-helpers';

import { useState } from '@storybook/preview-api';
import { Badge, Spinner } from 'react-bootstrap';
import Bluebird from 'bluebird';

import { Transition } from '@avstantso/node-or-browser-js--utils';

import { Viewport } from '.';

type TestComponent = React.FC<{
  behaviour: 'div' | 'body';
  'std-cursor': Viewport.Cursor;
  cursor: Viewport.Cursor;
}>;
const TestComponent: TestComponent = () => null;

const meta = {
  ...Meta(TestComponent, {
    title: 'Components/Viewport',
    argTypes: {
      behaviour: {
        control: 'radio',
        options: ['div', 'body'],
      },
      'std-cursor': {
        control: 'radio',
        options: Viewport.Cursor.Values,
      },
      cursor: { control: { type: 'text' } },
    },
  }),
};

export default meta;

const timeout = 3000;

export const viewport = Story(meta, {
  args: { behaviour: 'div', 'std-cursor': 'wait' },
  render: (args) => {
    const [loading, setLoading] = useState(false);
    const loadingTransition = Transition(setLoading, true, false);

    const VP = args.behaviour === 'div' ? Viewport.Div : Viewport;

    const externalZone = (
      <p style={{ color: 'white', margin: '1em' }}>External zone!</p>
    );

    return (
      <div style={{ background: 'green', padding: '1em' }}>
        {externalZone}
        <VP>
          <Viewport.Context.Consumer>
            {({ cursor: { current, transition } }) => {
              async function load() {
                return Transition.Stack(
                  transition(args.cursor || args['std-cursor']),
                  loadingTransition
                )(Bluebird.delay(timeout));
              }

              return (
                <div style={{ background: 'white', padding: '1em' }}>
                  {loading && (
                    <>
                      <Spinner variant="primary" size="sm" />
                      {' Loading'}&hellip;{` (${timeout} msec) Cursor `}
                      <Badge bg="primary">{current()}</Badge>
                    </>
                  )}
                  <ul>
                    <li>A</li>
                    <li>B</li>
                    <li>C</li>
                    <li
                      className="ignore-viewport-cursor"
                      style={{ cursor: 'auto', background: 'yellow' }}
                    >
                      <b>Custom</b> cursor on this text.
                    </li>
                  </ul>
                  <button onClick={load}>Load &hellip;</button>
                  <button
                    className="ignore-viewport-cursor"
                    onClick={() => alert('click')}
                  >
                    Ignore Viewport cursor
                  </button>
                </div>
              );
            }}
          </Viewport.Context.Consumer>
        </VP>
        {externalZone}
      </div>
    );
  },
});
