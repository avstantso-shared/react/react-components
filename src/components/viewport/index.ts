import { Viewport as Types } from './types';
import { Viewport as _Viewport } from './component';

export { useViewport } from './component';

export namespace Viewport {
  export type Cursor = Types.Cursor;

  export namespace Cursor {
    export type Stack = Types.Cursor.Stack;
  }

  export type Props = Types.Props;
  export type FC = Types.FC;
}

export type Viewport = Types;

export const Viewport = _Viewport;
