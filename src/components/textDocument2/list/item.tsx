import React from 'react';

import { Combiner, trimStartChildren } from '@avstantso/react--data';

import { TextDocHierarchyError } from '@classes';
import { useTranslation } from '@i18n';

import { TxtDoc, TxtDocListKind } from '../types';
import {
  LiContext,
  useArticleContext,
  useDocumentContext,
  useParagraphContext,
  useListContext,
  useLiContext,
} from '../context';
import { useRefChild } from '../ref-management';

import './item.scss';
import './num.scss';
import './let-en.scss';
import './let-ru.scss';

const TxtDocListItem: TxtDoc.Li.FC = (props) => {
  const { children } = props;

  const art = useArticleContext();
  const par = useParagraphContext();
  const list = useListContext();
  TextDocHierarchyError.checkContext(list, 'TxtDoc.Li', 'TxtDoc.List');
  const { preferLang } = useDocumentContext();
  const parentLi = useLiContext();

  const [, i18n] = useTranslation();

  const indexRef = useRefChild(list.setRefsList);

  const id =
    (parentLi?.id ||
      `art-${art.indexRef.current + 1}-par-${par.indexRef.current + 1}`) +
    `-item-${indexRef.current + 1}`;

  return (
    <LiContext.Provider value={{ indexRef, id }}>
      <li
        id={id}
        {...Combiner.className(
          'txt-doc-li',
          TxtDocListKind.Let === list.kind &&
            `letter-${preferLang || i18n.language}-${indexRef.current + 1}`,
          list.inline && 'inline'
        )}
      >
        {trimStartChildren(children)}
      </li>
    </LiContext.Provider>
  );
};

export default TxtDocListItem;
