import React from 'react';

import { JS } from '@avstantso/node-or-browser-js--utils';

import { Combiner } from '@avstantso/react--data';

import { TextDocHierarchyError } from '@classes';

import { TxtDoc, TxtDocListKind } from '../types';
import { useRefsList } from '../ref-management';
import { ListContext, useParagraphContext } from '../context';

import './list.scss';

const typeInfo: JS.TypeInfo = {
  inline: JS.boolean,
};

const TxtDocList: TxtDoc.List.FC = (props) => {
  const { children, className, ...rest } = props;

  const par = useParagraphContext();
  TextDocHierarchyError.checkContext(par, 'TxtDoc.List', 'TxtDoc.P');

  const [refsList, setRefsList] = useRefsList();
  const value = { ...rest, refsList, setRefsList };

  return (
    <ListContext.Provider value={value}>
      <ol
        {...Combiner.className(
          'txt-doc-list',
          rest.inline && 'inline',
          className
        )}
      >
        {children}
      </ol>
    </ListContext.Provider>
  );
};
TxtDocList.typeInfo = typeInfo;

const TxtDocListNum: TxtDoc.List.Base.FC = (props) => (
  <TxtDocList
    kind={TxtDocListKind.Num}
    className="txt-doc-list-num"
    {...props}
  />
);
TxtDocListNum.typeInfo = typeInfo;

const TxtDocListLet: TxtDoc.List.Base.FC = (props) => (
  <TxtDocList
    kind={TxtDocListKind.Let}
    className="txt-doc-list-let"
    {...props}
  />
);
TxtDocListLet.typeInfo = typeInfo;

TxtDocList.Num = TxtDocListNum;
TxtDocList.Let = TxtDocListLet;

export default TxtDocList;
