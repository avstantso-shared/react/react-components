import type { TxtDoc as Types } from './types';
import MemTxtDoc from './memoization';

export namespace TxtDoc {
  export type ChildrenRefs = Types.ChildrenRefs;

  export namespace H {
    export type Props = Types.H.Props;
    export type FC = Types.H.FC;
  }

  export namespace P {
    export type Props = Types.P.Props;
    export type FC = Types.P.FC;
    export type Context = Types.P.Context;
  }

  export namespace Li {
    export type Props = Types.Li.Props;
    export type FC = Types.Li.FC;
  }

  export namespace List {
    export namespace Base {
      export type Props = Types.List.Base.Props;
      export type FC = Types.List.Base.FC;
    }

    export type Props = Types.List.Props;
    export type FC = Types.List.FC;
    export type Context = Types.List.Context;
  }

  export namespace Section {
    export type Props = Types.Section.Props;
    export type FC = Types.Section.FC;
    export type Context = Types.Section.Context;
  }

  export namespace Article {
    export type Props = Types.Article.Props;
    export type FC = Types.Article.FC;
    export type Context = Types.Article.Context;
  }

  export type Props = Types.Props;
  export type FC = Types.FC;
  export type Context = Types.Context;
}

export const TxtDoc = MemTxtDoc;
