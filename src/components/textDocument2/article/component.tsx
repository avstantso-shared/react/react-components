import React from 'react';

import { JS } from '@avstantso/node-or-browser-js--utils';

import { TextDocHierarchyError } from '@classes';
import { HtmlEval } from '@components/html-eval';

import type { TxtDoc } from '../types';
import { useRefChild, useRefsList } from '../ref-management';
import { ArticleContext, useDocumentContext } from '../context';

const TxtDocArticle: TxtDoc.Article.FC = (props) => {
  const { children } = props;

  const doc = useDocumentContext();
  TextDocHierarchyError.checkContext(doc, 'TxtDoc.Article', 'TxtDoc');

  const indexRef = useRefChild(doc.setRefsList);
  const [refsList, setRefsList] = useRefsList();

  const value = { refsList, setRefsList, indexRef };

  return (
    <article className="txt-doc-article">
      <ArticleContext.Provider {...{ value }}>
        <ol>
          {!Array.isArray(children) ? (
            !JS.is.string(children) ? (
              children
            ) : (
              <HtmlEval components={doc.components}>{children}</HtmlEval>
            )
          ) : (
            children.map((child, index) => {
              if (!JS.is.string(child))
                return <React.Fragment key={index}>{child}</React.Fragment>;

              return (
                <HtmlEval key={index} components={doc.components}>
                  {child}
                </HtmlEval>
              );
            })
          )}
        </ol>
      </ArticleContext.Provider>
    </article>
  );
};

export default TxtDocArticle;
