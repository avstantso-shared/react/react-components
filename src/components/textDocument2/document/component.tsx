import React from 'react';

import { Combiner } from '@avstantso/react--data';

import type { TxtDoc } from '../types';
import { useRefsList } from '../ref-management';
import { DocumentContext } from '../context';

import './document.scss';

const TextDocument: React.FC<TxtDoc.Props & React.HTMLProps<HTMLDivElement>> = (
  props
) => {
  const { children, className, components, preferLang, ...rest } = props;

  const [refsList, setRefsList] = useRefsList();

  const value = { components, preferLang, refsList, setRefsList };

  return (
    <section {...Combiner.className('txt-doc', className)} {...rest}>
      <DocumentContext.Provider {...{ value, children }} />
    </section>
  );
};

export default TextDocument;
