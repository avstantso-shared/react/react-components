import { useEffect, useRef, useState } from 'react';

export function useRefsList() {
  return useState<React.MutableRefObject<number>[]>([]);
}

export function useRefChild(
  setRefsList: React.Dispatch<
    React.SetStateAction<React.MutableRefObject<number>[]>
  >
) {
  const indexRef = useRef<number>();

  useEffect(() => {
    setRefsList((prev) => {
      const next = [...prev];
      indexRef.current = next.push(indexRef) - 1;
      return next;
    });
  }, []);

  return indexRef;
}

export function useRefChildPeek(refsList: React.MutableRefObject<number>[]) {
  const indexRef = useRef<number>();

  useEffect(() => {
    indexRef.current = refsList.length;
  }, []);

  return indexRef;
}
