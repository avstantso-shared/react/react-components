import React from 'react';
import type { TxtDoc } from './types';

export const DocumentContext = React.createContext<TxtDoc.Context>(undefined);
export const useDocumentContext = () => React.useContext(DocumentContext);

export const ArticleContext =
  React.createContext<TxtDoc.Article.Context>(undefined);
export const useArticleContext = () => React.useContext(ArticleContext);

export const SectionContext =
  React.createContext<TxtDoc.Section.Context>(undefined);
export const useSectionContext = () => React.useContext(SectionContext);

export const ParagraphContext =
  React.createContext<TxtDoc.P.Context>(undefined);
export const useParagraphContext = () => React.useContext(ParagraphContext);

export const ListContext = React.createContext<TxtDoc.List.Context>(undefined);
export const useListContext = () => React.useContext(ListContext);

export const LiContext = React.createContext<TxtDoc.Li.Context>(undefined);
export const useLiContext = () => React.useContext(LiContext);
