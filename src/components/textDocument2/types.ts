import type React from 'react';

import type { JS } from '@avstantso/node-or-browser-js--utils';
import type { Indexed } from '@avstantso/react--data';

import type { HtmlEval } from '@components/html-eval';

export enum TxtDocListKind {
  Num,
  Let,
}

export namespace TxtDoc {
  export interface ChildrenRefs {
    refsList: React.MutableRefObject<number>[];
    setRefsList: React.Dispatch<
      React.SetStateAction<React.MutableRefObject<number>[]>
    >;
  }

  export namespace H {
    export interface Props {}

    export type FC = React.FC<Props>;
  }

  export namespace P {
    export interface Props {}

    export type FC = React.FC<Props>;

    export type Context = Props & Indexed.Ref;
  }

  export namespace Li {
    export interface Props {}

    export type FC = React.FC<Props>;

    export type Context = Props & Indexed.Ref & { id: string };
  }

  export namespace List {
    export namespace Base {
      export interface Props {
        inline?: boolean;
        className?: string;
      }

      export type FC = React.FC<Props> & JS.TypeInfo.Provider;
    }

    export interface Props extends Base.Props {
      kind?: TxtDocListKind;
    }

    export interface FC extends React.FC<Props>, JS.TypeInfo.Provider {
      Num: Base.FC;
      Let: Base.FC;
    }

    export type Context = Props & ChildrenRefs;
  }

  export namespace Section {
    export interface Props {}

    export type FC = React.FC<Props>;

    export type Context = Props & Indexed.Ref;
  }

  export namespace Article {
    export interface Props {}

    export type FC = React.FC<Props>;

    export type Context = Props & ChildrenRefs & Indexed.Ref;
  }

  export interface Props {
    components?: HtmlEval.Components;
    preferLang?: string;
  }

  export interface FC
    extends React.FC<Props & React.HTMLProps<HTMLDivElement>> {
    H: H.FC;
    P: P.FC;
    Li: Li.FC;
    List: List.FC;
    Section: Section.FC;
    Article: Article.FC;
  }

  export type Context = Props & ChildrenRefs;
}
