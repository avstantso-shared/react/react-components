import React from 'react';

import { TextDocHierarchyError } from '@classes';
import { H } from '@components/h';

import {
  useDocumentContext,
  useArticleContext,
  useSectionContext,
} from '../context';

import type { TxtDoc } from '../types';

const TxtDocHeader: TxtDoc.Article.FC = (props) => {
  const { children } = props;

  const doc = useDocumentContext();
  const art = useArticleContext();
  const sct = useSectionContext();
  TextDocHierarchyError.checkContext(
    doc || art || sct,
    'TxtDoc.H',
    'TxtDoc or TxtDoc.Article or TxtDoc.Section'
  );

  if (!art && !sct) return <h2 className="txt-doc-header">{children}</h2>;

  const id = `art-${art.indexRef.current + 1}${
    !sct ? '' : `-sec-${sct.indexRef.current + 1}`
  }`;

  return (
    <H id={id} className="txt-doc-header" size={!sct ? 3 : 4}>
      {children}
    </H>
  );
};

export default TxtDocHeader;
