import React from 'react';

import { TextDocHierarchyError } from '@classes';

import type { TxtDoc } from '../types';
import { SectionContext, useArticleContext } from '../context';
import { useRefChildPeek } from '../ref-management';

const TxtDocSection: TxtDoc.Article.FC = (props) => {
  const { children } = props;

  const art = useArticleContext();
  TextDocHierarchyError.checkContext(art, 'TxtDoc.Section', 'TxtDoc.Article');

  const indexRef = useRefChildPeek(art.refsList);

  const value = { indexRef };

  return (
    <section className="txt-doc-section">
      <SectionContext.Provider {...{ value }}>
        <ol>{children}</ol>
      </SectionContext.Provider>
    </section>
  );
};

export default TxtDocSection;
