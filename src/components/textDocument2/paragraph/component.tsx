import React from 'react';

import { trimStartChildren } from '@avstantso/react--data';

import { TextDocHierarchyError } from '@classes';

import {
  ParagraphContext,
  useArticleContext,
  useSectionContext,
} from '../context';
import { useRefChild } from '../ref-management';
import type { TxtDoc } from '../types';

const TxtDocParagraph: TxtDoc.Article.FC = (props) => {
  const { children } = props;

  const art = useArticleContext();
  const sct = useSectionContext();
  TextDocHierarchyError.checkContext(
    art || sct,
    'TxtDoc.P',
    'TxtDoc.Article or TxtDoc.Section'
  );

  const indexRef = useRefChild(art.setRefsList);

  const id = `art-${art.indexRef.current + 1}-par-${indexRef.current + 1}`;

  return (
    <ParagraphContext.Provider value={{ indexRef }}>
      <li id={id} className="txt-doc-paragraph">
        {trimStartChildren(children)}
      </li>
    </ParagraphContext.Provider>
  );
};

export default TxtDocParagraph;
