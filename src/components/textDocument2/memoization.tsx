import { useMemo } from 'react';

import { useTranslation } from '@i18n';

import type { TxtDoc } from './types';

import { TextDocument } from './document';

import { TxtDocArticle } from './article';
import { TxtDocSection } from './section';
import { TxtDocHeader } from './header';
import { TxtDocParagraph } from './paragraph';
import { TxtDocList, TxtDocListItem } from './list';

const MemTxtDoc: TxtDoc.FC = (props) => {
  const { components, ...rest } = props;

  const [, i18n] = useTranslation();

  return useMemo(() => {
    const c = { TxtDoc: MemTxtDoc, ...(components || {}) };

    return <TextDocument {...rest} components={c} />;
  }, [i18n.language, Object.values(rest)]);
};

MemTxtDoc.Article = TxtDocArticle;
MemTxtDoc.Section = TxtDocSection;
MemTxtDoc.H = TxtDocHeader;
MemTxtDoc.P = TxtDocParagraph;
MemTxtDoc.List = TxtDocList;
MemTxtDoc.Li = TxtDocListItem;

export default MemTxtDoc;
