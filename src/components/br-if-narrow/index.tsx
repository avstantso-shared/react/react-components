import React from 'react';

import { Extend, JS, TS } from '@avstantso/node-or-browser-js--utils';
import { InvalidArgumentError } from '@avstantso/node-or-browser-js--errors';

import { BindFC, Combiner } from '@avstantso/react--data';

import './br-if-narrow.scss';

const norrowsLimits = ((Df = TS.Type.Definer<number>()) => ({
  min: Df(250),
  max: Df(410),
}))();

const norrows: BrN.Narrows = (() => {
  const r: any = [];
  for (let i = norrowsLimits.min as number; i <= norrowsLimits.max; i = i + 10)
    r.push(i);

  return r;
})();

const typeInfo: JS.TypeInfo = {
  narrow: JS.number,
  h: JS.boolean,
  hyphen: JS.boolean,
  c: JS.number,
  cancel: JS.number,
};

/**
 * @summary Break line if screen width less than `narrows`
 */
export namespace BrN {
  export namespace Narrows {
    export type Limits = typeof norrowsLimits;

    export type Calc<
      R extends number[] = [],
      I extends number = Limits['min']
    > = I extends Limits['max']
      ? [...R, I]
      : `${I}` extends `${infer I1}${infer I2}${infer I3}`
      ? Calc<I3 extends '0' ? [...R, I] : R, TS.Increment<I>>
      : never;

    export type Keys = TS.Array.KeyFrom<Narrows>;
  }

  export type Narrows = Narrows.Calc;

  export namespace Props {
    export type Hyphen = [{ h?: boolean }, { hyphen?: boolean }];
    export type Cancel = [{ c?: Narrows.Keys }, { cancel?: Narrows.Keys }];

    export type Alts = TS.Alt<Hyphen> & TS.Alt<Cancel>;

    export type Union = TS.Array.ObjectCombine<[...Hyphen, ...Cancel]>;

    export type Base = React.ComponentPropsWithoutRef<'span'> & Alts;
  }

  export type Props = Props.Base & { narrow: Narrows.Keys };

  export namespace FC {
    export type Main = React.FC<Props> &
      JS.TypeInfo.Provider & {
        limits: Narrows.Limits & Pick<typeof norrows, 'forEach'>;
      };

    export type Sub = React.FC<Props.Base>;

    export type Subs = {
      [K in Narrows.Keys as `If${K}`]: Sub;
    };
  }

  export type FC = FC.Main & FC.Subs;
}

const BrIfNarrow: BrN.FC.Main = ({
  className,
  narrow,
  h,
  hyphen,
  c,
  cancel: cc,
  ...rest
}: BrN.Props & BrN.Props.Union) => {
  const cancel = c || cc;

  Object.entries({ narrow, cancel }).forEach(([key, value], index) => {
    if (index && undefined === value) return;

    if (value < norrowsLimits.min || value > norrowsLimits.max)
      throw new InvalidArgumentError(
        `BrN ${key} must be in [${norrowsLimits.min}, ${norrowsLimits.max}] but it's ${value}`
      );

    if (value % 10 !== 0)
      throw new InvalidArgumentError(
        `BrN ${key} must be a multiple of ten but it's ${value}`
      );
  });

  return (
    <span
      {...Combiner.className(
        'break-if-narrow',
        `narrow-${narrow}px`,
        cancel && `cancel-${cancel}px`,
        (h || hyphen) && 'hyphen',
        className
      )}
      {...rest}
    >
      <br />
    </span>
  );
};
BrIfNarrow.typeInfo = typeInfo;
BrIfNarrow.limits = {
  ...norrowsLimits,
  forEach: (...params: any[]) => (norrows.forEach as Function)(...params),
};

export const BrN: BrN.FC = Extend(BrIfNarrow);

norrows.forEach((narrow) =>
  JS.set.raw(
    BrN,
    `If${narrow}`,
    BindFC<BrN.Props, BrN.Props.Base>(`If${narrow}`, BrIfNarrow, { narrow })
  )
);
