import React, { useMemo } from 'react';
import { Gender, Filter, UseTranslationEx } from '@avstantso/react--data';
import { Fa } from '@instances';
import { useTranslation, LOCALES, T } from '@i18n';

const { FaLock, FaLockOpen } = Fa;

export namespace SystemSign {
  export namespace Props {
    export namespace I18n {
      export type Keys = Readonly<{
        systemic: string;
        notSystemic: string;
        both: string;
      }>;
    }

    export interface I18n {
      useTranslation?: UseTranslationEx;
      keys?: I18n.Keys;
    }
  }

  export namespace Common {
    export interface Props {
      gender?: Gender;
      i18n?: Props.I18n;
    }
  }

  export type Props = Common.Props &
    Readonly<{
      system?: boolean;
      both?: boolean;
      labled?: boolean;
    }>;

  export namespace FC {
    export type UseFilterCaption = {
      (props?: Common.Props): Filter.Bool.Captions;
      Male(props?: Props.I18n): Filter.Bool.Captions;
      Female(props?: Props.I18n): Filter.Bool.Captions;
      Neuter(props?: Props.I18n): Filter.Bool.Captions;
    };
  }

  export interface FC extends React.FC<Props> {
    useFilterCaption: FC.UseFilterCaption;
  }
}

const getLables = (t: T, gender: Gender, i18n?: SystemSign.Props.I18n) => {
  const {
    systemic = LOCALES.systemSign.systemic,
    notSystemic = LOCALES.systemSign.notSystemic,
  } = i18n?.keys || {};

  const context = Gender.toName(gender);

  return {
    systemic: t(systemic, {
      context,
    }),
    notSystemic: t(notSystemic, {
      context,
    }),
  };
};

export const SystemSign: SystemSign.FC = (props) => {
  const { system, both, labled, gender, i18n } = props;

  const [t] = (i18n?.useTranslation || useTranslation)();

  const labels = getLables(t, gender, i18n);

  if (both) {
    const bothTitle =
      (i18n?.keys?.both && t(i18n.keys.both)) ||
      `${labels.systemic} / ${labels.notSystemic}`;
    return (
      <span title={bothTitle}>
        <FaLock className="sign system-sign" />/
        <FaLockOpen className="sign system-sign" />
      </span>
    );
  }

  const Sign = system ? FaLock : FaLockOpen;
  const title = system ? labels.systemic : labels.notSystemic;

  return labled ? (
    <>
      <Sign className="sign system-sign" title={title} /> {title}
    </>
  ) : (
    <Sign className="sign system-sign" title={title} />
  );
};

const useFilterCaption: SystemSign.FC.UseFilterCaption = (props?) => {
  const { gender, i18n } = props || {};
  const [t] = (i18n?.useTranslation || useTranslation)();

  return useMemo(
    () => ({
      false: <SystemSign labled {...{ gender, i18n }} />,
      true: <SystemSign labled {...{ gender, i18n }} system />,
    }),
    [t, gender]
  );
};
useFilterCaption.Male = (props?) =>
  useFilterCaption({ gender: Gender.male, ...(props || {}) });
useFilterCaption.Female = (props?) =>
  useFilterCaption({ gender: Gender.female, ...(props || {}) });
useFilterCaption.Neuter = (props?) =>
  useFilterCaption({ gender: Gender.neuter, ...(props || {}) });

SystemSign.useFilterCaption = useFilterCaption;
