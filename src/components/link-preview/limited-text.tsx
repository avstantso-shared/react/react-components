import React from 'react';
import { Card, Placeholder } from 'react-bootstrap';

import { SiteMetadata } from '@avstantso/node-or-browser-js--model-core';

import type * as Types from './types';
import { Context, useRequiredContext } from './context';
import { stripDuplicates } from './utils';

export function LimitedText<
  T extends Types.LimitedText.FC = Types.LimitedText.FC
>(
  cmp: keyof Pick<typeof Card, 'Title' | 'Text'>,
  field: keyof Pick<Context['meta'], 'title' | 'description'>
): T {
  const Cmp = Card[cmp];

  const LimitedText: Types.LimitedText.FC = ({ limit, children }) => {
    const { merged, loading } = useRequiredContext(LimitedText);

    const c = stripDuplicates(SiteMetadata.Data.unpack.Field(merged[field])[0]);

    return !c ? null : (
      <Cmp className={field}>
        {loading ? (
          <Placeholder xs={12} />
        ) : (limit || 0) > 0 && c.length > limit ? (
          <span title={c}>{c.slice(0, limit)}&hellip;</span>
        ) : (
          c
        )}
        {children}
      </Cmp>
    );
  };

  return LimitedText as T;
}

LimitedText.Title = LimitedText<Types.Title.FC>('Title', 'title');
LimitedText.Description = LimitedText<Types.Description.FC>(
  'Text',
  'description'
);
