import React from 'react';
import { Placeholder } from 'react-bootstrap';

import { SiteMetadata } from '@avstantso/node-or-browser-js--model-core';

import type * as Types from './types';
import { Context, useRequiredContext } from './context';
import { stripDuplicates } from './utils';

export function InlineText<T extends Types.Inline.FC = Types.Inline.FC>(
  field: keyof Pick<Context['meta'], 'sitename' | 'domain'>
): T {
  const InlineText: Types.Inline.FC = ({ sep, children }) => {
    const { merged, loading } = useRequiredContext(InlineText);

    if (loading)
      return (
        <>
          <Placeholder className={field} xs={6} />
          {sep}
          {children}
        </>
      );

    const c = stripDuplicates(SiteMetadata.Data.unpack.Field(merged[field])[0]);

    return !c ? null : (
      <>
        <span className={field}>
          {c}
          {children}
        </span>
        {sep}
      </>
    );
  };

  return InlineText as T;
}

InlineText.SiteName = InlineText<Types.SiteName.FC>('sitename');
InlineText.Domain = InlineText<Types.Domain.FC>('domain');
