export function stripDuplicates(content: string): string {
  let parts = !content ? [] : content.split('|');
  if (parts.length < 2) return content;

  parts = parts.map((part) => part.trim());

  return [...new Set(parts)].join(' | ');
}
