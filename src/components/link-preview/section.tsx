import React from 'react';
import { Card } from 'react-bootstrap';

import { Combiner, Dimension } from '@avstantso/react--data';

import type * as Types from './types';
import { useRequiredContext } from './context';

export const LinkPreviewSection: Types.Section.FC = ({
  loadingOppositeMinSize = '200px',
  style,
  ...rest
}) => {
  const { loading, isVertical } = useRequiredContext(LinkPreviewSection);

  return (
    <Card.Body
      {...rest}
      {...Combiner.style(
        loading &&
          Dimension.Size(isVertical, loadingOppositeMinSize, 'min', true),
        style
      )}
    />
  );
};
