import React from 'react';
import { Card, Placeholder } from 'react-bootstrap';

import { useRequiredContext } from './context';

export const LinkPreviewLabels: typeof Card.Text = (props) => {
  const { loading } = useRequiredContext(LinkPreviewLabels);

  return loading ? (
    <Placeholder as={Card.Text} xs={5} />
  ) : (
    <Card.Text {...props} />
  );
};
