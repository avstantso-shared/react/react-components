import React from 'react';

import { Card } from 'react-bootstrap';

import { Extend } from '@avstantso/node-or-browser-js--utils';

import type * as Types from './types';
import * as Internals from './internals';

import './link-preview.scss';

/**
 * @summary Preview link site metadata
 * @see Source of inspiration https://www.npmjs.com/package/@dhaiwat10/react-link-preview
 */
export namespace LinkPreview {
  export type Props = Types.Props;

  /**
   * @summary `LinkPreview` component for display `SiteMetadata` and contains `LinkPreview`-subcomponents
   */
  export type FC = Types.FC & {
    /**
     * @summary `LinkPreview.Img` component for display `SiteMetadata['images']`
     */
    Img: Types.Img.FC;

    /**
     * @summary `LinkPreview.Section` component for display `Title`, `Description`, `Labels`
     */
    Section: typeof Card.Body;

    /**
     * @summary `LinkPreview.Title` component for display `SiteMetadata['title']`
     */
    Title: Types.Title.FC;

    /**
     * @summary `LinkPreview.Description` component for display `SiteMetadata['description']`
     */
    Description: Types.Description.FC;

    /**
     * @summary `LinkPreview.Section` component for display `Favicon`, `SiteName`, `Domain`
     */
    Labels: typeof Card.Text;

    /**
     * @summary `LinkPreview.Favicon` component for display `SiteMetadata['favicon']`
     */
    Favicon: Types.Favicon.FC;

    /**
     * @summary `LinkPreview.SiteName` component for display `SiteMetadata['sitename[0]']`
     */
    SiteName: Types.SiteName.FC;

    /**
     * @summary `LinkPreview.Domain` component for display `SiteMetadata['domain']`
     */
    Domain: Types.Domain.FC;
  };
}

export const LinkPreview: LinkPreview.FC = Extend.Arbitrary(
  Internals.LinkPreview,
  {
    Img: Internals.LinkPreviewImg,
    Section: Internals.LinkPreviewSection,
    Title: Internals.LimitedText.Title,
    Description: Internals.LimitedText.Description,
    Labels: Internals.LinkPreviewLabels,
    Favicon: Internals.LinkPreviewFavicon,
    SiteName: Internals.InlineText.SiteName,
    Domain: Internals.InlineText.Domain,
  }
);
