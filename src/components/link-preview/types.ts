import React, { CSSProperties } from 'react';
import type { Card, CardProps, SpinnerProps } from 'react-bootstrap';
import type { IconType } from 'react-icons';

import type { SiteMetadata } from '@avstantso/node-or-browser-js--model-core';
import type { Dimension, Link } from '@avstantso/react--data';

import type { Fa } from '@instances';

export type Width = CSSProperties['width'];
export type Height = CSSProperties['height'];

type AnchorProps = Pick<React.HTMLProps<HTMLAnchorElement>, 'target'> &
  Link.Rel;

namespace Loading {
  export type Provider = {
    /**
     * @summary `truthy` — data is not loaded and cannot be displayed
     */
    loading?: boolean;
  };
}

export namespace Meta {
  export type Props = {
    /**
     * @summary Metadata for displaying
     */
    meta: SiteMetadata.Data;

    /**
     * @summary Metadata for displaying fields if it's is empty in `meta`
     */
    fallback?: SiteMetadata.Data;

    /**
     * @summary Metadata for force displaying fields instead of `meta` fields
     */
    override?:
      | SiteMetadata.Data
      | ((meta: SiteMetadata.Data) => SiteMetadata.Data);
  };

  export type Context = Props & {
    /**
     * @summary Merged metadata from `override`, `meta`, `fallback`
     */
    merged: SiteMetadata.Data;
  };
}

export type Context = Meta.Context &
  Loading.Provider &
  Dimension.Provider &
  Dimension.IsVertical.Provider;

export type Props = Meta.Props &
  Loading.Provider &
  Dimension.Provider.Default &
  AnchorProps &
  CardProps &
  Pick<CSSProperties, 'width' | 'height' | 'maxWidth' | 'maxHeight'>;

/**
 * @summary `LinkPreview` component for display `SiteMetadata` and contains `LinkPreview`-subcomponents
 */
export type FC = React.FC<Props>;

export namespace Section {
  export type Props = {
    /**
     * @summary Minimal size of `opposite dimension` in `loading` state. `height` for `horizontal` / `width` for `vertical`
     * @default 200px
     */
    loadingOppositeMinSize?: Height | Width;
  } & Parameters<typeof Card.Body>[0];

  export type FC = React.FC<Props>;
}

export namespace Img {
  export type Props = Section.Props & {
    /**
     * @summary Size of `dimension`. `width` for `horizontal` / `height` for `vertical`
     * @default 100%
     */
    size?: Width | Height;

    /**
     * @summary This image will showed on `loading` or if all `merged.images` of metadata is inaccessible
     */
    placeholderSrc?: string;

    /**
     * @summary This `ReactIcon` `Font Awesome 5` will showed on `loading` or if all `merged.images` of metadata and `placeholderSrc` is inaccessible
     */
    placeholderFa?: IconType | Fa.Key;

    /**
     * @summary If all `merged.images` of metadata is inaccessible tries pass `favicon` as image
     */
    tryFavicon?: boolean;

    /**
     * @summary Loading spinner variant
     * @default 'light'
     */
    loadingVariant?: SpinnerProps['variant'];
  };

  /**
   * @summary `LinkPreview.Img` component for display `SiteMetadata['images']`
   */
  export type FC = React.FC<Props>;
}

export namespace LimitedText {
  export type Props = {
    /**
     * @summary If not empty and greater than `0`, target text (`title` or `description`) part exceeded this length will be replaced to `...`
     */
    limit?: number;
  };

  export type FC = React.FC<Props>;
}

export namespace Title {
  /**
   * @summary `LinkPreview.Title` component for display `SiteMetadata['title']`
   */
  export type FC = LimitedText.FC;
}

export namespace Description {
  /**
   * @summary `LinkPreview.Description` component for display `SiteMetadata['description']`
   */
  export type FC = LimitedText.FC;
}

export namespace Labels {
  export type Props = {};

  export type FC = React.FC<Props>;
}

export namespace Inline {
  export type Props = {
    /**
     * @summary Separator between this element and next element
     */
    sep?: React.ReactNode;
  };

  export type FC = React.FC<Props>;
}

export namespace Favicon {
  export type Props = {
    /**
     * @summary Sets up `width` and `height` values
     * @default '16px'
     */
    size?: Width | Height;

    /**
     * @summary Favicon width. Overrides `size` value
     */
    width?: Width;

    /**
     * @summary Favicon height. Overrides `size` value
     */
    height?: Height;
  } & Pick<Img.Props, 'placeholderSrc'> &
    Inline.Props;

  /**
   * @summary `LinkPreview.Favicon` component for display `SiteMetadata['favicon']`
   */
  export type FC = React.FC<Props>;
}

export namespace SiteName {
  /**
   * @summary `LinkPreview.SiteName` component for display `SiteMetadata['sitename[0]']`
   */
  export type FC = Inline.FC;
}

export namespace Domain {
  /**
   * @summary `LinkPreview.Domain` component for display `SiteMetadata['domain']`
   */
  export type FC = Inline.FC;
}
