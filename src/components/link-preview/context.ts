import { createContext, useContext } from 'react';

import { NeedProviderError } from '@classes';

import type * as Types from './types';
export type Context = Types.Context;

const LinkPreviewContext = createContext<Context>(undefined);

export const Context = LinkPreviewContext;

export function useRequiredContext(subComponent: string | Function): Context {
  const context = useContext(Context);

  if (!context) throw new NeedProviderError(subComponent, 'LinkPreview');

  return context;
}
