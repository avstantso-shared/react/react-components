import React, { CSSProperties, useLayoutEffect, useRef } from 'react';
import { Card } from 'react-bootstrap';

import { JS } from '@avstantso/node-or-browser-js--utils';
import { SiteMetadata } from '@avstantso/node-or-browser-js--model-core';

import { Combiner, Dimension } from '@avstantso/react--data';

import { Fa } from '@instances';
import { Loading } from '@components/loading';

import type * as Types from './types';
import { useRequiredContext } from './context';

export const LinkPreviewImg: Types.Img.FC = ({
  size = '100%',
  loadingOppositeMinSize = '300px',
  placeholderSrc,
  placeholderFa,
  tryFavicon,
  loadingVariant = 'light',

  className,
  style,
  children,
}) => {
  const { merged, loading, isVertical } = useRequiredContext(LinkPreviewImg);

  const imgRef = useRef<HTMLImageElement>();

  const ResolvedFa =
    placeholderFa &&
    (JS.is.function(placeholderFa) ? placeholderFa : Fa[placeholderFa]);

  const images = [
    ...SiteMetadata.Data.unpack.Field(!loading && merged.images),
    ...SiteMetadata.Data.unpack.Field(!loading && tryFavicon && merged.favicon),
    placeholderSrc,
  ].pack();

  let imageIndex = 0;

  useLayoutEffect(() => {
    const src = images[imageIndex];
    const needShow = !(loading || !src);

    if (needShow) imgRef.current.src = src;

    imgRef.current.style.display = needShow ? 'initial' : 'none';
  }, [!loading && JSON.stringify(images)]);

  function setFaDisplay(
    e: React.SyntheticEvent<HTMLImageElement, Event>,
    value: CSSProperties['display']
  ) {
    if (ResolvedFa)
      (e.currentTarget.nextElementSibling as SVGAElement).style.display = value;
  }

  function setCardBodySize(
    e: React.SyntheticEvent<HTMLImageElement, Event>,
    value: CSSProperties['width' | 'height']
  ) {
    (e.currentTarget.parentElement as HTMLDivElement).style[
      Dimension.Size.Field(isVertical, 'min', true)
    ] = `${value}`;
  }

  const onError: React.ReactEventHandler<HTMLImageElement> = (e) => {
    imageIndex++;

    const src = images[imageIndex];

    if (src) e.currentTarget.src = src;
    else {
      e.currentTarget.style.display = 'none';
      setFaDisplay(e, 'unset');
      setCardBodySize(e, loadingOppositeMinSize);
    }
  };

  const onLoad: React.ReactEventHandler<HTMLImageElement> = (e) => {
    setFaDisplay(e, 'none');
    setCardBodySize(e, 'unset');
  };

  return (
    <Card.Body
      {...Combiner.className('image', className)}
      {...Combiner.style(
        (loading || !images[imageIndex]) &&
          Dimension.Size(isVertical, loadingOppositeMinSize, 'min', true),
        style
      )}
    >
      <img
        ref={imgRef}
        style={loading ? {} : { ...Dimension.Size(isVertical, size) }}
        alt="⏳"
        {...{ onError, onLoad }}
      />
      {ResolvedFa && (
        <ResolvedFa
          {...(images[imageIndex] ? { style: { display: 'none' } } : {})}
        />
      )}

      {loading && <Loading variant={loadingVariant} />}

      {children}
    </Card.Body>
  );
};
