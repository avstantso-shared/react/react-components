import React from 'react';

import { SiteMetadata } from '@avstantso/node-or-browser-js--model-core';

import type * as Types from './types';
import { useRequiredContext } from './context';

export const LinkPreviewFavicon: Types.Favicon.FC = ({
  sep,
  size = '16px',
  placeholderSrc,
  width = size,
  height = size,
  children,
}) => {
  const { merged, loading } = useRequiredContext(LinkPreviewFavicon);

  const images = [
    ...SiteMetadata.Data.unpack.Field(!loading && merged.favicon),
    placeholderSrc,
  ].pack();

  let imageIndex = 0;

  const onError: React.ReactEventHandler<HTMLImageElement> = (e) => {
    imageIndex++;

    const src = images[imageIndex];

    if (src) e.currentTarget.src = src;
    else e.currentTarget.style.display = 'none';
  };

  const onLoad: React.ReactEventHandler<HTMLImageElement> = (e) => {
    e.currentTarget.style.display = 'initial';
  };

  return !images[imageIndex] ? null : (
    <>
      <img
        className="favicon"
        src={images[imageIndex]}
        {...{ width, height, onError, onLoad }}
      />
      {children}
      {sep}
    </>
  );
};
