import React from 'react';
import { Meta, Story, SubProps, Bootstrap } from '@story-helpers';

import { Dimension } from '@avstantso/react--data';

import { Fa } from '@instances';

import type * as Types from './types';
import { LinkPreview as LPV } from '.';

import { Presets } from './stories.presets';

namespace LinkPreview {
  namespace Props {
    export type Raw = Presets.Prop &
      SubProps<Types.Img.Props, 'Img.'> &
      SubProps<Types.Section.Props, 'Section.'> &
      SubProps<Types.LimitedText.Props, 'Title.'> &
      SubProps<Types.LimitedText.Props, 'Description.'> &
      SubProps<Types.Favicon.Props, 'Favicon.'> &
      SubProps<Types.Inline.Props, 'SiteName.'> &
      SubProps<Types.Inline.Props, 'Domain.'> &
      LPV.Props;
  }

  export type Props = { [K in keyof Props.Raw]: Props.Raw[K] };

  export type FC = React.FC<Props>;
}
const LinkPreview: LinkPreview.FC = () => null;

const meta = {
  ...Meta(LinkPreview, {
    argTypes: {
      ...Presets.argTypes,
      dimension: {
        options: Dimension.Values,
        control: { type: 'inline-radio' },
      },
      'Img.placeholderFa': {
        options: [null, ...Object.keys(Fa)],
        control: { type: 'select' },
      },
      'Img.loadingVariant': {
        ...Bootstrap.Variant.argType,
        control: { type: 'inline-radio' },
      },
    },
  }),
};

export default meta;

export const linkPreview = Story(meta, {
  args: {
    preset: Presets.default,
    loading: false,
    dimension: Dimension.Horizontal,

    target: '_blank',
    opener: false,
    referrer: false,
    nofollow: false,

    width: 'unset',
    height: 'unset',
    maxWidth: 'unset',
    maxHeight: 'unset',

    'Img.size': '100%',
    'Img.loadingOppositeMinSize': '200px',
    'Img.placeholderSrc': '',
    'Img.placeholderFa': 'FaLink',
    'Img.tryFavicon': false,
    'Img.loadingVariant': 'light',

    'Section.loadingOppositeMinSize': '200px',

    'Title.limit': -1,
    'Description.limit': -1,

    'Favicon.size': '16px',
    'Favicon.placeholderSrc': '',
    'Favicon.sep': ' ',

    'SiteName.sep': ' • ',
    'Domain.sep': '',

    // json, after all
    meta: Presets[Presets.default],
    fallback: {},
    override: {},
  },
  render: ({ preset, ...args }) => {
    if (preset && 'none' !== preset) {
      args.meta = Presets[preset];
    }

    const imgProps = { ...SubProps(args, 'Img.') };
    const sectionProps = { ...SubProps(args, 'Section.') };
    const titleProps = { ...SubProps(args, 'Title.') };
    const descriptionProps = { ...SubProps(args, 'Description.') };
    const faviconProps = { ...SubProps(args, 'Favicon.') };
    const siteNameProps = { ...SubProps(args, 'SiteName.') };
    const domainProps = { ...SubProps(args, 'Domain.') };

    const LinkPreview = LPV;
    return (
      <LinkPreview {...args}>
        <LinkPreview.Img {...imgProps} />
        <LinkPreview.Section {...sectionProps}>
          <LinkPreview.Title {...titleProps} />
          <LinkPreview.Description {...descriptionProps} />
          <LinkPreview.Labels>
            <LinkPreview.Favicon {...faviconProps} />
            <LinkPreview.SiteName {...siteNameProps} />
            <LinkPreview.Domain {...domainProps} />
          </LinkPreview.Labels>
        </LinkPreview.Section>
      </LinkPreview>
    );
  },
});
