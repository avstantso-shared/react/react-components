import React from 'react';
import { Card } from 'react-bootstrap';

import { Perform } from '@avstantso/node-or-browser-js--utils';
import { SiteMetadata } from '@avstantso/node-or-browser-js--model-core';

import { Combiner, Dimension, Link } from '@avstantso/react--data';

import { NeedPropError } from '@classes';

import type * as Types from './types';
import { Context } from './context';

export const LinkPreview: Types.FC = ({
  meta,
  fallback,
  override,

  loading,
  dimension = Dimension.Horizontal,

  width,
  height,
  maxWidth,
  maxHeight,

  opener,
  referrer,
  nofollow,
  target = '_blank',

  onClick,
  className,
  style,
  children,
  ...rest
}) => {
  const isVertical = Dimension.isVertical(dimension);
  if (isVertical < 0)
    throw new NeedPropError(
      LinkPreview,
      { dimension },
      Dimension.Values.map((v) => `'${v}'`).join(' | ')
    );

  const merged = SiteMetadata.Data.merge(
    Perform(override, meta),
    meta,
    fallback
  );

  const handleClick: typeof onClick = (e) => {
    if (window.getSelection && window.getSelection().toString()) return;

    onClick && onClick(e);

    if (!e.isDefaultPrevented()) {
      const anchor = e.currentTarget.getElementsByTagName(
        'a'
      )[0] as HTMLAnchorElement;
      anchor.click();
    }
  };

  return (
    <Card
      {...rest}
      {...Combiner.className('link-preview', loading && 'loading', className)}
      {...Combiner.style({ width, height, maxWidth, maxHeight }, style)}
      onClick={handleClick}
    >
      <a
        style={{ display: 'none' }}
        href={merged.url}
        {...Link.Rel.toRel({ opener, referrer, nofollow })}
        {...{ target }}
      >
        {merged.url}
      </a>

      <Context.Provider
        value={{
          meta,
          fallback,
          override,
          merged,

          loading,
          dimension,
          isVertical,
        }}
        {...{ children }}
      />
    </Card>
  );
};
