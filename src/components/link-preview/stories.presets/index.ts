import npmjsCom from './npmjs.com.json';
import reactBootstrapGithubIo from './react-bootstrap.github.io.json';
import reactIconsGithubIo from './react-icons.github.io.json';
import reactDev from './react.dev.json';
import storybookJsOrg from './storybook.js.org.json';

const raw = {
  'npmjs.com': npmjsCom,
  'react-bootstrap.github.io': reactBootstrapGithubIo,
  'react-icons.github.io': reactIconsGithubIo,
  'react.dev': reactDev,
  'storybook.js.org': storybookJsOrg,
};

export namespace Presets {
  export type Name = keyof typeof raw;
  export type Prop = { preset: Name | 'none' };
}

export const Presets = {
  ...raw,

  get default(): keyof typeof raw {
    return 'storybook.js.org';
  },

  get argTypes() {
    return {
      preset: {
        options: ['none', ...Object.keys(raw)],
        control: { type: 'select' },
      },
    };
  },
};
