import React from 'react';
import { IconBaseProps } from 'react-icons';
import * as Fa from 'react-icons/fa';

import { Extend } from '@avstantso/node-or-browser-js--utils';

import { Combiner, InnerRef } from '@avstantso/react--data';

import * as IconsWraps from '@instances';

import { Iconed } from './iconed';

export namespace LiIcon {
  export namespace Props {
    export type Base = React.ComponentPropsWithoutRef<'li'> &
      InnerRef.Provider<HTMLLIElement> &
      Iconed.Content.Provider;
  }

  export namespace ReactIcons {
    export type Props = Props.Base & {
      iconProps?: IconBaseProps;
    };
    export type FC = React.FC<Props>;

    export type FaFCs = Record<keyof typeof Fa, FC>;
  }

  export type Props = Props.Base & Iconed.Icon.Provider;
  export type FC = React.FC<Props> & ReactIcons.FaFCs;
}

export const LiIcon: LiIcon.FC = Extend((props: LiIcon.Props) => {
  const { icon, className, children, content, innerRef, ...rest } = props;

  return (
    <li {...Combiner.className('iconed', className)} {...rest} ref={innerRef}>
      <Iconed.IconContainer>{icon}</Iconed.IconContainer>
      <Iconed.Content {...{ content, children }} />
    </li>
  );
});

function wrap<T extends NodeJS.Dict<React.FC<IconBaseProps>>>(
  template: NodeJS.Dict<T>
): void {
  const [[packName, src]] = Object.entries(template);

  Object.keys(src).forEach((iconName) => {
    Extend(LiIcon)[iconName] = (props: LiIcon.ReactIcons.Props) => {
      const { iconProps, ...rest } = props;

      const Icon: React.FC<IconBaseProps> =
        IconsWraps[packName as keyof object][iconName];

      return <LiIcon icon={<Icon {...iconProps} />} {...rest} />;
    };
  });
}

wrap({ Fa });
