import React from 'react';

import { JS } from '@avstantso/node-or-browser-js--utils';

import { Fa } from '@instances';

import { SummaryIcon } from '.';

export const iconsArgTypes = {
  icon: {
    control: 'select',
    options: Object.keys(Fa.instance),
  },
  iconOpen: {
    control: 'select',
    options: ['<none>', ...Object.keys(Fa.instance)],
  },
};

export function iconsApplyArgs<TArgs extends SummaryIcon.Props.Specific>(
  args: TArgs
): TArgs {
  const props = { ...args };

  const Icon: React.FC = JS.get.raw(Fa, `${args.icon}`);
  const IconOpen: React.FC = JS.get.raw(Fa, `${args.iconOpen}`);

  JS.patch(props, {
    icon: Icon && <Icon />,
    iconOpen: IconOpen && <IconOpen />,
  });

  return props;
}
