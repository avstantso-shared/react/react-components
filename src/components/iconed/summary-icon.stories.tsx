import React from 'react';
import { Meta, Story } from '@story-helpers';

import { iconsApplyArgs, iconsArgTypes } from './for-stories';
import { SummaryIcon } from '.';

const meta = {
  ...Meta(SummaryIcon, {
    title: 'Components/Iconed/SummaryIcon',
    argTypes: iconsArgTypes,
  }),
};

export default meta;

export const summaryIcon = Story(meta, {
  args: {
    icon: 'FaCat' as any,
    iconOpen: 'FaDog' as any,
    children: 'Summary title',
  },
  render: (args) => {
    const props = iconsApplyArgs(args);

    return (
      <>
        Details:
        <details>
          <SummaryIcon {...props} />
          Text or
          <div>block</div>
        </details>
      </>
    );
  },
});
