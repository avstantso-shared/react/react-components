import React from 'react';
import { Meta, Story } from '@story-helpers';

import { iconsApplyArgs, iconsArgTypes } from './for-stories';
import { DetailsSwitch } from '.';
import './details-switch.stories.scss';

const meta = {
  ...Meta(DetailsSwitch, {
    title: 'Components/Iconed/DetailsSwitch',
    argTypes: iconsArgTypes,
  }),
};

export default meta;

export const detailsSwitch = Story(meta, {
  args: {
    icon: 'FaCat' as any,
    iconOpen: 'FaDog' as any,
    children: '',
  },
  render: (args) => {
    const props = iconsApplyArgs(args);

    return (
      <div className="details-switch-container">
        DetailsSwitch: <DetailsSwitch {...props} />
        <div className="opened">Dog text</div>
        <div className="closed">Cat text</div>
      </div>
    );
  },
});

export const detailsSwitchCircle = Story(meta, {
  args: {
    icon: 'FaCat' as any,
    iconOpen: 'FaDog' as any,
    children: '',
  },
  render: (args) => {
    const props = iconsApplyArgs(args);

    return (
      <div className="details-switch-container">
        DetailsSwitch: <DetailsSwitch.Circle {...props} />
        <div className="opened">Dog text</div>
        <div className="closed">Cat text</div>
      </div>
    );
  },
});
