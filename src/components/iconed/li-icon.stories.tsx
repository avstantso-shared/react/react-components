import React from 'react';
import { Meta, Story } from '@story-helpers';

import { LiIcon } from '.';
import { Fa } from '@instances';

const { FaCat, FaDog, FaHeart } = Fa;

const meta = { ...Meta(LiIcon, { title: 'Components/Iconed/LiIcon' }) };

export default meta;

export const liIcon = Story(meta, {
  render: (args) => (
    <>
      List:
      <ul>
        <LiIcon icon={<FaCat />}>FaCat</LiIcon>
        <LiIcon icon={<FaDog />}>FaDog</LiIcon>
        <LiIcon icon={<FaHeart color="red" />}>Red FaHeart</LiIcon>

        <LiIcon.FaCogs>FaCogs</LiIcon.FaCogs>
        <LiIcon.FaCog iconProps={{ color: 'green' }}>Green FaCog</LiIcon.FaCog>
      </ul>
    </>
  ),
});
