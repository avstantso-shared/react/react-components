import React from 'react';

import { Combiner } from '@avstantso/react--data';

import { SummaryIcon } from './summary-icon';

export namespace DetailsSwitch {
  export type Props = React.ComponentProps<'details'> &
    SummaryIcon.Props.Specific;

  export type FC = React.FC<Props> & { Circle: React.FC<Props> };
}

export const DetailsSwitch: DetailsSwitch.FC = (props) => {
  const { children, className, icon, iconOpen, ...rest } = props;

  return (
    <details {...Combiner.className('iconed-switch', className)} {...rest}>
      <SummaryIcon {...{ icon, iconOpen }}>{children}</SummaryIcon>
    </details>
  );
};

DetailsSwitch.Circle = ({ className, ...rest }) => (
  <DetailsSwitch
    {...Combiner.className('iconed-switch-circle', className)}
    {...rest}
  />
);
