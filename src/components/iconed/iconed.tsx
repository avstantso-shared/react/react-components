import React from 'react';

import { DivWithClass } from '@root/utils';
import { Combiner } from '@avstantso/react--data';

export namespace Iconed {
  export type Content = 'div' | 'span';

  export namespace Content {
    export type Provider = {
      /**
       * @summary Content tag.
       * @default 'span'
       */
      content?: 'div' | 'span';
    };
  }

  export type Icon = React.ReactNode;

  export namespace Icon {
    export type Provider<Postfix extends string = ''> = {
      [K in 'icon' as `${K}${Capitalize<Postfix>}`]: Icon;
    };
  }
}

const IconContainer = DivWithClass('icon-container');

const Content: React.FC<
  Iconed.Content.Provider & React.ComponentPropsWithoutRef<Iconed.Content>
> = ({ content: Content = 'span', className, ...rest }) => (
  <Content {...rest} {...Combiner.className('iconed-content', className)} />
);

export const Iconed = { IconContainer, Content };
