import React from 'react';

import { Combiner, InnerRef } from '@avstantso/react--data';

import { Iconed } from './iconed';

export namespace SummaryIcon {
  export namespace Props {
    export type Specific = Iconed.Icon.Provider &
      Partial<Iconed.Icon.Provider<'open'>>;
  }

  export type Props = React.ComponentPropsWithoutRef<'summary'> &
    Props.Specific &
    InnerRef.Provider<HTMLLIElement> &
    Iconed.Content.Provider;
  export type FC = React.FC<Props>;
}

export const SummaryIcon: SummaryIcon.FC = (props) => {
  const { icon, iconOpen, className, children, content, innerRef, ...rest } =
    props;

  return (
    <summary
      {...Combiner.className('iconed', className)}
      {...rest}
      ref={innerRef}
    >
      <Iconed.IconContainer className={iconOpen && 'closed'}>
        {icon}
      </Iconed.IconContainer>
      {iconOpen && (
        <Iconed.IconContainer className="opened">
          {iconOpen}
        </Iconed.IconContainer>
      )}
      <Iconed.Content {...{ content, children }} />
    </summary>
  );
};
