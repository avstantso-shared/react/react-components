import { makeLink } from '../make';

export const Stackoverflow = makeLink(
  'https://stackoverflow.com/',
  'stackoverflow.com'
);
