import type * as Link from '../types';
import { makeLink, makeLinkEx } from '../make';

export namespace NPMJS {
  export namespace Search {
    export interface Props {
      q: string;
    }

    export namespace Keywords {
      export interface Props {
        keywords: string;
      }

      export type FC = Link.FC<Props>;
    }

    export interface FC extends Link.FC<Props> {
      Keywords: Keywords.FC;
    }
  }

  export namespace Package {
    export interface Props {
      package: string;
    }

    export type FC = Link.FC<Props>;
  }

  export interface FC extends Link.FC {
    Search: Search.FC;
    Package: Package.FC;
  }
}

const npmjs = 'https://www.npmjs.com';

const Search = makeLinkEx<NPMJS.Search.Props, NPMJS.Search.FC>(
  ({ q }) => `${npmjs}/search?q=${q}`
);
Search.Keywords = makeLinkEx<
  NPMJS.Search.Keywords.Props,
  NPMJS.Search.Keywords.FC
>(({ keywords }) => `${npmjs}/search?q=keywords:${keywords}`);

export const NPMJS = makeLink<NPMJS.FC>(npmjs);
NPMJS.Search = Search;
NPMJS.Package = makeLinkEx<NPMJS.Package.Props, NPMJS.Package.FC>(
  (props) => `${npmjs}/package/${props.package}`
);
