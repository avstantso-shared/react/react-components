import type React from 'react';

import type { JS } from '@avstantso/node-or-browser-js--utils';
import type { LocalString } from '@avstantso/node-or-browser-js--model-core';
import type { UseTranslationEx, Link } from '@avstantso/react--data';
import type { Lang } from '@components/lang';

export namespace Make {
  export type Href<TProps extends object = {}> =
    | string
    | LocalString
    | ((props: TProps) => string);

  export type Options = Link.Rel & {
    defaultChildren?: string | JSX.Element;
    defaultI18Key?: string;
    defaultLangContent?: Lang.Content<string | JSX.Element>;
    defaultHash?: string;
    defaultSearch?: string;
    local?: boolean;
    useTranslation?: UseTranslationEx;
    name?: string;
  };

  export type Overload = {
    <TFC extends FC = FC>(href: Href, options?: Options): TFC;
    <TFC extends FC = FC>(
      href: Href,
      defaultChildren?: string | JSX.Element
    ): TFC;
    <TFC extends FC = FC>(
      href: Href,
      defaultLangContent?: Lang.Content<string | JSX.Element>
    ): TFC;
  };

  export namespace Ex {
    export type Overload = {
      <TProps extends object = {}, TFC extends FC<TProps> = FC<TProps>>(
        href: Href<TProps>,
        options?: Options
      ): TFC;
      <TProps extends object = {}, TFC extends FC<TProps> = FC<TProps>>(
        href: Href<TProps>,
        defaultChildren?: string | JSX.Element
      ): TFC;
      <TProps extends object = {}, TFC extends FC<TProps> = FC<TProps>>(
        href: Href<TProps>,
        defaultLangContent?: Lang.Content<string | JSX.Element>
      ): TFC;
    };
  }
}

export type Props = {
  quoted?: boolean;
  mapmarker?: boolean;
  hash?: string;
  search?: string;
};

export type FC<TProps extends object = {}> = React.FC<Props & TProps> &
  JS.TypeInfo.Provider & {
    Href: string;
  };
