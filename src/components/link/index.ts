import type * as Type from './types';
import { makeLinkEx, makeLink, makeTransCmpsByLinks } from './make';
import * as Links from './links';

export { Links };

export namespace Link {
  export namespace Make {
    export type Href<TProps extends object = {}> = Type.Make.Href<TProps>;
    export type Options = Type.Make.Options;
    export type Overload = Type.Make.Overload;

    export namespace Ex {
      export type Overload = Type.Make.Ex.Overload;
    }
  }

  export type Props = Type.Props;
  export type FC<TProps extends object = {}> = Type.FC<TProps>;
}

export const Link = {
  makeEx: makeLinkEx,
  make: makeLink,
  makeTransCmpsByLinks,
};
