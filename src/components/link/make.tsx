import * as ReactRouterDOM from 'react-router-dom';
import type { TransProps } from 'react-i18next';

import { Extend, Generics, JS } from '@avstantso/node-or-browser-js--utils';
import { LocalString } from '@avstantso/node-or-browser-js--model-core';

import { DynamicFC, Link as DataLink } from '@avstantso/react--data';

import { Lang } from '@components/lang';
import { Nowrap as N } from '@components/nowrap';
import { Quotes as Q } from '@components/quotes';

import { Trans, useTranslation as useTranslationRC } from '@i18n';
import { Fa } from '@instances';

import type * as Link from './types';

const { FaMapMarkerAlt } = Fa;

const linkTypeInfo: JS.TypeInfo = { quoted: JS.boolean, mapmarker: JS.boolean };

export const makeLinkEx: Link.Make.Ex.Overload = <
  TProps extends object = {},
  TFC extends Link.FC<TProps> = Link.FC<TProps>
>(
  href: Link.Make.Href<TProps>,
  param?: any
): TFC => {
  const {
    defaultChildren,
    defaultI18Key: i18nKey,
    defaultLangContent,
    defaultHash,
    defaultSearch,
    local,
    useTranslation,
    name,

    ...rel
  }: Link.Make.Options = !param
    ? {}
    : !JS.is.object<any>(param)
    ? { defaultChildren: param }
    : param.En
    ? { defaultLangContent: param }
    : param;

  let L: Link.FC<TProps> = Extend.Arbitrary(
    (props: Parameters<Link.FC<TProps>>[0]) => {
      const {
        children,
        quoted,
        mapmarker,
        hash = defaultHash,
        search = defaultSearch,
      } = props;

      const [t, i18n] = (useTranslation || useTranslationRC)();

      const c =
        children ||
        defaultChildren ||
        (i18nKey && <Trans.N {...{ i18n, t, i18nKey }} />) ||
        (defaultLangContent && Lang.useSelect(defaultLangContent));

      const m = !mapmarker ? (
        c
      ) : (
        <N>
          <FaMapMarkerAlt /> {c}
        </N>
      );

      const q = quoted ? <Q>{m}</Q> : m;
      const h = [
        JS.is.function(href)
          ? href(props)
          : LocalString.is(href)
          ? LocalString.select(href, i18n.language)
          : href,
        search && `?${search}`,
        hash && `#${hash}`,
      ]
        .pack()
        .join('');

      return local ? (
        <ReactRouterDOM.Link to={h}>{q}</ReactRouterDOM.Link>
      ) : (
        <a href={h} target="_blank" {...DataLink.Rel.toRel(rel)}>
          {q}
        </a>
      );
    },
    {
      typeInfo: linkTypeInfo,
      Href: [
        `${href}`,
        defaultSearch && `?${defaultSearch}`,
        defaultHash && `#${defaultHash}`,
      ]
        .pack()
        .join(''),
    }
  );

  return Generics.Cast.To(name ? DynamicFC<TProps>(name, L) : L);
};

export const makeLink: Link.Make.Overload = <TFC extends Link.FC = Link.FC>(
  href: Link.Make.Href,
  param?: any
): TFC => makeLinkEx<object, TFC>(href, param);

export function makeTransCmpsByLinks(
  links: object
): TransProps<any>['components'] {
  const r: any = {};

  function recursion(obj: object) {
    Object.keys(obj).forEach((key) => {
      const Value: unknown = obj[key as keyof object];

      if (JS.is.object(Value)) recursion(Value);
      else if (JS.is.function(Value)) {
        try {
          r[key] = <Value />;
        } catch (e) {
          // -localization dependent
        }
      } else throw Error(`Unknown type ${typeof Value}`);
    });
  }

  recursion(links);

  return r;
}
