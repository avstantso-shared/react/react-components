import { ScrollHolderContext as Context } from './context';
import Provider from './provider';

export { useScrollHolder } from './hook';
export const ScrollHolder = { Context, Provider };
