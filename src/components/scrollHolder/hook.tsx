import React, { useContext, useLayoutEffect } from 'react';

import { DebugLabel } from '@avstantso/react--data';

import { DOMGet } from '@root/utils';

import type { ScrollHolder, ScrollInfo } from './types';
import { ScrollHolderContext } from './context';

const sScroll = 'scroll';

export function useScrollHolder(props: ScrollHolder.Props) {
  const {
    ref,
    noHorizontal,
    noVertical,
    id: idToSet,
    loading,
    behavior = 'smooth',
  } = props;
  const debugPrefix = DebugLabel.Prefix(useScrollHolder, props);

  const context = useContext(ScrollHolderContext);

  useLayoutEffect(() => {
    if (loading) return;

    const div = DOMGet.getElement(ref || idToSet);
    if (!div) return;

    const id = DOMGet.getElementId(div, idToSet);

    {
      const info: ScrollInfo = context.current[id];

      if (info && (!noHorizontal || !noVertical)) {
        div.scrollTo({
          ...(noHorizontal ? {} : { left: info.left }),
          ...(noVertical ? {} : { top: info.top }),
          behavior,
        });
      }
    }

    const handleScroll: EventListener = (event) => {
      const currentTarget = event.currentTarget as HTMLDivElement;

      const info: ScrollInfo = {
        ...(!noHorizontal ? { left: currentTarget.scrollLeft } : {}),
        ...(!noVertical ? { top: currentTarget.scrollTop } : {}),
      };

      context.current[id] = info;
    };

    div.addEventListener(sScroll, handleScroll);
    return () => {
      div.removeEventListener(sScroll, handleScroll);
    };
  }, [loading]);
}
