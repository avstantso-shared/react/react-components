import React, { useRef } from 'react';
import type { ScrollsInfo } from './types';
import { ScrollHolderContext } from './context';

const ScrollHolderContextProvider: React.FC = (props) => {
  const { children } = props;

  const value = useRef<ScrollsInfo>();

  return (
    <ScrollHolderContext.Provider value={value}>
      {children}
    </ScrollHolderContext.Provider>
  );
};

export default ScrollHolderContextProvider;
