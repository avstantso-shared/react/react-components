import React from 'react';

import { DebugLabel } from '@avstantso/react--data';

import type { DOMGet } from '@root/utils';

export namespace ScrollHolder {
  export interface Props extends DebugLabel.Option {
    ref?: DOMGet.Ref;
    noHorizontal?: boolean;
    noVertical?: boolean;
    id?: string;
    loading?: boolean;
    behavior?: ScrollBehavior;
  }

  export type Context = React.MutableRefObject<ScrollsInfo>;
}

export interface ScrollInfo {
  left?: number;
  top?: number;
}

export type ScrollsInfo = NodeJS.Dict<ScrollInfo>;
