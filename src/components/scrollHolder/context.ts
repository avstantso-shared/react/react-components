import { createContext } from 'react';
import type { ScrollHolder, ScrollsInfo } from './types';

const defaultScrolls: React.MutableRefObject<ScrollsInfo> = { current: {} };

export const ScrollHolderContext =
  createContext<ScrollHolder.Context>(defaultScrolls);
