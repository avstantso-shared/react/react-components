import type { Dispatch, SetStateAction } from 'react';

import type { T } from '@avstantso/react--data';
import type { H } from '@components/h';
import type { HtmlEval } from '@components/html-eval';

export interface PartNode {
  parts: ReadonlyArray<PartInfo>;
}

export interface PartNodeContext {
  setParts: Dispatch<SetStateAction<ReadonlyArray<PartInfo>>>;
}

export interface PartInfo extends PartNode {
  id: string;
  header: string;
  level: number;
}

export interface ArticleInfo extends PartNode {
  id: string;
  tocId: string;
  pathname: string;
  tocTitle?: string;
  tocHide?: boolean;
  evalHtml: boolean;
  components: HtmlEval.Components;
}

export interface PartContext extends PartInfo, PartNodeContext {}

export interface ArticleContext extends ArticleInfo, PartNodeContext {
  baseHeaderSize: H.Size;
  t?: T;
  i18nPrefer?: boolean;
}

export interface CurrentArticleContext {
  readonly article: Readonly<ArticleInfo>;
  setArticle: Dispatch<SetStateAction<ArticleInfo>>;
}
