import { createContext, useContext } from 'react';
import type * as Types from './types';

export const CurrentArticleContext =
  createContext<Types.CurrentArticleContext>(undefined);
export const useCurrentArticle = () => useContext(CurrentArticleContext);

export const ArticleContext = createContext<Types.ArticleContext>(undefined);
export const useArticle = () => useContext(ArticleContext);

export const PartContext = createContext<Types.PartContext>(undefined);
export const usePart = () => useContext(PartContext);
