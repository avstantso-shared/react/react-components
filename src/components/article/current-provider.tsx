import React, { useEffect, useState } from 'react';
import { useLocation } from 'react-router';

import type { ArticleInfo } from './types';
import { CurrentArticleContext } from './context';

export const CurrentProvider: React.FC = (props) => {
  const { pathname } = useLocation();
  const [article, setArticle] = useState<ArticleInfo>();

  useEffect(() => {
    setArticle((prev) => (prev?.pathname === pathname ? prev : null));
  }, [pathname]);

  return (
    <CurrentArticleContext.Provider
      value={{ article, setArticle }}
      {...props}
    />
  );
};
