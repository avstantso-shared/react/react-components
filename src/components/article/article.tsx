import React, { useEffect, useState } from 'react';
import { useLocation } from 'react-router';

import { InnerRef, T } from '@avstantso/react--data';

import type { H } from '@components/h';
import type { HtmlEval } from '@components/html-eval';

import type { PartInfo } from './types';
import { ArticleContext, useCurrentArticle } from './context';
import { Part } from './part';
import { TableOfContents } from './table-of-contents';
import { CurrentProvider } from './current-provider';

export namespace Article {
  export namespace Props {
    export type Strict = InnerRef.Provider<HTMLDivElement> & {
      baseHeaderSize?: H.Size;
      tocTitle?: string;
      /**
       * @summary For current only
       */
      tocHide?: boolean;

      /**
       * @summary Id value for TOC
       */
      tocId?: string;

      /**
       * @summary Use HtmlEval. Transferred to nested Parts
       */
      evalHtml?: boolean;
      /**
       * @summary Use in HtmlEval. Transferred to nested Parts
       */
      components?: HtmlEval.Components;

      /**
       * @summary i18n t function
       */
      t?: T;

      /**
       * @summary Prefer intrpretate parts headers as i18n keys
       */
      i18nPrefer?: boolean;
    };
  }

  export type Props = React.HTMLProps<HTMLDivElement> & Props.Strict;
  export interface FC extends React.FC<Props> {
    Part: Part.FC;
    TableOfContents: TableOfContents.FC;
    CurrentProvider: React.FC;
  }
}

export const Article: Article.FC = (props) => {
  const {
    innerRef,
    baseHeaderSize,
    tocTitle,
    tocHide,
    tocId,
    evalHtml,
    components,
    t,
    i18nPrefer,
    ...rest
  } = props;
  const { id } = rest;

  const { pathname } = useLocation();
  const { setArticle } = useCurrentArticle() || {};
  const [parts, setParts] = useState<ReadonlyArray<PartInfo>>([]);

  useEffect(() => {
    setArticle &&
      setArticle({
        id,
        tocId,
        pathname,
        tocTitle,
        tocHide,
        evalHtml,
        components,
        parts,
      });
  }, [pathname, tocTitle, tocHide, evalHtml, parts]);

  return (
    <ArticleContext.Provider
      value={{
        id,
        tocId,
        pathname,
        tocTitle,
        evalHtml,
        components,
        t,
        i18nPrefer,
        parts,
        setParts,
        baseHeaderSize: baseHeaderSize || 2,
      }}
    >
      <article ref={innerRef} {...rest} />
    </ArticleContext.Provider>
  );
};

Article.Part = Part;
Article.TableOfContents = TableOfContents;
Article.CurrentProvider = CurrentProvider;
