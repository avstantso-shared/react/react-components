import React, { useEffect, useMemo, useRef, useState } from 'react';

import { HTMLUtils, JS } from '@avstantso/node-or-browser-js--utils';

import { Combiner } from '@avstantso/react--data';

import { Trans } from '@i18n';

import { H } from '@components/h';
import { HtmlEval } from '@components/html-eval';

import type { PartInfo } from './types';
import { PartContext, useArticle, usePart } from './context';

export namespace Part {
  export namespace Props {
    export interface Strict {
      header?: string | React.ReactNode;
      headerStr?: string;
      headerI18n?: string;
      headerSize?: H.Size;

      /**
       * @summary Use HtmlEval
       * @override article.evalHtml
       */
      evalHtml?: boolean;
      /**
       * @summary Use in HtmlEval
       */
      components?: HtmlEval.Components;
    }
  }

  export type Props = React.HTMLProps<HTMLDivElement> & Props.Strict;
  export type FC = React.FC<Props>;
}

export const Part: Part.FC = (props) => {
  const {
    children,
    className,
    header: headerNode,
    headerStr,
    headerI18n,
    headerSize,
    evalHtml,
    components: propsComponents,
    ...rest
  } = props;
  const { id } = rest;
  const [parts, setParts] = useState<ReadonlyArray<PartInfo>>([]);
  const indexRef = useRef<number>();

  const article = useArticle();
  const parent = usePart();

  const level = parent ? parent.level + 1 : 0;

  const headerFromi18n =
    article.i18nPrefer &&
    article.t &&
    JS.is.string(headerNode) &&
    article.t(headerNode);

  const info = {
    id,
    header:
      headerStr ||
      (headerI18n && article.t && HTMLUtils.noTags(article.t(headerI18n))) ||
      (headerFromi18n && HTMLUtils.noTags(headerFromi18n)) ||
      (headerNode &&
        ((JS.is.string(headerNode) && headerNode) ||
          `"header" is ${typeof headerNode}`)),
    level,
    parts,
    setParts,
  };

  useEffect(() => {
    if (!id) return;

    const setParts = parent?.setParts || article?.setParts;
    if (!setParts) {
      console.warn('Article.Part must be nested in Article.');
      return;
    }

    setParts((prev) => {
      const next = [...prev];

      if (undefined === indexRef.current)
        indexRef.current = next.push(info) - 1;
      else next.splice(indexRef.current, 1, info);

      return next;
    });
  }, [id, parts, level, info.header]);

  const needTrans = !!(
    children &&
    (JS.is.string(children) || Array.isArray(children)) &&
    (JS.is.boolean(evalHtml) ? evalHtml : article?.evalHtml)
  );

  const memo = useMemo(() => {
    if (!needTrans) return children;

    const components = {
      ...(article?.components || {}),
      ...(propsComponents || {}),
    };

    return !Array.isArray(children) ? (
      <HtmlEval {...{ components }}>{children as string}</HtmlEval>
    ) : (
      children.map((child, index) => {
        if (!JS.is.string(child))
          return <React.Fragment key={index}>{child}</React.Fragment>;

        return (
          <HtmlEval key={index} {...{ components }}>
            {child}
          </HtmlEval>
        );
      })
    );
  }, [children, needTrans]);

  const header =
    (headerFromi18n && <Trans.N t={article.t}>{headerNode}</Trans.N>) ||
    headerNode ||
    (headerI18n && article.t && <Trans.N t={article.t}>{headerI18n}</Trans.N>);

  return (
    <PartContext.Provider value={info}>
      <div {...Combiner.className('article-part', className)} {...rest}>
        {header && (
          <H
            className="article-part-header"
            size={
              headerSize || (((article?.baseHeaderSize || 1) + level) as H.Size)
            }
          >
            {header}
          </H>
        )}
        {memo}
      </div>
    </PartContext.Provider>
  );
};
