import React from 'react';
import { Link } from 'react-router-dom';
import { Alert } from 'react-bootstrap';

import { Combiner } from '@avstantso/react--data';

import type { PartInfo, ArticleInfo } from './types';
import { useArticle, useCurrentArticle } from './context';

export namespace TableOfContents {
  export namespace Props {
    export type Element = Pick<React.HTMLProps<HTMLElement>, 'className'>;

    export interface Elements {
      AsMenuItem?: React.FC<Element>;
      AsSubMenu?: React.FC<Element>;
    }

    export type RenderSubMenu = (
      props: PartInfo & { content: React.ReactNode[] }
    ) => React.ReactNode;

    export type RenderItem = (
      props: PartInfo & { link: React.ReactNode }
    ) => React.ReactNode;

    export type RenderLink = (
      props: PartInfo & { link: React.ReactNode }
    ) => React.ReactNode;

    export interface Strict extends Elements {
      article?: ArticleInfo;
      showArticleTitle?: boolean;
      simple?: boolean;
      renderSubMenu?: RenderSubMenu;
      renderLink?: RenderLink;
      renderItem?: RenderItem;
    }
  }

  export type Props = React.HTMLProps<HTMLDivElement> & Props.Strict;
  export type FC = React.FC<Props>;
}

export const TableOfContents: TableOfContents.FC = (props) => {
  const {
    children,
    className,
    article,
    showArticleTitle,
    AsMenuItem,
    AsSubMenu,
    renderSubMenu,
    renderLink,
    renderItem,
    simple,
    ...rest
  } = props;

  const MenuItem: React.FC<TableOfContents.Props.Element> = (miProps) =>
    AsMenuItem ? <AsMenuItem {...miProps} /> : <li {...miProps} />;

  const SubMenu: React.FC<TableOfContents.Props.Element> = (smProps) =>
    AsSubMenu ? <AsSubMenu {...smProps} /> : <ul {...smProps} />;

  function partsTreeContent(
    parts: ReadonlyArray<PartInfo>,
    level = 0
  ): React.ReactNode[] {
    if (!parts?.length) return null;

    const r: React.ReactNode[] = [];

    for (let i = 0; i < parts.length; i++) {
      const part = parts[i];
      if (part.parts.length) {
        const content = partsTreeContent(part.parts, level + 1);
        r.push(
          <React.Fragment key={`${part.level}-${i}`}>
            {renderSubMenu ? (
              renderSubMenu({ ...part, content })
            ) : (
              <SubMenu className={`level-${level}`}>{content}</SubMenu>
            )}
          </React.Fragment>
        );
      } else {
        const link = <Link to={`#${part.id}`}>{part.header || part.id}</Link>;

        r.push(
          <React.Fragment key={i}>
            {renderItem ? (
              renderItem({ ...part, link })
            ) : (
              <MenuItem key={i} className={`level-${level}`}>
                {renderLink ? renderLink({ ...part, link }) : link}
              </MenuItem>
            )}
          </React.Fragment>
        );
      }
    }

    return r;
  }

  const inside = useArticle();
  const { article: current } = useCurrentArticle() || {};
  const { tocTitle, tocHide, parts } =
    article || inside ? { ...(article || inside), tocHide: false } : current;
  if (tocHide) return null;

  const content = partsTreeContent(parts);

  if (simple) return <>{content}</>;

  return (
    <div
      {...Combiner.className('article-table-of-contents', className)}
      {...rest}
    >
      {showArticleTitle && tocTitle}
      {content ? (
        <SubMenu className="level-0">{content}</SubMenu>
      ) : (
        <Alert variant="warning">No&nbsp;content!</Alert>
      )}
      {children}
    </div>
  );
};
