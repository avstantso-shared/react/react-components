import React from 'react';
import { Alert, Button } from 'react-bootstrap';

import { ObjectViewer } from '@components/object-viewer';

import './error-details-dev.scss';

export namespace ErrorDetailsDev {
  export type Props = {
    readonly error: Error;
    readonly componentStack?: any;
  };
  export type FC = React.FC<Props>;
}

export const ErrorDetailsDev: ErrorDetailsDev.FC = ({
  error,
  componentStack,
}) => {
  if (!process.env.DEBUG_INFO_ENABLED) return null;

  const { stack = componentStack as string, name, message, ...rest } = error;

  const hasDetails = !!Object.keys(rest).length;

  if (!hasDetails && !stack) return null;

  const stackArr = stack?.split(' at ');

  return (
    <div className="error-details-dev">
      <Alert variant="secondary">
        {hasDetails && (
          <details className="details">
            <summary>
              <b>Details</b>
            </summary>
            <ObjectViewer data={rest} noRoot />
          </details>
        )}
        {stack && (
          <details className="stack">
            <summary>
              <b>Stack for</b>{' '}
              <span className="stack-top">{stackArr.shift()}</span>
            </summary>
            <ul>
              {stackArr.map((item, index) => (
                <li key={index}>at {item}</li>
              ))}
            </ul>
          </details>
        )}
      </Alert>
      <Button variant="secondary" onClick={() => console.warn(error)}>
        Warn to console
      </Button>
    </div>
  );
};
