import React from 'react';
import { Outlet } from 'react-router-dom';

import { RouteObject } from '@root/utils/routes';

import { ErrorBoundary } from './component';

export const useEBRoute = (route: RouteObject): RouteObject => {
  const { path, element, ...rest } = route;

  return {
    path,
    element: (
      <ErrorBoundary>
        <>{element || (rest.children && <Outlet />)}</>
      </ErrorBoundary>
    ),
    ...rest,
  };
};
