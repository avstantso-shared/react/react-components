import React from 'react';
import { CustomError } from '../types';

import { UnexpectedErrorHasOccurred } from './UnexpectedError';
import { DebugSentryErrorHasOccurred } from './DebugSentryError';
import { GridErrorOccurred } from './GridError';

const knownErrors = [
  DebugSentryErrorHasOccurred,
  GridErrorOccurred,
  UnexpectedErrorHasOccurred,
];

export const SelectError: CustomError.FC<Error> = () => null;

SelectError.try = (error: Error) => {
  for (let i = 0; i < knownErrors.length; i++) {
    const r = knownErrors[i].try(error);
    if (r) return r;
  }

  return null;
};
