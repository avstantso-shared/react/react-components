import React from 'react';
import { Generics } from '@avstantso/node-or-browser-js--utils';

import { GridError } from '@classes';

import { CustomError } from '../types';

export const GridErrorOccurred: CustomError.FC<GridError> = ({ error }) => {
  return (
    <>
      <h2 id="eb-header">Grid error has occurred.</h2>
      <p>{error.message}</p>
    </>
  );
};

GridErrorOccurred.try = (error: Error) =>
  GridError.is(error) && <GridErrorOccurred error={Generics.Cast.To(error)} />;
