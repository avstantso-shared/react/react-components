import React from 'react';
import { Table } from 'react-bootstrap';

import { Generics, JS } from '@avstantso/node-or-browser-js--utils';

import { DebugSentryError } from '@avstantso/react--data';

import { CustomError } from '../types';

export const DebugSentryErrorHasOccurred: CustomError.FC<DebugSentryError> = ({
  error,
}) => {
  return (
    <>
      <h2 id="eb-header">
        DebugSentry error has occurred for "{error.label}".
      </h2>
      <h5>Dependency difference:</h5>
      <Table responsive>
        <thead>
          <tr>
            <th
              rowSpan={2}
              className="text-center"
              style={{ verticalAlign: 'middle' }}
            >
              Index
            </th>
            <th colSpan={2} className="text-center">
              Old
            </th>
            <th colSpan={2} className="text-center">
              New
            </th>
          </tr>
          <tr>
            <th className="text-center">type</th>
            <th className="text-center">value/name</th>
            <th className="text-center">type</th>
            <th className="text-center">value/name</th>
          </tr>
        </thead>
        <tbody>
          {error.depsDiffs.map((dd, index1) => (
            <tr key={index1}>
              <td className="text-center">{dd.index}</td>
              {[dd.old, dd.new].map((dep, index2) => {
                const tp = typeof dep;
                return (
                  <React.Fragment key={`${index1}-${index2}`}>
                    <td className="text-center">
                      <i>{tp}</i>
                    </td>
                    <td className="text-center">
                      {JS.function === tp
                        ? Generics.Cast.To<Function>(dep).name
                        : JSON.stringify(dep)}
                    </td>
                  </React.Fragment>
                );
              })}
            </tr>
          ))}
        </tbody>
      </Table>
    </>
  );
};

DebugSentryErrorHasOccurred.try = (error: Error) =>
  DebugSentryError.is(error) && (
    <DebugSentryErrorHasOccurred error={Generics.Cast.To(error)} />
  );
