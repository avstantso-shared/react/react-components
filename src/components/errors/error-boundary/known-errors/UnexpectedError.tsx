import React from 'react';
import { Trans, LOCALES } from '@i18n';

import { CustomError } from '../types';

export const UnexpectedErrorHasOccurred: CustomError.FC<Error> = () => (
  <h2 id="eb-header">
    <Trans.N i18nKey={LOCALES.errorBoundary.unexpectedErrorHasOccurred} />
  </h2>
);

UnexpectedErrorHasOccurred.try = (error: Error) => (
  <UnexpectedErrorHasOccurred error={error} />
);
