import React from 'react';

export namespace CustomError {
  export interface Props<T extends Error> {
    readonly error: Readonly<T>;
  }

  export interface FC<T extends Error> extends React.FC<Props<T>> {
    try(error: Error): boolean | JSX.Element;
  }
}
