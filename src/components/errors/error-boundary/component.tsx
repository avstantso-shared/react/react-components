import React from 'react';

import { ErrorDetailsDev } from '../error-details-dev';

import { SelectError } from './known-errors';

import './error-boundary.scss';

export namespace ErrorBoundary {
  export interface Props {
    readonly children: JSX.Element | JSX.Element[];
    readonly title?: string | JSX.Element;
  }

  export interface State {
    readonly error: any;
    readonly errorInfo: any;
  }
}

export class ErrorBoundary extends React.Component<
  ErrorBoundary.Props,
  ErrorBoundary.State
> {
  readonly state: ErrorBoundary.State = {
    error: undefined,
    errorInfo: undefined,
  };

  componentDidCatch(error: any, errorInfo: any) {
    this.setState({
      error,
      errorInfo,
    });
  }

  render() {
    const { error, errorInfo } = this.state;

    return !errorInfo ? (
      this.props.children
    ) : (
      <div className="error-boundary">
        {SelectError.try(error)}
        {this.props.title && <h2>{this.props.title}</h2>}
        {error && (
          <ErrorDetailsDev
            {...{ error, componentStack: errorInfo.componentStack }}
          />
        )}
      </div>
    );
  }
}
