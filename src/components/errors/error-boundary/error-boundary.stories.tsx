import React from 'react';
import { Meta, Story } from '@story-helpers';

import { JS } from '@avstantso/node-or-browser-js--utils';

import { LOCALES } from '@i18n';

import { ErrorBoundary } from '.';

type TestComponentProps = Omit<ErrorBoundary.Props, 'children'> & {
  'error-kind': string;
  'known-error': string;
  'error-as-str': string;
  'error-details': NodeJS.Dict<any>;
};
type TestComponent = React.FC<TestComponentProps>;
const TestComponent: TestComponent = () => null;

const knownErrors = Object.keys(LOCALES.errors).filter(
  (key) => key !== toString.name
);

const meta = {
  ...Meta(TestComponent, {
    title: 'Components/Errors/ErrorBoundary',
    argTypes: {
      'error-kind': {
        control: 'radio',
        options: ['known-error', 'as-str'],
      },
      'known-error': {
        control: 'radio',
        options: knownErrors,
      },
      'error-as-str': { control: { type: 'text' } },
      'error-details': { control: { type: 'object' } },
    },
  }),
  component: ErrorBoundary,
};

export default meta;

const SubComponent: TestComponent = ({
  'error-kind': kind,
  'known-error': knownError,
  'error-as-str': message,
  'error-details': details,
}) => {
  const error = Error('known-error' === kind ? knownError : message);

  if (error instanceof Error && details)
    Object.entries(details).forEach(([key, value]) =>
      JS.set.raw(error, key, value)
    );

  throw error;
};

export const errorBoundary = Story<typeof TestComponent>({
  args: {
    'error-kind': 'known-error',
    'known-error': knownErrors[0],
    'error-as-str': 'Error...😞',
    'error-details': {
      x: 17,
      y: 11,
    },
    title: '',
  },
  render: (args) => (
    <div style={{ background: 'cyan' }}>
      <h3>Test error in {ErrorBoundary.name}:</h3>
      <div style={{ background: 'white', margin: '2em', padding: '0.5em' }}>
        <ErrorBoundary title={args.title}>
          <SubComponent {...args} />
        </ErrorBoundary>
      </div>
    </div>
  ),
});
