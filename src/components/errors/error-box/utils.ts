import { UseTranslationEx } from '@avstantso/react--data';

import { ErrorBox } from './types';

export type MakeI18nLayers = (
  ...useTrans: UseTranslationEx[]
) => ReadonlyArray<ErrorBox.Context.I18Item>;

export const MakeI18nLayers: MakeI18nLayers = (...useTrans) => {
  const r: ErrorBox.Context.I18Item[] = [];

  for (let i = 0; i < useTrans.length; i++) {
    const [t, i18n] = useTrans[i]();
    r.push({ t, i18n });
  }

  return r;
};
