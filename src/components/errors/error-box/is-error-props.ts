import { JS } from '@avstantso/node-or-browser-js--utils';
import { ErrorBox } from './types';

export function isErrorProps(
  candidate: unknown
): candidate is ErrorBox.ErrorProps {
  return (
    null !== candidate &&
    JS.is.object(candidate) &&
    ('caption' in candidate || 'description' in candidate)
  );
}
