import { useContext } from 'react';
import { Id } from 'react-toastify';

import { toast } from '@instances';

import type { ErrorBox as Types } from './types';
import { ErrorBoxContext } from './context';
import { useLocalizeError } from './localize-error-hook';
import { ErrorBox } from './component';

export namespace Toaster {
  export type Use = () => Toaster;
}

export type Toaster = {
  (error: Types.Union): Id | undefined;
  warn(error: Types.Union): Id | undefined;
  info(error: Types.Union): Id | undefined;
};

export const useToaster: Toaster.Use = () => {
  type Toast = typeof toast.error;

  const { isErrorIgnored } = useContext(ErrorBoxContext);
  const localize = useLocalizeError();

  function doToast(method: Toast, error: Types.Union): Id | undefined {
    if (error instanceof Error) {
      if (isErrorIgnored && isErrorIgnored(error)) return;
    }

    return method(<ErrorBox simple {...{ error, localize }} />);
  }

  const toaster: Toaster = (error) => doToast(toast.error, error);
  toaster.warn = (error) => doToast(toast.warn, error);
  toaster.info = (error) => doToast(toast.info, error);

  return toaster;
};

export const Toaster = { use: useToaster };
