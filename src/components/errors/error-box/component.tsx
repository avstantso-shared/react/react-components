import React from 'react';
import { Alert, Form } from 'react-bootstrap';

import { JS } from '@avstantso/node-or-browser-js--utils';

import { Trans, LOCALES } from '@i18n';
import { ObjectViewer } from '@components/object-viewer';

import { ErrorDetailsDev } from '../error-details-dev';

import type { ErrorBox as Types } from './types';
import { isErrorProps } from './is-error-props';
import { useLocalizeError } from './localize-error-hook';

import './error-box.scss';

export const ErrorBox: Types.FC = (props) => {
  const {
    children,
    error,
    localize: propsLocalize,
    data,
    simple,
    inForm,
    hr: addHr,
  } = props;

  const localize = useLocalizeError(propsLocalize);
  if (!error) return null;

  const localized = localize(error);
  const { caption, description } = localized;

  const content = (
    <>
      {caption && <b>{caption}</b>}
      {caption && description && <br />}
      {description}
      {data && <ObjectViewer data={data} noRoot />}
    </>
  );

  if (simple) return content;

  const alert = (
    <>
      <Alert className="error-box" variant="danger">
        {content}
        {children && <div className="error-box-children">{children}</div>}
        {JS.is.object(error) && !isErrorProps(error) && (
          <ErrorDetailsDev {...{ error }} />
        )}
      </Alert>
      {addHr && <hr />}
    </>
  );

  return !inForm ? (
    alert
  ) : (
    <>
      <Form.Label className="error-box">
        <Trans i18nKey={LOCALES.error} />
      </Form.Label>
      {alert}
    </>
  );
};

ErrorBox.Simple = (props) => <ErrorBox {...props} simple />;
ErrorBox.InForm = (props) => <ErrorBox {...props} inForm />;
