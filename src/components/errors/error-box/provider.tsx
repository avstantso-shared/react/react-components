import React, { useContext } from 'react';

import { InvalidArgumentError } from '@avstantso/node-or-browser-js--errors';

import { useTranslation } from '@i18n';

import type { ErrorBox } from './types';
import { ErrorBoxContext } from './context';
import { MakeI18nLayers } from './utils';

export namespace ErrorBoxContextProvider {
  export type Props = ErrorBox.Context & { override?: boolean };
  export type FC = React.FC<Props> & {
    Test: React.FC<ErrorBox.Context.Provider.Props>;
  };
}

export const ErrorBoxContextProvider: ErrorBoxContextProvider.FC = (props) => {
  const {
    i18nLayers: propsi18nLayers,
    isErrorIgnored: propsIsErrorIgnored,
    override,
    ...rest
  } = props;

  const parent = useContext(ErrorBoxContext);

  const i18nLayers: typeof propsi18nLayers = override
    ? propsi18nLayers
    : [...(parent?.i18nLayers || []), ...propsi18nLayers];

  const isErrorIgnored: typeof propsIsErrorIgnored = override
    ? propsIsErrorIgnored
    : parent?.isErrorIgnored && propsIsErrorIgnored
    ? (error) => parent.isErrorIgnored(error) || propsIsErrorIgnored(error)
    : parent?.isErrorIgnored || propsIsErrorIgnored;

  if (process.env.DEBUG_INFO_ENABLED)
    for (let i = 0, n = i18nLayers.length; i < n - 1; i++)
      for (let j = i + 1; j < n; j++)
        if (
          i18nLayers[i].i18n === i18nLayers[j].i18n &&
          i18nLayers[i].t === i18nLayers[j].t
        )
          throw new InvalidArgumentError(
            `${ErrorBoxContextProvider.name}: Duplicate found: i18nLayers[${i}] === i18nLayers[${j}]`
          );

  return (
    <ErrorBoxContext.Provider
      value={{ i18nLayers, ...(isErrorIgnored && { isErrorIgnored }) }}
      {...rest}
    />
  );
};

ErrorBoxContextProvider.Test = (props) => {
  const i18nLayers = MakeI18nLayers(useTranslation);
  return <ErrorBoxContextProvider {...props} {...{ i18nLayers }} />;
};
