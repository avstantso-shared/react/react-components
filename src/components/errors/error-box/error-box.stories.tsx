import React, { useRef } from 'react';
import { Meta, Story } from '@story-helpers';
import Bluebird from 'bluebird';

import { JS } from '@avstantso/node-or-browser-js--utils';

import { useTranslation, LOCALES } from '@i18n';

import { ErrorBox } from '.';

type TestComponentProps = {
  'error-kind': string;
  'known-error': string;
  'error-as-str': string;
  'error-as-props': ErrorBox.ErrorProps;
  'error-details': NodeJS.Dict<any>;
} & Omit<ErrorBox.Props, 'error'>;
type TestComponent = React.FC<TestComponentProps>;
const TestComponent: TestComponent = () => null;

const defaultText = 'No error';

const knownErrors = Object.keys(LOCALES.errors).filter(
  (key) => key !== toString.name
);

const meta = {
  ...Meta(TestComponent, {
    title: 'Components/Errors/ErrorBox',
    argTypes: {
      'error-kind': {
        control: 'radio',
        options: ['known-error', 'as-str', 'as-props'],
      },
      'known-error': {
        control: 'radio',
        options: knownErrors,
      },
      'error-as-str': { control: { type: 'text' } },
      'error-as-props': { control: { type: 'object' } },
      'error-details': { control: { type: 'object' } },
      simple: { control: { type: 'boolean' } },
    },
  }),
  component: ErrorBox,
};

export default meta;

function parseKnownError(
  props: Omit<TestComponentProps, 'lang--need-reload'>
): ErrorBox.Props {
  const {
    'error-kind': kind,
    'known-error': knownError,
    'error-as-str': asStr,
    'error-as-props': asProps,
    'error-details': details,
    ...rest
  } = props;

  const error =
    'known-error' === kind
      ? Error(knownError)
      : 'as-str' === kind
      ? asStr
      : asProps;

  if (error instanceof Error && details)
    Object.entries(details).forEach(([key, value]) =>
      JS.set.raw(error, key, value)
    );

  return { ...rest, error };
}

const defArgs = {
  'error-kind': 'known-error',
  'known-error': knownErrors[0],
  'error-as-str': knownErrors[0],
  'error-as-props': {
    caption: knownErrors[0],
    description: '😞',
  },
  'error-details': {
    value: 'ABC',
  },
  simple: false,
};

export const standard = Story({
  args: defArgs,
  render: (args) => (
    <>
      <h3>Standard {ErrorBox.name}:</h3>
      <ErrorBox.Provider.Test>
        <ErrorBox {...parseKnownError(args)} />
      </ErrorBox.Provider.Test>
    </>
  ),
});

const TemplateEffectSub: TestComponent = (args) => {
  const localize = ErrorBox.useLocalizeError();

  const [, i18n] = useTranslation();

  const divRef = useRef<HTMLDivElement>();
  const execRef = useRef<Promise<void>>();

  React.useEffect(() => {
    if (!divRef.current) return;

    async function wait(secs: number, prefix?: string) {
      for (let i = 0; i < secs; i++) {
        if (divRef.current)
          divRef.current.innerHTML = `${
            prefix ? `<p>${prefix}<p/>` : ''
          }<i>Waiting <b>${(secs - i) * 1000}</b> msec&hellip;</i>`;

        await Bluebird.delay(1000);
      }
    }

    async function execute() {
      try {
        await wait(3);

        try {
          const { error } = parseKnownError(args);
          throw error;
        } catch (error) {
          const { caption, description } = localize({
            error,
            strictString: true,
          });

          await wait(10, `<b>${caption}</b><br>${description}`);
        }
      } finally {
        if (divRef.current) divRef.current.innerText = defaultText;

        execRef.current = undefined;
      }
    }

    const current = execRef.current;
    execRef.current = current ? current.then(execute) : execute();
  }, [i18n.language, JSON.stringify(args)]);

  return (
    <div style={{ background: 'cyan', padding: '0.5em' }}>
      <div
        ref={divRef}
        style={{ background: 'white', margin: '2em', padding: '0.5em' }}
      >
        {defaultText}
      </div>
      <p>Change props or language for view error</p>
      <p>
        A new asynchronous function is called after the previous one has
        completed (in theory)
      </p>
    </div>
  );
};

export const effect = Story({
  args: defArgs,
  render: (args) => (
    <>
      <h3>Effect {ErrorBox.name}:</h3>
      <ErrorBox.Provider.Test>
        <TemplateEffectSub {...args} />
      </ErrorBox.Provider.Test>
    </>
  ),
});
