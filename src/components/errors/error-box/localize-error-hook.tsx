import React, { useContext } from 'react';

import { JS } from '@avstantso/node-or-browser-js--utils';
import { FIELDS } from '@avstantso/node-or-browser-js--model-core';

import { Trans, LOCALES, noTags } from '@i18n';

import type { ErrorBox, LocalizeError } from './types';
import { ErrorBoxContext } from './context';
import { isErrorProps } from './is-error-props';

export const useLocalizeError: LocalizeError.Use = (
  received?: LocalizeError
) => {
  const { i18nLayers } = useContext(ErrorBoxContext) || {};

  return (
    received ||
    ((props) => {
      const { error, strictString }: LocalizeError.Props =
        props instanceof Error || isErrorProps(props) || JS.is.string(props)
          ? { error: props }
          : props;

      if (!error) return null;

      if (isErrorProps(error)) return error;

      const message: string = error instanceof Error ? error.message : error;
      if (!message) return null;

      const [rawPostfix] = /\s*".*$/.exec(message) || [];
      const postfix = (rawPostfix || '').trim();
      const clearMessage = `${
        !rawPostfix
          ? message
          : message.substring(0, message.length - rawPostfix.length)
      }`.replaceAll(':', '');

      const keys = ((
        errorsPrefix = `${LOCALES.errors}.`,
        prefix = `${
          message.startsWith(errorsPrefix) ? '' : errorsPrefix
        }${clearMessage}.`
      ) => ({
        caption: `${prefix}${FIELDS.caption}`,
        description: `${prefix}${FIELDS.description}`,
      }))();

      let values: ErrorBox.ErrorProps;
      i18nLayers?.some(({ t, i18n }) => {
        const caption = t(keys.caption);
        const description = t(keys.description);

        const hasCaption = caption !== keys.caption;
        const hasDescription = description !== keys.description;

        if (!(hasCaption || hasDescription)) return false;

        if (strictString)
          values = {
            caption: hasCaption ? noTags(caption) : message,
            ...(hasDescription ? { description: noTags(description) } : {}),
          };
        else
          values = {
            caption: hasCaption ? (
              <>
                <Trans.N {...{ t, i18n, i18nKey: keys.caption }} />
                {postfix && ` ${postfix}`}
              </>
            ) : (
              message
            ),
            ...(hasDescription
              ? {
                  description: (
                    <Trans.N
                      {...{
                        t,
                        i18n,
                        i18nKey: keys.description,
                      }}
                    />
                  ),
                }
              : {}),
          };

        return true;
      });

      if (!values) values = { caption: message };

      return values;
    })
  );
};
