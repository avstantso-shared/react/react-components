import React from 'react';

import { Extend } from '@avstantso/node-or-browser-js--utils';

import * as Types from './types';
import { useLocalizeError } from './localize-error-hook';
import { ErrorBoxContext as Context } from './context';
import { ErrorBoxContextProvider as Provider } from './provider';
import { ErrorBox as Box } from './component';
import { MakeI18nLayers } from './utils';
import { Toaster as _Toaster, useToaster } from './toast-hook';

export namespace ErrorBox {
  export type Toaster = _Toaster;

  export type ErrorProps = Types.ErrorBox.ErrorProps;

  export type Props = Types.ErrorBox.Props;

  export type FC = Types.ErrorBox.FC & {
    Context: typeof Context;
    Provider: Provider.FC;
    useLocalizeError: Types.LocalizeError.Use;
    makeI18nLayers: MakeI18nLayers;
    useToaster: _Toaster.Use;
  };
}

export const ErrorBox: ErrorBox.FC = Extend.Arbitrary(Box, {
  Context,
  Provider,
  useLocalizeError,
  makeI18nLayers: MakeI18nLayers,
  useToaster,
});
