import { createContext } from 'react';
import type { ErrorBox } from './types';

export const ErrorBoxContext = createContext<ErrorBox.Context>(undefined);
