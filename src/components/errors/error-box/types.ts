import React from 'react';
import { i18n } from 'i18next';

import { T } from '@i18n';

export namespace ErrorBox {
  export interface ErrorProps {
    caption: string | JSX.Element;
    description?: string | JSX.Element;
  }

  export type Union = Error | string | ErrorProps;

  export namespace Props {
    export type Base<TError extends Union = Union> = {
      readonly error: TError;
      readonly localize?: LocalizeError;
      readonly data?: any;
      readonly hr?: boolean;
    };
  }

  export type Props<TBase = Props.Base> = TBase & {
    readonly simple?: boolean;
    readonly inForm?: boolean;
  };

  export type FC<TBase = Props.Base> = React.FC<Props<TBase>> & {
    Simple: React.FC<TBase>;
    InForm: React.FC<TBase>;
  };

  export namespace Context {
    export namespace Provider {
      export type Props = { isErrorIgnored?(error: Error): boolean };
    }

    export type I18Item = {
      t: T;
      i18n: i18n;
    };
  }

  export type Context = {
    i18nLayers: ReadonlyArray<Context.I18Item>;
  } & Context.Provider.Props;
}

export namespace LocalizeError {
  export type Props = {
    readonly error: ErrorBox.Union;
    readonly strictString?: boolean;
  };

  export type Use = (received?: LocalizeError) => LocalizeError;
}

export type LocalizeError = (
  props: ErrorBox.Union | LocalizeError.Props
) => ErrorBox.ErrorProps;
