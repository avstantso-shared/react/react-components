import React from 'react';
import { Table, Accordion } from 'react-bootstrap';

import { JS } from '@avstantso/node-or-browser-js--utils';
import { Modifications } from '@avstantso/react--data';

import { Fa } from '@instances';
import { ObjectViewer } from '@components/object-viewer';

const { FaCogs } = Fa;

export namespace MDFDebugger {
  export interface Props<TEntity> {
    mdf: Modifications<TEntity>;
  }
}

function showType(v: any) {
  const t = typeof v;
  if (JS.object === t) {
    if (Array.isArray(v)) return 'Array';
    if (v instanceof Date) return 'Date';
  }

  return t;
}

export function MDFDebugger<TEntity extends any>(
  props: MDFDebugger.Props<TEntity>
): JSX.Element {
  const { mdf } = props;

  return (
    <Accordion>
      <Accordion.Item eventKey="0">
        <Accordion.Header>
          <FaCogs />
          &nbsp;<b>Modifications debugger</b>
        </Accordion.Header>
        <Accordion.Body>
          <Table size="sm" responsive hover>
            <thead>
              <tr>
                <th
                  rowSpan={2}
                  className="text-center"
                  style={{ verticalAlign: 'middle' }}
                >
                  Key
                </th>
                <th rowSpan={2} style={{ verticalAlign: 'middle' }}>
                  isMatch
                </th>
                <th rowSpan={2}></th>
                <th colSpan={2} className="text-center">
                  Current
                </th>
                <th rowSpan={2}></th>
                <th colSpan={2} className="text-center">
                  Unmodified
                </th>
              </tr>
              <tr>
                <th>
                  <i>Type</i>
                </th>
                <th>Value</th>
                <th>
                  <i>Type</i>
                </th>
                <th>Value</th>
              </tr>
            </thead>
            <tbody>
              {Object.entries(mdf.details).map(([key, item]) => {
                return (
                  <tr key={key}>
                    <td>
                      <b>{key}</b>
                    </td>
                    <td
                      className="text-center"
                      style={{ color: item.isMatch ? 'green' : 'red' }}
                    >{`${item.isMatch}`}</td>
                    <td />
                    <td>
                      <i>{showType(item.current)}</i>
                    </td>
                    <td>
                      <ObjectViewer
                        data={item.current}
                        diffData={{ data: item.unmodified }}
                      />
                    </td>
                    <td />
                    <td>
                      <i>{showType(item.unmodified)}</i>
                    </td>
                    <td>
                      <ObjectViewer
                        data={item.unmodified}
                        diffData={{ data: item.current }}
                      />
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </Table>
        </Accordion.Body>
      </Accordion.Item>
    </Accordion>
  );
}
