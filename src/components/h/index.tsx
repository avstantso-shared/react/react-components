import { InvalidArgumentError } from '@avstantso/node-or-browser-js--errors';
import { JS } from '@avstantso/node-or-browser-js--utils';
import React from 'react';

export namespace H {
  export type Size = 1 | 2 | 3 | 4 | 5 | 6;
  export type Props = React.HTMLProps<HTMLHeadingElement> & { size: Size };
  export type FC = React.FC<Props>;
}

export const H: H.FC = (props) => {
  const { size = 1, ...rest } = props;

  if (!JS.is.number(size) || size < 1 || size > 6)
    throw new InvalidArgumentError(`H.size prop must be number in [1..6]`);

  const _H = `h${size}`;

  return <_H {...rest} />;
};
