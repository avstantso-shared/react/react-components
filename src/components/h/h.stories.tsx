import React from 'react';
import { Meta, Story } from '@story-helpers';

import { H } from '.';

const meta = {
  ...Meta(H, {
    argTypes: {
      size: {
        options: [1, 2, 3, 4, 5, 6],
        control: { type: 'radio' },
      },
    },
  }),
};

export default meta;

export const h = Story(meta, {
  render: (args) => <H {...args}>Header (size={`${args.size}`})</H>,
});
