import React, { useCallback } from 'react';
import { Meta, Story } from '@story-helpers';

import { useEffect, useMemo, useState } from '@storybook/preview-api';

import { Equality, Generics } from '@avstantso/node-or-browser-js--utils';
import { Model } from '@avstantso/node-or-browser-js--model-core';

import { OptionsGrid } from '.';

type Option = {
  name: string;
  HP: number;
  MP: number;
  readonly: boolean;
};

const meta = { ...Meta(OptionsGrid) };

export default meta;

const match: Equality<Option> = Model.Named.Equals.as();
const predicate = Model.Named.Predicate;

const selectedDef: Option[] = [
  { name: 'Frog', HP: 10, MP: 0, readonly: false },
  { name: 'Dragon', HP: 1000, MP: 10000, readonly: false },
];
const optionsDef: Option[] = [
  ...selectedDef,
  { name: 'Demon', HP: 700, MP: 300, readonly: false },
];

const row = (option: Option): OptionsGrid.Row => ({
  key: option.name,
  cells: [
    { item: option.name },
    { item: <>{option.HP}</> },
    { item: <>{option.MP}</> },
  ],
  readOnly: option.readonly,
});

export const optionsGrid = Story(meta, {
  args: Generics.Cast.To({
    label: 'Creatures (name, HP, MP)',
    loading: false,
    readOnly: false,
    options: Generics.Cast.To(JSON.stringify(optionsDef, null, 2)),
    selected: JSON.stringify(
      selectedDef.map(({ name }) => ({ name })),
      null,
      2
    ),
  }),
  render: (args) => {
    const { options: optionsJSON, selected: selectedJSON, ...rest } = args;

    const options = useMemo<Option[]>(() => {
      try {
        return JSON.parse(Generics.Cast(optionsJSON));
      } catch {
        return [];
      }
    }, [optionsJSON]);
    const makeSelected = () => {
      try {
        const s: Option[] = JSON.parse(Generics.Cast(selectedJSON));
        return options.filter((item) => s.find(predicate(item)));
      } catch {
        return [];
      }
    };

    const [selected, setSelected] = useState<Option[]>(makeSelected());

    useEffect(() => {
      setSelected(makeSelected());
    }, [selectedJSON, options]);

    const handleChange = useCallback(
      (option: Option, checked: boolean) => {
        const next = [...selected];
        const i = next.findIndex(predicate(option));

        if (!checked) next.splice(i, 1);
        else if (i < 0) next.push(option);

        setSelected(next);
      },
      [selected]
    );

    return (
      <OptionsGrid<Option>
        {...rest}
        {...{ options, selected, row, match }}
        onChange={handleChange}
      />
    );
  },
});
