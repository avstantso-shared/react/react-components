import React, { useCallback } from 'react';
import { Table, Form, TableProps } from 'react-bootstrap';

import { Equality, Perform } from '@avstantso/node-or-browser-js--utils';

import { Combiner } from '@avstantso/react--data';

import { Loading } from '@components/loading';

import './optionsGrid.scss';

export namespace OptionsGrid {
  export interface Cell {
    item: string | JSX.Element;
    class?: string;
  }

  export interface Row {
    key: string;
    cells: Cell[];
    readOnly?: Perform<boolean>;
  }

  export type OnChange<TOption = any> = (
    option: TOption,
    checked: boolean
  ) => void;

  export interface Props<TOption = any> extends Omit<TableProps, 'onChange'> {
    label?: string;
    options: ReadonlyArray<TOption>;
    selected: ReadonlyArray<TOption>;
    row(option: TOption): Row;
    onChange?: OnChange<TOption>;
    match?: Equality<TOption>;
    loading?: boolean;
    readOnly?: Perform<boolean>;
  }
}

export function OptionsGrid<TOption extends any = any>(
  props: OptionsGrid.Props<TOption>
) {
  const {
    className,
    label,
    options,
    selected,
    loading,
    readOnly,
    row,
    onChange,
    match,
    ...rest
  } = props;

  const Predicate = match && Equality.Predicate.Factory(match);

  const onClick = useCallback(
    (option: TOption, checked: boolean) => () => onChange(option, !checked),

    [onChange]
  );

  return (
    <div>
      {label && <b>{label}</b>}
      {loading ? (
        <Loading.Text />
      ) : (
        <Table
          {...Combiner.className('options-grid', className)}
          size="sm"
          responsive
          hover
          {...rest}
        >
          <thead />
          <tbody>
            {options.map((option) => {
              const r = row(option);
              const c = Predicate
                ? selected.some(Predicate(option))
                : selected.includes(option);
              const isReadOnlyRow = Perform(readOnly) || Perform(r.readOnly);
              return (
                <tr
                  key={r.key}
                  onClick={!isReadOnlyRow ? onClick(option, c) : null}
                >
                  <td className="text-center options-check-column">
                    <Form.Check
                      id={`${r.key}-chb`}
                      name={`${r.key}-chb`}
                      type="checkbox"
                      checked={c}
                      readOnly
                      disabled={isReadOnlyRow}
                    />
                  </td>
                  {r.cells.map((cell, index) => (
                    <td key={index} className={cell.class}>
                      {cell.item}
                    </td>
                  ))}
                </tr>
              );
            })}
          </tbody>
        </Table>
      )}
    </div>
  );
}
