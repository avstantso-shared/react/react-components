import React from 'react';

import { TS } from '@avstantso/node-or-browser-js--utils';

import { Combiner, GFC } from '@avstantso/react--data';

import './sizes.scss';

const keysAsc = TS.Type.Definer<keyof Sizes.Props.Base>().Arr(
  'xs',
  'sm',
  'md',
  'lg',
  'xl'
);

// Todo: use GFC.AsElement
export namespace Sizes {
  export namespace Props {
    export type Base = {
      xs?: React.ReactNode;
      sm?: React.ReactNode;
      md?: React.ReactNode;
      lg?: React.ReactNode;
      xl?: React.ReactNode;
    };
  }
  export type Props<T extends keyof JSX.IntrinsicElements = 'span'> =
    React.ComponentPropsWithoutRef<T> & Props.Base & { as?: T };

  export type FC = <T extends keyof JSX.IntrinsicElements = 'span'>(
    props: React.PropsWithChildren<Props<T>>,
    context: any
  ) => JSX.Element;
}

export const Sizes: Sizes.FC = (props) => {
  const {
    children,
    className,

    as: As = 'span',
    xs,
    sm,
    md,
    lg,
    xl,
    ...rest
  } = props;

  function getContent(key: keyof Sizes.Props.Base): JSX.Element {
    const content: React.ReactNode = props[key];
    if (!content) return null;

    const innerProps = {
      ...rest,
      ...Combiner.className('sizes', key, className),
      key,
      children: content,
      ref: (element: Element) => {
        if (!element) return;

        const options = {
          root: document.documentElement,
        };

        const observer = new IntersectionObserver((entries, observer) => {
          entries.forEach((entry) => {
            entry.target.classList.toggle(
              'visible-now',
              entry.intersectionRatio > 0
            );
          });
        }, options);

        observer.observe(element);
      },
    };

    const C: any = As;

    return <C {...innerProps} />;
  }

  return (
    <>
      {keysAsc.map(getContent)}
      {children}
    </>
  );
};
