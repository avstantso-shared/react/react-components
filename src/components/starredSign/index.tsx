import React, { useMemo } from 'react';
import { Gender, Filter, UseTranslationEx } from '@avstantso/react--data';

import { useTranslation, LOCALES, T } from '@i18n';
import { Fa } from '@instances';

const { FaStarHalfAlt, FaStar, FaRegStar } = Fa;

export namespace StarredSign {
  export namespace Props {
    export namespace I18n {
      export type Keys = Readonly<{
        starred: string;
        notStarred: string;
        both: string;
      }>;
    }

    export interface I18n {
      useTranslation?: UseTranslationEx;
      keys?: I18n.Keys;
    }
  }

  export namespace Common {
    export interface Props {
      gender?: Gender;
      i18n?: Props.I18n;
    }
  }

  export type Props = Common.Props &
    Readonly<{
      starred?: boolean;
      both?: boolean;
      labled?: boolean;
      onClick?: React.MouseEventHandler;
    }>;

  export namespace FC {
    export type UseFilterCaption = {
      (props?: Common.Props): Filter.Bool.Captions;
      Male(props?: Props.I18n): Filter.Bool.Captions;
      Female(props?: Props.I18n): Filter.Bool.Captions;
      Neuter(props?: Props.I18n): Filter.Bool.Captions;
    };
  }

  export interface FC extends React.FC<Props> {
    useFilterCaption: FC.UseFilterCaption;
  }
}

const getLables = (t: T, gender: Gender, i18n?: StarredSign.Props.I18n) => {
  const {
    starred = LOCALES.starredSign.starred,
    notStarred = LOCALES.starredSign.notStarred,
  } = i18n?.keys || {};

  const context = Gender.toName(gender);

  return {
    starred: t(starred, {
      context,
    }),
    notStarred: t(notStarred, {
      context,
    }),
  };
};

export const StarredSign: StarredSign.FC = (props) => {
  const { starred, both, labled, onClick, gender, i18n } = props;

  const [t] = (i18n?.useTranslation || useTranslation)();

  const labels = getLables(t, gender, i18n);

  if (both) {
    const bothTitle =
      (i18n?.keys?.both && t(i18n.keys.both)) ||
      `${labels.starred} / ${labels.notStarred}`;
    return <FaStarHalfAlt className="sign starred-sign" title={bothTitle} />;
  }

  const Sign = starred ? FaStar : FaRegStar;
  const title = starred ? labels.starred : labels.notStarred;

  return labled ? (
    <>
      <Sign className="sign starred-sign" title={title} onClick={onClick} />{' '}
      {title}
    </>
  ) : (
    <Sign className="sign starred-sign" title={title} onClick={onClick} />
  );
};

const useFilterCaption: StarredSign.FC.UseFilterCaption = (props?) => {
  const { gender, i18n } = props || {};
  const [t] = (i18n?.useTranslation || useTranslation)();

  return useMemo(
    () => ({
      false: <StarredSign labled {...{ gender, i18n }} />,
      true: <StarredSign labled {...{ gender, i18n }} starred />,
    }),
    [t, gender]
  );
};
useFilterCaption.Male = (props?) =>
  useFilterCaption({ gender: Gender.male, ...(props || {}) });
useFilterCaption.Female = (props?) =>
  useFilterCaption({ gender: Gender.female, ...(props || {}) });
useFilterCaption.Neuter = (props?) =>
  useFilterCaption({ gender: Gender.neuter, ...(props || {}) });

StarredSign.useFilterCaption = useFilterCaption;
