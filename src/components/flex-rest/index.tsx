import React from 'react';

import { Combiner, InnerRef } from '@avstantso/react--data';

import './flex-rest.scss';

export namespace FlexRest {
  export type Props = React.ComponentPropsWithoutRef<'div'> &
    InnerRef.Provider<HTMLDivElement>;

  export type FC = React.FC<Props>;
}

export const FlexRest: FlexRest.FC = ({
  children,
  className,
  innerRef,
  ...rest
}) => (
  <div
    {...Combiner.className('flex-rest-outer', className)}
    {...rest}
    ref={innerRef}
  >
    <div className="flex-rest-inner">{children}</div>
  </div>
);
