import type { Notifications as Types } from './types';
import { Context, Provider, List } from './outside';
import { NotificationsReg as Reg } from './reg';
import { NotificationsSign as Sign } from './sign';

import './notifications.scss';

export namespace Notifications {
  export type Color = Types.Color;

  export type Props = Types.Props;

  export namespace Reg {
    export type Props = Types.Reg.Props;
    export type FC = Types.Reg.FC;
  }

  export namespace Sign {
    export type Props = Types.Sign.Props;
    export type FC = Types.Sign.FC;
  }
}

export type Notifications = Types;

export const Notifications: Notifications = {
  Context,
  Provider,
  List,
  Reg,
  Sign,
};
