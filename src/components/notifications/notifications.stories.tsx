import React from 'react';
import { useState } from '@storybook/preview-api';

import { Meta, Story, Data } from '@story-helpers';

import { Notifications } from '.';

const FV = Data.FruitsVegetables;

const meta = { ...Meta(Notifications.Provider, { title: 'Notifications' }) };

export default meta;

export const notification = Story(meta, {
  render: (args) => {
    const [paths, setPaths] = useState<string[]>([
      FV.Path.fruits.citrus.orange,
      FV.Path.fruits.citrus.lemon,
    ]);

    const PathCheck: React.FC<{ path: string }> = ({ path }) => (
      <input
        type="checkbox"
        checked={paths.includes(path)}
        onChange={(e) =>
          setPaths((prev) => {
            const next = [...prev];

            if (e.currentTarget.checked) next.push(path);
            else {
              const i = next.indexOf(path);
              next.splice(i, 1);
            }

            return next;
          })
        }
      />
    );

    return (
      <Notifications.Provider>
        <Notifications.Reg
          path={FV.Path.fruits.citrus.orange}
          color="warning"
          disabled={!paths.includes(FV.Path.fruits.citrus.orange)}
        >
          This orange is very sweet
        </Notifications.Reg>
        <Notifications.Reg
          node={FV.fruits.citrus.lemon}
          color="danger"
          disabled={!paths.includes(FV.Path.fruits.citrus.lemon)}
        >
          This lemon is very sour
        </Notifications.Reg>
        <Notifications.Reg
          node={FV.vegetables}
          color="info"
          disabled={!paths.includes(`${FV.Path.vegetables}`)}
        >
          This vegetables is so so
        </Notifications.Reg>
        Level 0
        <ul>
          <li>
            Fruits <Notifications.Sign path={`${FV.Path.fruits}`} />
            <ul>
              <li>
                Citrus <Notifications.Sign node={FV.fruits.citrus} />
                <ul>
                  <li>
                    <PathCheck path={`${FV.Path.fruits.citrus.orange}`} />{' '}
                    Orange <Notifications.Sign node={FV.fruits.citrus.orange} />
                  </li>
                  <li>
                    <PathCheck path={`${FV.Path.fruits.citrus.lemon}`} /> Lemon{' '}
                    <Notifications.Sign node={FV.fruits.citrus.lemon} />
                  </li>
                </ul>
              </li>
            </ul>
          </li>

          <li>
            <PathCheck path={`${FV.Path.vegetables}`} /> Vegetables{' '}
            <Notifications.Sign node={FV.vegetables} />
            <ul>
              <li>
                Tomato{' '}
                <Notifications.Sign path={`${FV.Path.vegetables.tomato}`} />
              </li>
            </ul>
          </li>
        </ul>
      </Notifications.Provider>
    );
  },
});
