import { useLayoutEffect } from 'react';

import { Reducer } from '@avstantso/react--data';
import { Outside } from '@meta-components/outside';

import type { Notifications } from './types';

type CustomContextHook =
  Outside.Options<Notifications.TypeMap>['customContextHook'];

function detectPlacement(): Notifications.TypeMap['Context']['defaultPlacement'] {
  return document.body.clientWidth < 600 ? 'bottom-end' : 'right';
}

const initialState: Notifications.TypeMap['Context'] = {
  defaultPlacement: detectPlacement(),
};

const { useRefReducer } = Reducer(initialState).as((f) => ({
  SET_DEFAULT_PLACEMENT: f('defaultPlacement'),
}));

export const useCustomContext: CustomContextHook = () => {
  const [state, dispatch] = useRefReducer();

  useLayoutEffect(() => {
    let timeout: NodeJS.Timeout;

    const resizeObserver = new ResizeObserver(() => {
      clearTimeout(timeout);
      timeout = setTimeout(
        () => dispatch.SET_DEFAULT_PLACEMENT(detectPlacement()),
        200
      );
    });

    resizeObserver.observe(document.body);

    return () => {
      clearTimeout(timeout);
      resizeObserver.unobserve(document.body);
    };
  }, []);

  return state;
};
