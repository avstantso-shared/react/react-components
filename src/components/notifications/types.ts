import React from 'react';
import { BadgeProps, OverlayTriggerProps } from 'react-bootstrap';

import type { PathXorNode } from '@types';
import { Outside } from '@meta-components/outside';

export namespace Notifications {
  export type Color = BadgeProps['bg'];

  export type Props = {
    path: string;
    content: React.ReactNode;
    color: Color;
    disabled?: boolean;
  };

  export type TypeMap = Outside.TypeMap<
    Props,
    { defaultPlacement: OverlayTriggerProps['placement'] }
  >;

  export namespace Reg {
    export type Props = Omit<Notifications.Props, 'content' | 'path'> &
      Partial<Pick<Notifications.Props, 'content'>> &
      PathXorNode;

    export type FC = React.FC<Props>;
  }

  export namespace Sign {
    export type Props = PathXorNode &
      Pick<OverlayTriggerProps, 'placement'> & {
        size?: -2 | -1 | 0 | 1 | 2;
        prefix?: JSX.Element | string;
        withPrefix?: boolean;
        postfix?: JSX.Element | string;
        withPostfix?: boolean;
        as?: 'sup' | 'sub' | 'span';
      };
    export type FC = React.FC<Props>;
  }

  export namespace Notification {
    export type Props = Pick<Notifications.Props, 'content'>;

    export type FC = React.FC<Props>;
  }
}

export type Notifications = Omit<
  ReturnType<typeof Outside<Notifications.TypeMap>>,
  'ItemReg'
> & {
  Reg: Notifications.Reg.FC;
  Sign: Notifications.Sign.FC;
};
