import React from 'react';

import type { Notifications } from './types';
import { ItemReg } from './outside';

export const NotificationsReg: Notifications.Reg.FC = (props) => {
  const {
    children,
    content = children,
    node,
    path = node?._path,
    ...rest
  } = props;

  return (
    <ItemReg
      outKey={path}
      {...rest}
      {...{ path, content }}
      deps={[content, props.color, props.disabled]}
    />
  );
};
