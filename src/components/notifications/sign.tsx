import React, { useContext } from 'react';
import { Badge, OverlayTrigger, Popover, PopoverBody } from 'react-bootstrap';

import { NeedProviderError } from '@classes';

import type { Notifications } from './types';
import { colorPriority } from './utils';
import { Context } from './outside';
import { Notification } from './notification';

const onClick: React.MouseEventHandler = (e) => {
  e.stopPropagation();
  e.preventDefault();
};

export const NotificationsSign: Notifications.Sign.FC = (props) => {
  const { list, defaultPlacement } = useContext(Context) || {};
  if (!list)
    throw new NeedProviderError('Notifications.Sign', 'Notifications.Provider');

  const {
    node,
    path = node?._path,
    placement = defaultPlacement,
    size = 0,
    prefix = <>&nbsp;</>,
    postfix = <>&nbsp;</>,
    withPrefix,
    withPostfix,
    as: As = 'span',
  } = props;

  const filtered = list
    .filter(
      ({ disabled, path: itemPath }) => !disabled && itemPath.startsWith(path)
    )
    .sort(({ color: a }, { color: b }) => {
      const ai = colorPriority.indexOf(a);
      const bi = colorPriority.indexOf(b);

      return ai - bi;
    });

  if (!filtered.length) return null;

  const { color } = filtered[0];

  const popover = (
    <Popover>
      <PopoverBody>
        {filtered.map((itemProps) => (
          <Notification {...itemProps} />
        ))}
      </PopoverBody>
    </Popover>
  );

  return (
    <span className="notifications-sign-container" {...{ onClick }}>
      <OverlayTrigger
        trigger="click"
        rootClose
        {...{ placement }}
        overlay={popover}
      >
        {({ ref, ...triggerHandler }) => (
          <>
            {withPrefix && prefix}
            <As className="notifications-sign" {...triggerHandler}>
              <Badge className={`size-${size}`} {...{ ref }} bg={color}>
                {filtered.length}
              </Badge>
            </As>
            {withPostfix && postfix}
          </>
        )}
      </OverlayTrigger>
    </span>
  );
};
