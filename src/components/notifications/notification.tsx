import React from 'react';

import type { Notifications } from './types';

export const Notification: Notifications.Notification.FC = ({
  content,
  children,
}) => (
  <li>
    {content}
    {children}
  </li>
);
