import type { Notifications } from './types';

export const colorPriority: Notifications.Color[] = [
  'danger',
  'warning',
  'info',
  'primary',
  'secondary',
  'success',
  'light',
  'dark',
];
