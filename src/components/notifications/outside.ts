import { Outside } from '@meta-components/outside';

import type { Notifications } from './types';
import { Notification } from './notification';
import { useCustomContext } from './custom-context';

export const { Context, Provider, ItemReg, List } =
  Outside<Notifications.TypeMap>({
    label: 'Notification',
    ItemView: Notification,
    customContextHook: useCustomContext,
  });
