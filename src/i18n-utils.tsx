import React from 'react';
import { Link } from 'react-router-dom';
import i18next, {
  ParseKeys,
  Namespace,
  TypeOptions,
  TOptions,
  KeyPrefix,
} from 'i18next';
import * as ReactI18next from 'react-i18next';

import {
  Cases,
  Generics,
  JS,
  HTMLUtils,
} from '@avstantso/node-or-browser-js--utils';
import type { LocalString } from '@avstantso/node-or-browser-js--model-core';

import * as ReactData from '@avstantso/react--data';

import { Nowrap } from '@components/nowrap';
import { Decorations } from '@components/decorations';
import { BrN } from '@root/components/br-if-narrow';
import Quotes from '@components/quotes/raw';

type _DefaultNamespace = TypeOptions['defaultNS'];

export type TransSimple = <
  Key extends ParseKeys<Ns, TOpt, KPrefix>,
  Ns extends Namespace = _DefaultNamespace,
  TOpt extends TOptions = {},
  KPrefix extends KeyPrefix<Ns> = undefined,
  E = React.HTMLProps<HTMLDivElement>
>(
  props: ReactI18next.TransProps<Key, Ns, TOpt, KPrefix, E>
) => React.ReactElement;

export type TransObj2CmpOptions = {
  [key: string]: React.FC;
};

export type Trans = TransSimple & {
  N: TransSimple;
  tagsCmp: ReactI18next.TransProps<any>['components'];
  obj2Cmp(obj: TransObj2CmpOptions): ReactI18next.TransProps<any>['components'];
  hash2LinksCmp(...hashs: string[]): ReactI18next.TransProps<any>['components'];
};

const TransNComponents = (() => {
  const Nbsp: React.FC = () => <>&nbsp;</>;
  const Emsp: React.FC = () => <>&emsp;</>;
  const Hellip: React.FC = () => <>&hellip;</>;
  const Shy: React.FC = () => <>&shy;</>;

  const BrNs = {};
  BrN.limits.forEach((narrow) => {
    const C = JS.get.raw(BrN, `If${narrow}`);

    for (let i = 0; i <= 1; i++)
      JS.set.raw(
        BrNs,
        `BrN${narrow}${i > 0 ? 'h' : ''}`,
        <C {...(i > 0 ? { h: true } : {})} />
      );
  });

  return {
    N: <Nowrap />,
    D: <Decorations />,
    Nbsp: <Nbsp />,
    Emsp: <Emsp />,
    Hellip: <Hellip />,
    Shy: <Shy />,
    b: <b />,
    i: <i />,
    u: <u />,
    s: <s />,
    br: <br />,
    wbr: <wbr />,
    ...BrNs,
  };
})();

export function MakeUseTranslation(
  defaultNS: string
): [ReactData.UseTranslationEx, Trans] {
  const f = ReactData.MakeUseTranslation(defaultNS);

  function Stub<
    Key extends ParseKeys<Ns, TOpt, KPrefix>,
    Ns extends Namespace = _DefaultNamespace,
    TOpt extends TOptions = {},
    KPrefix extends KeyPrefix<Ns> = undefined,
    E = React.HTMLProps<HTMLDivElement>
  >(
    props: ReactI18next.TransProps<Key, Ns, TOpt, KPrefix, E>
  ): ReactData.UseTranslationEx {
    const { t: pT, i18n: pI18n } = props;
    return pT && pI18n
      ? ((() => [pT, pI18n, true]) as ReactData.UseTranslationEx)
      : f;
  }

  function Trans<
    Key extends ParseKeys<Ns, TOpt, KPrefix>,
    Ns extends Namespace = _DefaultNamespace,
    TOpt extends TOptions = {},
    KPrefix extends KeyPrefix<Ns> = undefined,
    E = React.HTMLProps<HTMLDivElement>
  >(
    props: ReactI18next.TransProps<Key, Ns, TOpt, KPrefix, E>
  ): React.ReactElement {
    const { t: pT, i18n: pI18n, ...rest } = props;
    const [t, i18n] = Stub<Key, Ns, TOpt, KPrefix, E>(props)();

    return (
      <ReactI18next.Trans<Key, Ns, TOpt, KPrefix, E>
        t={pT || t}
        i18n={pI18n || i18n}
        {...Generics.Cast(rest)}
      />
    );
  }

  Trans.N = function <
    Key extends ParseKeys<Ns, TOpt, KPrefix>,
    Ns extends Namespace = _DefaultNamespace,
    TOpt extends TOptions = {},
    KPrefix extends KeyPrefix<Ns> = undefined,
    E = React.HTMLProps<HTMLDivElement>
  >(
    props: ReactI18next.TransProps<Key, Ns, TOpt, KPrefix, E>
  ): React.ReactElement {
    const { children, components: outerComponents, ...rest } = props;
    const [, i18n] = Stub<Key, Ns, TOpt, KPrefix, E>(props)();
    const Q = <Quotes i18n={i18n || rest.i18n} />;

    const components = {
      ...TransNComponents,
      Q,
      q: Q,
      ...outerComponents,
    };

    return (
      <Trans<Key, Ns, TOpt, KPrefix, E>
        {...{ components }}
        {...Generics.Cast(rest)}
      >
        {!JS.is.string(children)
          ? children
          : children
              .replaceAll('&nbsp;', '<Nbsp/>')
              .replaceAll('&emsp;', '<Emsp/>')
              .replaceAll('&hellip;', '<Hellip/>')
              .replaceAll('&shy;', '<Shy/>')}
      </Trans>
    );
  };

  Trans.tagsCmp = {
    b: <b />,
    i: <i />,
    s: <s />,
    ul: <ul />,
    ol: <ol />,
    li: <li />,
    details: <details />,
    summary: <summary />,
  };

  Trans.obj2Cmp = function (
    obj: TransObj2CmpOptions
  ): ReactI18next.TransProps<any>['components'] {
    const r: any = {};

    Object.keys(obj).forEach((key) => {
      const Value: unknown = obj[key as keyof object];
      if (JS.is.function(Value)) r[key] = <Value />;
      else throw Error(`Unexpected type ${typeof Value} for "${key}"`);
    });

    return r;
  };

  Trans.hash2LinksCmp = function (
    ...hashs: string[]
  ): ReactI18next.TransProps<any>['components'] {
    const r: any = {};

    hashs.forEach((hash) => {
      const key = `HashLink${Cases.pascal(hash).replaceAll(
        '-',
        ''
      )}` as keyof object;
      r[key] = <Link to={`#${hash}`} />;
    });

    return r;
  };

  return [f, Trans];
}

export function noTags<TArg extends string | LocalString>(
  localeStrWithTags: TArg
): TArg {
  if (JS.is.string(localeStrWithTags))
    return HTMLUtils.noTags(localeStrWithTags) as TArg;

  const r: any = {};
  Object.keys(localeStrWithTags).forEach((key) => {
    r[key] = HTMLUtils.noTags(JS.get.raw(localeStrWithTags, key));
  });

  return r;
}
