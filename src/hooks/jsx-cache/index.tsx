import React, { useMemo, useRef } from 'react';

import { JS } from '@avstantso/node-or-browser-js--utils';

export namespace JSXCache {
  export namespace Value {
    export type Default = () => Value;
  }

  export type Value = React.ReactNode;

  export type Options = {
    defaultValue?: Value.Default;
    recalc?: boolean;
  };

  export type Use = {
    (options?: Options): JSXCache;
    (defaultValue?: Value.Default): JSXCache;
  };
}

function isOptions(candidate: unknown): candidate is JSXCache.Options {
  return (
    null !== candidate &&
    JS.is.object(candidate) &&
    ('defaultValue' in candidate || 'recalc' in candidate)
  );
}

export type JSXCache = {
  peek(): JSXCache.Value;
  Peek: React.FC;

  pop(): JSXCache.Value;
  Pop: React.FC;

  push(current: JSXCache.Value): JSXCache.Value;
  Push: React.FC<{ children: JSXCache.Value }>;
};

export const useJSXCache: JSXCache.Use = (
  param?: JSXCache.Options | JSXCache.Value.Default
): JSXCache => {
  const { defaultValue, recalc }: JSXCache.Options = isOptions(param)
    ? param
    : { defaultValue: param };

  const cache = useRef<JSXCache.Value>();

  return useMemo(() => {
    function peek() {
      return cache.current || (defaultValue && defaultValue()) || null;
    }

    const Peek: React.FC = ({ children }) => (
      <>
        {children}
        {peek()}
      </>
    );

    function pop() {
      const r = peek();
      cache.current = undefined;
      return r;
    }

    const Pop: React.FC = ({ children }) => (
      <>
        {children}
        {pop()}
      </>
    );

    function push(value: JSXCache.Value) {
      return (cache.current = value);
    }

    const Push: React.FC<{ children: JSXCache.Value }> = ({ children }) => (
      <>{push(children)}</>
    );

    return { peek, Peek, pop, Pop, push, Push };
  }, [recalc]);
};

export const JSXCache = { use: useJSXCache, Options: { is: isOptions } };
