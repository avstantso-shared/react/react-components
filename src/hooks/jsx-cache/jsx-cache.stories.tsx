import React from 'react';
import { Meta, Story } from '@story-helpers';
import { useState } from '@storybook/preview-api';
import { Badge } from 'react-bootstrap';

import { JSXCache } from '.';

type JSXCacheTestFC = React.FC;
const JSXCacheTestFC: JSXCacheTestFC = (props) => null;

const meta = { ...Meta(JSXCacheTestFC, { title: 'Hooks/JsxCache' }) };

export default meta;

const defaultValue = () => <Badge bg="secondary">Default</Badge>;

const ifStateLt1 = (
  <>
    (if state index &lt; <b>1</b>)
  </>
);

export const jsxCache = Story(meta, {
  render: () => {
    const { Push, Peek, Pop } = JSXCache.use({ defaultValue });

    const [state, setState] = useState(0);

    return (
      <>
        <h3>JSXCache</h3>
        Current state index is <b>{state}.</b>
        <ul>
          <li>
            {state < 2 ? 'Default' : 'Current'} value:&nbsp;
            <Peek />
          </li>
          <li>
            New value 1:&nbsp;
            <Push>
              <Badge bg="primary">Value1</Badge>
            </Push>
          </li>
          <li>
            Crrent value:&nbsp;
            <Peek />
          </li>
          <li>
            New value 2:&nbsp;
            <Push>
              <Badge bg="danger">Value2</Badge>
            </Push>
          </li>
          <li>
            Crrent value:&nbsp;
            <Peek />
          </li>
          {state < 1 && (
            <>
              <li>
                Pop value {ifStateLt1}:&nbsp;
                <Pop />
              </li>
              <li>
                Current value is default {ifStateLt1}:&nbsp;
                <Peek />
              </li>
            </>
          )}
        </ul>
        <button onClick={() => setState(state + 1)}>Inc state index</button>
      </>
    );
  },
});
