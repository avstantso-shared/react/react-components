import React, { useLayoutEffect } from 'react';

import { Generic } from '@avstantso/node-or-browser-js--utils';

import { DOMGet } from '@root/utils';

export namespace ScrollbarsWatcher {
  export type Options = {
    /**
     * @summary target Get element for setup classes. Default is `observable`
     */
    target?: DOMGet.Ref;

    /**
     * @summary Additional elements for triiger update
     */
    observables?: DOMGet.Ref | DOMGet.Ref[];
  };

  export type Overload = {
    /**
     * @summary Hook. Add to `options.target` or `observable` element `scroll-x-visible` and `scroll-y-visible` if `observable` scrollbars visible
     * @param observable Observable Element ref
     * @param options Options
     */
    (observable: DOMGet.Ref, options?: Options): void;
  };
}

export type ScrollbarsWatcher = {
  /**
   * @summary Hook. Add to `options.target` or `observable` element `scroll-x-visible` and `scroll-y-visible` if `observable` scrollbars visible
   * @param observable Observable Element ref
   * @param options Options
   */
  use: ScrollbarsWatcher.Overload;

  /**
   * @summary Add to `target` element `scroll-x-visible` and `scroll-y-visible` if `element` scrollbars visible
   * @param element Element
   * @param target Get element for setup classes. Default is `element`
   */
  exec: typeof execScrollbarsWatcher;
};

function execScrollbarsWatcher(element: Element, target: DOMGet.Ref): void {
  const t = target ? DOMGet.getElement(target) : element;
  const has = {
    x: element.scrollWidth > element.clientWidth,
    y: element.scrollHeight > element.clientHeight,
  };

  Object.entries(has).forEach(([key, value]) =>
    t.classList.toggle(`scroll-${key}-visible`, value)
  );
}

/**
 * @summary Hook. Add to `options.target` or `observable` element `scroll-x-visible` and `scroll-y-visible` if `observable` scrollbars visible
 * @param observable Observable Element ref
 * @param options Options
 */
export const useScrollbarsWatcher: ScrollbarsWatcher.Overload = (
  observable,
  { target, observables }
) => {
  useLayoutEffect(() => {
    function resizeHandler() {
      const element = DOMGet.getElement(observable);
      execScrollbarsWatcher(element, target || (() => element as Generic));
    }

    const resizeObserver = new ResizeObserver(resizeHandler);

    resizeObserver.observe(DOMGet.getElement(observable));

    if (observables)
      (Array.isArray(observables) ? observables : [observables]).forEach((o) =>
        resizeObserver.observe(DOMGet.getElement(o))
      );

    return () => {
      resizeObserver.disconnect();
    };
  }, []);
};

export const ScrollbarsWatcher: ScrollbarsWatcher = {
  use: useScrollbarsWatcher,
  exec: execScrollbarsWatcher,
};
