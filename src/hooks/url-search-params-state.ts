import React, { useRef, useEffect, useMemo } from 'react';
import { useLocation, useNavigate } from 'react-router';

import { JS, Perform, TS } from '@avstantso/node-or-browser-js--utils';

import { SetStateAction } from '@avstantso/react--data';

/**
 * @summary Create "state" based on `URLSearchParams` of `location` from `react-router`
 * @param mapping Mapping for define "state" object
 * @returns Hook
 */
export namespace URLSearchParamsState {
  /**
   * @summary Mapping constraint
   */
  export type Mapping = NodeJS.Dict<TS.Type.Literal>;

  /**
   * @summary `URLSearchParams` "state" hook
   * @param options Hook options
   * @returns Array "state" and dispatch method
   */
  export namespace Hook {
    export type Dispatch<
      TMapping extends Mapping,
      TResolved extends TS.Type.Resolve.Structure<TMapping>
    > = React.Dispatch<SetStateAction<TResolved>> & {
      [K in keyof TMapping as `set${Capitalize<
        Extract<K, string>
      >}`]: React.Dispatch<SetStateAction<TResolved[K]>>;
    };

    /**
     * @summary `URLSearchParams` "state" hook result
     */
    export type Result<
      TMapping extends Mapping,
      TResolved extends TS.Type.Resolve.Structure<TMapping>
    > = [TResolved, Dispatch<TMapping, TResolved>];

    /**
     * @summary `URLSearchParams` "state" hook options
     */
    export type Options<
      TMapping extends Mapping,
      TResolved extends TS.Type.Resolve.Structure<TMapping>
    > = {
      /**
       * @summary Initial state. Apply only if all fields `undefined`
       */
      initialize?(): Partial<TResolved>;

      /**
       * @summary Default values for `undefined` fields
       * @default TS.Type.emptyLiteralValue
       */
      defaults?<K extends keyof TMapping>(
        key: K,
        type: TMapping[K]
      ): TResolved[K];

      /**
       * @summary Convert not string values
       * @default baseConvert
       */
      convert?<K extends keyof TMapping>(
        key: K,
        type: TMapping[K],
        value: string,
        base?: () => TResolved[K]
      ): TResolved[K];

      /**
       * @summary Idle before implement new state.
       *
       * if `falsy` — apply changes immediately
       * else wait `Idle` milliseconds before apply changes. if new changes is coming in this timeout, previos changes will ignored
       *
       * @default baseConvert
       */
      idle?: number;
    };
  }

  export type Hook<
    TMapping extends Mapping,
    TResolved extends TS.Type.Resolve.Structure<TMapping> = TS.Type.Resolve.Structure<TMapping>
  > = (
    options?: Perform<Hook.Options<TMapping, TResolved>>
  ) => Hook.Result<TMapping, TResolved>;
}

export type URLSearchParamsState = {
  <TMapping extends URLSearchParamsState.Mapping>(
    mapping: TMapping
  ): URLSearchParamsState.Hook<TMapping>;

  <TMapping extends Function & { map: URLSearchParamsState.Mapping }>(
    mapping: TMapping
  ): URLSearchParamsState.Hook<TMapping['map']>;
};

const emptyDefaults = (key: any, type: any) =>
  TS.Type.emptyLiteralValue<any>(type);

const baseConvert = (key: any, type: any, value: string): any => {
  const L = TS.Type.Literal;

  switch (type) {
    case L.boolean:
      return value && '0' !== value && !/^false$/i.test(value);
    case L.number:
      return Number.parseInt(value, 10) || Number.parseFloat(value);
    case L.bigint:
      return Number.parseInt(value, 10) || Number.parseFloat(value);
    case L.Date:
      return new Date(value);
    case L.Buffer:
      return Buffer.from(value, 'base64url');
    default:
      return value;
  }
};

export const URLSearchParamsState: URLSearchParamsState = (
  mappingOverload: any
) => {
  type Ref = {
    state: any;
    dispatch: Function;
    timeout?: NodeJS.Timeout;
    searchParams?: URLSearchParams;
  };

  const mapping = JS.is.function(mappingOverload)
    ? mappingOverload.map
    : mappingOverload;

  return (options?: any) => {
    const {
      initialize,
      defaults = emptyDefaults,
      convert = baseConvert,
      idle,
    } = Perform(options) || {};

    const location = useLocation();
    const navigate = useNavigate();

    const ref = useRef<Ref>();
    const initial = !ref.current;

    if (initial) {
      function doDispatch(param: any) {
        const next = JS.is.function(param) ? param(ref.current.state) : param;

        Object.keys(mapping).forEach((key) =>
          undefined === next[key]
            ? ref.current.searchParams.delete(key)
            : ref.current.searchParams.set(key, next[key])
        );

        navigate(
          { ...location, search: ref.current.searchParams.toString() },
          { state: { location } }
        );
      }

      function dispatch(param: any) {
        if (!idle) return doDispatch(param);

        clearTimeout(ref.current.timeout);

        ref.current.timeout = setTimeout(() => doDispatch(param), idle);
      }

      Object.keys(mapping).forEach((key) =>
        JS.set.raw(dispatch, `set${key.toCapitalized()}`, (param: any) => {
          const oldField = ref.current.state[key];

          const nextField = JS.is.function(param) ? param(oldField) : param;

          if (oldField === nextField) return;

          const next = { ...ref.current.state };

          next[key] = nextField;

          dispatch(next);
        })
      );

      ref.current = {
        state: {},
        dispatch,
      };
    }

    useEffect(() => {
      if (
        initial &&
        initialize &&
        !Object.keys(mapping).some((key) => ref.current.searchParams.has(key))
      )
        ref.current.dispatch(initialize);

      return () => {
        clearTimeout(ref.current.timeout);
      };
    }, []);

    return useMemo<any>(() => {
      ref.current.searchParams = new URLSearchParams(location.search);

      const resultState: any = {};

      Object.entries(mapping).forEach(([key, type]) => {
        const field: any = ref.current.searchParams.get(key);

        if (undefined === field || null === field) {
          delete ref.current.state[key];
          resultState[key] = defaults(key, type);
        } else {
          const value = convert(key, type, field, () =>
            baseConvert(key, type, field)
          );

          ref.current.state[key] = value;
          resultState[key] = value;
        }
      });

      return [resultState, ref.current.dispatch];
    }, [location.search]);
  };
};
