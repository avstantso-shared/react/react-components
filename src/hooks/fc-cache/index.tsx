import React, { useMemo, useRef } from 'react';

import { JS } from '@avstantso/node-or-browser-js--utils';

export namespace FCCache {
  export type FC = React.FC | ((...args: any[]) => JSX.Element);
  export type DefaultProps = React.PropsWithChildren<NodeJS.Dict<any>>;

  export namespace Value {
    export type Default<TProps extends {} = DefaultProps> = {
      (): Value<TProps> | FC;
    };

    export type Initializer = {
      <TProps extends {} = DefaultProps>(value: Value<TProps>): Value<TProps>;
      <TProps extends {} = DefaultProps>(FC: FC, props?: TProps): Value<TProps>;
      <TProps extends {} = DefaultProps>(
        valueOrFC: Value<TProps> | FC
      ): Value<TProps>;

      is<TProps extends {} = DefaultProps>(
        candidate: unknown
      ): candidate is Value<TProps>;
    };
  }

  export type Value<TProps extends {} = DefaultProps> = {
    FC: FC;
    props?: TProps;
  };

  export type Options<TProps extends {} = DefaultProps> = {
    defaultValue?: Value.Default<TProps>;
    recalc?: boolean;
  };

  export type Push<TProps extends {} = DefaultProps> = {
    (value: Value<TProps>): Value<TProps>;
    (FC: FC, props?: TProps): Value<TProps>;
  };

  export type Use = {
    <TProps extends {} = DefaultProps>(
      options?: Options<TProps>
    ): FCCache<TProps>;
    <TProps extends {} = DefaultProps>(
      defaultValue?: Value.Default<TProps>
    ): FCCache<TProps>;
  };
}

export type FCCache<TProps extends {} = FCCache.DefaultProps> = {
  peek(): FCCache.Value<TProps>;
  Peek: React.FC;

  pop(): FCCache.Value<TProps>;
  Pop: React.FC;

  push: FCCache.Push<TProps>;
  Push: React.FC<FCCache.Value<TProps>>;
};

function isValue<TProps extends {} = FCCache.DefaultProps>(
  candidate: unknown
): candidate is FCCache.Value<TProps> {
  return null !== candidate && JS.is.object(candidate) && 'FC' in candidate;
}

function isOptions<TProps extends {} = FCCache.DefaultProps>(
  candidate: unknown
): candidate is FCCache.Options<TProps> {
  return (
    null !== candidate &&
    JS.is.object(candidate) &&
    ('defaultValue' in candidate || 'recalc' in candidate)
  );
}

function render<TProps extends {} = FCCache.DefaultProps>(
  value: FCCache.Value<TProps>,
  children: React.ReactNode
): JSX.Element {
  const { FC, props } = value || {};
  return (
    <>
      {children}
      {FC && <FC {...props} />}
    </>
  );
}

function Value<TProps extends {} = FCCache.DefaultProps>(
  ...params: any[]
): FCCache.Value<TProps> {
  return isValue<TProps>(params[0])
    ? params[0]
    : { FC: params[0], ...(params[1] ? { props: params[1] } : {}) };
}
Value.is = isValue;

export const useFCCache: FCCache.Use = <
  TProps extends {} = FCCache.DefaultProps
>(
  param: FCCache.Options<TProps> | FCCache.Value.Default<TProps>
): FCCache<TProps> => {
  const { defaultValue, recalc }: FCCache.Options<TProps> = isOptions<TProps>(
    param
  )
    ? param
    : { defaultValue: param };

  const cache = useRef<FCCache.Value<TProps>>();

  return useMemo(() => {
    function peek() {
      return cache.current || (defaultValue && Value(defaultValue()));
    }

    const Peek: React.FC = ({ children }) => render(peek(), children);

    function pop() {
      const r = peek();
      cache.current = undefined;
      return r;
    }

    const Pop: React.FC = ({ children }) => render(pop(), children);

    function push(...params: any[]) {
      return (cache.current = Value<TProps>(...params));
    }

    const Push: React.FC<FCCache.Value<TProps>> = ({ FC, props, children }) =>
      render(push({ FC, props }), children);

    return { peek, Peek, pop, Pop, push, Push };
  }, [recalc]);
};

export const FCCache = {
  use: useFCCache,
  Value: Value as FCCache.Value.Initializer,
  Options: { is: isOptions },
};
