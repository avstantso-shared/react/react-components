import React from 'react';
import { Meta, Story } from '@story-helpers';
import { useState } from '@storybook/preview-api';
import { Badge } from 'react-bootstrap';

import { FCCache } from '.';

type FCCacheTestFC = React.FC;
const FCCacheTestFC: FCCacheTestFC = (props) => null;

const meta = { ...Meta(FCCacheTestFC, { title: 'Hooks/FcCache' }) };

export default meta;

const defaultValue = () =>
  FCCache.Value(Badge, {
    bg: 'secondary',
    children: 'Default',
  });

const ifStateLt1 = (
  <>
    (if state index &lt; <b>1</b>)
  </>
);

export const fcCache = Story(meta, {
  render: () => {
    const { Push, Peek, Pop } = FCCache.use(defaultValue);

    const [state, setState] = useState(0);

    return (
      <>
        <h3>FCCache</h3>
        Current state index is <b>{state}.</b>
        <ul>
          <li>
            {state < 2 ? 'Default' : 'Current'} value:&nbsp;
            <Peek />
          </li>
          <li>
            New value 1:&nbsp;
            <Push FC={Badge} props={{ bg: 'primary', children: 'Value1' }} />
          </li>
          <li>
            Crrent value:&nbsp;
            <Peek />
          </li>
          <li>
            New value 2:&nbsp;
            <Push FC={Badge} props={{ bg: 'danger', children: 'Value2' }} />
          </li>
          <li>
            Crrent value:&nbsp;
            <Peek />
          </li>
          {state < 1 && (
            <>
              <li>
                Pop value {ifStateLt1}:&nbsp;
                <Pop />
              </li>
              <li>
                Current value is default {ifStateLt1}:&nbsp;
                <Peek />
              </li>
            </>
          )}
        </ul>
        <button onClick={() => setState(state + 1)}>Inc state index</button>
      </>
    );
  },
});
