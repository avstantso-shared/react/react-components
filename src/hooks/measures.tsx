import { useLayoutEffect } from 'react';

import './measures.scss';

/**
 * @see source https://davidwalsh.name/detect-scrollbar-width
 */
function scrollbarWidth() {
  let div = document.getElementById(
    'measure-scrollbar-width'
  ) as HTMLDivElement;
  if (!div) {
    div = document.createElement('div');

    div.id = 'measure-scrollbar-width';

    document.body.appendChild(div);
  } else div.style.display = 'block';

  try {
    const sbWidth = div.offsetWidth - div.clientWidth;

    document.documentElement.style.setProperty(
      '--measure-scrollbar-width',
      `${sbWidth}px`
    );
  } finally {
    div.style.display = 'none';
  }

  return div;
}

export function useMeasures() {
  useLayoutEffect(() => {
    scrollbarWidth();

    const resizeObserver = new ResizeObserver(scrollbarWidth);

    resizeObserver.observe(document.body);
    return () => {
      resizeObserver.disconnect();
    };
  }, []);
}

export const Measures: React.FC = () => {
  useMeasures();

  return null;
};
