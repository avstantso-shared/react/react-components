// based on:
// https://ericdcobb.medium.com/scrolling-to-an-anchor-in-react-when-your-elements-are-rendered-asynchronously-8c64f77b5f34
import React, { useLayoutEffect, useRef } from 'react';
import { useLocation } from 'react-router';

import { JS } from '@avstantso/node-or-browser-js--utils';

import { DOMGet } from '@root/utils';

export namespace ScrollToLocation {
  export interface Props<T extends HTMLElement = HTMLDivElement> {
    ref?: DOMGet.Ref<T>;
    id?: string;
    correctiveY?: number | ((container: T) => number);
    topIfNotFound?: boolean | ((pathname: string) => boolean);
    loading?: boolean;
  }
}

function openParentDetails(element: HTMLElement): void {
  if (element === document.body) return;

  if ('DETAILS' === element.tagName)
    (element as HTMLDetailsElement).open = true;

  return openParentDetails(element.parentElement);
}

/**
 * @summary Scroll container to  element id hash
 */
export function useScrollToLocation<T extends HTMLElement = HTMLDivElement>({
  ref: containerRef,
  id: contaiterId,
  correctiveY,
  topIfNotFound,
  loading,
}: ScrollToLocation.Props<T>) {
  const { pathname, hash } = useLocation();
  const ref = useRef({ pathname, hash, scrolled: false });

  useLayoutEffect(() => {
    if (loading) return;

    if (ref.current.pathname !== pathname || ref.current.hash !== hash)
      ref.current = { pathname, hash, scrolled: false };

    if (!ref.current.scrolled) {
      const container =
        DOMGet.getElement<T>(containerRef || contaiterId) ||
        (document.body as T);
      DOMGet.getElementId<T>(container, contaiterId);

      const id = hash.replace('#', '');
      const element = document.getElementById(id);

      if (element) {
        openParentDetails(element);

        const top =
          element.offsetTop +
          (JS.is.function(correctiveY)
            ? correctiveY(container)
            : correctiveY || 0);

        container.scrollTo({
          top,
          behavior: 'smooth',
        });
      } else if (
        JS.is.function(topIfNotFound) ? topIfNotFound(pathname) : topIfNotFound
      )
        container.scrollTop = 0;

      ref.current.scrolled = true;
    }
  }, [pathname, hash, loading]);
}
