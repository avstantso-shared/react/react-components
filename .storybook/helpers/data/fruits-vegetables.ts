import { NamesTree } from '@avstantso/node-or-browser-js--utils';

const s = '';

export const FruitsVegetables = NamesTree.Urls({
  fruits: { citrus: { orange: s, lemon: s } },
  vegetables: { tomato: s },
});
