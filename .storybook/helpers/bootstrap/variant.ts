import { SpinnerProps } from 'react-bootstrap';
import * as SBR from '@storybook/react';

export type Variant = SpinnerProps['variant'];

const argType = {
  options: [
    'primary',
    'secondary',
    'success',
    'danger',
    'warning',
    'info',
    'dark',
    'light',
  ],
  control: { type: 'radio' },
};

type Overload = (<Key extends string = 'variant'>(
  key?: Key
) => Pick<
  SBR.Meta<{
    [K in Key]: Variant;
  }>,
  'argTypes'
>) & {
  argType: typeof argType;
};

function variant(key: any = 'variant') {
  const argTypes: any = {};
  argTypes[key] = argType;

  return {
    argTypes,
  };
}
variant.argType = argType;

export const Variant: Overload = variant;
