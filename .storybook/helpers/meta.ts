import * as SBR from '@storybook/react';

export type Meta<TCmpOrArgs = SBR.Args> = SBR.Meta<TCmpOrArgs>;

export function Meta<TCmp extends Function>(
  component: TCmp,
  ext?: Omit<Meta<TCmp>, 'component'>
): Meta<TCmp> {
  const { title = `Components/${component.name}`, ...rest } = (ext ||
    {}) as Omit<Meta<TCmp>, 'component'>;

  return { component, title, ...rest } as Meta<TCmp>;
}

type StoryOverload = {
  <
    TMeta,
    TStory extends TMeta extends Meta<infer TCmp>
      ? SBR.StoryObj<Meta<TCmp>>
      : never
  >(
    meta: TMeta,
    story: TStory
  ): TStory;
  <TArgs>(story: SBR.StoryObj<TArgs>): SBR.StoryObj<TArgs>;
};

export const Story: StoryOverload = (...params: any[]) => params.peek();
