export type SubProps<T extends {}, P extends string> = {
  [K in keyof T as `${P}${Exclude<K, symbol>}`]: T[K];
};

export namespace SubProps {
  export type Restore<T extends {}, P extends string> = {
    [K in keyof T as K extends `${P}${infer S}` ? S : never]: T[K];
  };
}

namespace Test {
  type p = { angle: string; border: number };
  type s = SubProps<p, 'Prefix.'>;
  type r = SubProps.Restore<s, 'Prefix.'>;
}

export function SubProps<T extends {}, P extends string>(
  props: T,
  prefix: P
): SubProps.Restore<T, P> {
  const r: any = {};

  Object.entries(props).forEach(([key, value]) => {
    if (!key.startsWith(prefix)) return;

    const field = key.substring(prefix.length);

    r[field] = value;
    delete props[key as keyof T];
  });

  return r;
}
