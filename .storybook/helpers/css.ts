import { CSSProperties } from 'react';

type Globals = Extract<
  'unset' | 'initial' | 'revert' | 'inherit',
  CSSProperties['gap']
>;

export const globals: Globals[] = ['unset', 'initial', 'revert', 'inherit'];

export const backgroundClips: CSSProperties['backgroundClip'][] = [
  ...globals,
  'border-box',
  'padding-box',
  'content-box',
];

export const backgroundPositionX: CSSProperties['backgroundPositionX'][] = [
  ...globals,
  'center',
  'left',
  'right',
  'x-end',
  'x-start',
];

export const backgroundPositionY: CSSProperties['backgroundPositionY'][] = [
  ...globals,
  'bottom',
  'center',
  'top',
  'y-end',
  'y-start',
];
