import path from 'path';
import fs from 'fs';
import TsconfigPathsPlugin from 'tsconfig-paths-webpack-plugin';

const sassLoaderOptions = {
  implementation: require('sass'),
  additionalData: [
    ...['sass:list', 'sass:map', 'sass:string'].map(
      (module) => `@use "${module}";`
    ),

    ...[
      '~bootswatch/dist/cerulean/variables',
      ...fs
        .readdirSync(path.resolve(__dirname, '../src/scss'))
        .filter((filename) => '.scss' === path.extname(filename))
        .map((filename) => `~/src/scss/${filename}`),
    ].map((filename) => `@import "${filename}";`),
  ].join(' '),
};

module.exports = {
  stories: ['../src/**/*.mdx', '../src/**/*.stories.@(js|jsx|ts|tsx)'],

  addons: [
    getAbsolutePath('@storybook/addon-links'),
    getAbsolutePath('@storybook/addon-essentials'),
    getAbsolutePath('@storybook/addon-interactions'),

    { name: '@storybook/preset-scss', options: { sassLoaderOptions } },

    '@storybook/addon-webpack5-compiler-babel',
  ],

  framework: {
    name: getAbsolutePath('@storybook/react-webpack5'),
    options: {},
  },

  webpackFinal: async (config, { configType }) => {
    config.resolve.plugins = config.resolve.plugins || [];
    config.resolve.plugins.push(
      new TsconfigPathsPlugin({
        configFile: path.resolve(__dirname, '../tsconfig.json'),
      })
    );

    config.resolve.fallback = {
      ...config.resolve.fallback,
      buffer: require.resolve('buffer/'),
    };

    config.stats = { ...config.stats, loggingDebug: ['sass-loader'] };

    return config;
  },

  docs: {
    autodocs: true,
  },
};

function getAbsolutePath(value) {
  return path.dirname(require.resolve(path.join(value, 'package.json')));
}
