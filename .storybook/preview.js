import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useEffect, useState, useCallback, Suspense } from 'react';
import { I18nextProvider } from 'react-i18next';

import { InstanceProxy } from '@avstantso/node-or-browser-js--utils';

import { Combiner } from '@avstantso/react--data';

import i18n from './i18n';

InstanceProxy.strict = false;

const Prod = 'Prod';
const Dev = 'Dev';

export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
};

// Source: https://storybook.js.org/blog/internationalize-components-with-storybook/
// Icons: https://www.chromatic.com/component?appId=5a375b97f4b14f0020b0cda3&name=Basics%7CIcon&buildNumber=13899&k=5e0bbe46a505730020a406b3-1200px-snapshot-true&h=2&b=-2
const withI18nextAndDebugDecorator = (Story, context) => {
  const { locale = i18n.language, debugInfo = Prod } = context.globals;

  const [renderCounter, setRenderCounter] = useState(0);
  const reRender = useCallback(() => setRenderCounter((n) => n + 1), []);

  useEffect(() => {
    if (i18n.language === locale) return;

    i18n.changeLanguage(locale).then(reRender);
  }, [locale]);

  useEffect(() => {
    const prev = process.env.DEBUG_INFO_ENABLED;
    const next = Prod !== debugInfo || undefined;

    if (next === prev) return;

    process.env.DEBUG_INFO_ENABLED = next;

    reRender();
  }, [debugInfo]);

  return (
    <div
      {...Combiner.className(
        withI18nextAndDebugDecorator.name,
        `i18n-lang-${i18n.language}`,
        `debug-info-${debugInfo.toLowerCase()}`,
        `DEBUG_INFO_ENABLED-${process.env.DEBUG_INFO_ENABLED}`,
        `render-${renderCounter}`
      )}
    >
      <Suspense fallback={<div>loading translations...</div>}>
        <I18nextProvider i18n={i18n}>
          <Story />
        </I18nextProvider>
      </Suspense>
    </div>
  );
};

export const decorators = [withI18nextAndDebugDecorator];

const localeToolbarItems = [
  { value: 'en', title: 'English' },
  { value: 'ru', title: 'Russian' },
];

export const globalTypes = {
  locale: {
    name: 'Locale',
    description: 'Internationalization locale',
    toolbar: {
      icon: 'globe',
      title:
        localeToolbarItems.find(({ value }) => value === i18n.language)
          ?.title || 'Locale',
      items: localeToolbarItems,
      dynamicTitle: true,
    },
  },
  debugInfo: {
    name: 'DebugInfo',
    description: 'Debug info enabled',
    toolbar: {
      icon: 'lock',
      title: Prod,
      items: [
        { value: Prod, title: Prod, icon: 'lock' },
        { value: Dev, title: Dev, icon: 'unlock' },
      ],
      dynamicTitle: true,
    },
  },
};
