import i18next from 'i18next';
import { initReactI18next, useTranslation } from 'react-i18next';

import { JS } from '@avstantso/node-or-browser-js--utils';
import { LocalString } from '@avstantso/node-or-browser-js--model-core';

import {
  i18next_resources as res_react_data,
  i18next_setUseTranslation,
} from '@avstantso/react--data';
import { i18next_resources as res_react_components } from '../src/i18n';

i18next_setUseTranslation(useTranslation);

const resources = {};
LocalString.Languages.forEach((lang) => {
  let langRes = {};

  [res_react_data, res_react_components].forEach((res) => {
    langRes = { ...langRes, ...JS.get.raw(res, lang) };
  });

  JS.set.raw(resources, lang, langRes);
});

const defaultLang: string = (
  navigator.language ||
  JS.get.raw(navigator, 'userLanguage') ||
  'en-EN'
).slice(0, 2);

i18next.use(initReactI18next).init({
  interpolation: { escapeValue: false }, // React already does escaping
  lng: defaultLang, // language to use
  resources,
});

export default i18next;
