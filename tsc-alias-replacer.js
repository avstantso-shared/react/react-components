// Doc: https://github.com/justkey007/tsc-alias/discussions/73
'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
function debugReplacer({ orig, file, config }) {
  // A.V.Stantso: fix attempt resolve 'debug' as './debug'
  if (/\.\/debug['"]/.test(orig)) return "from 'debug'";
  return orig;
}
exports.default = debugReplacer;
